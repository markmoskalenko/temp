<?php
namespace aliexpress\epn;

class ApiClient
{
    const URL = 'http://api.epn.bz/json';
    const VERSION = 2;

    // Параметры
    private $user_api_key = 0;
    private $user_hash = '';
    private $prepared_requests = [];
    private $request_results = [];
    private $last_error = '';

    /**
     * @param string $user_api_key
     * @param string $user_hash
     */
    public function __construct($user_api_key, $user_hash)
    {
        $this->user_api_key = $user_api_key;
        $this->user_hash = $user_hash;
    }

    /**
     * Добавление запроса на получение списка категорий
     *
     * @param string $name
     * @param string $lang
     * @return boolean
     */
    public function addRequestCategoriesList($name, $lang = 'en')
    {
        $this->addRequest($name, 'list_categories', ['lang' => $lang]);
    }

    /**
     * Запрос на поиск
     *
     * @param string $name
     * @param array  $options
     */
    public function addRequestSearch($name, $options = [])
    {
        // TODO: Здесь надо бы написать валидацию опций. Кто сделает тому шоколадка.

        // Добавляем запрос в список
        $this->addRequest($name, 'search', $options);
    }

    /**
     * Запрос на получение количества товаров в категориях
     *
     * @param string $name
     * @param array  $options
     * @return boolean
     */
    public function addRequestCountForSearch($name, $options = [])
    {
        // TODO: Здесь надо бы написать валидацию опций. Кто сделает тому шоколадка.

        // Добавляем запрос в список
        $this->addRequest($name, 'count_for_categories', $options);
    }

    /**
     * Запрос на получение информации о товаре
     *
     * @param string $name
     * @param string $id
     * @param string $lang
     * @param string $currency
     */
    public function addRequestGetOfferInfo($name, $id, $lang = 'en', $currency = 'USD')
    {
        // На всякий случай Нормализуем то что на входе
        // intval не используем в связи с проблемами на 32-битных системах
        $id = preg_replace('{[^\d]+}ui', '', $id);
        if ($id == '') {
            $id = 0;
        }

        // Добавим запрос в список
        $this->addRequest($name, 'offer_info', [
            'id' => $id,
            'lang' => $lang,
            'currency' => $currency,
        ]);
    }

    /**
     * Добавление запроса в список
     *
     * @param string $name
     * @param string $action
     * @param array $params
     */
    private function addRequest($name, $action, $params = [])
    {
        // Нормализуем входные данные
        if ( ! is_array($params)) {
            $params = [];
        }
        $params['action'] = $action;
        $this->prepared_requests[ $name ] = $params;
    }

    /**
     * Выполнение всех запросов
     *
     * @return boolean
     */
    public function runRequests()
    {
        // Сбрасываем переменные
        $this->request_results = [];
        $this->last_error = '';

        // Если список запросов пуст
        if ( ! sizeof($this->prepared_requests)) {
            return true;
        }

        // Структура для отправки запросса
        $data = [
            'user_api_key' => $this->user_api_key,
            'user_hash' => $this->user_hash,
            'api_version' => self::VERSION,
            'requests' => $this->prepared_requests,
        ];

        // Строка запроса
        $post_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/plain']);
        $result = curl_exec($ch);
        $curl_error_msg = curl_error($ch);

        // Если http-запрос обработан с ошибкой
        if ($curl_error_msg != '') {
            $this->last_error = $curl_error_msg;
        } else {
            // Парсим данные
            $result_data = json_decode($result, true);
            $this->last_error = isset($result_data['error']) ? $result_data['error'] : '';
            $this->request_results = isset($result_data['results']) && is_array($result_data['results']) ? $result_data['results'] : [];
        }

        // Независимо от результата сбрасываем список запросов
        $this->prepared_requests = [];

        // Если не было ошибок то всё хорошо
        return $this->last_error == '' ? true : false;
    }

    /**
     * Получение отклика
     *
     * @param string $name
     * @return array|null
     */
    public function getRequestResult($name)
    {
        return isset($this->request_results[ $name ]) ? $this->request_results[ $name ] : null;
    }

    /**
     * Информация о последней ошибке
     *
     * @return string|null
     */
    public function getLastError()
    {
        return $this->last_error;
    }
}