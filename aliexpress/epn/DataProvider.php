<?php
namespace aliexpress\epn;

use Yii;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DataProvider extends BaseDataProvider
{
    /**
     * @var string search query
     */
    public $query;


    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     */
    protected function prepareModels()
    {
        $options= ['query' => $this->query];

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            $options['limit'] = $pagination->getLimit();
            $options['offset'] = $pagination->getOffset();
        }

        $this->getApi()->addRequestSearch('products', $options);
        $this->getApi()->runRequests();

        $products = $this->getApi()->getRequestResult('products');

        return ArrayHelper::getValue($products, 'offers', []);
    }

    /**
     * Prepares the keys associated with the currently available data models.
     * @param array $models the available data models
     * @return array the keys
     */
    protected function prepareKeys($models)
    {
        return ArrayHelper::getColumn($models, 'id', false);
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     * @return integer total number of data models in this data provider.
     */
    protected function prepareTotalCount()
    {
        $this->getApi()->addRequestSearch('productCount', ['query' => $this->query]);
        $this->getApi()->runRequests();

        $products = $this->getApi()->getRequestResult('productCount');

        return ArrayHelper::getValue($products, 'total_found', 0);
    }

    private $_api;

    /**
     * @return ApiClient
     */
    public function getApi()
    {
        if ($this->_api === null) {
            $this->_api = new ApiClient(Yii::$app->params['epn.apiKey'], Yii::$app->params['epn.deeplinkHash']);
        }
        return $this->_api;
    }
}