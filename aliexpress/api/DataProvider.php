<?php
namespace aliexpress\api;

use Yii;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DataProvider extends BaseDataProvider
{
    /**
     * @var string search query
     */
    public $searchQuery;
    /**
     * @var integer
     */
    public $categoryId;
    /**
     * @var float
     */
    public $priceFrom;
    /**
     * @var float
     */
    public $priceTo;
    /**
     * @var string
     */
    public $orderBy;


    /**
     * Initializes the object.
     */
    public function init()
    {
        $this->setPagination([
            'validatePage' => false,
        ]);
    }

    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     */
    protected function prepareModels()
    {
        $this->pagination->totalCount = $this->getTotalCount();
        return $this->getApiParam('products', []);
    }

    /**
     * Prepares the keys associated with the currently available data models.
     *
     * @param array $models the available data models
     * @return array the keys
     */
    protected function prepareKeys($models)
    {
        return ArrayHelper::getColumn($models, 'id', false);
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     *
     * @return integer total number of data models in this data provider.
     */
    protected function prepareTotalCount()
    {
        return $this->getApiParam('totalResults', 0);
    }

    protected $apiResponse;

    /**
     * Returns specified property from api response.
     *
     * @param string $param
     * @param mixed  $default
     * @return mixed
     */
    protected function getApiParam($param, $default = null)
    {
        if ($this->apiResponse === null) {
            if ($this->searchQuery) {
                $this->apiResponse = $this->makeApiRequest();
            }
        }

        return ArrayHelper::getValue($this->apiResponse, $param, $default);
    }

    /**
     * Makes API request for extracting list of products products.
     *
     * @return array
     */
    protected function makeApiRequest()
    {
        $api = new Client(Yii::$app->params['ali.apiKey']);

        $params = [
            'fields' => 'productId,productTitle,productUrl,imageUrl,salePrice,originalPrice,discount,evaluateScore,lotNum',
            'keywords' => $this->searchQuery,
            'pageNo' => $this->pagination->getPage() + 1,
            'pageSize' => $this->pagination->getPageSize(),
        ];

        if ($this->categoryId) {
            $params['categoryId'] = $this->categoryId;
        }

        if ($this->priceFrom) {
            $params['originalPriceFrom'] = $this->priceFrom;
        }

        if ($this->priceTo) {
            $params['originalPriceTo'] = $this->priceTo;
        }

        if ($this->orderBy) {
            $params['sort'] = $this->orderBy;
        }

        try {
            return $api->getList($params);
        } catch (Exception $e) {
            Yii::$app->raven->extraContext($params);
            Yii::$app->raven->captureException($e);
            return [];
        }
    }
}