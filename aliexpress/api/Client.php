<?php
namespace aliexpress\api;

use aliexpress\CurlException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Client
{
    /**
     * @var string
     */
    private $appKey;

    /**
     * @param string $appKey
     */
    public function __construct($appKey)
    {
        $this->appKey = $appKey;
    }

    /**
     * @param array $params
     * @throws Exception
     * @return array
     */
    public function getList($params)
    {
        return $this->execute($this->createUrl('listPromotionProduct', $params));
    }

    /**
     * @param array $params
     * @return array
     */
    public function getItem($params)
    {
        return $this->execute($this->createUrl('getPromotionProductDetail', $params));
    }

    /**
     * @param array $params
     * @return array
     */
    public function getLinks($params)
    {
        return $this->execute($this->createUrl('getPromotionLinks', $params));
    }

    /**
     * @param string $method
     * @param array  $params
     * @return string
     */
    public function createUrl($method, $params)
    {
        return 'http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.'
            . $method . '/'
            . $this->appKey . '?'
            . http_build_query($params);
    }

    /**
     * @param string $url
     * @throws Exception
     * @return array
     */
    protected function execute($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result === false) {
            throw new Exception('Curl error: ' . curl_error($ch));
        }

        $data = json_decode($result, true, 512, JSON_BIGINT_AS_STRING);

        if ( ! isset($data['errorCode'])) {
            throw new Exception('Wrong answer format');
        } elseif ($data['errorCode'] !== 20010000) {
            throw new Exception('Api returns error code ' . $data['errorCode']);
        } elseif (isset($data['result'])) {
            return $data['result'];
        } else {
            return [];
        }
    }
}