var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');

gulp.task('less', function () {
    return gulp.src('./web/less/frontend.less')
        .pipe(less())
        .pipe(gulp.dest('./web/css'));
});

gulp.task('watch', function () {
    gulp.watch('./web/less/*.less', ['less']);
});