
module.exports = function(grunt) {

    grunt.initConfig({
        mails: {
            source: 'app/mail/templates/friendly',
            dest: 'app/mail/templates/compiled'
        },

        copy: {
            mails: {
                expand: true,
                cwd: '<%= mails.source %>/',
                src: '**',
                dest: '<%= mails.dest %>/',
                flatten: true,
                filter: 'isFile'
            }
        },

        inlinecss: {
            mails: {
                files: {
                    '<%= mails.dest %>/email-confirmation.html': '<%= mails.dest %>/email-confirmation.html',
                    '<%= mails.dest %>/password-reset.html': '<%= mails.dest %>/password-reset.html',
                    '<%= mails.dest %>/password-delivery.html': '<%= mails.dest %>/password-delivery.html',
                    '<%= mails.dest %>/site-update-info.html': '<%= mails.dest %>/site-update-info.html',
                    '<%= mails.dest %>/review-rejected.html': '<%= mails.dest %>/review-rejected.html'
                }
            }
        },

        'string-replace': {
            mails: {
                files: {
                    '<%= mails.dest %>/': '<%= mails.dest %>/*'
                },
                options: {
                    replacements: [
                        {
                            pattern: /src="\.\.\/\.\.\/(.*?)"/ig,
                            replacement: "src=\"<?= $message->embed(Yii::getAlias('@app/mail/$1')) ?>\""
                        },
                        {
                            pattern: /APP_NAME/ig,
                            replacement: "<?= Yii::$app->name ?>"
                        },
                        {
                            pattern: /#SITE_URL/ig,
                            replacement: "<?= Url::toRoute(['@home'], true) ?>"
                        }
                    ]
                }
            }
        },

        less: {
            dev: {
                files: {
                    "web/css/frontend.css":
                        "web/less/frontend.less"

                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-inline-css');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('mails', ['copy:mails', 'inlinecss:mails', 'string-replace:mails']);

    grunt.registerTask('default', ['less:dev']);
};