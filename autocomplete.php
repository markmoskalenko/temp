<?php

/**
 * Class Yii
 *
 * Этот класс просто помогает автокомлиту PhpStorm правильно находить нужные классы.
 * Найдите основной файл Yii в @app/vendor/yiisoft/yii2/Yii.php и "Mark as Plain Text"
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var \Application
     */
    static $app;
}

/**
 * Class Application

 * @property app\components\Polyglot $polyglot
 * @property app\components\Raven $raven
 * @property app\components\ThumbManager $thumbManager
 * @property app\components\User $user
 *
 * @mixin yii\console\Application
 * @mixin yii\web\Application
 */
abstract class Application extends \yii\base\Application
{
    /**
     * @return app\components\User
     */
    public function getUser() {
    }
}
