<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

$security = Yii::$app->getSecurity();

return [
    'email' => $faker->email,
    'name' => $faker->userName,
    'authKey' => $security->generateRandomString(),
    'timeCreated' => new \DateTime(),
];
