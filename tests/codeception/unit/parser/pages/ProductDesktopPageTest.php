<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\ProductDesktopPage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductDesktopPageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/product_desktop.html');
        $html = file_get_contents($filename);

        $page = new ProductDesktopPage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getSellerId(), '205652906');
        $this->assertEquals($page->getStoreId(), '427290');
        $this->assertEquals($page->getTitle(), '1pcs Wireless FM Car MP3 Player Transmitter Red Color Backlight With USB SD MMC Card Slot Free shipping Hot sale!');
        $this->assertEquals($page->getRatingValue(), 4.6);
        $this->assertEquals($page->getReviewCount(), 565);
        $this->assertEquals($page->getPurchasesCount(), 277);
    }
}