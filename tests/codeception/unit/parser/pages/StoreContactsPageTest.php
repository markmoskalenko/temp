<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\StoreContactsPage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreContactsPageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/store_contacts.html');
        $html = file_get_contents($filename);

        $page = new StoreContactsPage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getSellerId(), '200980496');
        $this->assertEquals($page->getSellerName(), 'Mr. jack leng');

        $this->assertEquals($page->getStoreId(), '809129');
        $this->assertEquals($page->getStoreName(), 'Tesco Electronic Commerce co., LTD');

        $this->assertEquals($page->getCountry(), 'China (Mainland)');
        $this->assertEquals($page->getRegion(), 'Guangdong');
        $this->assertEquals($page->getCity(), 'Shenzhen City');
        $this->assertEquals($page->getAddress(), 'Longhua Town');
        $this->assertEquals($page->getZipCode(), '518131');
    }
}