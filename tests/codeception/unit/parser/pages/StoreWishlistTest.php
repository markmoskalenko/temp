<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\StoreWishlistPage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreWishlistTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/store_wishlist.html');
        $html = file_get_contents($filename);

        $page = new StoreWishlistPage();
        $page->setHtml($html);

        $this->assertEquals($page->getLikesCount(), 1004);
    }
}