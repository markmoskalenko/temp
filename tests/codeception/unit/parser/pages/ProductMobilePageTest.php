<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\ProductMobilePage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductMobilePageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/product_mobile.html');
        $html = file_get_contents($filename);

        $page = new ProductMobilePage();
        $page->setHtml($html);
        $page->tidyHtml();

        $images = [
            'http://i01.i.aliimg.com/wsphoto/v2/1762176703_1/1pcs-Car-MP3-Player-Wireless-FM-Transmitter-Red-Color-Backlight-With-USB-SD-MMC-Card-Slot.jpg_250x250.jpg',
            'http://i01.i.aliimg.com/wsphoto/v2/1762176703_2/1pcs-Car-MP3-Player-Wireless-FM-Transmitter-Red-Color-Backlight-With-USB-SD-MMC-Card-Slot.jpg_250x250.jpg',
            'http://i01.i.aliimg.com/wsphoto/v2/1762176703_3/1pcs-Car-MP3-Player-Wireless-FM-Transmitter-Red-Color-Backlight-With-USB-SD-MMC-Card-Slot.jpg_250x250.jpg'
        ];

        $this->assertEquals($page->getSellerId(), '205652906');
        $this->assertEquals($page->getTitle(), '1pcs Wireless FM Car MP3 Player Transmitter Red Color Backlight With USB SD MMC Card Slot Free shipping Hot sale!');
        $this->assertEquals($page->getPrice(), 7.29);
        $this->assertEquals($page->getRatingValue(), 91.3);
        $this->assertEquals($page->getReviewCount(), 584);
        $this->assertEquals($page->getPurchasesCount(), 284);
        $this->assertEquals($page->getImages(), $images);
    }
}