<?php
namespace tests\codeception\unit\parser\pages;

use Codeception\Specify;
use app\parser\pages\FeedbackPage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FeedbackPageTest extends TestCase
{
    use Specify;

    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/feedback.html');
        $html = file_get_contents($filename);

        $page = new FeedbackPage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->specify('basic properties', function () use ($page) {
            expect('proper seller id', $page->getSellerId())->equals(220238235);
            expect('proper store id', $page->getStoreId())->equals(540179);
            expect('proper store name', $page->getStoreName())->equals('DressClub');
            expect('proper score', $page->getScore())->equals(2227);
            expect('proper score icon', $page->getScoreIcon())->equals('23-s.gif');
            expect('proper register date', $page->getRegisterDate()->format('Y-m-d'))->equals('2013-10-26');
        });

        $this->specify('detailed seller ratings', function () use ($page) {
            expect('item as described rating value', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_ITEM_AS_DESCRIBED, FeedbackPage::DETAILED_LABEL_RATING_VALUE))->equals(4.5);
            expect('item as described review count', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_ITEM_AS_DESCRIBED, FeedbackPage::DETAILED_LABEL_REVIEW_COUNT))->equals(2254);
            expect('item as described comparing rating', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_ITEM_AS_DESCRIBED, FeedbackPage::DETAILED_LABEL_RATING_COMPARING))->equals(7.40);

            expect('communication rating value', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_COMMUNICATION, FeedbackPage::DETAILED_LABEL_RATING_VALUE))->equals(4.6);
            expect('communication review count', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_COMMUNICATION, FeedbackPage::DETAILED_LABEL_REVIEW_COUNT))->equals(2254);
            expect('communication comparing rating', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_COMMUNICATION, FeedbackPage::DETAILED_LABEL_RATING_COMPARING))->equals(0);

            expect('shipping speed rating value', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_SHIPPING_SPEED, FeedbackPage::DETAILED_LABEL_RATING_VALUE))->equals(4.5);
            expect('shipping speed review count', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_SHIPPING_SPEED, FeedbackPage::DETAILED_LABEL_REVIEW_COUNT))->equals(2254);
            expect('shipping speed comparing rating', $page->getDetailed(FeedbackPage::DETAILED_CATEGORY_SHIPPING_SPEED, FeedbackPage::DETAILED_LABEL_RATING_COMPARING))->equals(-1.72);
        });

        $this->specify('feedback history', function () use ($page) {
            expect('positive 1 month', $page->getHistory(FeedbackPage::HISTORY_CATEGORY_POSITIVE, FeedbackPage::HISTORY_PERIOD_MONTH))->equals(356);
            expect('neutral 3 months', $page->getHistory(FeedbackPage::HISTORY_CATEGORY_NEUTRAL, FeedbackPage::HISTORY_PERIOD_3_MONTHS))->equals(95);
            expect('negative 6 months', $page->getHistory(FeedbackPage::HISTORY_CATEGORY_NEGATIVE, FeedbackPage::HISTORY_PERIOD_6_MONTHS))->equals(73);
            expect('positive rate 12 months', $page->getHistory(FeedbackPage::HISTORY_CATEGORY_POSITIVE_RATE, FeedbackPage::HISTORY_PERIOD_12_MONTHS))->equals(96.4);
            expect('positive overall', $page->getHistory(FeedbackPage::HISTORY_CATEGORY_POSITIVE, FeedbackPage::HISTORY_PERIOD_OVERALL))->equals(2311);
        });
    }
}