<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\StoreHomePage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreHomePageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/store_home.html');
        $html = file_get_contents($filename);

        $page = new StoreHomePage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getSellerId(), '220238235');
        $this->assertEquals($page->getName(), 'DressClub');
    }
}