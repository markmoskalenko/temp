<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\StoreMobilePage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreMobilePageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/store_mobile.html');
        $html = file_get_contents($filename);

        $page = new StoreMobilePage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getSaleItemsCount(), 2846);
    }
}