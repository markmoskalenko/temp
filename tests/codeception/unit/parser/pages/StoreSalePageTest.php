<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\StoreSalePage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreSalePageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/store_sale_items.html');
        $html = file_get_contents($filename);

        $page = new StoreSalePage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getSellerId(), '200980496');
        $this->assertEquals($page->getSaleItemsCount(), 62);
    }
}