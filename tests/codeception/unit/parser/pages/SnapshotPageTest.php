<?php
namespace tests\codeception\unit\parser\pages;

use app\parser\pages\SnapshotPage;
use tests\codeception\unit\TestCase;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotPageTest extends TestCase
{
    public function testData()
    {
        $filename = Yii::getAlias('@tests/codeception/unit/_data/snapshot.html');
        $html = file_get_contents($filename);

        $page = new SnapshotPage();
        $page->setHtml($html);
        $page->tidyHtml();

        $this->assertEquals($page->getProductId(), '1900240940');
        $this->assertEquals($page->getProductName(), 'knee length women new 2014 pu leather elastic high waist office party wear vintage bodycon pencil skirt');
        $this->assertEquals($page->getStoreId(), '540179');
        $this->assertEquals($page->getStoreName(), 'DressClub');
        $this->assertEquals($page->getPrice(), 16.14);
        $this->assertEquals($page->getUnit(), 'piece');
        $this->assertEquals($page->getMainImage(), 'http://i00.i.aliimg.com/kf/UT8gp9YXfxXXXagOFbXi.jpg');
    }
}