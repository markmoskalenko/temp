<?php
namespace tests\codeception\unit\parser;

use app\parser\entities\ProductEntity;
use app\parser\entities\SnapshotEntity;
use app\parser\entities\StoreEntity;
use app\parser\UrlParser;
use tests\codeception\unit\TestCase;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UrlParserTest extends TestCase
{
    /**
     * @var UrlParser
     */
    public $urlParser;

    /**
     * @inheritdoc
     */
    public function _before()
    {
        $this->urlParser = new UrlParser();
    }

    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     * @param string $className
     * @param string $id
     */
    public function testProcessing($url, $className, $id)
    {
        $result = $this->urlParser->process($url);

        expect('result object to be instance of entity', get_class($result))->equals($className);
        expect('result object has ID from URL', $result->getId())->equals($id);
    }

    public function urlProvider()
    {
        return [
            [
                'http://ru.aliexpress.com/store/427290',
                StoreEntity::className(),
                '427290',
            ],
            [
                'http://fr.aliexpress.com/store/427290?some_get_param=',
                StoreEntity::className(),
                '427290',
            ],
            [
                'http://fr.aliexpress.com/store/contactinfo/427290.html',
                StoreEntity::className(),
                '427290',
            ],
            [
                'http://ru.aliexpress.com/store/sale-items/427290.html',
                StoreEntity::className(),
                '427290',
            ],

            [
                'http://group.aliexpress.com/258466012-32263739665-detail.html',
                ProductEntity::className(),
                '32263739665',
            ],
            [
                'http://ru.aliexpress.com/item/free-shipping-2015-women-s-bag-fashion-women-s-handbag-genuine-leather-bags-for-women-brand/32263739665.html',
                ProductEntity::className(),
                '32263739665',
            ],
            [
                'http://fr.aliexpress.com/item/free-shipping-2015-women-s-bag-fashion-women-s-handbag-genuine-leather-bags-for-women-brand/32263739665.html?recommendVersion=1',
                ProductEntity::className(),
                '32263739665',
            ],
            [
                'http://ru.aliexpress.com/store/product/Vintage-sunglasses-for-man-Gradient-lens-with-good-quality-Free-shipping-via-China-post-air/1079550_1605826620.html',
                ProductEntity::className(),
                '1605826620',
            ],
            [
                'http://www.aliexpress.com/item//32263739665.html',
                ProductEntity::className(),
                '32263739665',
            ],
            [
                'http://ru.aliexpress.com/item/2015/32362977421.html',
                ProductEntity::className(),
                '32362977421',
            ],

            [
                'http://www.aliexpress.com/snapshot/6329468646.html',
                SnapshotEntity::className(),
                '6329468646',
            ],
        ];
    }
}