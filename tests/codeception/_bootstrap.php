<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

defined('YII_BASE_PATH') or define('YII_BASE_PATH', dirname(dirname(__DIR__)));

defined('YII_TEST_ENTRY_URL') or define('YII_TEST_ENTRY_URL', parse_url(\Codeception\Configuration::config()['config']['test_entry_url'], PHP_URL_PATH));
defined('YII_TEST_ENTRY_FILE') or define('YII_TEST_ENTRY_FILE', YII_BASE_PATH . '/web/index-test.php');

require_once(YII_BASE_PATH . '/vendor/autoload.php');
require_once(YII_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php');
require_once(YII_BASE_PATH . '/app/config/bootstrap.php');

$_SERVER['SCRIPT_FILENAME'] = YII_TEST_ENTRY_FILE;
$_SERVER['SCRIPT_NAME'] = YII_TEST_ENTRY_URL;
$_SERVER['SERVER_NAME'] = parse_url(\Codeception\Configuration::config()['config']['test_entry_url'], PHP_URL_HOST);
$_SERVER['SERVER_PORT'] =  parse_url(\Codeception\Configuration::config()['config']['test_entry_url'], PHP_URL_PORT) ?: '80';

Yii::setAlias('@app', YII_BASE_PATH . '/app');
Yii::setAlias('@tests', dirname(__DIR__));
