<?php
return [
    'components' => [
        'db' => [
            // 'dsn' => 'mysql:host=localhost;dbname=yii2_basic_tests',
        ],

        'mailer' => [
            'useFileTransport' => true,
        ],

        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
];
