<?php
/**
 * Application configuration for unit tests
 */
return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../../app/config/common.php'),
    require(__DIR__ . '/../../../app/config/common-local.php'),
    require(__DIR__ . '/../../../app/config/web.php'),
    require(__DIR__ . '/../../../app/config/web-local.php'),
    require(__DIR__ . '/config.php'),
    [

    ]
);
