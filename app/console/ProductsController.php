<?php
namespace app\console;

use app\db\records\Product;
use app\db\records\ProductReplica;
use app\parser\importers\ProductImporter;
use app\parser\NotFoundHttpException;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * CLI helper for parsing product records.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductsController extends Controller
{
    /**
     * Updates product with specified identifier.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /* @var $record Product */
        $record = Product::findOne($id);

        if ( ! $record) {
            Console::error(sprintf('Product with id %s was not found', $id));
            return static::EXIT_CODE_ERROR;
        }

        $importer = new ProductImporter();

        if ( ! $this->import($importer, $record)) {
            Console::error('Import process fails.');
            return static::EXIT_CODE_ERROR;
        }

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Runs replication mechanism for products.
     *
     * @return mixed
     */
    public function actionReplicate()
    {
        /* @var $product Product */
        $product = Product::find()->replicable()->one();

        if ( ! $product) {
            Console::output('There are no replicable products found');
            return static::EXIT_CODE_NORMAL;
        }

        $product->touch(Product::TIME_REPLICATION_STARTED);

        $importer = new ProductImporter();
        if ( ! $this->import($importer, $product)) {
            Console::error('Import process fails.');
            return static::EXIT_CODE_ERROR;
        }

        $replica = new ProductReplica();
        $replica->setOriginal($product);
        if ( ! $replica->save()) {
            Console::error('Replication process fails.');
            return static::EXIT_CODE_ERROR;
        }

        $product->touch(Product::TIME_REPLICATION_SUCCEED);

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * @param ProductImporter $importer
     * @param Product         $record
     *
     * @return boolean
     */
    protected function import(ProductImporter $importer, Product $record)
    {
        try {
            $entity = $importer->createProductEntity($record);
            $mobilePage = $entity->downloadMobilePage();
        } catch (NotFoundHttpException $e) {
            $record->setClosed(true);
            $record->save();
            return false;
        }

        $importer->loadProductMobilePage($mobilePage);
        return $importer->save($record);
    }
}