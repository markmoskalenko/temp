<?php
namespace app\console;

use app\db\records\Mail;
use app\senders\ImpossibleDeliveryException;
use app\senders\SenderFactory;
use Exception;
use Yii;
use yii\console\Controller;

/**
 * Sends mails from queue.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class MailerController extends Controller
{
    /**
     * Picks up first mail from queue and tries to send it.
     *
     * @throws Exception
     * @return integer
     */
    public function actionIndex()
    {
        /* @var Mail $mail */
        $mail = Mail::find()->sendable()->one();

        if ( ! $mail) {
            return static::EXIT_CODE_NORMAL;
        }

        Yii::$app->language = $mail->language;

        try {
            $sender = SenderFactory::create($mail);
            $sender->send();
        } catch (ImpossibleDeliveryException $e) {
            $mail->error = $e->getMessage();
            $mail->save();
        } catch (Exception $e) {
            $mail->touch('timeSendFailed');
            throw $e;
        }

        $mail->touch('timeSendSucceed');

        return static::EXIT_CODE_NORMAL;
    }
}