<?php
namespace app\console;

use app\db\records\Comment;
use app\db\records\QuestionAnswer;
use app\modules\forum\entities\Theme;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class QuestionsController extends Controller
{
    public function actionExport()
    {
        $questions = QuestionAnswer::find()->where('userId IS NOT NULL')->all();

        foreach ($questions as $qa) {
            $boardId = $qa->qaCategory->boardId;

            if ( ! $boardId) {
                continue;
            }

            $data = [
                'boardId' => $boardId,
                'userId' => $qa->userId,
                'title' => $qa->title,
                'timeCreated' => $qa->timeCreated,
                'timeUpdated' => $qa->timeCreated,
            ];

            \Yii::$app->db->createCommand()->insert('forum_board_theme', $data)->execute();
            $themeId = \Yii::$app->db->getLastInsertID();

            $qa->updateAttributes(['themeId' => $themeId]);

            $data = [
                'themeId' => $themeId,
                'userId' => $qa->userId,
                'content' => $qa->question,
                'timeCreated' => $qa->timeCreated,
                'timeUpdated' => $qa->timeCreated,
                'isInitial' => 1,
            ];

            \Yii::$app->db->createCommand()->insert('forum_comment', $data)->execute();

            foreach ($qa->comments as $comment) {
                if ($comment->parentId) {
                    $parent = Comment::findOne(['id' => $comment->parentId]);

                    if ($parent && $parent->author) {
                        $quoted = $parent->author->name;
                        $comment->content = '<b>' . $quoted . '</b>, ' . $comment->content;
                    }
                }

                $data = [
                    'themeId' => $themeId,
                    'userId' => $comment->authorId,
                    'content' => $comment->content,
                    'timeCreated' => $comment->timeCreated,
                    'timeUpdated' => $comment->timeCreated,
                ];

                \Yii::$app->db->createCommand()->insert('forum_comment', $data)->execute();
            }
        }
    }

    public function actionCleanup()
    {
        $questions = QuestionAnswer::find()->where('themeId IS NOT NULL')->all();
        $primaryKeys = ArrayHelper::getColumn($questions, 'themeId');

        Theme::deleteAll(['id' => $primaryKeys]);
        QuestionAnswer::updateAll(['themeId' => NULL]);
    }
}