<?php
namespace app\console;

use app\rbac\BoastVoteRule;
use app\rbac\CompositeRule;
use app\rbac\NotCommentedRule;
use app\rbac\NotOwnerRule;
use app\rbac\OwnerRule;
use app\rbac\CompareRule;
use app\rbac\TimeoutRule;
use app\rbac\UserGroupRule;
use app\rbac\NotVotedRule;
use Yii;
use yii\console\Controller;
use yii\rbac\ManagerInterface;
use yii\rbac\Role;

/**
 * Command-line interface for RBAC module.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class RbacController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = 'init';


    /**
     * Generates new set of rules.
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $userRule = new UserGroupRule();
        $auth->add($userRule);

        $user = $auth->createRole('user');
        $user->ruleName = $userRule->name;
        $auth->add($user);

        /**
         * Forum
         */
        $forumThemeModerator = $auth->createRole('forumThemeModerator');
        $forumThemeModerator->ruleName = $userRule->name;
        $auth->add($forumThemeModerator);
        $auth->addChild($forumThemeModerator, $user);

        $forumTopicModerator = $auth->createRole('forumTopicModerator');
        $forumTopicModerator->ruleName = $userRule->name;
        $auth->add($forumTopicModerator);
        $auth->addChild($forumTopicModerator, $forumThemeModerator);

        $forumModerator = $auth->createRole('forumModerator');
        $forumModerator->ruleName = $userRule->name;
        $auth->add($forumModerator);
        $auth->addChild($forumModerator, $forumTopicModerator);
        /**
         * End Forum
         */

        $redactor = $auth->createRole('redactor');
        $redactor->ruleName = $userRule->name;
        $auth->add($redactor);
        $auth->addChild($redactor, $user);

        $admin = $auth->createRole('admin');
        $admin->ruleName = $userRule->name;
        $auth->add($admin);
        $auth->addChild($admin, $redactor);
        $auth->addChild($admin, $forumModerator);

        $this->boasts($auth, $user, $admin);
        $this->comments($auth, $user, $admin);
        $this->complaints($auth, $user, $admin);
        $this->reviews($auth, $user, $admin);
        $this->forum($auth, $user, $admin);
    }
    
    protected function forum(ManagerInterface $auth, Role $user, Role $admin)
    {
        // create theme
        $createForumTheme = $auth->createPermission('createForumTheme');
        $auth->add($createForumTheme);
        $auth->addChild($admin, $createForumTheme);

        $openBoardRule = new CompareRule(['name' => 'openBoardRule', 'attribute' => 'board.isClosed', 'value' => 0]);
        $auth->add($openBoardRule);

        $createForumThemeInOpenBoard = $auth->createPermission('createForumThemeInOpenBoard');
        $createForumThemeInOpenBoard->ruleName = $openBoardRule->name;
        $auth->add($createForumThemeInOpenBoard);
        $auth->addChild($user, $createForumThemeInOpenBoard);
        $auth->addChild($createForumThemeInOpenBoard, $createForumTheme);

        // update theme
        $updateForumTheme = $auth->createPermission('updateForumTheme');
        $auth->add($updateForumTheme);
        $auth->addChild($admin, $updateForumTheme);

        $updateOwnForumThemeRule = new CompositeRule(['name' => 'updateOwnForumThemeRule']);
        $updateOwnForumThemeRule->rules[] = new OwnerRule(['attribute' => 'theme.userId']);
        $updateOwnForumThemeRule->rules[] = new TimeoutRule(['attribute' => 'theme.timeCreated', 'interval' => '1 day']);
        $auth->add($updateOwnForumThemeRule);

        $updateOwnForumTheme = $auth->createPermission('updateOwnForumTheme');
        $updateOwnForumTheme->ruleName = $updateOwnForumThemeRule->name;
        $auth->add($updateOwnForumTheme);
        $auth->addChild($user, $updateOwnForumTheme);
        $auth->addChild($updateOwnForumTheme, $updateForumTheme);

        // delete theme
        $deleteForumTheme = $auth->createPermission('deleteForumTheme');
        $auth->add($deleteForumTheme);
        $auth->addChild($admin, $deleteForumTheme);

        // moderate theme
        $moderateForumTheme = $auth->createPermission('moderateForumTheme');
        $auth->add($moderateForumTheme);
        $auth->addChild($admin, $moderateForumTheme);

        // pin theme
        $pinForumTheme = $auth->createPermission('pinForumTheme');
        $auth->add($pinForumTheme);
        $auth->addChild($admin, $pinForumTheme);

        // close theme
        $closeForumTheme = $auth->createPermission('closeForumTheme');
        $auth->add($closeForumTheme);
        $auth->addChild($admin, $closeForumTheme);

        // move theme
        $moveForumTheme = $auth->createPermission('moveForumTheme');
        $auth->add($moveForumTheme);
        $auth->addChild($admin, $moveForumTheme);

        // create comment
        $createForumComment = $auth->createPermission('createForumComment');
        $auth->add($createForumComment);
        $auth->addChild($admin, $createForumComment);

        $openThemeRule = new CompareRule(['name' => 'openThemeRule', 'attribute' => 'theme.isClosed', 'value' => 0]);
        $auth->add($openThemeRule);

        $createForumCommentInOpenTheme = $auth->createPermission('createForumCommentInOpenTheme');
        $createForumCommentInOpenTheme->ruleName = $openThemeRule->name;
        $auth->add($createForumCommentInOpenTheme);
        $auth->addChild($user, $createForumCommentInOpenTheme);
        $auth->addChild($createForumCommentInOpenTheme, $createForumComment);

        // update comment
        $updateForumComment = $auth->createPermission('updateForumComment');
        $auth->add($updateForumComment);
        $auth->addChild($admin, $updateForumComment);

        $updateOwnForumCommentRule = new CompositeRule(['name' => 'updateOwnForumCommentRule']);
        $updateOwnForumCommentRule->rules[] = new OwnerRule(['attribute' => 'comment.userId']);
        $updateOwnForumCommentRule->rules[] = new TimeoutRule(['attribute' => 'comment.timeCreated', 'interval' => '1 day']);
        $auth->add($updateOwnForumCommentRule);

        $updateOwnForumComment = $auth->createPermission('updateOwnForumComment');
        $updateOwnForumComment->ruleName = $updateOwnForumCommentRule->name;
        $auth->add($updateOwnForumComment);
        $auth->addChild($user, $updateOwnForumComment);
        $auth->addChild($updateOwnForumComment, $updateForumComment);

        // delete comment
        $deleteForumComment = $auth->createPermission('deleteForumComment');
        $auth->add($deleteForumComment);
        $auth->addChild($admin, $deleteForumComment);

        // pin comment
        $pinForumComment = $auth->createPermission('pinForumComment');
        $auth->add($pinForumComment);
        $auth->addChild($admin, $pinForumComment);

        // hide comment
        $hideForumComment = $auth->createPermission('hideForumComment');
        $auth->add($hideForumComment);
        $auth->addChild($admin, $hideForumComment);
    }

    protected function boasts(ManagerInterface $auth, Role $user, Role $admin)
    {
        $voteBoastRule = new BoastVoteRule();
        $auth->add($voteBoastRule);

        $voteBoast = $auth->createPermission('voteBoast');
        $voteBoast->ruleName = $voteBoastRule->name;
        $auth->add($voteBoast);
        $auth->addChild($user, $voteBoast);

        // update boast -> owner, timeout

        $updateBoast = $auth->createPermission('updateBoast');
        $auth->add($updateBoast);
        $auth->addChild($admin, $updateBoast);

        $updateOwnBoastRule = new CompositeRule(['name' => 'updateOwnBoastRule']);
        $updateOwnBoastRule->rules[] = new OwnerRule(['attribute' => 'boast.userId']);
        $updateOwnBoastRule->rules[] = new TimeoutRule(['attribute' => 'boast.timeCreated', 'interval' => '1 day']);
        $auth->add($updateOwnBoastRule);

        $updateOwnBoast = $auth->createPermission('updateOwnBoast');
        $updateOwnBoast->ruleName = $updateOwnBoastRule->name;
        $auth->add($updateOwnBoast);
        $auth->addChild($updateOwnBoast, $updateBoast);
        $auth->addChild($user, $updateOwnBoast);

        // delete boast -> owner, timeout

        $deleteBoast = $auth->createPermission('deleteBoast');
        $auth->add($deleteBoast);
        $auth->addChild($updateBoast, $deleteBoast);
    }

    protected function comments(ManagerInterface $auth, Role $user, Role $admin)
    {
        // delete comment -> owner, timeout, not commented

        $deleteCommentRule = new CompositeRule(['name' => 'deleteCommentRule']);
        $deleteCommentRule->rules[] = new OwnerRule(['attribute' => 'comment.authorId']);
        $deleteCommentRule->rules[] = new TimeoutRule(['attribute' => 'comment.timeCreated', 'interval' => '1 day']);
        $deleteCommentRule->rules[] = new NotCommentedRule();
        $auth->add($deleteCommentRule);

        $deleteComment = $auth->createPermission('deleteComment');
        $deleteComment->ruleName = $deleteCommentRule->name;
        $auth->add($deleteComment);
        $auth->addChild($user, $deleteComment);

        // vote comment -> not owner, not voted

        $voteCommentRule = new CompositeRule(['name' => 'voteCommentRule']);
        $voteCommentRule->rules[] = new NotOwnerRule(['attribute' => 'comment.authorId']);
        $voteCommentRule->rules[] = new NotVotedRule(['attribute' => 'comment']);
        $auth->add($voteCommentRule);

        $voteComment = $auth->createPermission('voteComment');
        $voteComment->ruleName = $voteCommentRule->name;
        $auth->add($voteComment);
        $auth->addChild($user, $voteComment);
    }

    protected function complaints(ManagerInterface $auth, Role $user, Role $admin)
    {
        // update complaint -> owner, timeout, rejected
        // delete complaint -> owner, timeout, rejected
        // upload photo -> owner, timeout, rejected
        // delete photo -> owner, timeout, rejected

        $updateComplaint = $auth->createPermission('updateComplaint');
        $auth->add($updateComplaint);
        $auth->addChild($admin, $updateComplaint);

        $updateCreatedComplaintRule = new CompositeRule(['name' => 'updateCreatedComplaintRule']);
        $updateCreatedComplaintRule->rules[] = new OwnerRule(['attribute' => 'complaint.userId']);
        $updateCreatedComplaintRule->rules[] = new TimeoutRule(['attribute' => 'complaint.timeCreated', 'interval' => '1 day']);
        $auth->add($updateCreatedComplaintRule);

        $updateCreatedComplaint = $auth->createPermission('updateCreatedComplaint');
        $updateCreatedComplaint->ruleName = $updateCreatedComplaintRule->name;
        $auth->add($updateCreatedComplaint);
        $auth->addChild($updateCreatedComplaint, $updateComplaint);
        $auth->addChild($user, $updateCreatedComplaint);

        $updateRejectedComplaintRule = new CompositeRule(['name' => 'updateRejectedComplaintRule']);
        $updateRejectedComplaintRule->rules[] = new OwnerRule(['attribute' => 'complaint.userId']);
        $updateRejectedComplaintRule->rules[] = new CompareRule(['attribute' => 'complaint.rejected', 'value' => 1]);
        $auth->add($updateRejectedComplaintRule);

        $updateRejectedComplaint = $auth->createPermission('updateRejectedComplaint');
        $updateRejectedComplaint->ruleName = $updateRejectedComplaintRule->name;
        $auth->add($updateRejectedComplaint);
        $auth->addChild($updateRejectedComplaint, $updateComplaint);
        $auth->addChild($user, $updateRejectedComplaint);

        $deleteComplaint = $auth->createPermission('deleteComplaint');
        $auth->add($deleteComplaint);
        $auth->addChild($updateComplaint, $deleteComplaint);
    }

    protected function reviews(ManagerInterface $auth, Role $user, Role $admin)
    {
        $updateReview = $auth->createPermission('updateReview');
        $auth->add($updateReview);
        $auth->addChild($admin, $updateReview);

        $updateOwnReviewRule = new CompositeRule(['name' => 'updateOwnReviewRule']);
        $updateOwnReviewRule->rules[] = new OwnerRule(['attribute' => 'review.userId']);
        $updateOwnReviewRule->rules[] = new TimeoutRule(['attribute' => 'review.timeCreated', 'interval' => '1 day']);
        $auth->add($updateOwnReviewRule);

        $updateOwnReview = $auth->createPermission('updateOwnReview');
        $updateOwnReview->ruleName = $updateOwnReviewRule->name;
        $auth->add($updateOwnReview);
        $auth->addChild($updateOwnReview, $updateReview);
        $auth->addChild($user, $updateOwnReview);

        $updateRejectedReviewRule = new CompositeRule(['name' => 'updateRejectedReviewRule']);
        $updateRejectedReviewRule->rules[] = new OwnerRule(['attribute' => 'review.userId']);
        $updateRejectedReviewRule->rules[] = new CompareRule(['attribute' => 'review.isRejected', 'value' => 1]);
        $auth->add($updateRejectedReviewRule);

        $updateRejectedReview = $auth->createPermission('updateRejectedReview');
        $updateRejectedReview->ruleName = $updateRejectedReviewRule->name;
        $auth->add($updateRejectedReview);
        $auth->addChild($updateRejectedReview, $updateReview);
        $auth->addChild($user, $updateRejectedReview);

        $deleteReview = $auth->createPermission('deleteReview');
        $auth->add($deleteReview);
        $auth->addChild($updateReview, $deleteReview);
    }
}
