<?php
namespace app\console;

use app\db\records\Notifications;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Notifications manager.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class NotificationsController extends Controller
{
    /**
     * Deletes all notifications in database.
     */
    public function actionDeleteAll()
    {
        $records = Notifications::deleteAll();
        Console::output(sprintf('%d records were deleted.', $records));
    }

    /**
     * Deletes qa notifications in database.
     */
    public function actionDeleteQa()
    {
        $types = ['NOTICE_QA', 'NOTICE_QA_COMMENT'];

        $records = Notifications::deleteAll(['type' => $types]);
        Console::output(sprintf('%d records were deleted.', $records));
    }

    /**
     *
     */
    public function actionDelete()
    {
        Notifications::deleteAll([
            'isRead' => Notifications::NOTIFICATION_READ,
        ]);
    }

    /**
     *
     */
    public function actionView()
    {
        foreach (Notifications::find()->all() as $notify) {
            Console::output(print_r($notify->attributes, true));
        }
    }
}