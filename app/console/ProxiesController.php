<?php
namespace app\console;

use app\db\records\Proxy;
use Yii;
use yii\console\Controller;

/**
 * Работа с прокси-листом
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProxiesController extends Controller
{
    /**
     * Загружает новые проксики
     */
    public function actionLoad()
    {
        $url = Yii::$app->params['proxies.url'];

        $content = file_get_contents($url);
        $items = explode("\n", $content);

        foreach ($items as $item) {
            if ($this->isStringIp($item)) {
                list($ip, $port) = explode(':', $item);

                $ip = trim($ip);
                $port = trim($port);

                $this->addProxy($ip, $port);
            }
        }
    }

    /**
     * Возвращает в работу проксики, которые отказали 5 минут назад
     */
    public function actionReset()
    {
        $timeout = date('Y-m-d H:i:s', strtotime('5 minutes ago'));

        Yii::$app->db->createCommand()
            ->update('proxy', ['isValid' => 1], 'timeFailed < :timeout', [':timeout' => $timeout])
            ->execute();
    }

    /**
     * @param string $string
     * @return boolean
     */
    protected function isStringIp($string)
    {
        return (bool) strpos($string, ':');
    }

    /**
     * @param string $ip
     * @param string $port
     */
    protected function addProxy($ip, $port)
    {
        if (Proxy::findOne(compact('ip', 'port'))) {
            return;
        }

        $proxy = new Proxy();
        $proxy->ip = $ip;
        $proxy->port = $port;
        $proxy->save();
    }
}