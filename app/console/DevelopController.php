<?php
namespace app\console;

use app\db\records\Boast;
use app\db\records\Review;
use app\modules\forum\entities\Comment;
use yii\console\Controller;

class DevelopController extends Controller
{
    public function actionBoastSlug()
    {
        foreach (Boast::find()->where('slug IS NULL')->each() as $boast) {
            $boast->save();
        }
    }

    /**
     * Convert forum comments content from Html to BBCodes
     */
    public function actionForumBb()
    {
        foreach (Comment::find()->where('contentOriginal IS NOT NULL')->each() as $comment) {
            $text = $this->htmlToBBCode($comment->contentOriginal);
            $comment->updateAttributes(['content' => $text]);
        }
    }

    /**
     * Convert review content from Html to BBCodes
     */
    public function actionReviewBb()
    {
        foreach (Review::find()->where('contentOriginal IS NOT NULL')->each() as $review) {
            $text = $this->htmlToBBCode($review->contentOriginal);
            $review->updateAttributes(['content' => $text]);
        }
    }

    /**
     * @param string $string
     * @return mixed
     */
    protected function htmlToBBCode($string)
    {
        $string = str_replace('<h4>', '[h4]', $string);
        $string = str_replace('</h4>', '[/h4]', $string);

        $string = str_replace('<h5>', '[h5]', $string);
        $string = str_replace('</h5>', '[/h5]', $string);

        $string = str_replace('<h6>', '[h6]', $string);
        $string = str_replace('</h6>', '[/h6]', $string);

        $string = str_replace('<b>', '[b]', $string);
        $string = str_replace('</b>', '[/b]', $string);

        $string = str_replace('<i>', '[i]', $string);
        $string = str_replace('</i>', '[/i]', $string);

        $string = str_replace('<s>', '[s]', $string);
        $string = str_replace('</s>', '[/s]', $string);

        $string = preg_replace('#<img src="(.*?)">#', '[img]$1[/img]', $string);

        $string = preg_replace('#<a href="(.*?)">#', '[url="$1"]', $string);
        $string = str_replace('</a>', '[/url]', $string);

        $string = str_replace('<spoiler>', '[spoiler]', $string);
        $string = preg_replace('#<spoiler title="(.*?)">#', '[spoiler="$1"]', $string);
        $string = str_replace('</spoiler>', '[/spoiler]', $string);

        $string = str_replace('<blockquote>', '[quote]', $string);
        $string = preg_replace('#<blockquote title="(.*?)">#', '[quote="$1"]', $string);
        $string = str_replace('</blockquote>', '[/quote]', $string);

        $string = str_replace('<video>', '[video]', $string);
        $string = str_replace('</video>', '[/video]', $string);

        $string = str_replace('<cut>', '[cut]', $string);

        $string = str_replace('<ul>', '[ul]', $string);
        $string = str_replace('</ul>', '[/ul]', $string);

        $string = str_replace('<ol>', '[ol]', $string);
        $string = str_replace('</ol>', '[/ol]', $string);

        $string = str_replace('<li>', '[*]', $string);
        $string = str_replace('</li>', '', $string);

        return $string;
    }
}