<?php
namespace app\console;

use app\db\records\Article;
use app\db\records\Boast;
use app\db\records\News;
use app\db\records\Complaint;
use app\db\records\Seller;
use app\db\records\Review;
use app\helpers\Url;
use Tackk\Cartographer\ChangeFrequency;
use Tackk\Cartographer\Sitemap;
use Yii;
use yii\console\Controller;

/**
 * Sitemap XML generator.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class SitemapController extends Controller
{
    /**
     * Generates new sitemap fiel and places it in webroot directory.
     */
    public function actionGenerate()
    {
        $sitemap = new Sitemap();
        $query = Seller::find()->hasComplaints();

        /* @var $seller Seller */
        /* @var $complaint Complaint */
        foreach ($query->each() as $seller) {
            $sitemap->add(Url::toSeller($seller, [], true), $seller->timeUpdated, ChangeFrequency::WEEKLY);
            $sitemap->add(Url::toSellerComplaints($seller, [], true));
        }

        /* @var $article Article */
        foreach (Article::find()->published()->each() as $article) {
            $sitemap->add(Url::entity($article, true), $article->timePublished);
        }

        /* @var $news News */
        foreach (News::find()->published()->each() as $news) {
            $sitemap->add(Url::entity($news, true), $news->timePublished);
        }

        /* @var $reviews Reviews */
        foreach (Review::find()->visible()->each() as $reviews) {
            $sitemap->add(Url::entity($reviews, true), $reviews->timeCreated);
        }

        /* @var $boast Boast */
        $sitemap->add(Url::to(['/boasts/index'], true));
        foreach (Boast::find()->visible()->each() as $boast) {
            $sitemap->add(Url::entity($boast, true), $boast->timeCreated);
        }

        $file = Yii::getAlias('@webroot/sitemap.xml');
        file_put_contents($file, (string) $sitemap);
    }
}