<?php
namespace app\console;

use app\db\records\LatestNews;
use yii\console\Controller;
use yii\helpers\Console;

/**
 */
class LatestNewsController extends Controller
{
    /**
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete()
    {
        $count = LatestNews::find()->count();
        $limit = $count - 10;

        if($limit > 0){
            foreach(LatestNews::find()->limit($limit)->all() as $item){
                $item->delete();
            }
        }
    }

    /**
     *
     */
    public function actionView()
    {
        foreach (LatestNews::find()->all() as $item) {
            Console::output(print_r($item->attributes, true));
        }
    }
}