<?php
namespace app\console;

use aliexpress\api\Client;
use aliexpress\epn\ApiClient;
use yii\base\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ApiTestController extends Controller
{
    public function actionDirect()
    {
        $api = new Client(57756);

        $result = $api->getList([
            'fields' => 'productId,productTitle,productUrl',
            'keywords' => 'шлепки',
            'originalPriceFrom' => 0.69,
            'originalPriceTo' => 9999.69,
        ]);

        $products = $result['products'];

        $urls = [
            'http://ru.aliexpress.com/',
            'http://www.aliexpress.com/item//1360130582.html',
            'http://ru.aliexpress.com/item/Magic-car-slip-resistant-pad-slip-resistant-mobile-phone-pad-b435/779703763.html?s=p&spm=0.0.0.0.l4yFTa',
            'http://ru.aliexpress.com/store/419019',
            'http://ru.aliexpress.com/store/feedback-score/419019.html',
            'http://www.aliexpress.com/snapshot/6329468646.html',
            'http://ru.itao.com/item/3147104090?tracelog=itao_aepost',
            'http://google.com/',
        ];

        $result = $api->getLinks([
            'trackingId' => 'products',
            'urls' => implode(',', $urls),
        ]);

        die( var_dump($result) );
    }

    public function actionEpn()
    {
        $api = new ApiClient('5aa896d6a16559301b9e33c16ea202c1', 'b0c6350076f5a23f1f27e7cc088f5fb9');

        $api->addRequestCountForSearch('counts', ['query' => 'iphone']);
        $api->addRequestSearch('search', ['query' => 'шлепки', 'limit' => 100, 'offset' => 950]);
        $api->runRequests();

        $counts = $api->getRequestResult('counts');
        $search = $api->getRequestResult('search');

        die( var_dump($counts, $search) );

    }
}
