<?php
namespace app\console;

use app\db\records\LatestNews;
use yii\console\Controller;
use yii\helpers\Console;

class LastNewsController extends Controller
{
    public function actionIndex()
    {
        $news = LatestNews::find()->all();

        foreach ($news as $item) {
            Console::output('[' . $item->id . '] ' . $item->title . ' ' . $item->text);
        }
    }

    public function actionRemove($id)
    {
        LatestNews::deleteAll(['id' => $id]);
    }
}