<?php
namespace app\console;

use app\db\records\Seller;
use app\db\records\SellerReplica;
use app\parser\entities\SellerEntity;
use app\parser\NotFoundHttpException;
use app\parser\importers\SellerImporter;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * CLI helper for parsing seller records.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellersController extends Controller
{
    /**
     * Updates seller with specified identifier.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /* @var $record Seller */
        $record = Seller::findOne($id);

        if ( ! $record) {
            Console::error(sprintf('Seller with id %s was not found', $id));
            return static::EXIT_CODE_ERROR;
        }

        $importer = new SellerImporter();

        if ( ! $this->import($importer, $record)) {
            Console::error('Import process fails.');
            return static::EXIT_CODE_ERROR;
        }

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Runs replication mechanism for sellers.
     *
     * @return mixed
     */
    public function actionReplicate()
    {
        /* @var $seller Seller */
        $seller = Seller::find()->replicable()->one();

        if ( ! $seller) {
            Console::output('There are no replicable sellers found');
            return static::EXIT_CODE_NORMAL;
        }

        $seller->touch(Seller::TIME_REPLICATION_STARTED);

        $importer = new SellerImporter();
        if ( ! $this->import($importer, $seller)) {
            Console::error('Import process fails.');
            return static::EXIT_CODE_ERROR;
        }

        $replica = new SellerReplica();
        $replica->setOriginal($seller);
        if ( ! $replica->save()) {
            Console::error('Replication process fails.');
            return static::EXIT_CODE_ERROR;
        }

        $seller->touch(Seller::TIME_REPLICATION_SUCCEED);

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * @param SellerImporter $importer
     * @param Seller         $record
     *
     * @return boolean
     */
    protected function import(SellerImporter $importer, Seller $record)
    {
        try {
            $storeEntity = $importer->createStoreEntity($record);
            $contactsPage = $storeEntity->downloadContactsPage();
            $salePage = $storeEntity->downloadSalePage();

            $sellerEntity = new SellerEntity($contactsPage->getSellerId());
            $feedbackPage = $sellerEntity->downloadFeedbackPage();
            $wishlistPage = $sellerEntity->downloadWishlistPage();
        } catch (NotFoundHttpException $e) {
            $record->setClosed();
            $record->save();
            return false;
        }

        $importer->loadFeedbackPage($feedbackPage);
        $importer->loadSalePage($salePage);
        $importer->loadContactsPage($contactsPage);
        $importer->loadWishlistPage($wishlistPage);

        return $importer->save($record);
    }
}