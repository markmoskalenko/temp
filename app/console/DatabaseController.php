<?php
namespace app\console;

use app\db\records\Seller;
use Yii;
use yii\console\Controller;

class DatabaseController extends Controller
{
    public function actionStore()
    {
        $data = [];

        /* @var $seller Seller */
        foreach (Seller::find()->each() as $seller) {
            $complaintsCount = $seller->getComplaints()->visible()->count();
            if ($complaintsCount) {
                $data[] = ['s' => (int) $seller->originalStoreId, 'c' => (int) $complaintsCount];
            }
        };

        $data = json_encode($data);
        $signature = md5($data);

        $storage = Yii::getAlias('@runtime/browser-extension');
        file_put_contents($storage . '/data.json', $data);
        file_put_contents($storage . '/version.txt', $signature);
    }
}
