<?php
namespace app\helpers;

use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Angular
{
    /**
     * @param string $name
     * @param array  $attributes
     * @return string
     */
    public static function directive($name, $attributes = [])
    {
        return Html::tag('div', '', [$name => true, 'data' => $attributes]);
    }
}