<?php
namespace app\helpers;

use app\db\records\Seller;
use app\interfaces\UploadableModel;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseUrl;
use yii\helpers\StringHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Url extends BaseUrl
{
    /**
     * Оборачивает ссылку в редирект с автоматической авторизацией.
     *
     * @param mixed $url
     * @param string $token
     * @param mixed $scheme
     * @return string
     */
    public static function auth($url, $token, $scheme = false)
    {
        $url = static::to($url, $scheme);
        return static::to(['redirect/auth', 'url' => $url, 'token' => (string) $token], $scheme);
    }

    /**
     * Оборачивает ссылку в CPA редирект.
     *
     * @param string  $url
     * @param string  $category
     * @param boolean $scheme
     * @return string
     */
    public static function cpa($url, $category, $scheme = false)
    {
        $url = static::to($url, $scheme);
        return static::to(['redirect/cpa', 'url' => $url, 'category' => $category], $scheme);
    }

    /**
     * @param $object
     * @param bool $scheme
     * @param array $params
     * @return string
     * @throws NotSupportedException
     */
    public static function entity($object, $scheme = false, $params = [])
    {
        if ($object instanceof \app\db\records\Article) {
            return static::to(ArrayHelper::merge(['/articles/view', 'id' => $object->id, 'slug' => $object->slug, 'category' => $object->category->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\News) {
            return static::to(ArrayHelper::merge(['/news/view', 'id' => $object->id, 'slug' => $object->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\Boast) {
            return static::to(ArrayHelper::merge(['/boasts/view', 'id' => $object->id, 'slug' => $object->slug, 'category' => $object->category->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\Complaint) {
            return static::to(ArrayHelper::merge(['/complaints/view', 'id' => $object->id], $params), $scheme);
        }

        if ($object instanceof \app\db\records\QuestionAnswer) {
            return static::to(ArrayHelper::merge(['/question-answer/view', 'id' => $object->id, 'slug' => $object->slug, 'categorySlug' => $object->qaCategory->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\Review) {
            return static::to(ArrayHelper::merge(['/reviews/view', 'id' => $object->id, 'slug' => $object->slug, 'category' => $object->category->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\Seller) {
            return static::to(ArrayHelper::merge(['/sellers/view', 'originalStoreId' => $object->originalStoreId, 'slug' => $object->slug], $params), $scheme);
        }

        if ($object instanceof \app\db\records\User) {
            return static::to(ArrayHelper::merge(['/profile/view', 'id' => $object->id], $params), $scheme);
        }

        if ($object instanceof \app\db\records\Follower) {
            return static::to(ArrayHelper::merge(['/profile/view', 'id' => $object->userId], $params), $scheme);
        }

        if ($object instanceof \app\modules\forum\entities\Board) {
            return static::to(['/forum/themes/index', 'boardId' => $object->id], $scheme);
        }

        if ($object instanceof \app\modules\forum\entities\Theme) {
            return static::to(['/forum/comments/index', 'themeId' => $object->id], $scheme);
        }

        if ($object instanceof \app\modules\forum\entities\Comment) {
            return static::to(['/forum/comments/view', 'id' => $object->id], $scheme);
        }

        throw new NotSupportedException(sprintf('Entity of type "%s" is not supported.', get_class($object)));
    }

    /**
     * @param UploadableModel $model
     * @param mixed           $scheme
     * @return string
     */
    public static function uploaded(UploadableModel $model, $scheme = false)
    {
        return static::to('/uploads/' . $model->getFileName(), $scheme);
    }

    /**
     * @param UploadableModel $file
     * @param array           $params
     * @param mixed           $scheme
     * @return string
     */
    public static function thumb(UploadableModel $file, array $params, $scheme = false)
    {
        $filename = $file->getFileName();
        $thumbs = Yii::$app->thumbManager;

        try {
            if ( ! $thumbs->exists($filename, $params)) {
                $thumbs->create($filename, $params);
            }
        } catch (\Exception $e) {
            Yii::$app->raven->captureException($e);
            return static::uploaded($file);
        }

        return static::to('/thumbs/' . $thumbs->hash($params) . '/' . $filename, $scheme);
    }

    /**
     * @param \app\db\records\User $user
     * @param array                $params
     * @param mixed                $scheme
     * @return string
     */
    public static function avatar($user = null, $params = [], $scheme = false)
    {
        if ($user instanceof \app\db\records\User && $user->avatarUrl) {
            $avatarUrl = $user->avatarUrl;
        } elseif (is_string($user)) {
            $avatarUrl = $user;
        }

        if (empty($avatarUrl)) {
            return static::to('/images/avatar.png', $scheme);
        }

        if ( ! $params || ! StringHelper::startsWith($avatarUrl, '/uploads/')) {
            return $avatarUrl;
        }

        $filename = str_replace('/uploads/', '', $avatarUrl);
        $thumbs = Yii::$app->thumbManager;

        try {
            if ( ! $thumbs->exists($filename, $params)) {
                $thumbs->create($filename, $params);
            }
        } catch (\Exception $e) {
            Yii::$app->raven->captureException($e);
            return $avatarUrl;
        }

        return static::to('/thumbs/' . $thumbs->hash($params) . '/' . $filename, $scheme);
    }

    /**
     * @param Seller $seller
     * @param array  $params
     * @param mixed  $scheme
     *
     * @return string
     */
    public static function toSeller(Seller $seller, $params = [], $scheme = false)
    {
        return static::toRoute(array_merge([
            '/sellers/view',
            'originalStoreId' => $seller->originalStoreId,
            'slug' => $seller->slug
        ], $params), $scheme);
    }

    /**
     * @param Seller $seller
     * @param array  $params
     * @param mixed  $scheme
     *
     * @return string
     */
    public static function toSellerComplaints(Seller $seller, $params = [], $scheme = false)
    {
        return static::toRoute(array_merge([
            '/sellers/complaints',
            'originalStoreId' => $seller->originalStoreId,
            'slug' => $seller->slug,
        ], $params), $scheme);
    }
}
