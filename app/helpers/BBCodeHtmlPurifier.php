<?php
namespace app\helpers;

use app\purifier\HTMLPurifier_Filter_Video;
use yii\helpers\BaseHtmlPurifier;

/**
 * Этот
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class BBCodeHtmlPurifier extends BaseHtmlPurifier
{
    /**
     * @param \HTMLPurifier_Config $config
     */
    protected static function configure($config)
    {
        // convert urls to links
        $config->set('AutoFormat.Linkify', true);

        // force target=_blank urls
        $config->set('HTML.TargetBlank', true);

        // deny images from other hosts
        $config->set('URI.DisableExternalResources', true);

        // munge all urls via redirect
        $config->set('URI.Munge', 'http://alitrust.ru/redirect?url=%s');

        // allow video tag
        $html = $config->getHTMLDefinition($raw = true);
        $html->addElement('video', 'Inline', 'Inline', 'Common');

        // allow attributes for spoiler collapse
        $html->addAttribute('div', 'id', 'CDATA');
        $html->addAttribute('div', 'data-toggle', 'CDATA');
        $html->addAttribute('div', 'data-target', 'CDATA');

        // add video tag
        $config->set('Filter.Custom', [
            new HTMLPurifier_Filter_Video(),
        ]);
    }
}