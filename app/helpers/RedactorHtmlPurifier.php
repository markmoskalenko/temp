<?php
namespace app\helpers;

use app\purifier\HTMLPurifier_Filter_Video;
use yii\helpers\BaseHtmlPurifier;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RedactorHtmlPurifier extends BaseHtmlPurifier
{
    /**
     * @param \HTMLPurifier_Config $config
     */
    protected static function configure($config)
    {
        // convert urls to links
        $config->set('AutoFormat.Linkify', true);

        // force target=_blank urls
        $config->set('HTML.TargetBlank', true);

        // allow youtube and vimeo iframes
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%');

        // munge all urls via redirect
        $config->set('URI.Munge', 'http://alitrust.ru/redirect?url=%s');
    }
}