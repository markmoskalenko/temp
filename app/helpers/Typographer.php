<?php
namespace app\helpers;

use EMT\EMTypograph;

class Typographer
{
    /**
     * @param string $text
     * @return string
     */
    public static function prettify($text)
    {
        $typograf = new EMTypograph();

        $typograf->trets = [
            '\EMT\Tret\Quote',
            '\EMT\Tret\Dash',
            '\EMT\Tret\Symbol',
            '\EMT\Tret\Punctmark',
            '\EMT\Tret\Number',
            '\EMT\Tret\Space',
            '\EMT\Tret\Abbr',
            '\EMT\Tret\Nobr',
            '\EMT\Tret\Date',
            '\EMT\Tret\Etc',
        ];

        $typograf->setup([
            'Abbr.nbsp_money_abbr' => 'off',
            'Etc.unicode_convert' => 'on',
        ]);

        $typograf->set_text($text);
        $output = $typograf->apply();

        // Вырезаем все HTML теги
        $output = strip_tags($output);

        // делаем первую букву прописной
        $output = static::ucfirst($output);

        // делаем прописной первые буквы в начале предложений
        $output = preg_replace_callback('/([….!?])\s+([а-яё]{2,})/iu', function ($matches) {
            return $matches[1] . ' ' . static::ucfirst($matches[2]);
        }, $output);

        return $output;
    }

    /**
     * @param string $text
     * @return string
     */
    public static function ucfirst($text)
    {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }
}