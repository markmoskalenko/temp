<?php
namespace app\helpers;

use app\bbcode\MarkerListDefinition;
use app\bbcode\NumberListDefinition;
use app\bbcode\SpoilerDefinition;
use JBBCode\CodeDefinitionBuilder;
use JBBCode\Parser;

class BBCodeParser
{
    /**
     * @param string $string
     * @return string
     */
    public static function process($string)
    {
        $parser = new Parser();

        static::configure($parser);

        $parser->parse($string);

        static::filter($parser);

        return static::compile($parser);
    }

    /**
     * @param Parser $parser
     */
    public static function configure(Parser $parser)
    {
        /* [h1] header tag */
        $builder = new CodeDefinitionBuilder('h1', '<div class="b-publication__h1">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [h2] header tag */
        $builder = new CodeDefinitionBuilder('h2', '<div class="b-publication__h2">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [h3] header tag */
        $builder = new CodeDefinitionBuilder('h3', '<div class="b-publication__h3">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [b] bold tag */
        $builder = new CodeDefinitionBuilder('b', '<strong>{param}</strong>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [i] italics tag */
        $builder = new CodeDefinitionBuilder('i', '<em>{param}</em>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [u] underline tag */
        $builder = new CodeDefinitionBuilder('u', '<u>{param}</u>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        $urlValidator = new \JBBCode\validators\UrlValidator();

        /* [url] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{param}">{param}</a>');
        $builder->setParseContent(false)->setBodyValidator($urlValidator);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [url=http://example.com] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{option}">{param}</a>');
        $builder->setUseOption(true)->setParseContent(true)->setOptionValidator($urlValidator);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [img] image tag */
        $builder = new CodeDefinitionBuilder('img', '<img src="{param}" />');
        $builder->setUseOption(false)->setParseContent(false);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [video] video tag */
        // отдаём разбираться фильтру HtmlPurifier, из-за глюка с запрещением картинок с других ресурсов.
        $builder = new CodeDefinitionBuilder('video', '<video>{param}</video>');
        $builder->setUseOption(false)->setParseContent(false);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [quote] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="b-publication__quote">{param}</div>');
        $builder->setParseContent(true);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [quote="Chuck Norris"] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="b-publication__quote"><div class="b-publication__quote-header">{option}</div>{param}</div>');
        $builder->setUseOption(true)->setParseContent(true);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [spoiler] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(false);
        $parser->addCodeDefinition($definition);

        /* [spoiler="Заголовок"] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(true);
        $parser->addCodeDefinition($definition);

        /* [ul] list tag */
        $parser->addCodeDefinition(new MarkerListDefinition());

        /* [ol] list tag */
        $parser->addCodeDefinition(new NumberListDefinition());
    }

    /**
     * @param Parser $parser
     */
    public static function filter(Parser $parser)
    {
        $parser->accept(new \JBBCode\visitors\HTMLSafeVisitor());
        $parser->accept(new \app\bbcode\ParagraphsVisitor());
        $parser->accept(new \app\bbcode\TrimVisitor());
        $parser->accept(new \app\bbcode\LineBreakVisitor());
    }

    /**
     * @param Parser $parser
     * @return string
     */
    public static function compile(Parser $parser)
    {
        $html = $parser->getAsHTML();
        $html = str_replace('[cut]', '', $html);
        $html = BBCodeHtmlPurifier::process($html);

        return $html;
    }
}