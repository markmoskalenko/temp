<?php
namespace app\helpers;

class Smile
{
    public static $map = [
        '(acute)' => 'acute.gif',
        '(aggressive)' => 'aggressive.gif',
        '(bad)' => 'bad.gif',
        '(biggrin)' => 'biggrin.gif',
        '(clapping)' => 'clapping.gif',
        '(cool)' => 'cool.gif',
        '(dance)' => 'dance2.gif',
        '(drinks)' => 'drinks.gif',
        '(empathy)' => 'empathy.gif',
        '(flag)' => 'flag_of_truce.gif',
        '(fool)' => 'fool.gif',
        '(good)' => 'good.gif',
        '(greeting)' => 'greeting.gif',
        '(help)' => 'help.gif',
        '(lol)' => 'lol.gif',
        '(music)' => 'music.gif',
        '(negative)' => 'negative.gif',
        '(new_russian)' => 'new_russian.gif',
        '(ok)' => 'ok.gif',
        '(pardon)' => 'pardon.gif',
        '(rofl2)' => 'rofl2.gif',
        '(rolleyes2)' => 'rolleyes2.gif',
        '(sad)' => 'sad.gif',
        '(scratch)' => 'scratch_one-s_head.gif',
        '(secret)' => 'secret.gif',
        '(shok)' => 'shok.gif',
        '(shout)' => 'shout.gif',
        '(timeout)' => 'timeout.gif',
        '(girl_blush)' => 'girl_blush.gif',
        '(girl_blush2)' => 'girl_blush2.gif',
        '(girl_cray)' => 'girl_cray.gif',
        '(girl_haha)' => 'girl_haha.gif',
        '(girl_blum)' => 'girl_blum.gif',
        '(girl_impossible)' => 'girl_impossible.gif',
        '(girl_hysteric)' => 'girl_hysteric.gif',
    ];


    /**
     * @return array
     */
    public static function all()
    {
        return array_keys(static::$map);
    }

    /**
     * @param string $code
     * @return string
     */
    public static function url($code)
    {
        return Url::to('/images/smiles/' . static::$map[ $code ]);
    }
}