<?php
namespace app\oauth;

use yii\authclient\OAuthToken;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
interface ClientInterface extends \yii\authclient\ClientInterface
{
    /**
     * @return OAuthToken
     */
    public function getAccessToken();

    /**
     * @return string
     */
    public function getUserId();

    /**
     * @return string
     */
    public function getUserEmail();

    /**
     * @return string
     */
    public function getUserName();

    /**
     * @return string
     */
    public function getAvatarUrl();
}