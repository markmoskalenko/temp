<?php
namespace app\oauth;

use yii\authclient\clients\VKontakte;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class VK extends VKontakte implements ClientInterface
{
    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'vk';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Вконтакте';
    }

    /**
     * @inheritdoc
     */
    public function getUserId()
    {
        return ArrayHelper::getValue($this->userAttributes, 'id');
    }

    /**
     * @inheritdoc
     */
    public function getUserEmail()
    {
        return $this->getAccessToken()->getParam('email');
    }

    /**
     * @inheritdoc
     */
    public function getUserName()
    {
        $firstName = ArrayHelper::getValue($this->userAttributes, 'first_name');
        $lastName = ArrayHelper::getValue($this->userAttributes, 'last_name');
        return implode(' ', array_filter([$firstName, $lastName]));
    }

    /**
     * @inheritdoc
     */
    public function getAvatarUrl()
    {
        return ArrayHelper::getValue($this->userAttributes, 'photo');
    }
}