<?php
namespace app\oauth;

use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Facebook extends \yii\authclient\clients\Facebook implements ClientInterface
{
    /**
     * @inheritdoc
     */
    public function getUserId()
    {
        return ArrayHelper::getValue($this->userAttributes, 'id');
    }

    /**
     * @inheritdoc
     */
    public function getUserEmail()
    {
        return ArrayHelper::getValue($this->userAttributes, 'email');
    }

    /**
     * @inheritdoc
     */
    public function getUserName()
    {
        $firstName = ArrayHelper::getValue($this->userAttributes, 'first_name');
        $lastName = ArrayHelper::getValue($this->userAttributes, 'last_name');
        return implode(' ', array_filter([$firstName, $lastName]));
    }

    /**
     * @inheritdoc
     */
    public function getAvatarUrl()
    {
        return null;
    }
}