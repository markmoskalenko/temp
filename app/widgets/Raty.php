<?php
namespace app\widgets;

use app\assets\RatyAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Raty extends InputWidget
{
    /**
     * @var array the JQuery plugin options for the raty plugin.
     * @see https://github.com/wbotelhos/raty
     */
    public $clientOptions = [];
    /**
     * @var array options for a container of the raty plugin
     */
    public $containerOptions = [];


    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if (empty($this->clientOptions['targetScore'])) {
            $this->clientOptions['targetScore'] = '#' . $this->options['id'];
        }

        if (empty($this->clientOptions['score'])) {
             if ($this->hasModel()) {
                 $this->clientOptions['score'] = $this->model->{$this->attribute};
             } else {
                 $this->clientOptions['score'] = $this->value;
             }
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->hasModel()) {
            echo Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::hiddenInput($this->name, $this->value, $this->options);
        }

        $containerOptions = $this->containerOptions;
        $containerTag = ArrayHelper::remove($containerOptions, 'tag', 'div');
        echo Html::tag($containerTag, '', $containerOptions);

        $this->registerClientScript();
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $view = $this->getView();

        $id = $this->options['id'];
        $js = '$("#' . $id . '").next().raty(' . Json::encode($this->clientOptions) . ');' . PHP_EOL;

        RatyAsset::register($view);
        $view->registerJs($js);
    }
}