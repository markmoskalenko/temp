<?php
namespace app\widgets;

use Browser;
use yii\base\Widget;

class Reformal extends Widget
{
    public function run()
    {
        $browser = new Browser();

        if ($browser->isMobile() || $browser->isTablet()) {
            return false;
        }

        echo $this->renderFile('@app/views/widgets/reformal.php', ['widget' => $this]);
    }
}