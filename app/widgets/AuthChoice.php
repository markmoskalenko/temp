<?php
namespace app\widgets;

use app\helpers\Url;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AuthChoice extends \yii\authclient\widgets\AuthChoice
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $listOptions = [];
    /**
     * @var array
     */
    public $itemOptions = [];
    /**
     * @var array
     */
    public $popupMode = false;
    /**
     * @var string
     */
    public $prefix;
    /**
     * @var mixed
     */
    public $redirect;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->baseAuthUrl = ['auth/social'];

        if ($this->redirect) {
            $this->redirect = Url::to($this->redirect);
        } else {
            $this->redirect = Url::current();
        }

        Html::addCssClass($this->options, 'auth-clients');
        Html::addCssClass($this->listOptions, 'auth-clients-list');
        Html::addCssClass($this->itemOptions, 'auth-clients-item');

        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function renderMainContent()
    {
        echo Html::beginTag('div', $this->listOptions);

        foreach ($this->getClients() as $externalService) {
            echo Html::beginTag('div', $this->itemOptions);
            $this->clientLink($externalService);
            echo Html::endTag('div');
        }

        echo Html::endTag('div');
    }

    /**
     * @inheritdoc
     */
    public function clientLink($client, $text = null, array $htmlOptions = [])
    {
        if ($text === null) {
            $text = FA::icon($client->getName(), ['class' => 'auth-icon'])->fixed_width();
            $text .= Html::tag('span', $this->prefix . $client->getTitle(), ['class' => 'auth-title']);
        }

        if (!array_key_exists('class', $htmlOptions)) {
            $htmlOptions['class'] = 'btn btn-' . $client->getName();
        }

        parent::clientLink($client, $text, $htmlOptions);
    }

    /**
     * @inheritdoc
     */
    public function createClientUrl($provider)
    {
        $this->autoRender = false;
        $url = $this->getBaseAuthUrl();
        $url[$this->clientIdGetParamName] = $provider->getId();

        if ($this->redirect) {
            $url['redirect'] = $this->redirect;
        }

        return Url::to($url);
    }
}