<?php
namespace app\widgets;

use app\db\records\Seller;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerPjax extends Widget
{
    /**
     * @var Seller
     */
    public $seller;
    /**
     * @var array
     */
    public $clientOptions = [];


    /**
     * @inheritdoc
     */
    public function init()
    {
        if (empty($this->seller)) {
            throw new InvalidConfigException('Seller must be set.');
        }

        Pjax::begin([
            'id' => 'seller-pjax',
            'linkSelector' => false,
            'formSelector' => false,
            'timeout' => 5000,
        ]);

        $this->clientOptions['url'] = Url::to(['aliexpress/update-seller', 'id' => $this->seller->id]);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        Pjax::end();

        if ($this->isSellerOutdated()) {
            $this->registerClientScripts();
        }
    }

    /**
     * @return boolean
     */
    public function isSellerOutdated()
    {
        return strtotime($this->seller->timeUpdated) < strtotime('4 hours ago');
    }

    /**
     * Registers client scripts.
     */
    public function registerClientScripts()
    {
        $this->view->registerJsFile('@web/js/frontend-seller.js', [
            'depends' => ['yii\web\JqueryAsset', 'yii\widgets\PjaxAsset'],
        ]);

        $this->view->registerJs('$("#seller-pjax").sellerPage(' . Json::encode($this->clientOptions) . ')');
    }
}