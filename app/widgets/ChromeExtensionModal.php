<?php
namespace app\widgets;

use Browser;
use yii\base\Widget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ChromeExtensionModal extends Widget
{
    public function run()
    {
        $browser = new Browser();

        if ($browser->getBrowser() !== Browser::BROWSER_CHROME) {
            return false;
        }

        if ($browser->isMobile() || $browser->isTablet()) {
            return false;
        }

        $this->view->registerJsFile('@web/js/frontend-chrome-extension-modal.js', [
            'depends' => [
                'app\assets\AppAsset',
                'app\assets\JqueryCookieAsset',
            ],
        ]);

        echo $this->renderFile('@app/views/modals/chrome_extension.php', ['widget' => $this]);
    }
}