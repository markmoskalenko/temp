<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class VkGroup extends Widget
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $clientOptions = [
        'mode' => 0,
        'width' => 'auto',
        'height' => 400,
        'color1' => 'FFFFFF',
        'color2' => '2B587A',
        'color3' => '5B7FA6'
    ];
    /**
     * @var integer
     */
    public $groupId = '74802611';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->options['id'] = $this->getId();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::tag('div', '', $this->options);

        $this->view->registerJsFile('//vk.com/js/api/openapi.js?116');

        $js = 'VK.Widgets.Group("'.$this->getId().'", '.json_encode($this->clientOptions).', '.$this->groupId.');';
        $this->view->registerJs($js);
    }
}