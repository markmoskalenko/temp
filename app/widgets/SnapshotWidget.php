<?php
namespace app\widgets;

use app\db\records\Snapshot;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotWidget extends Widget
{
    /**
     * @var Snapshot
     */
    public $snapshot;


    /**
     * @return string
     */
    public function run()
    {
        if ($this->snapshot) {
            echo $this->render('@app/views/widgets/snapshot', ['snapshot' => $this->snapshot]);
        }
    }
}