<?php
namespace app\widgets;

use app\db\records\Comment;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Comments extends Widget
{
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $template = '@app/views/comments/templates';
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $clientOptions = [];


    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->url === null) {
            throw new InvalidConfigException('The "url" property must be set.');
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        $this->clientOptions['url'] = $this->url;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::tag('div', '', $this->options);

        $this->registerClientScript();
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $this->view->registerJsFile('@web/js/frontend-comments.js', [
            'depends' => [
                'app\assets\AppAsset',
            ],
        ]);

        $this->view->on(View::EVENT_END_BODY, function () {
            echo $this->render($this->template);
        });

        $id = $this->options['id'];
        $js = '$("#' . $id . '").comments(' . Json::encode($this->clientOptions) . ');' . PHP_EOL;

        $this->view->registerJs($js);
    }
}