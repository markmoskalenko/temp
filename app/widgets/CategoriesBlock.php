<?php
namespace app\widgets;

use app\db\records\ArticleCategory;
use Yii;
use yii\helpers\StringHelper;
use yii\widgets\Menu;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoriesBlock extends Menu
{
    /**
     * @var array
     */
    public $options = ['class' => 'list-group'];
    /**
     * @var array
     */
    public $itemOptions = ['class' => 'list-group-item'];


    /**
     * @inheritdoc
     */
    public function init()
    {
        /* @var $categories ArticleCategory[] */
        $categories = ArticleCategory::find()->all();

        foreach ($categories as $category) {
            $this->items[] = [
                'label' => $category->name,
                'url' => $category->url,
            ];
        }
    }

    /**
     * @param array $item
     * @return boolean
     */
    protected function isItemActive($item)
    {
        return false; // StringHelper::startsWith(Yii::$app->request->url, $item['url']);
    }
}