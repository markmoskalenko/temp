<?php
namespace app\widgets;

use app\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastRatingGridView extends GridView
{
    /**
     * @var boolean
     */
    public $showPrize = false;
    /**
     * @var string
     */
    public $layout = '{items}';
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'b-rating-table table table-hover'];


    /**
     * @inheritdoc
     */
    public function init()
    {
        $prizes = [50, 50, 30, 10, 10, 10, 5, 5, 5, 5];

        $this->columns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'Место',
                'headerOptions' => ['class' => 'b-rating-table__serial-header'],
                'contentOptions' => ['class' => 'b-rating-table__serial-column'],
            ],
            [
                'headerOptions' => ['class' => 'b-rating-table__avatar-header'],
                'contentOptions' => ['class' => 'b-rating-table__avatar-column'],
                'format' => 'raw',
                'value' => function ($model) {
                    $avatar = Url::avatar($model['avatarUrl'], ['w' => 50, 'h' => 50, 'fit' => 'crop']);
                    return Html::a(Html::img($avatar, ['style'=>'width:50px']), ['profile/view', 'id' => $model['id']]);
                },
            ],
            [
                'headerOptions' => ['class' => 'b-rating-table__username-header'],
                'contentOptions' => ['class' => 'b-rating-table__username-column'],
                'attribute' => 'name',
                'header' => 'Пользователь',
                'format' => 'raw',
                'value' => function ($model, $index) use ($prizes) {
                    $username = Html::a($model['name'], ['profile/view', 'id' => $model['id']], ['class' => 'b-rating-table__username-link']);
                    return $username;
                }
            ],
            [
                'header' => 'Опубликовано хвастов',
                'format' => 'raw',
                'headerOptions' => ['class' => 'b-rating-table__counter-header'],
                'contentOptions' => ['class' => 'b-rating-table__counter-column'],
                'value' => function ($model) {
                    $total = Html::tag('span', intval($model['boasts_total']), ['class' => 'b-rating-table__counter-lg']);
                    return $total;
                },
            ],
            [
                'header' => 'Получено лайков',
                'format' => 'raw',
                'headerOptions' => ['class' => 'b-rating-table__counter-header'],
                'contentOptions' => ['class' => 'b-rating-table__counter-column'],
                'value' => function ($model) {
                    $total = Html::tag('span', intval($model['likes_total']), ['class' => 'b-rating-table__counter-lg']);
                    return $total;
                }
            ],
        ];

        $this->rowOptions = function ($model, $index) use ($prizes) {
            $options = ['class' => 'b-rating-table__row'];

            if ($index < count($prizes)) {
                Html::addCssClass($options, 'warning');
            }

            return $options;
        };

        parent::init();
    }
}