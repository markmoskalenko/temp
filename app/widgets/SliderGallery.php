<?php
namespace app\widgets;

use app\helpers\Url;
use app\interfaces\UploadableModel;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SliderGallery extends Widget
{
    /**
     * @var UploadableModel[]
     */
    public $images = [];
    /**
     * @var integer
     */
    public $imageHeight = 200;
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $clientOptions = [];


    public function init()
    {
        if ( ! isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    public function run()
    {
        if ( ! count($this->images)) {
            return;
        }

        $this->renderImages();
        $this->registerClientScripts();
    }

    public function renderImages()
    {
        Html::addCssClass($this->options, 'cs-hidden');

        echo Html::beginTag('div', $this->options);

        foreach ($this->images as $image) {
            $width = round($this->imageHeight / $image->getHeight() * $image->getWidth());
            echo Html::beginTag('div', ['class' => 'b-slider-gallery__images']);
            echo Html::img(Url::uploaded($image), ['width' => $width, 'height' => $this->imageHeight, 'rel' => 'gallery-'.$this->id]);
            echo Html::endTag('div');
        }

        echo Html::endTag('div');
    }

    public function registerClientScripts()
    {
        $this->view->registerJsFile('@web/js/frontend-slider-gallery.js', [
            'depends' => ['app\assets\LightSliderAsset', 'app\assets\FancyboxAsset', 'app\assets\UnderscoreAsset'],
        ]);

        $js = '$("#' . $this->options['id'] . '").sliderGallery(' . Json::encode($this->clientOptions) . ')';
        $this->view->registerJs($js);
    }
}