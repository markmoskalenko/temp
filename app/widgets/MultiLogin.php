<?php
namespace app\widgets;

use app\db\records\User;
use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MultiLogin extends Widget
{
    /**
     * @var boolean
     */
    public $enable = false;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if (Yii::$app->session->get('adminVerificationKey') === 'ZxAXydhuCtNXVj7zk6lSqHjpxNpfxKg') {
            $this->enable = true;
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ( ! $this->enable) {
            return;
        }

        echo ButtonDropdown::widget([
            'label' => '<span class="fa fa-users"></span>',
            'encodeLabel' => false,
            'containerOptions' => ['class' => 'multi-login'],
            'options' => ['class' => 'btn-danger'],
            'dropdown' => [
                'options' => ['class' => 'dropdown-menu dropdown-menu-right'],
                'items' => $this->getItems(),
            ],
        ]);
    }

    public function getItems()
    {
        $items = [];

        foreach (User::findAll(['isFavorited' => 1]) as $user) {
            $items[] = ['label' => $user->name, 'url' => ['/auth/manual', 'userId' => $user->id]];
        }

        $items[] = Html::tag('li', '', ['class' => 'divider']);
        $items[] = ['label' => 'Вернуться к админу', 'url' => ['/auth/manual', 'userId' => Yii::$app->session->get('adminUserId')]];

        return $items;
    }
}
