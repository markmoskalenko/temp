<?php
namespace app\widgets;

use yii\base\Widget;

class LatestNews extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->view->registerAssetBundle('app\assets\LatestNewsAsset');
        echo $this->renderFile('@app/views/widgets/latest-news.php');
    }
}