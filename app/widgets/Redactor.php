<?php
namespace app\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Redactor extends InputWidget
{
    /**
     * @var string
     */
    public $assetBundle = 'app\assets\RedactorAsset';
    /**
     * @var string
     */
    public $viewFile = '/widgets/redactor';


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->registerClientScripts();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->view->render($this->viewFile, [
            'input' => $this->renderTextarea(),
            'widget' => $this,
        ]);
    }

    /**
     * @param array $options
     * @return string
     */
    public function renderTextarea($options = [])
    {
        $options = ArrayHelper::merge($this->options, $options);

        if ($this->hasModel()) {
            return Html::activeTextarea($this->model, $this->attribute, $options);
        } else {
            return Html::textarea($this->name, $this->value, $options);
        }
    }

    /**
     * Registers client scripts.
     */
    public function registerClientScripts()
    {
        if ($this->assetBundle) {
            $this->view->registerAssetBundle($this->assetBundle);
        }
    }
}