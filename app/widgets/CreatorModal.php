<?php
namespace app\widgets;

use Browser;
use yii\base\Widget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CreatorModal extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->renderFile('@app/views/modals/creator.php');
    }
}