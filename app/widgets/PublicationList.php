<?php
namespace app\widgets;

use yii\widgets\ListView;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PublicationList extends ListView
{
    /**
     * @inheritdoc
     */
    public $layout = '<div class="b-publication-list">{items}</div> {pager}';
    /**
     * @inheritdoc
     */
    public $itemOptions = ['class' => 'b-publication-list__item'];


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $js = <<<JS
            $('.b-publication-list__item').each(function () {
                var url = $(this).find('.b-publication__title').attr('href');
                $(this).find('.b-publication__content img').wrap('<a href="' + url + '">');
            });
JS;

        $this->view->registerJs($js);
    }
}