<?php
namespace app\widgets;

use yii\base\Widget;

class LoginModal extends Widget
{
    public function run()
    {
        echo $this->renderFile('@app/views/modals/login.php', ['widget' => $this]);
    }
}