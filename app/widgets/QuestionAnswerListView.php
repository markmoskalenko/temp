<?php
namespace app\widgets;

use yii\widgets\ListView;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class QuestionAnswerListView extends ListView
{
    public $layout = "<div class=\"b-qa-list\">{items}</div>\n{pager}";
    public $itemOptions = ['class' => 'b-question-answer-list__item'];
    public $itemView = '@app/views/question-answer/_item.php';


    public function init()
    {
        parent::init();
    }
}