<?php
namespace app\widgets;

use app\db\records\Category;
use app\repositories\CategoryRepository;
use app\rest\CategorySelectizeFormatter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Categorize extends InputWidget
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $clientOptions = [];


    /**
     * @inheritdoc
     */
    public function run()
    {
        $formatter = new CategorySelectizeFormatter();
        $repository = new CategoryRepository();

        $categories = $repository->getAll();

        $this->clientOptions['options'] = ArrayHelper::toArray($categories, [
            Category::className() => $formatter->fields(),
        ]);

        $items = ArrayHelper::map($categories, 'id', function (Category $category) {
            return $category->present()->namesPath();
        });

        if ($this->hasModel()) {
            echo Html::activeDropDownList($this->model, $this->attribute, $items, $this->options);
        } else {
            echo Html::dropDownList($this->name, $this->value, $items, $this->options);
        }

        $this->registerClientScript();
    }

    public function registerClientScript()
    {
        $this->view->registerJsFile('@web/js/frontend-categories-selectize.js', [
            'depends' => [
                'app\assets\SelectizePluginAsset',
                'yii\web\JqueryAsset',
            ],
        ]);

        $js = '$("#' . $this->options['id'] . '").categorize(' . Json::encode($this->clientOptions) .')';
        $this->view->registerJs($js);
    }
}