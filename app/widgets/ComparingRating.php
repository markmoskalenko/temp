<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComparingRating extends Widget
{
    /**
     * @var float
     */
    public $value;


    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->value > 0) {
            echo Html::tag('strong', sprintf('На %s%% выше', $this->value));
            echo PHP_EOL;
            echo Html::tag('span', 'Чем у остальных продавцов');
        } elseif ($this->value < 0) {
            echo Html::tag('strong', sprintf('На %s%% ниже', abs($this->value)), ['class' => 'text-danger']);
            echo PHP_EOL;
            echo Html::tag('span', 'Чем у остальных продавцов');
        } else {
            echo Html::tag('strong', 'Точно такой же');
            echo PHP_EOL;
            echo Html::tag('span', 'Как у остальных продавцов');
        }
    }
}