<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotField extends InputWidget
{
    /**
     * @var array
     */
    public $clientOptions = [];
    /**
     * @var string
     */
    public $loaderText = 'подождите, проверяем ссылку';
    /**
     * @var string
     */
    public $helpUrl = 'http://alitrust.ru/articles/znakomstvo-s-aliexpress/snapshot-aliekspress-poleznyi-instrument-pokupatelya';


    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::a('Что это такое?', $this->helpUrl, ['class' => 'control-label-help', 'target' => '_blank']);

        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }

        $this->renderLoader();

        $this->registerClientScript();
    }

    /**
     * Рендрим блок, который будет отображаться при загрузке данных о снэпшоте.
     */
    public function renderLoader()
    {
        echo Html::beginTag('span', ['class' => 'form-control-feedback form-control-loader hidden']);
        echo Html::tag('span', '', ['class' => 'fa fa-spin fa-spinner']) . PHP_EOL;
        echo Html::tag('span', $this->loaderText);
        echo Html::endTag('span');
    }

    /**
     * Registers the needed client script and options.
     */
    public function registerClientScript()
    {
        $view = $this->getView();

        $view->registerJsFile('@web/js/frontend-snapshot-field.js', [
            'depends' => [
                'app\assets\AppAsset',
            ],
        ], __CLASS__);

        $id = $this->options['id'];
        $js = '$("#' . $id . '").snapshotField(' . Json::encode($this->clientOptions) . ');' . PHP_EOL;

        $view->registerJs($js);
    }
}