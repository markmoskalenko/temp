<?php
namespace app\widgets;

use app\assets\LikeButtonAsset;
use app\repositories\VoteRepository;
use Yii;
use yii\base\Widget;
use yii\bootstrap\Button;
use yii\bootstrap\ButtonGroup;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LikeButton extends Widget
{
    /**
     * @var object likable entity.
     */
    public $entity;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $label = 'понравилось';

    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->votes = new VoteRepository();

        LikeButtonAsset::register($this->view);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $data = [
            'isVoted' => (int) $this->votes->exists(Yii::$app->user->id, $this->entity),
            'votesCount' => $this->votes->count($this->entity),
            'url' => $this->url,
        ];

        $action = Button::widget([
            'encodeLabel' => false,
            'label' => '<span class="fa fa-thumbs-up"></span> ' . $this->label,
            'options' => ['class' => 'btn-action ' . ($data['isVoted'] ? 'btn-primary' : 'btn-default')],
        ]);

        $counter = [
            'label' => $data['votesCount'],
            'options' => ['class' => 'btn-default btn-counter'],
        ];

        echo ButtonGroup::widget([
            'encodeLabels' => false,
            'options' => ['class' => 'btn-group-xs', 'id' => $this->id, 'data' => $data],
            'buttons' => [$action, $counter],
        ]);

        $this->view->registerJs('$("#' . $this->id . '").likable()');
    }
}