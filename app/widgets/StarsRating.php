<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class StarsRating
 * @author Artem Belov <razor2909@gmail.com>
 */
class StarsRating extends Widget
{
    /**
     * @var array
     */
    public $options = ['class' => 'rating'];
    /**
     * @var string
     */
    public $emptyStarClass = 'fa fa-star-o';
    /**
     * @var string
     */
    public $halfStarClass = 'fa fa-star-half-o';
    /**
     * @var string
     */
    public $fullStarClass = 'fa fa-star';
    /**
     * @var float
     */
    public $value = 0;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        echo Html::beginTag($tag, $options);
        $this->drawStars();
        echo Html::endTag($tag);
    }

    public function drawStars()
    {
        for ($i = 0; $i < 5; $i++) {
            if ($this->value <= $i) {
                $this->drawStar($this->emptyStarClass);
            } elseif ($this->value >= $i + 1) {
                $this->drawStar($this->fullStarClass);
            } else {
                $this->drawStar($this->halfStarClass);
            }
        }
    }

    /**
     * @param string $class
     */
    public function drawStar($class)
    {
        echo Html::tag('span', '', ['class' => $class]);
    }
}