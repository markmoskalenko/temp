<?php
namespace app\widgets;

use yii\widgets\ListView;

class NotificationsListView extends ListView
{
    /**
     * @inheritdoc
     */
    public $layout = '<div>{items}</div> {pager}';
    /**
     * @var string
     */
    public $itemView = '@app/views/notifications/_item.php';
    /**
     * @inheritdoc
     */
}