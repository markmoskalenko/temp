<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\LinkPager;

/**
 * Class TopicLinkPager
 * @package app\widgets
 */
class TopicLinkPager extends LinkPager
{
    public $options = ['class' => 'forum-theme-pagination'];

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];

        if ($pageCount <= 4) {
            for ($i = 0; $i <= $pageCount-1; ++$i) {
                $buttons[] = $this->renderPageButton('['.($i + 1).']', $i, null, false, false);
            }
        }else{
            $buttons[] = $this->renderPageButton('[1]', 1, null, false, false);
            $buttons[] = '<li> ... </li>';

            for ($i = $pageCount - 3; $i <= $pageCount - 1; ++$i) {
                $buttons[] = $this->renderPageButton('['.($i + 1).']', $i, null, false, false);
            }
        }

        return Html::tag('ul', implode("\n", $buttons), $this->options);
    }
}