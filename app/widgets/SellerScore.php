<?php
namespace app\widgets;

use app\db\records\Seller;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class SellerScore
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerScore extends Widget
{
    /**
     * @var Seller
     */
    public $seller;
    
    /**
     * @inheritdoc
     */
    public function run()
    {
        if ( ! ($this->seller instanceof Seller)) {
            throw new InvalidConfigException('Seller property is required');
        }

        echo Html::tag('span', Yii::$app->formatter->asInteger($this->seller->score), ['class' => 'score-value']);
        echo PHP_EOL;
        echo Html::img('/images/icons/' . Html::encode($this->seller->scoreIcon), ['class' => 'score-icon']);
    }
}