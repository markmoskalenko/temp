<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImagesUploader extends InputWidget
{
    /**
     * @var string
     */
    public $template = '@app/views/widgets/images-uploader.php';
    /**
     * URL, который будет обрабатывать загрузку картинок
     *
     * @var string
     */
    public $uploadUrl;
    /**
     * Функция, генерирующая URL картинки, используя её ID.
     *
     * @var callable
     */
    public $imageUrl;
    /**
     * Опции angular директивы.
     *
     * @var array
     */
    public $options = ['template' => '#images-uploader-template'];


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->options['upload-url'] = $this->uploadUrl;
        $this->options['images'] = $this->getImages();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::hiddenInput($this->getInputName());

        echo Html::tag('images-uploader', '', $this->options);

        echo $this->render($this->template, [
            'inputName' => $this->getInputName(),
        ]);

        $this->registerClientScripts();
    }

    /**
     * Registers client scripts.
     */
    public function registerClientScripts()
    {
        $this->view->registerAssetBundle('app\assets\ImagesUploaderAsset');
    }

    /**
     * @return array
     */
    public function getImages()
    {
        $imageKeys = $this->getInputValue();
        $images = [];

        if (is_array($imageKeys)) {
            foreach ($imageKeys as $imageId) {
                $url = call_user_func($this->imageUrl, $imageId);
                $images[] = ['id' => $imageId, 'url' => $url];
            }
        }

        return $images;
    }

    /**
     * @return string
     */
    public function getInputName()
    {
        if ($this->hasModel()) {
            return Html::getInputName($this->model, $this->attribute);
        } else {
            return $this->name;
        }
    }

    /**
     * @return mixed
     */
    public function getInputValue()
    {
        if ($this->hasModel()) {
            return $this->model->{$this->attribute};
        } else {
            return $this->value;
        }
    }
}
