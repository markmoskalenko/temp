<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastListView extends ListView
{
    /**
     * @var string
     */
    public $layout = "{items}\n{pager}";
    /**
     * @var array
     */
    public $options = ['class' => 'b-boast-list-view'];
    /**
     * @var array
     */
    public $itemOptions = ['class' => 'b-boast-list__item'];
    /**
     * @var string
     */
    public $itemView = '@app/views/boasts/_item.php';
    /**
     * @var array
     */
    public $pager = [
        'options' => ['class' => 'b-boast-list-pagination hidden'],
    ];


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->view->registerAssetBundle('app\assets\BoastListAsset');
    }

    /**
     * @inheritdoc
     */
    public function renderItems()
    {
        $items = Html::beginTag('div', ['class' => 'b-boast-list-view__items b-boast-list']);
        $items .= parent::renderItems();
        $items .= Html::endTag('div');

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function renderPager()
    {
        if ($pager = parent::renderPager()) {
            Html::addCssClass($this->options, 'b-boast-list-view_pagination_enabled');

            $this->view->registerJsFile('@web/js/frontend-boast-pagination.js', [
                'depends' => ['app\assets\BoastListAsset'],
            ]);
        }

        return $pager;
    }
}