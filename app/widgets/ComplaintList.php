<?php
namespace app\widgets;

use app\assets\ComplaintAsset;
use app\assets\ComplaintPhotosAsset;
use app\db\records\Complaint;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintList extends ListView
{
    /**
     * @inheritdoc
     */
    public $itemOptions = ['class' => 'b-complaint b-complaint_style_bordered'];
    /**
     * @var string
     */
    public $itemClassRejected = 'b-complaint_state_rejected';
    /**
     * @inheritdoc
     */
    public $itemView = '/complaints/_item';
    /**
     * @inheritdoc
     */
    public $layout = '{items} {pager}';
    /**
     * @var boolean
     * Выводит информацию о продавце рядом с отзывом.
     */
    public $showSellerInfo = false;
    /**
     * @var boolean
     * Выводит автора отзыва.
     */
    public $showAuthor = true;
    /**
     * @var boolean
     * Генерирует кнопку лайк.
     */
    public $showLikeButton = true;
    /**
     * @var boolean
     * Выводит авторам отзыва панель для редактирования.
     */
    public $showOwnerControls = false;
    /**
     * @inheritdoc
     */
    public $sorter = false;


    /**
     * @inheritdoc
     */
    public function init()
    {
        ComplaintAsset::register($this->view);
    }

    /**
     * Renders a single data model.
     *
     * @param Complaint  $model
     * @param mixed      $key
     * @param integer    $index
     * @return string
     */
    public function renderItem($model, $key, $index)
    {
        $content = $this->getView()->render($this->itemView, array_merge([
            'model' => $model,
            'key' => $key,
            'index' => $index,
            'widget' => $this,
        ], $this->viewParams));

        $options = $this->itemOptions;
        $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string) $key;
        if ($model->rejected) {
            Html::addCssClass($options, $this->itemClassRejected);
        }
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        return Html::tag($tag, $content, $options);
    }
}