<?php
namespace app\widgets;

use rmrevin\yii\fontawesome\FA;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class HelpIcon
 * @author Artem Belov <razor2909@gmail.com>
 */
class HelpIcon extends Widget
{
    /**
     * @var string
     */
    public $text;
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $iconOptions = [];
    /**
     * @var array
     */
    public $bgOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        Html::addCssClass($this->options, 'help-icon');

        $this->options['data-toggle'] = 'popover';
        $this->options['data-content'] = $this->text;
        $this->options['data-placement'] = 'top';

        return FA::stack($this->options)
            ->icon('question', $this->iconOptions)
            ->on('circle', $this->bgOptions);
    }
}