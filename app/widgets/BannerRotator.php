<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BannerRotator extends Widget
{
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var array
     */
    public $imageOptions = [];
    /**
     * @var array
     */
    public $items = [
        ['src' => '/images/banner-boast.jpg', 'url' => ['/boasts/index']],
        ['src' => '/images/banner-complaint.jpg', 'url' => ['/complaints/index']],
        ['src' => '/images/banner-forum.jpg', 'url' => '/forum'],
    ];


    /**
     * @inheritdoc
     */
    public function run()
    {
        $index = mt_rand(0, count($this->items) - 1);
        $item = $this->items[ $index ];

        $image = Html::img($item['src'], $this->imageOptions);
        $link = Html::a($image, $item['url'], $this->options);

        echo $link;
    }
}