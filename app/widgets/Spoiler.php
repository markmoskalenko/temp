<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Spoiler extends Widget
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $body;


    /**
     * Returns the ID of the widget.
     * @param boolean $autoGenerate whether to generate an ID if it is not set previously
     * @return string ID of the widget.
     */
    public function getId($autoGenerate = true)
    {
        if ($autoGenerate && parent::getId(false) === null) {
            $this->setId(uniqid(static::$autoIdPrefix));
        }

        return parent::getId(false);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo Html::beginTag('div', ['class' => 'b-publication-spoiler']);

        echo Html::beginTag('div', ['class' => 'b-publication-spoiler__header collapsed', 'data-toggle' => 'collapse', 'data-target' => '#'.$this->id]);
        echo Html::tag('div', $this->title, ['class' => 'b-publication-spoiler__title']);
        echo Html::tag('span', '', ['class' => 'b-publication-spoiler__header-icon fa fa-plus']);
        echo Html::endTag('div');

        echo Html::beginTag('div', ['class' => 'b-publication-spoiler__body collapse', 'id' => $this->id]);
        echo Html::tag('div', $this->body, ['class' => 'b-publication-spoiler__content']);
        echo Html::endTag('div');

        echo Html::endTag('div');
    }
}
