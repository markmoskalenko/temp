<?php
namespace app\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MainMenuNav extends Menu
{
    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        $options = ArrayHelper::remove($item, 'linkOptions', []);

        Html::addCssClass($options, 'main-menu-link');

        if ($item['active']) {
            Html::addCssClass($options, 'main-menu-link--active');
        }

        return Html::a($item['label'], $item['url'], $options);
    }
}