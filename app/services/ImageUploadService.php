<?php
namespace app\services;

use Imagine\Image\Box;
use Yii;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImageUploadService
{
    /**
     * @param string $source
     * @param string $destination
     */
    public function save($source, $destination)
    {
        FileHelper::createDirectory(dirname($destination));

        if ($this->isImageGif($source)) {
            copy($source, $destination);
        }

        Image::getImagine()
            ->open($source)
            ->thumbnail(new Box(1000, 1000))
            ->save($destination);
    }

    /**
     * @param string $filename
     * @return boolean
     */
    protected function isImageGif($filename)
    {
        return FileHelper::getMimeType($filename) === 'image/gif';
    }
}