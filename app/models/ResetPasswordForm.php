<?php
namespace app\models;

use app\db\records\User;
use yii\base\InvalidParamException;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ResetPasswordForm extends UpdatePasswordForm
{
    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array  $config
     *
     * @throws InvalidParamException
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || ! is_string($token)) {
            throw new InvalidParamException(Yii::t('app/alerts', 'password-reset-token-wrong'));
        }

        /* @var User $user */
        $user = User::findOne(['passwordResetToken' => $token]);

        if ( ! $user) {
            throw new InvalidParamException(Yii::t('app/alerts', 'password-reset-token-wrong'));
        }

        if (User::isPasswordTokenExpired($token)) {
            throw new InvalidParamException(Yii::t('app/alerts', 'password-reset-token-expired'));
        }

        parent::__construct($user, $config);
    }

    /**
     * Resets password.
     *
     * @param boolean $runValidation
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        $user->setEmailConfirmed();
        $user->saveOrFail();

        return $user->save();
    }
}