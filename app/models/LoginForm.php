<?php
namespace app\models;

use app\db\records\User;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LoginForm extends Model
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;


    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'required', 'message' => Yii::t('app/validation', 'user.email-required')],
            ['email', 'email', 'message' => Yii::t('app/validation', 'user.email-incorrect-format')],

            ['password', 'string'],
            ['password', 'trim'],
            ['password', 'required', 'message' => Yii::t('app/validation', 'user.password-required')],
            ['password', 'validatePassword', 'params' => ['message' => Yii::t('app/validation', 'login-form.login-failed')]],
        ];
    }

    /**
     * Returns the attribute labels.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/models', 'user.email'),
            'password' => Yii::t('app/models', 'user.password'),
        ];
    }

    /**
     * @param string $attribute
     * @param array  $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, $params['message']);
            }
        }
    }

    /**
     * @return boolean
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        } else {
            return false;
        }
    }

    /**
     * @var User|false|null
     */
    private $_user = false;

    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne(['email' => $this->email]);
        }

        return $this->_user;
    }
}
