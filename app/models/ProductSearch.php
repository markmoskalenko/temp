<?php
namespace app\models;

use aliexpress\api\DataProvider;
use yii\base\Model;

/**
 * @property boolean $isEmpty
 * @property array   $categories
 * @property array   $orders
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductSearch extends Model
{
    /**
     * @var string
     */
    public $searchQuery;
    /**
     * @var string
     */
    public $translatedSearchQuery;
    /**
     * @var integer
     */
    public $categoryId;
    /**
     * @var float
     */
    public $priceFrom;
    /**
     * @var float
     */
    public $priceTo;
    /**
     * @var string
     */
    public $orderBy;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['searchQuery', 'string'],
            ['searchQuery', 'trim'],

            ['categoryId', 'string'],
            ['categoryId', 'trim'],

            [['priceFrom', 'priceTo'], 'filter', 'filter' => [$this, 'filterPrice']],

            ['orderBy', 'string'],
            ['orderBy', 'trim'],
        ];
    }

    /**
     * @return boolean
     */
    public function getIsEmpty()
    {
        return empty($this->searchQuery);
    }

    /**
     * @param mixed $price
     * @return float
     */
    public function filterPrice($price)
    {
        $price = (float) $price;
        return $price ?: null;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return [
            3 => 'Одежда и аксессуары',
            34 => 'Автомобили и мотоциклы',
            1501 => 'Детские товары',
            66 => 'Красота и здоровье',
            7 => 'Компьютеры и комплектующие',
            13 => 'Всё для дома и ремонта',
            44 => 'Электроника',
            5 => 'Электротехническое оборудование и материалы',
            502 => 'Электронные компоненты и материалы',
            2 => 'Еда',
            1503 => 'Мебель',
            15 => 'Дом и сад',
            6 => 'Домашние принадлежности',
            36 => 'Ювелирные изделия',
            39 => 'Свет и освещение',
            1524 => 'Сумки и чемоданы',
            21 => 'Офисные и школьные принадлежности',
            509 => 'Телефоны и телекоммуникации',
            322 => 'Обувь',
            18 => 'Спорт и развлечения',
            1420 => 'Tools',
            26 => 'Игрушки и хобби',
            1511 => 'Часы',
        ];
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return [
            'sellerRateDown' => 'По рейтингу продавца',
            'orignalPriceDown' => 'Самые дорогие',
            'orignalPriceUp' => 'Самые дешёвые',
            'validTimeDown' => 'Самые новые',
            'validTimeUp' => 'Самые старые',
        ];
    }

    /**
     * @return DataProvider
     */
    public function dataProvider()
    {
        $data = $this->getAttributes();

        if ($data['translatedSearchQuery']) {
            $data['searchQuery'] = $this->translatedSearchQuery;
        }

        unset($data['translatedSearchQuery']);

        return new DataProvider($data);
    }
}