<?php
namespace app\models;

use app\db\records\User;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UpdatePasswordForm extends Model
{
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $passwordRepeat;

    /**
     * @param User  $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'string'],
            ['password', 'trim'],
            ['password', 'string', 'min' => 6, 'tooShort' => Yii::t('app/validation', 'user.password-too-short')],
            ['password', 'required', 'message' => Yii::t('app/validation', 'required')],

            ['passwordRepeat', 'string'],
            ['passwordRepeat', 'trim'],
            ['passwordRepeat', 'string', 'min' => 6, 'tooShort' => Yii::t('app/validation', 'user.password-too-short')],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'message' => Yii::t('app/validation', 'update-password-form.password-repeat-not-match')],
            ['passwordRepeat', 'required', 'message' => Yii::t('app/validation', 'required')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app/models', 'update-password-form.password'),
            'passwordRepeat' => Yii::t('app/models', 'update-password-form.password-repeat'),
        ];
    }

    /**
     * Updates password.
     *
     * @param boolean $runValidation
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->setPassword($this->password);
        $user->saveOrFail();

        return true;
    }

    /**
     * @var User
     */
    private $_user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}
