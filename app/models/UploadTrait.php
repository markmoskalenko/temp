<?php
namespace app\models;

use Imagine\Image\Box;
use Yii;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
trait UploadTrait
{
    /**
     * @param UploadedFile $file
     * @param boolean      $unique
     * @return string
     */
    public function createFileName(UploadedFile $file, $unique = false)
    {
        $hash = $this->createFileHash($file->tempName);

        $subPath = substr($hash, 0, 1) . '/' . substr($hash, 1, 1);
        $baseImageName = substr($hash, 2);

        if ($unique) {
            $baseImageName .= '-' . time();
        }

        return $subPath . '/' . $baseImageName . '.' . $file->extension;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function createUniqueFileName(UploadedFile $file)
    {
        return $this->createFileName($file, true);
    }

    /**
     * @param string $filepath
     * @return string
     */
    public function createFileHash($filepath)
    {
        return md5_file($filepath);
    }

    /**
     * @param UploadedFile $file
     * @param string $destination
     */
    public function saveImage(UploadedFile $file, $destination)
    {
        FileHelper::createDirectory(dirname($destination));

        Image::getImagine()
            ->open($file->tempName)
            ->thumbnail(new Box(1000, 1000))
            ->save($destination);
    }
}