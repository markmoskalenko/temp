<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "followers".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $objectId
 * @property string $timeCreated
 *
 */

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class FollowersForm extends Model
{
    use FillableTrait;

    /**
    * @var integer 
    */
    public $id;
    /**
    * @var integer 
    */
    public $userId;
    /**
    * @var integer 
    */
    public $objectId;
    /**
    * @var string 
    */
    public $timeCreated;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['objectId', 'required'],
            ['objectId', 'integer'],
            ['timeCreated', 'safe'],
            ['userId', 'required'],
            ['userId', 'integer']
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['id','userId','objectId','timeCreated'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'objectId' => 'Object ID',
            'timeCreated' => 'Time Created',
        ];
    }
}
