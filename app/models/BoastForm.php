<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastForm extends Model
{
    use FillableTrait;

    /**
     * @var integer
     */
    public $categoryId;
    /**
     * @var string
     */
    public $externalUrl;
    /**
     * @var integer
     */
    public $storeId;
    /**
     * @var integer
     */
    public $productId;
    /**
     * @var integer
     */
    public $snapshotId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var float
     */
    public $price;
    /**
     * @var integer
     */
    public $score;
    /**
     * @var string
     */
    public $text;
    /**
     * @var array
     */
    public $photos = [];


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'integer'],
            ['categoryId', 'required', 'message' => Yii::t('app/validation', 'required')],

            ['externalUrl', 'string'],
            ['externalUrl', 'trim'],
            ['externalUrl', 'required', 'message' => Yii::t('app/validation', 'required')],
            ['externalUrl', 'url', 'defaultScheme' => 'http', 'message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-url')],
            ['externalUrl', 'app\validators\AliexpressUrlValidator', 'entities' => ['app\parser\entities\SnapshotEntity'], 'message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-format')],
            ['externalUrl', 'parseUrl', 'params' => ['message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-format')]],

            ['title', 'string'],
            ['title', 'app\validators\HeaderFilter'],
            ['title', 'required', 'message' => Yii::t('app/validation', 'required')],

            ['price', 'number'],
            ['price', 'required', 'message' => Yii::t('app/validation', 'required-short')],

            ['score', 'integer'],
            ['score', 'required', 'message' => Yii::t('app/validation', 'required-short')],

            ['text', 'string'],
            ['text', 'trim', 'enableClientValidation' => false],
            ['text', 'required'],

            ['photos', 'each', 'rule' => ['integer']],
            ['photos', 'required', 'enableClientValidation' => false],
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['categoryId', 'externalUrl', 'storeId', 'productId', 'snapshotId', 'title', 'price', 'score', 'text'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'externalUrl' => 'Ссылка на снэпшот',
            'title' => 'Название товара',
            'price' => 'Цена покупки',
            'score' => 'Ваша оценка товару',
            'photos' => 'Фотографии полученного товара',
            'text' => 'Ваши впечатления о полученном товаре',
        ];
    }

    public function attributeHints()
    {
        return [
            'title' => 'Указывайте правильные названия товара изображенного на ваших фотографиях',
            'text'  => 'Старайтесь подробно описать ваш товар, рассказать все его плюсы и минусы. Помните: вас читают тысячи посетителей!'
        ];
    }

    /**
     * Inline validator for AliExpress URL.
     *
     * @param string $attribute
     * @param array  $params
     */
    public function parseUrl($attribute, $params)
    {
        $this->snapshotId = null;
        $this->productId = null;
        $this->storeId = null;

        if ($this->hasErrors()) {
            return;
        }

        $form = new SnapshotForm();
        $form->externalUrl = $this->$attribute;

        if ( ! $form->validate()) {
            $this->addError($attribute, $form->getFirstError('externalUrl'));
            return;
        }

        if ($snapshot = $form->getSnapshot()) {
            $this->snapshotId = $snapshot->snapshotId;
        }

        if ( ! $this->snapshotId) {
            $this->addError($attribute, $params['message']);
        }
    }
}