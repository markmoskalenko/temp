<?php
namespace app\models;

use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintForm extends Model
{
    /**
     * @var integer
     */
    public $sellerId;
    /**
     * @var integer
     */
    public $productId;
    /**
     * @var string
     */
    public $link;
    /**
     * @var string
     */
    public $text;
    /**
     * @var array
     */
    public $photos = [];


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['link', 'string'],
            ['link', 'trim'],
            ['link', 'url', 'defaultScheme' => 'http', 'message' => 'Такая ссылка не поддерживается'],
            ['link', 'required', 'message' => 'Поле обязательно для заполнения'],
            ['link', 'app\validators\AliexpressUrlValidator', 'message' => 'Такая ссылка не поддерживается'],
            ['link', 'parseUrl', 'params' => ['message' => 'Такая ссылка не поддерживается']],

            ['text', 'string'],
            ['text', 'trim', 'enableClientValidation' => false],
            ['text', 'required', 'message' => 'Поле обязательно для заполнения'],

            ['photos', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'link' => 'Ссылка на AliExpress',
            'text' => 'Текст жалобы',
            'photos' => 'Прикрепите фото некачественного товара',
        ];
    }

    /**
     * Inline validator for AliExpress URL.
     *
     * @param string $attribute
     * @param array  $params
     */
    public function parseUrl($attribute, $params)
    {
        $this->productId = null;
        $this->sellerId = null;

        if ($this->hasErrors()) {
            return;
        }

        $form = new AliExpressForm();
        $form->link = $this->$attribute;

        if ( ! $form->validate()) {
            $this->addError($attribute, $form->getFirstError('link'));
            return;
        }

        if ($product = $form->getProduct()) {
            $this->productId = $product->id;
        }

        if ($seller = $form->getSeller()) {
            $this->sellerId = $seller->id;
        }

        if ( ! $this->sellerId) {
            $this->addError($attribute, $params['message']);
        }
    }
}