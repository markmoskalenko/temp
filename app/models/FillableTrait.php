<?php
namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * Трейт для удобной работы с моделями форм и репозиториями.
 *
 * Fillable поля - это поля которые повторяют поля в БД.
 * Методы [[export()]] и [[import()]] позволяют быстро импортировать данных из БД или экспортировать в неё.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
trait FillableTrait
{
    /**
     * @return array
     */
    public function fillable()
    {
        return $this->attributes();
    }

    /**
     * @return array
     */
    public function export()
    {
        return $this->getAttributes($this->fillable());
    }

    /**
     * @param array $data
     */
    public function import($data)
    {
        foreach ($this->fillable() as $attribute) {
            $this->$attribute = ArrayHelper::getValue($data, $attribute);
        }
    }
}