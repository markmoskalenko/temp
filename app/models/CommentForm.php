<?php
namespace app\models;

use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentForm extends Model
{
    /**
     * @var integer
     */
    public $parentId;
    /**
     * @var string
     */
    public $content;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['parentId', 'integer'],
            ['parentId', 'default', 'value' => null],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],
        ];
    }
}