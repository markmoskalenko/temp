<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "tracking_number".
 *
 * @property integer $id
 * @property string $trackingNumber
 * @property string $timeCreated
 */

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberForm extends Model
{
    use FillableTrait;

    /**
    * @var integer 
    */
    public $id;
    /**
    * @var string 
    */
    public $trackingNumber;
    /**
    * @var string 
    */
    public $timeCreated;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['timeCreated', 'safe'],
            ['trackingNumber', 'required'],
            ['trackingNumber', 'string', 'max' => 255]
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['id','trackingNumber','timeCreated'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trackingNumber' => 'Tracking Number',
            'timeCreated' => 'Time Created',
        ];
    }
}
