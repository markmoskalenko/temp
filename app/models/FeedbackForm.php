<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FeedbackForm extends Model
{
    const SCENARIO_GUEST = 'guest';
    const SCENARIO_USER = 'user';

    public $userName;
    public $userEmail;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if (Yii::$app->user->isGuest) {
            $this->scenario = static::SCENARIO_GUEST;
        } else {
            $this->scenario = static::SCENARIO_USER;
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['userName', 'string'],
            ['userName', 'trim'],

            ['userEmail', 'string'],
            ['userEmail', 'trim'],
            ['userEmail', 'required', 'message' => 'Поле обязательно для заполнения.'],
            ['userEmail', 'email', 'message' => 'Мы не сможем ответить вам на такой адрес.'],

            ['subject', 'string'],
            ['subject', 'trim'],
            ['subject', 'required', 'message' => 'Поле обязательно для заполнения.'],
            ['subject', 'in', 'range' => ['errors', 'content', 'tracking', 'extensions', 'moderators', 'other']],

            ['body', 'string'],
            ['body', 'trim'],
            ['body', 'required', 'message' => 'Поле обязательно для заполнения.'],

            ['verifyCode', 'string'],
            ['verifyCode', 'trim'],
            ['verifyCode', 'required', 'on' => static::SCENARIO_GUEST, 'message' => 'Поле обязательно для заполнения.'],
            ['verifyCode', 'captcha', 'on' => static::SCENARIO_GUEST, 'message' => 'Неверный код.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_GUEST => ['userName', 'userEmail', 'subject', 'body', 'verifyCode'],
            static::SCENARIO_USER => ['userName', 'userEmail', 'subject', 'body'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'userName' => 'Ваше имя',
            'userEmail' => 'Ваш e-mail',
            'subject' => 'Тема для письма',
            'body' => 'Сообщение',
            'verifyCode' => 'Код подтверждения',
        ];
    }

    /**
     * @return array
     */
    public function subjectLabels()
    {
        return [
            'errors' => 'Ошибки на сайте',
            'content' => 'Предложить новость',
            'tracking' => 'Трекинг посылок',
            'extensions' => 'Браузерные расширения',
            'moderators' => 'Жалобы на действие модераторов',
            'other' => 'Другое',
        ];
    }
}