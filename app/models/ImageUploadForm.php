<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImageUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            ['file', 'image'],
        ];
    }
}