<?php
namespace app\models;

use app\db\records\Complaint;

/**
 * @property string $link
 * @property string $text
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintCreateForm extends AliExpressForm
{
    /**
     * @var string
     */
    public $text;


    /**
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = ['text', 'string'];
        $rules[] = ['text', 'trim'];
        $rules[] = ['text', 'required', 'message' => 'Вы не написали отзыв, обязательно сделайте это!'];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'link' => 'Ссылка на продавца или товар',
            'text' => 'Текст отзыва',
        ];
    }

    /**
     * @param boolean $runValidation
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $complaint = $this->getComplaint();
        
        if ($seller = $this->getSeller()) {
            $complaint->sellerId = $seller->id;
        }

        if ($product = $this->getProduct()) {
            $complaint->productId = $product->id;
        }

        $complaint->link = $this->getLink();
        $complaint->originalText = $this->text;
        $complaint->text = $this->text;
        $complaint->saveOrFail();

        return true;
    }

    /**
     * @var Complaint
     */
    private $_complaint;

    /**
     * @return Complaint
     */
    public function getComplaint()
    {
        if ($this->_complaint === null) {
            $this->_complaint = new Complaint();
        }
        return $this->_complaint;
    }

}