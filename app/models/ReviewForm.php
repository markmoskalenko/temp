<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewForm extends Model
{
    use FillableTrait;

    /**
     * @var integer
     */
    public $categoryId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $content;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'integer'],
            ['categoryId', 'required', 'message' => Yii::t('app/validation', 'required')],

            ['title', 'string'],
            ['title', 'app\validators\HeaderFilter'],
            ['title', 'required', 'message' => Yii::t('app/validation', 'required')],

            ['content', 'string'],
            ['content', 'trim', 'enableClientValidation' => false],
            ['content', 'required', 'message' => Yii::t('app/validation', 'required')],
            ['content', 'app\validators\CutValidator', 'message' => 'Для длинного текста необходимо вставить тег <cut>.'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'title' => 'Заголовок',
            'content' => 'Содержимое',
        ];
    }
}