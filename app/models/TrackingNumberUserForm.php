<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "tracking_number_user".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $trackingNumberId
 * @property string $timeCreated
 */

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberUserForm extends Model
{
    use FillableTrait;

    /**
    * @var integer 
    */
    public $id;
    /**
    * @var integer 
    */
    public $userId;
    /**
    * @var integer 
    */
    public $trackingNumberId;
    /**
    * @var string 
    */
    public $timeCreated;

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['timeCreated', 'safe'],
            ['trackingNumberId', 'required'],
            ['trackingNumberId', 'integer'],
            ['userId', 'required'],
            ['userId', 'integer']
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['id','userId','trackingNumberId','timeCreated'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'trackingNumberId' => 'Tracking Number ID',
            'timeCreated' => 'Created At',
        ];
    }
}
