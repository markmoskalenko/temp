<?php
namespace app\models;

use app\db\records\Product;
use app\db\records\Seller;
use app\db\records\Snapshot;
use app\parser\entities\StoreEntity;
use app\parser\handlers\BaseHandler;
use app\parser\HttpException;
use app\parser\NotFoundHttpException;
use app\parser\UrlParser;
use Yii;
use yii\base\Model;
use yii\db\IntegrityException;

/**
 * @property string   $link
 * @property boolean  $isLinkParsed
 * @property Seller   $seller
 * @property Product  $product
 * @property Snapshot $snapshot
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class AliExpressForm extends Model
{
    /**
     * @var string
     */
    private $_link;
    /**
     * @var Seller
     */
    private $_seller;
    /**
     * @var Product
     */
    private $_product;
    /**
     * @var Snapshot
     */
    private $_snapshot;
    /**
     * @var boolean
     */
    private $_isLinkParsed = false;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['link', 'string'],
            ['link', 'trim'],
            ['link', 'required', 'message' => Yii::t('app/validation', 'ali-express-form.link-required')],
            ['link', 'url', 'defaultScheme' => 'http', 'message' =>  Yii::t('app/validation', 'ali-express-form.link-incorrect-url')],
            ['link', 'validateLink', 'params' => ['message' =>  Yii::t('app/validation', 'ali-express-form.link-incorrect-format')]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'link' => 'Ссылка на продавца или товар',
        ];
    }

    /**
     * Inline validator for model [[rules()]]
     *
     * @param string $attribute
     * @param array  $params
     */
    public function validateLink($attribute, $params)
    {
        $this->parseLink();

        if ( ! $this->getSeller() || $this->seller->closed) {
            $this->addError($attribute, $params['message']);
        }
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        if ($this->_link === $link) {
            return;
        }

        $this->_link = $link;
        $this->_seller = null;
        $this->_product = null;
        $this->_snapshot = null;
        $this->_isLinkParsed = false;
    }

    /**
     * @return Seller|null
     */
    public function getSeller()
    {
        return $this->_seller;
    }

    /**
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @return Snapshot|null
     */
    public function getSnapshot()
    {
        return $this->_snapshot;
    }

    /**
     * @return boolean
     */
    public function getIsLinkParsed()
    {
        return $this->_isLinkParsed;
    }

    /**
     * Parses link and loads Seller and/or Product db records
     */
    public function parseLink()
    {
        if ($this->_isLinkParsed) {
            return;
        }

        $parser = new UrlParser();
        $entity = $parser->process($this->getLink());

        if ($entity) {
            try {
                $handler = BaseHandler::process($entity);
                $this->_seller = $handler->getSellerRecord();
                $this->_product = $handler->getProductRecord();
                $this->_snapshot = $handler->getSnapshotRecord();
            } catch (NotFoundHttpException $exception) {
                if ($entity instanceof StoreEntity) {
                    $this->addError('link', Yii::t('app/validation', 'ali-express-form.link-store-removed'));
                } else {
                    $this->addError('link', Yii::t('app/validation', 'ali-express-form.link-product-removed'));
                }
            } catch (IntegrityException $exception) { // originalStoreId cannot be null
                $this->addError('link', Yii::t('app/validation', 'ali-express-form.link-store-removed'));
            } catch (HttpException $exception) {
                $this->addError('link', Yii::t('app/validation', 'ali-express-form.link-http-error'));
                Yii::$app->raven->captureException($exception);
            }
        }

        $this->_isLinkParsed = true;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        Yii::info($this->link, get_class($this));
        return parent::beforeValidate();
    }
}