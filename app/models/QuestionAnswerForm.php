<?php
namespace app\models;

use app\db\records\QuestionAnswer;
use Yii;
use yii\base\Model;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class QuestionAnswerForm extends Model
{
    use FillableTrait;

    /**
     * @var integer
     */
    public $qaCategoryId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $question;
    /**
     * @var integer
     */
    public $userId;
    /**
     * @var integer
     */
    public $isMailNotification;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var array
     */
    public $photos = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['qaCategoryId', 'required'],
            ['qaCategoryId', 'integer'],
            ['title', 'required'],
            ['title', 'string', 'max' => 255],
            ['title', 'trim'],
            ['email', 'required', 'when' => function() { return Yii::$app->user->isGuest; }],
            ['email', 'string', 'max' => 255],
            ['email', 'trim'],
            ['email', 'email'],
            ['question', 'required'],
            ['question', 'string'],
            ['question', 'trim'],
            ['userId', 'integer'],
            ['isMailNotification', 'boolean'],
            ['isMailNotification', 'default', 'value'=>1],
            ['status', 'integer'],
            ['status', 'default', 'value' => QuestionAnswer::STATUS_NEW],
            ['status', 'in', 'range' => [QuestionAnswer::STATUS_NEW, QuestionAnswer::STATUS_ANSWERED]],
            ['firstName', 'string', 'max' => 255],
            ['firstName', 'trim'],
            ['photos', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['qaCategoryId', 'userId', 'title', 'email', 'firstName', 'question', 'isMailNotification', 'status'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'qaCategoryId' => 'Категория вопроса',
            'userId' => 'Пользователь',
            'title' => 'Заголовок вопроса',
            'email' => 'Email',
            'firstName' => 'Имя',
            'question' => 'Вопрос',
            'isMailNotification' => 'Сообщать мне об ответах и комментариях на e-mail',
            'status' => 'Статус',
            'timeCreated' => 'Дата создания',
            'timeUpdated' => 'Дата обновления',
            'photos' => 'Фотографии',
        ];
    }
}