<?php
namespace app\models;

use app\db\records\User;
use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SignUpForm extends Model
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'string', 'max' => 200],
            ['email', 'trim'],
            ['email', 'required', 'message' => Yii::t('app/validation', 'user.email-required')],
            ['email', 'email', 'message' => Yii::t('app/validation', 'user.email-incorrect-format')],
            ['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email'],

            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required', 'message' => Yii::t('app/validation', 'user.name-required')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/models', 'user.email'),
            'name' => Yii::t('app/models', 'user.name'),
        ];
    }

    /**
     * Signs user up.
     *
     * @throws Exception
     *
     * @return User
     */
    public function signUp()
    {
        if ( ! $this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->generateAuthKey();
        $user->saveOrFail();

        return true;
    }

    /**
     * @var User
     */
    private $_user;

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = new User();
        }
        return $this->_user;
    }
}
