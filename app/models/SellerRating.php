<?php
namespace app\models;

use app\parser\pages\FeedbackPage;
use Serializable;

/**
 * Class SellerRating.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerRating implements Serializable
{
    const CATEGORY_ITEM_AS_DESCRIBED = 'itemAsDescribed';
    const CATEGORY_COMMUNICATION = 'communication';
    const CATEGORY_SHIPPING_SPEED = 'shippingSpeed';

    const LABEL_VALUE = 'ratingValue';
    const LABEL_REVIEW_COUNT = 'reviewCount';
    const LABEL_COMPARING = 'ratingComparing';

    const HISTORY_POSITIVE = 'historyPositive';
    const HISTORY_NEUTRAL = 'historyNeural';
    const HISTORY_NEGATIVE = 'historyNegative';
    const HISTORY_POSITIVE_RATE = 'historyPositiveRate';

    const PERIOD_MONTH = 1;
    const PERIOD_3_MONTHS = 3;
    const PERIOD_6_MONTHS = 6;
    const PERIOD_12_MONTHS = 12;
    const PERIOD_OVERALL = 0;

    /**
     * @var array
     */
    private $data;

    /**
     * @param string $category
     * @param string $label
     *
     * @return string
     */
    public function get($category, $label)
    {
        return $this->data[ $category ][ $label ];
    }

    /**
     * @param FeedbackPage $feedbackPage
     */
    public function loadFeedbackPage(FeedbackPage $feedbackPage)
    {
        $categories = [
            static::CATEGORY_ITEM_AS_DESCRIBED => FeedbackPage::DETAILED_CATEGORY_ITEM_AS_DESCRIBED,
            static::CATEGORY_COMMUNICATION => FeedbackPage::DETAILED_CATEGORY_COMMUNICATION,
            static::CATEGORY_SHIPPING_SPEED => FeedbackPage::DETAILED_CATEGORY_SHIPPING_SPEED,
        ];

        $labels = [
            static::LABEL_VALUE => FeedbackPage::DETAILED_LABEL_RATING_VALUE,
            static::LABEL_REVIEW_COUNT => FeedbackPage::DETAILED_LABEL_REVIEW_COUNT,
            static::LABEL_COMPARING => FeedbackPage::DETAILED_LABEL_RATING_COMPARING,
        ];

        $history = [
            static::HISTORY_POSITIVE => FeedbackPage::HISTORY_CATEGORY_POSITIVE,
            static::HISTORY_NEUTRAL => FeedbackPage::HISTORY_CATEGORY_NEUTRAL,
            static::HISTORY_NEGATIVE => FeedbackPage::HISTORY_CATEGORY_NEGATIVE,
            static::HISTORY_POSITIVE_RATE => FeedbackPage::HISTORY_CATEGORY_POSITIVE_RATE,
        ];

        $periods = [
            static::PERIOD_MONTH => FeedbackPage::HISTORY_PERIOD_MONTH,
            static::PERIOD_3_MONTHS => FeedbackPage::HISTORY_PERIOD_3_MONTHS,
            static::PERIOD_6_MONTHS => FeedbackPage::HISTORY_PERIOD_6_MONTHS,
            static::PERIOD_12_MONTHS => FeedbackPage::HISTORY_PERIOD_12_MONTHS,
            static::PERIOD_OVERALL => FeedbackPage::HISTORY_PERIOD_OVERALL,
        ];

        foreach ($categories as $to1 => $from1) {
            foreach ($labels as $to2 => $from2) {
                $this->data[ $to1 ][ $to2 ] = $feedbackPage->getDetailed($from1, $from2);
            }
        }

        foreach ($history as $to1 => $from1) {
            foreach ($periods as $to2 => $from2) {
                $this->data[ $to1 ][ $to2 ] = $feedbackPage->getHistory($from1, $from2);
            }
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this->data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $this->data = json_decode($serialized);
    }
}