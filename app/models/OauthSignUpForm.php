<?php
namespace app\models;

use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class OauthSignUpForm extends SignUpForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'default', 'value' => null],

            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'default', 'value' => null],
        ];
    }
}
