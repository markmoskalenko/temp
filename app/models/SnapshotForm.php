<?php
namespace app\models;

use app\db\records\Snapshot;
use app\parser\entities\SnapshotEntity;
use app\parser\handlers\SnapshotHandler;
use app\parser\HttpException;
use app\parser\UrlParser;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotForm extends Model
{
    /**
     * @var string
     */
    public $externalUrl;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['externalUrl', 'string'],
            ['externalUrl', 'trim'],
            ['externalUrl', 'required', 'message' => Yii::t('app/validation', 'ali-express-form.link-required')],
            ['externalUrl', 'url', 'defaultScheme' => 'http', 'message' =>  Yii::t('app/validation', 'ali-express-form.link-incorrect-url')],
            ['externalUrl', 'validateUrl', 'params' => ['message' =>  Yii::t('app/validation', 'ali-express-form.link-incorrect-format')]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'externalUrl' => 'Ссылка на снапшот',
        ];
    }

    /**
     * Inline validator for model [[rules()]]
     *
     * @param string $attribute
     * @param array  $params
     */
    public function validateUrl($attribute, $params)
    {
        if ($this->hasErrors($attribute)) {
            return;
        }

        $parser = new UrlParser();
        $entity = $parser->process($this->externalUrl);

        if ( ! ($entity instanceof SnapshotEntity)) {
            $this->addError($attribute, $params['message']);
            return;
        }

        try {
            $handler = new SnapshotHandler();
            $handler->parseSnapshotEntity($entity);
            $this->_snapshot = $handler->getSnapshotRecord();
        } catch (HttpException $exception) {
            $this->addError('link', Yii::t('app/validation', 'ali-express-form.link-http-error'));
            Yii::$app->raven->captureException($exception);
        }

        if ( ! $this->getSnapshot()) {
            $this->addError($attribute, $params['message']);
        }
    }

    /**
     * @var null|Snapshot
     */
    private $_snapshot;

    /**
     * @return mixed
     */
    public function getSnapshot()
    {
        return $this->_snapshot;
    }
}