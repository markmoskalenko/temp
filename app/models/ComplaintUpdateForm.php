<?php
namespace app\models;

use app\db\records\Complaint;
use app\helpers\Typographer;
use Carbon\Carbon;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintUpdateForm extends Model
{
    /**
     * @var string
     */
    public $text;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['text', 'string'],
            ['text', 'trim'],
            ['text', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст отзыва',
        ];
    }

    /**
     * @param Complaint $review
     * @param boolean $runValidation
     * @return boolean
     */
    public function save(Complaint $review, $runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $review->text = $this->text;
        $review->originalText = $this->text;
        $review->moderated = 0;
        $review->rejected = 0;
        $review->timeUpdated = Carbon::now()->toDateTimeString();
        $review->saveOrFail();

        return true;
    }
}