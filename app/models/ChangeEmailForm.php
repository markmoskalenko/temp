<?php
namespace app\models;

use app\db\records\User;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ChangeEmailForm extends Model
{
    /**
     * @var string
     */
    public $email;


    /**
     * @param User  $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'string', 'max' => 200],
            ['email', 'required', 'message' => Yii::t('app/validation', 'user.email-required')],
            ['email', 'email', 'message' => Yii::t('app/validation', 'user.email-incorrect-format')],
            ['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/models', 'change-email-form.email'),
        ];
    }

    /**
     * @param boolean $runValidation
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->email = $this->email;
        $user->setEmailConfirmed(false);
        $user->saveOrFail();

        return true;
    }

    /**
     * @var User
     */
    private $_user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}