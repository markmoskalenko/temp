<?php
namespace app\models;

use app\db\records\User;
use Yii;
use yii\base\Model;
use yii\helpers\Json;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProfileForm extends Model
{
    /**
     * This scenario enables when user has no email set.
     */
    const SCENARIO_EMAILABLE = 'emailable';

    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $city;
    /**
     * @var array
     */
    public $notifications = [];


    /**
     * @param User  $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->_user  = $user;

        $this->email = $user->email;
        $this->name = $user->name;
        $this->city = $user->city;
        $this->notifications = $user->notifications;

        if ( ! $user->email) {
            $this->scenario = static::SCENARIO_EMAILABLE;
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'string', 'max' => 200],
            ['email', 'trim'],
            ['email', 'email', 'message' => Yii::t('app/validation', 'user.email-incorrect-format')],
            ['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email'],

            ['name', 'string'],
            ['name', 'filter', 'filter' => [$this, 'filterTextInput']],
            ['name', 'required'],

            ['city', 'string'],
            ['city', 'filter', 'filter' => [$this, 'filterTextInput']],

            // todo
            ['notifications', 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/models', 'user.email'),
            'name' => Yii::t('app/models', 'user.name'),
            'city' => Yii::t('app/models', 'user.city'),
            'notifications' => 'Уведомления',
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => ['name', 'city', 'notifications'],
            static::SCENARIO_EMAILABLE => ['name', 'city', 'email', 'notifications'],
        ];
    }

    /**
     * @param string $value
     * @return string
     */
    public function filterTextInput($value)
    {
        $value = preg_replace('#[^a-zа-яё\s-]#ui', '', $value);
        $value = trim($value);

        return $value;
    }

    /**
     * @param boolean $runValidation
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $attributes = $this->getAttributes();
        $attributes['notificationsJson'] = Json::encode($this->notifications);
        if (array_key_exists('email', $attributes) && empty($attributes['email'])) {
            unset($attributes['email']);
        }

        $user = $this->getUser();
        $user->setAttributes($attributes, false);
        $user->saveOrFail();


        return true;
    }

    /**
     * @var User
     */
    private $_user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}
