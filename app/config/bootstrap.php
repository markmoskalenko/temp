<?php
mb_internal_encoding('utf-8');

define('MINUTE', 60);
define('HOUR', 3600);
define('DAY', 86400);
define('WEEK', 604800);
define('MONTH', 2592000);
define('YEAR', 31536000);

define('LANG_RU', 'ru-RU');
define('LANG_ES', 'es-ES');
define('LANG_PT', 'pt-BR');

yii\helpers\Inflector::$transliterator = 'Russian-Latin/BGN;';

Yii::$container->set('app\widgets\Raty', [
    'containerOptions' => ['class' => 'b-raty'],
    'clientOptions' => [
        'hints' => ['ужасно', 'плохо', 'нормально', 'хорошо', 'отлично'],
        'starType' => 'i',
        'starOn' => 'b-raty__item b-raty__item_state_on star-on-png',
        'starOff' => 'b-raty__item b-raty__item_state_off star-off-png'
    ],
]);

Yii::$container->set('yii\behaviors\SluggableBehavior', [
    'immutable' => true,
]);

Yii::$container->set('yii\behaviors\TimestampBehavior', [
    'value' => function () {
        return date('Y-m-d H:i:s');
    },
]);

Yii::$container->set('yii\bootstrap\ActiveForm', [
    'successCssClass' => false,
    'validateOnBlur' => false,
]);

Yii::$container->set('yii\widgets\ActiveForm', [
    'successCssClass' => false,
    'validateOnBlur' => false,
]);
