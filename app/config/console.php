<?php
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'alitrust-console',

    'bootstrap' => ['gii'],
    'controllerNamespace' => 'app\console',

    'aliases' => [
        '@webroot' => dirname(dirname(__DIR__)) . '/web',
    ],

    'modules' => [
        'gii' => 'yii\gii\Module',
    ],

    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/console.log',
                ],
            ],
        ],

        'raven' => [
            'options' => [
                'tags' => [
                    'env' => 'console',
                ],
            ],
        ],

        'urlManager' => [
            'baseUrl' => '/',
            'hostInfo' => 'http://' . $params['host.frontend'],
        ],
    ],

    'params' => $params,
];
