<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'alitrust-web',

    'bootstrap' => ['user', 'api'],
    'controllerNamespace' => 'app\controllers',

    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\authclient\widgets\AuthChoiceStyleAsset' => [
                    'css' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'css' => ['css/bootstrap-theme.css'],
                ],
            ],
        ],

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vk' => [
                    'class' => 'app\oauth\VK',
                    'clientId' => '4780725',
                    'clientSecret' => 'O3k6J6MdtEs2BObJWCR4',
                    'scope' => 'email,friends,offline,notify',
                ],
                'facebook' => [
                    'class' => 'app\oauth\Facebook',
                    'clientId' => '586620414808106',
                    'clientSecret' => '6ccdf2e059601937d4d0a36646f24aeb',
                    'scope' => 'email,user_about_me',
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@runtime/logs/web.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['app\models\ReviewCreateForm', 'app\models\SellerSearchForm'],
                    'logFile' => '@runtime/logs/parser.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['app\controllers\BoastsController::actionVote'],
                    'logFile' => '@runtime/logs/likes.log',
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['app\parser\browsers\ProxyBrowser'],
                    'logFile' => '@runtime/logs/proxy_browser.log',
                    'logVars' => []
                ],
            ],
        ],

        'raven' => [
            'options' => [
                'tags' => [
                    'env' => 'frontend',
                ],
            ],
        ],

        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'thumbManager' => [
            'class' => 'app\components\ThumbManager',
        ],

        'user' => [
            'class' => 'app\components\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login'],
        ],

        'view' => [
            'class' => 'app\components\View',
        ],
    ],

    'modules' => [
        'admin' => 'app\modules\admin\Module',
        'api' => 'app\modules\api\Module',
        'forum' => 'app\modules\forum\Module',
    ],

    'params' => $params,
];
