<?php
return [
    'basePath' => dirname(dirname(__DIR__)),
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'viewPath' => dirname(__DIR__) . '/views',

    'name' => 'AliTrust',
    'bootstrap' => ['log', 'raven'],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',

    'aliases' => [
        '@app' => dirname(__DIR__),
        '@aliexpress' => dirname(dirname(__DIR__)) . '/aliexpress',

        '@home' => '/site/index',
        '@page' => '/site/page',
        '@signup' => '/auth/sign-up',
        '@login' => '/auth/login',
        '@logout' => '/auth/logout',
        '@forgot' => '/auth/request-password-reset-token',
    ],

    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user', 'redactor', 'admin', 'forumThemeModerator', 'forumTopicModerator', 'forumModerator'],
            'itemFile' => '@app/rbac/data/items.php',
            'assignmentFile' => '@app/rbac/data/assignments.php',
            'ruleFile' => '@app/rbac/data/rules.php',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],

        'dbCoupons' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=178.62.181.72;dbname=coupons',
            'charset' => 'utf8',
        ],

        'formatter' => [
            'timeZone' => 'UTC',
            'defaultTimeZone' => 'UTC',
        ],

        'i18n' => [
            'translations' => [
                'app/*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app/alerts' => 'alerts.php',
                        'app/mails' => 'mails.php',
                        'app/models' => 'models.php',
                        'app/validation' => 'validation.php',
                        'app/views' => 'views.php',
                    ],
                ],
            ],
        ],

        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '127.0.0.1',
            'port' => 6379,
            'database' => 1,
        ],

        'polyglot' => [
            'class' => 'app\components\Polyglot',
        ],

        'raven' => [
            'class' => 'app\components\Raven',
            'dsn' => 'https://3d26569ec9dc4cb8acd734dfaf8d8040:b98227c3adc44cdeace0ff3a51db3028@app.getsentry.com/38123',
            'registerErrorHandler' => ! YII_DEBUG,
            'registerExceptionHandler' => ! YII_DEBUG,
        ],

        'uploads' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => '@webroot/uploads',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'admin' => 'admin/site/index',
                'admin/<controller:\w+>' => 'admin/<controller>/index',
                'forum' => 'forum/default/index',
                'forum/board/<boardId:\d+>' => 'forum/themes/index',
                'forum/theme/<themeId:\d+>' => 'forum/comments/index',
                'forum/comment/<id:\d+>' => 'forum/comments/view',
                'forum/<controller:\w+>' => 'forum/<controller>/index',
                'feedback' => 'site/feedback',
                'retarget' => 'iframe/index',

                'rating/boasts/competition' => 'rating/index',

                'user/<id:\d+>' => 'profile/view',
                'user/<id:\d+>/boasts' => 'profile/boasts',
                'user/<id:\d+>/complaints' => 'profile/complaints',

                'qa/create' => 'question-answer/create',
                'qa/upload-photo' => 'question-answer/upload-photo',
                'qa/photo' => 'question-answer/photo',
                'qa/<categorySlug:[\w-]+>/<slug:[\w-]+>-<id:\d+>' => 'question-answer/view',
                'qa/<id:\d+>' => 'question-answer/view',
                'GET qa/<slug:[\w-]+>' => 'question-answer/category',
                'GET qa' => 'question-answer/index',
                'GET qa/<id:\d+>/comments' => 'question-answer/comments',
                'POST qa/<id:\d+>/comments' => 'question-answer/create-comment',

                'forum/theme/<id:\d+>' => 'forum/themes',
                'forum/topic/<id:\d+>' => 'forum/view',
                'forum/<action:[\w-]+>/<id:\d+>' => 'forum/<action>',

                '<controller:\w+>' => '<controller>/index',
                '<controller:\w+>/<id:\d+>/vote' => '<controller>/vote',

                'GET <controller:\w+>/<id:\d+>/comments' => '<controller>/comments',
                'POST <controller:\w+>/<id:\d+>/comments' => '<controller>/create-comment',
                'DELETE <controller:\w+>/<entityId:\d+>/comments/<id:\d+>' => 'comments/delete', // use CommentsController

                'articles/<category:[\w-]+>/<slug:[\w-]+>-<id:\d+>' => 'articles/view',
                'articles/<category:[\w-]+>/<slug:[\w-]+>' => 'articles/view',
                'articles/<slug:[\w-]+>' => 'articles/category',

                'boasts/<action:(mine|create|update|delete|photo|upload-photo)>' => 'boasts/<action>',
                'boasts/<category:[\w-]+>/<slug:[\w-]+>-<id:\d+>' => 'boasts/view',
                'boasts/<slug:[\w-]+>-<id:\d+>' => 'boasts/view',
                'boasts/<id:\d+>' => 'boasts/view', // legacy
                'boasts/<slug:[\w-]+>' => 'boasts/category',

                'complaints/<id:\d+>' => 'complaints/view',

                'info/<slug:[\w-]+>' => 'site/page',

                'news/<slug:[\w-]+>-<id:\d+>' => 'news/view',
                'news/<slug:[\w-]+>' => 'news/view',

                'reviews/category/<slug:[\w-]+>' => 'reviews/category',
                'reviews/<category:[\w-]+>/<slug:[\w-]+>-<id:\d+>' => 'reviews/view',
                'reviews/<id:\d+>' => 'reviews/view',

                'uploads/track/<name:[\w/.-]+>' => 'track/get-image',
                'track/history' => 'track/history',
                'track/<code:[\w]+>' => 'track/index',

                'uploads/thumbs/<filename:[\w/.-]+>' => 'thumbs/uploads',

                '<slug:[\w-]+>-<originalStoreId:\d+>/complaints' => 'sellers/complaints',
                '<slug:[\w-]+>-<originalStoreId:\d+>/reviews' => 'sellers/reviews',
                '<slug:[\w-]+>-<originalStoreId:\d+>' => 'sellers/view',
            ],
        ],

        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\db\records\User',
        ],
    ],
];
