<?php

return [
    'seller.replicationTimeout' => DAY,
    'product.replicationTimeout' => DAY,

    'review.updateTimeout' => 10 * MINUTE,

    'ali.apiKey' => 57756,

    'epn.apiKey' => '5aa896d6a16559301b9e33c16ea202c1',
    'epn.deeplinkHash' => 'b0c6350076f5a23f1f27e7cc088f5fb9',

    'forum.defaultUserId' => 5,
    'forum.sellersBoardId' => 5,

    'proxies.url' => 'http://account.fineproxy.org/api/getproxy/?format=txt&type=httpauth&login=VIP150444&password=BembP8SRfZ',
    'proxies.login' => 'VIP150444',
    'proxies.password' => 'BembP8SRfZ',

    'socials.facebook' => 'https://www.facebook.com/alitrustru',
    'socials.twitter' => 'https://twitter.com/alitrustru',
    'socials.vk' => 'https://vk.com/alitrust',
    'socials.youtube' => 'https://www.youtube.com/channel/UCOWhz4M6cKKfRnqHOJYxIjw',
    'socials.instagram' => 'https://instagram.com/alitrustru/',

    'user.loginDuration' => 4 * MONTH,
    'user.emailConfirmationTokenDuration' => DAY,
    'user.passwordResetTokenDuration' => HOUR,

    'tracking.apiKey' => 'c8c1c0f4c59f60edfbd0c3dcec514046b0f1d4fab8c2981822812b8e8b2f72022ad3c92fbb7f2bdc',

    'widget.api.access' => [
        'c8c1c0f4c59f60edfbd0c3dcec514046b0f1d4fab8c2981822812b8e8b2f72022ad3c92fbb7f2bdc'=>['host'=>'pozitivmag.ru', 'salt'=>'c8c1c0f4c59f60edfbd']
    ]
];
