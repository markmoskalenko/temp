<?php
namespace app\rest;

use Yii;
use yii\base\Behavior;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FormatterBehavior extends Behavior
{
    /**
     * @var BaseFormatter|array|string
     */
    public $formatter;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->formatter = Yii::createObject(['class' => $this->formatter]);
    }

    /**
     * @return array
     */
    public function fields()
    {
        return $this->formatter->fields();
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return $this->formatter->extraFields();
    }
}