<?php
namespace app\rest;

use app\helpers\Url;
use app\interfaces\UploadableModel;

/**
 * Formatter for files, uploaded by Imperavi Redactor.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImperaviRedactorFormatter extends BaseFormatter
{
    /**
     * @var array
     */
    protected $fields = [
        'filelink' => 'url',
        'image' => 'url',
        'thumb' => 'url',
    ];


    /**
     * @param UploadableModel $model
     * @return string
     */
    public function url(UploadableModel $model)
    {
        return Url::uploaded($model);
    }
}