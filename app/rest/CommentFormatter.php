<?php
namespace app\rest;

use app\db\records\Comment;
use app\helpers\Url;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentFormatter extends BaseFormatter
{
    /**
     * @var integer
     */
    public $authorId;
    /**
     * @var boolean
     */
    public $enableSort = false;

    /**
     * @var array
     */
    protected $fields = [
        'id',
        'parentId',
        'rootId',
        'authorId',
        'authorName',
        'authorAvatarUrl',
        'content',
        'votesCount',
        'timeCreated',
        'canVote',
        'canDelete',
        'isAuthor',
        'isModer',
        'priority',
    ];


    /**
     * @param Comment $comment
     * @return string
     */
    public function authorName(Comment $comment)
    {
        return $comment->author->name;
    }

    /**
     * @param Comment $comment
     * @return string
     */
    public function authorAvatarUrl(Comment $comment)
    {
        return Url::avatar($comment->author, ['w' => 50]);
    }

    /**
     * @param Comment $comment
     * @return string
     */
    public function content(Comment $comment)
    {
        return Yii::$app->formatter->asNtext($comment->content);
    }

    /**
     * @param Comment $comment
     * @return string
     */
    public function timeCreated(Comment $comment)
    {
        return Yii::$app->formatter->asDatetime($comment->timeCreated, 'dd MMM HH:mm');
    }

    /**
     * @param Comment $comment
     * @return integer
     */
    public function votesCount(Comment $comment)
    {
        return $comment->votesCount;
    }

    /**
     * @param Comment $comment
     * @return boolean
     */
    public function canVote(Comment $comment)
    {
        return Yii::$app->user->can('voteComment', ['comment' => $comment]);
    }

    /**
     * @param Comment $comment
     * @return boolean
     */
    public function canDelete(Comment $comment)
    {
        return Yii::$app->user->can('deleteComment', ['comment' => $comment]);
    }

    /**
     * @param Comment $comment
     * @return boolean
     */
    public function isAuthor(Comment $comment)
    {
        return $this->authorId !== null && $comment->authorId === $this->authorId;
    }

    /**
     * @param Comment $comment
     * @return boolean
     */
    public function isModer(Comment $comment)
    {
        return $comment->author->group === 'redactor';
    }

    /**
     * @param Comment $comment
     * @return integer
     */
    public function priority(Comment $comment)
    {
        if ($this->enableSort) {
            if ($this->isModer($comment)) {
                return 1;
            }
        }

        return 0;
    }
}