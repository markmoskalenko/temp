<?php
namespace app\rest;

use app\db\records\Category;
use app\repositories\CategoryRepository;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategorySelectizeFormatter extends BaseFormatter
{
    /**
     * @var array
     */
    protected $fields = [
        'id',
        'name',
        'namesPath',
        'synonyms',
    ];


    /**
     * @param Category $category
     * @return string|null
     */
    public function namesPath(Category $category)
    {
        return $category->present()->namesPath();
    }
}