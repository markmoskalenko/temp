<?php
namespace app\rest;

use Closure;
use yii\base\Object;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseFormatter extends Object
{
    /**
     * @var array
     */
    protected $fields = [];


    /**
     * @return array
     */
    public function fields()
    {
        $result = [];

        foreach ($this->fields as $key => $value) {
            if (is_int($key)) {
                $key = $value;
            }

            if (method_exists($this, $value)) {
                $value = $this->createClosure($this, $value);
            }

            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return [];
    }

    /**
     * @param object $class
     * @param string $method
     * @return Closure
     */
    protected function createClosure($class, $method)
    {
        $closure = function () use ($class, $method) {
            return call_user_func_array([$class, $method], func_get_args());
        };

        return $closure;
    }
}