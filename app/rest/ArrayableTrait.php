<?php
namespace app\rest;

use yii\base\Component;
use yii\base\NotSupportedException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
trait ArrayableTrait
{
    /**
     * @return array
     */
    public function fields()
    {
        return $this->getFormatterBehavior()->fields();
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return $this->getFormatterBehavior()->extraFields();
    }

    /**
     * @return FormatterBehavior
     * @throws NotSupportedException
     */
    private function getFormatterBehavior()
    {
        if ($this instanceof Component) {
            return $this->getBehavior('formatter');
        }

        throw new NotSupportedException();
    }
}