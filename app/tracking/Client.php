<?php
namespace app\tracking;

use app\tracking\Response\TrackingInfoResponse;
use GdePosylka\Client\Exception\EmptyCourierSlug;
use GdePosylka\Client\Exception\EmptyTrackingNumber;

/**
 * Class Client
 * @package app\tracking
 */
class Client extends \GdePosylka\Client\Client
{
    /**
     * @param $courierSlug
     * @param $trackingNumber
     * @return TrackingInfoResponse
     * @throws EmptyCourierSlug
     * @throws EmptyTrackingNumber
     * @throws \Exception
     * @throws \Guzzle\Common\Exception\GuzzleException
     */
    public function getTrackingInfo($courierSlug, $trackingNumber)
    {
        if (empty($courierSlug)) {
            throw new EmptyCourierSlug;
        }
        if (empty($trackingNumber)) {
            throw new EmptyTrackingNumber;
        }
        $tracking = array('tracking_number' => $trackingNumber, 'courier_slug' => $courierSlug);

        return new TrackingInfoResponse($this->getRequest()->call('getTrackingInfo', array('tracking' => $tracking)));
    }
}
