<?php
namespace app\tracking\Response;

class CheckpointResponse extends \GdePosylka\Client\Response\CheckpointResponse
{
    /**
     * @return string
     */
    public function getStatusName()
    {
        return $this->data['status_name'];
    }

    /**
     * @return string
     */
    public function getCourierName(){
        return $this->data['courier_name'];
    }

    /**
     * @return string
     */
    public function getLocationRu(){
        return $this->data['location_ru'];
    }
}