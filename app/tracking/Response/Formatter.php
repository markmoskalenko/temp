<?php
namespace app\tracking\Response;

use GdePosylka\Client\Response\CourierDetectResponse;
use GdePosylka\Client\Response\CourierListResponse;

/**
 * Class Formatter
 * @package app\components
 */
class Formatter
{
    /**
     * @param CourierDetectResponse $response
     * @return array
     */
    public function courier(CourierDetectResponse $response)
    {
        $items = (array)$response->getCouriers();

        $result = [];
        $result['total'] = $response->getTotal();
        foreach ($items as $item) {
            $result['couriers'][] = [
                'slug' => $item->getSlug(),
                'name' => $item->getName(),
                'countryCode' => $item->getCountryCode(),
                'trackingNumber' => $item->getTrackingNumber()
            ];
        }

        return $result;
    }

    /**
     * @param TrackingInfoResponse $response
     * @return array
     */
    public function info(TrackingInfoResponse $response)
    {
        $items = (array)$response->getCheckpoints();
        $result = [];
        foreach ($items as $item) {
            $result[] = [
                'zipCode' => $item->getZipCode(),
                'time' => \Yii::$app->formatter->asTime($item->getTime()->getTimestamp(), 'short'),
                'date' => \Yii::$app->formatter->asDate($item->getTime()->getTimestamp(), 'long'),
                'location' => $item->getLocationRu(),
                'statusName' => $item->getStatusName(),
                'courierName' => $item->getCourierName(),
                'courierSlug' => $item->getCourierSlug(),
            ];
        }

        return $result;
    }

    /**
     * @param CourierListResponse $response
     * @return array
     */
    public function couriers(CourierListResponse $response)
    {
        $items = (array)$response->getCouriers();
        $result = [];
        foreach ($items as $item) {
            $result[] = [
                'slug' => $item->getSlug(),
                'name' => $item->getName(),
                'countryCode' => $item->getCountryCode(),
                'trackingNumber' => $item->getTrackingNumber()
            ];
        }

        return $result;
    }
}