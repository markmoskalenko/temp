<?php
namespace app\bbcode;

use JBBCode\DocumentElement;
use JBBCode\ElementNode;
use JBBCode\Node;
use JBBCode\NodeVisitor;
use JBBCode\TextNode;
use ReflectionClass;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ParagraphsVisitor implements NodeVisitor
{
    /**
     * @var array
     */
    protected $blockNodes = [
        'h1',
        'h2',
        'h3',
        'spoiler',
        'video',
        'quote',
    ];


    /**
     * @param DocumentElement $documentElement
     */
    public function visitDocumentElement(DocumentElement $documentElement)
    {
        $paragraphs = $this->groupChildrenByParagraphs($documentElement);

        $this->removeChildren($documentElement);

        foreach ($paragraphs as $nodes) {
            $paragraph = new ElementNode();
            $paragraph->setCodeDefinition(new ParagraphDefinition());

            foreach ($nodes as $node) {
                $paragraph->addChild($node);
            }

            $documentElement->addChild($paragraph);
        }
    }

    /**
     * @param TextNode $textNode
     */
    public function visitTextNode(TextNode $textNode)
    {
        // No implementation
    }

    /**
     * @param ElementNode $elementNode
     */
    public function visitElementNode(ElementNode $elementNode)
    {
        // No implementation
    }

    /**
     * @param ElementNode $node
     * @return array
     */
    protected function groupChildrenByParagraphs(ElementNode $node)
    {
        $paragraphs = [];

        $inlines = [];

        foreach($node->getChildren() as $child) {
            if ($this->isInlineNode($child)) {
                $inlines[] = $child;
            }

            if ($this->isBlockNode($child)) {
                if (count($inlines)) {
                    $paragraphs[] = $inlines;
                }

                $paragraphs[] = [$child];
                $inlines = [];
            }
        }

        if (count($inlines)) {
            $paragraphs[] = $inlines;
        }

        return $paragraphs;
    }

    /**
     * @param Node $node
     * @return boolean
     */
    protected function isBlockNode(Node $node)
    {
        if ($node instanceof ElementNode) {
            if (in_array($node->getTagName(), $this->blockNodes)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Node $node
     * @return boolean
     */
    protected function isInlineNode(Node $node)
    {
        return ! $this->isBlockNode($node);
    }

    /**
     * @param ElementNode $node
     */
    protected function removeChildren(ElementNode $node)
    {
        $reflectionClass = new ReflectionClass('JBBCode\ElementNode');

        $reflectionProperty = $reflectionClass->getProperty('children');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($node, []);
    }
}