<?php
namespace app\bbcode;

use app\helpers\Smile;
use JBBCode\DocumentElement;
use JBBCode\ElementNode;
use JBBCode\NodeVisitor;
use JBBCode\TextNode;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SmileyVisitor implements NodeVisitor
{
    function visitDocumentElement(DocumentElement $documentElement)
    {
        foreach($documentElement->getChildren() as $child) {
            $child->accept($this);
        }
    }

    function visitTextNode(TextNode $textNode)
    {
        $replacements = [];
        foreach (Smile::all() as $code) {
            $replacements[ $code ] = Html::img(Smile::url($code));
        }

        $textNode->setValue(strtr($textNode->getValue(), $replacements));
    }

    function visitElementNode(ElementNode $elementNode)
    {
        /* We only want to visit text nodes within elements if the element's
         * code definition allows for its content to be parsed.
         */
        if ($elementNode->getCodeDefinition()->parseContent()) {
            foreach ($elementNode->getChildren() as $child) {
                $child->accept($this);
            }
        }
    }

}
