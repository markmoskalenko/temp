<?php
namespace app\bbcode;

use JBBCode\ElementNode;
use JBBCode\CodeDefinition;
use yii\helpers\Html;

/**
 * Implements a [list] code definition with marker that provides the following syntax:
 *
 * [ul]
 *   [*] first item
 *   [*] second item
 *   [*] third item
 * [/ul]
 *
 * @author Jackson Owens https://github.com/jbowens
 */
class MarkerListDefinition extends CodeDefinition
{
    public function __construct()
    {
        $this->parseContent = true;
        $this->useOption = false;
        $this->setTagName('ul');
        $this->nestLimit = -1;
    }

    public function asHtml(ElementNode $el)
    {
        $bodyHtml = '';
        foreach ($el->getChildren() as $child) {
            $bodyHtml .= $child->getAsHTML();
        }

        $listPieces = explode('[*]', $bodyHtml);
        unset($listPieces[0]);
        $listPieces = array_map(function($li) { return Html::tag('li', $li); }, $listPieces);

        return Html::tag('ul', implode('', $listPieces), ['class' => 'b-publication__list']);
    }
}