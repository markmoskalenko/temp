<?php
namespace app\bbcode;

use JBBCode\DocumentElement;
use JBBCode\ElementNode;
use JBBCode\NodeVisitor;
use JBBCode\TextNode;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LineBreakVisitor implements NodeVisitor
{
    public function visitDocumentElement(DocumentElement $documentElement)
    {
        foreach($documentElement->getChildren() as $child) {
            $child->accept($this);
        }
    }

    public function visitTextNode(TextNode $textNode)
    {
        $text = $textNode->getValue();

        $text = preg_replace('/\n{2,}/', "\n\n", $text);
        $text = nl2br($text);

        $textNode->setValue($text);
    }

    public function visitElementNode(ElementNode $elementNode)
    {
        /* We only want to visit text nodes within elements if the element's
         * code definition allows for its content to be parsed.
         */
        if ($elementNode->getCodeDefinition()->parseContent()) {
            foreach ($elementNode->getChildren() as $child) {
                $child->accept($this);
            }
        }
    }

}
