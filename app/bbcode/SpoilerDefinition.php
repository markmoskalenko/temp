<?php
namespace app\bbcode;

use app\widgets\Spoiler;
use JBBCode\CodeDefinition;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SpoilerDefinition extends CodeDefinition
{
    /**
     * @var string
     */
    protected $tagName = 'spoiler';
    /**
     * @var boolean
     */
    protected $parseContent = true;


    /**
     * @return string
     */
    public function getReplacementText()
    {
        if ($this->usesOption()) {
            $config = ['title' => '{option}', 'body' => '{param}'];
        } else {
            $config = ['title' => 'Скрытый текст', 'body' => '{param}'];
        }

        return Spoiler::widget($config);
    }

    /**
     * @param boolean $bool
     */
    public function setUseOption($bool)
    {
        $this->useOption = $bool;
    }
}
