<?php
namespace app\bbcode;

use JBBCode\DocumentElement;
use JBBCode\ElementNode;
use JBBCode\NodeVisitor;
use JBBCode\TextNode;

/**
 * Этот визитор пробегает по всем блочным узлам и проверяет его дочерние TextNode.
 * Первый TextNode тримается слева. Последний — справа.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class TrimVisitor implements NodeVisitor
{
    /**
     * @param DocumentElement $documentElement
     */
    public function visitDocumentElement(DocumentElement $documentElement)
    {
        foreach($documentElement->getChildren() as $child) {
            $child->accept($this);
        }
    }

    /**
     * @param TextNode $textNode
     */
    public function visitTextNode(TextNode $textNode)
    {
        // This method has no need in implementation.
    }

    /**
     * @param ElementNode $elementNode
     */
    public function visitElementNode(ElementNode $elementNode)
    {
        if ( ! $elementNode->getCodeDefinition()->parseContent()) {
            return;
        }

        $children = $elementNode->getChildren();
        if ( ! count($children)) {
            return;
        }

        $firstChild = $children[0];
        $lastChild = $children[ count($children) - 1 ];

        if ($firstChild instanceof TextNode) {
            $text = $firstChild->getValue();
            $text = ltrim($text);
            $firstChild->setValue($text);
        }

        if ($lastChild instanceof TextNode) {
            $text = $lastChild->getValue();
            $text = rtrim($text);
            $lastChild->setValue($text);
        }

        foreach ($children as $child) {
            $child->accept($this);
        }
    }
}
