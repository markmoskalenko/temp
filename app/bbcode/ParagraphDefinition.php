<?php
namespace app\bbcode;

use JBBCode\CodeDefinition;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ParagraphDefinition extends CodeDefinition
{
    /**
     * @inheritdoc
     */
    protected $replacementText = '<div class="b-publication__paragraph">{param}</div>';
    /**
     * @inheritdoc
     */
    protected $parseContent = true;
}