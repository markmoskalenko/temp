<?php
namespace app\bbcode;

use JBBCode\ElementNode;
use JBBCode\CodeDefinition;
use yii\helpers\Html;

/**
 * Implements a [list] code definition with number that provides the following syntax:
 *
 * [ol]
 *   [*] first item
 *   [*] second item
 *   [*] third item
 * [/ol]
 *
 * @author Vasilij Belosludcev http://github.com/bupy7
 */
class NumberListDefinition extends CodeDefinition
{
    public function __construct()
    {
        $this->parseContent = true;
        $this->useOption = true;
        $this->setTagName('ol');
        $this->nestLimit = -1;
    }

    public function asHtml(ElementNode $el)
    {
        $bodyHtml = '';
        foreach ($el->getChildren() as $child) {
            $bodyHtml .= $child->getAsHTML();
        }

        $listPieces = explode('[*]', $bodyHtml);
        unset($listPieces[0]);
        $listPieces = array_map(function($li) { return Html::tag('li', $li); }, $listPieces);

        return Html::tag('ol', implode('', $listPieces), ['class' => 'b-publication__list']);
    }
}