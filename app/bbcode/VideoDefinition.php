<?php
namespace app\bbcode;

use JBBCode\CodeDefinition;
use JBBCode\ElementNode;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class VideoDefinition extends CodeDefinition
{
    /**
     * @var string
     */
    protected $tagName = 'video';
    /**
     * @var boolean
     */
    protected $useOption = false;
    /**
     * @var boolean
     */
    protected $parseContent = false;
    /**
     * @var string
     */
    protected $pattern = '#\[video\](http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?)\[/video\]#';


    /**
     * @param ElementNode $el
     * @return boolean
     */
    public function hasValidInputs(ElementNode $el)
    {
        return preg_match($this->pattern, $el->getAsBBCode());
    }

    /**
     * @param ElementNode $el
     * @return string
     */
    public function asHtml(ElementNode $el)
    {
        if ($result = preg_match($this->pattern, $el->getAsBBCode(), $matches)) {
            $videoId = $matches[2];
            return '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$videoId.'" frameborder="0" allowfullscreen></iframe>';
        }

        return $el->getAsBBCode();
    }
}