<?php
namespace app\bbcode;

use app\widgets\Spoiler;
use JBBCode\CodeDefinition;
use JBBCode\CodeDefinitionBuilder;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CodeDefinitionSet implements \JBBCode\CodeDefinitionSet
{
    /**
     * @var CodeDefinition[] The default code definitions in this set.
     */
    protected $definitions = [];

    /**
     * Constructs the default code definitions.
     */
    public function __construct()
    {
        /* [h1] header tag */
        $builder = new CodeDefinitionBuilder('h1', '<div class="forum-comment-content__h1">{param}</div>');
        $this->definitions[] = $builder->build();

        /* [h2] header tag */
        $builder = new CodeDefinitionBuilder('h2', '<div class="forum-comment-content__h2">{param}</div>');
        $this->definitions[] = $builder->build();

        /* [h3] header tag */
        $builder = new CodeDefinitionBuilder('h3', '<div class="forum-comment-content__h3">{param}</div>');
        $this->definitions[] = $builder->build();

        /* [b] bold tag */
        $builder = new CodeDefinitionBuilder('b', '<strong>{param}</strong>');
        $this->definitions[] = $builder->build();

        /* [i] italics tag */
        $builder = new CodeDefinitionBuilder('i', '<em>{param}</em>');
        $this->definitions[] = $builder->build();

        /* [u] underline tag */
        $builder = new CodeDefinitionBuilder('u', '<u>{param}</u>');
        $this->definitions[] = $builder->build();

        $urlValidator = new \JBBCode\validators\UrlValidator();

        /* [url] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{param}">{param}</a>');
        $builder->setParseContent(false)->setBodyValidator($urlValidator);
        $this->definitions[] = $builder->build();

        /* [url=http://example.com] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{option}">{param}</a>');
        $builder->setUseOption(true)->setParseContent(true)->setOptionValidator($urlValidator);
        $this->definitions[] = $builder->build();

        /* [img] image tag */
        $builder = new CodeDefinitionBuilder('img', '<img class="forum-comment-content__image" src="{param}" />');
        $builder->setUseOption(false)->setParseContent(false);
        $this->definitions[] = $builder->build();

        /* [video] video tag */
        $this->definitions[] = new VideoDefinition();

        /* [quote] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="forum-comment-content__quote">{param}</div>');
        $builder->setParseContent(true);
        $this->definitions[] = $builder->build();

        /* [quote="Chuck Norris"] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="forum-comment-content__quote"><div class="forum-comment-content__quote-header">{option}</div>{param}</div>');
        $builder->setUseOption(true)->setParseContent(true);
        $this->definitions[] = $builder->build();

        /* [spoiler] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(false);
        $this->definitions[] = $definition;

        /* [spoiler="Заголовок"] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(true);
        $this->definitions[] = $definition;
    }

    /**
     * Returns an array of the default code definitions.
     *
     * @return CodeDefinition[]
     */
    public function getCodeDefinitions()
    {
        return $this->definitions;
    }
}