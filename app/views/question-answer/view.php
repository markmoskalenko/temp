<?php
use app\assets\FotoramaAsset;
use app\helpers\Url;
use app\widgets\Comments;
use yii\helpers\Html;

/* @var $qa app\db\records\QuestionAnswer */
/* @var $this  yii\web\View */

$this->title = $qa->title . ' — вопрос-ответ Алиэкспресс';
$this->registerMetaTag(['name' => 'description', 'content' => 'Ответ на вопрос ' . $qa->title . ' - вопрос-ответ Алиэкспресс']);

$this->params['breadcrumbs'][] = ['label' => 'Вопрос-Ответ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $qa->qaCategory->name, 'url' => ['category', 'slug' => $qa->qaCategory->slug]];

FotoramaAsset::register($this);
?>

<div class="page-header">
    <h1><?= Html::encode($qa->title) ?></h1>
</div>

<p><?= Yii::$app->formatter->asNtext($qa->question) ?></p>

<div class="b-qa__image-wrapper">
    <?php foreach ($qa->photos as $photo): ?>
        <img class="b-qa__image img-thumbnail" src="<?= Url::thumb($photo, ['w' => 500]) ?>" alt="<?= Html::encode($qa->title) ?>">
    <?php endforeach ?>
</div>

<ul class="b-publication-meta list-unstyled">
    <li>
        <img class="b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($qa->user, ['w' => 50, 'h' => 50]) ?>">
        <?php if($qa->userId):?>
            <a target="_blank" href="<?= Url::to(['profile/view', 'id' => $qa->user->id]) ?>"><?= $qa->present()->userName() ?></a>
        <?php else:?>
            <?= $qa->present()->userName() ?>
        <?php endif;?>
    </li>
    <li>
        <span class="fa fa-fw fa-clock-o"></span>
        <?= $qa->present()->timeCreated() ?>
    </li>
    <li>
        <span class="fa fa-fw fa-bookmark"></span>
        <a href="<?= Url::to(['question-answer/category', 'slug' => $qa->qaCategory->slug]) ?>"><?= Html::encode($qa->qaCategory->name) ?></a>
    </li>
    <li>
        <span class="fa fa-fw fa-comment-o"></span>
        <?= $qa->present()->commentCount() ?>
    </li>
</ul>

<?= Comments::widget([
    'url' => Url::to(['question-answer/comments', 'id' => $qa->id]),
    'template' => '@app/views/comments/qa-templates'
]) ?>
