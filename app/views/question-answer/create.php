<?php
use yii\helpers\Html;

/* @var $model      app\models\QuestionAnswerForm */
/* @var $this       yii\web\View */
/* @var $isGuest */

$this->title = 'Новый вопрос';
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render($isGuest ? '_form-guest' : '_form', compact('model', 'questionAnswerCategoryMap')) ?>