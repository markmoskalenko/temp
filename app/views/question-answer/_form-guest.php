<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model      app\models\QuestionAnswerForm */
/* @var $this       yii\web\View */
/* @var $questionAnswerCategoryMap array*/
?>

<?php $form = ActiveForm::begin([
    'id' => 'qa-form',
    'options' => ['autocomplete' => 'off'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ]
    ],
]) ?>

    <div id="snapshot-widget"></div>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'question')->textarea() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'qaCategoryId')->dropDownList($questionAnswerCategoryMap) ?>
    <?= $form->field($model, 'firstName')->textInput() ?>
    <?= $form->field($model, 'isMailNotification')->checkbox() ?>

    <br>

    <button type="submit" class="btn btn-lg btn-primary">Опубликовать вопрос</button>

<?php $form->end() ?>