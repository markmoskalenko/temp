<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $model app\db\records\QuestionAnswer */
/* @var $this  app\components\View */
?>

<div class="b-publication">
    <h3><?= Html::a(Html::encode($model->title), Url::to(['question-answer/view', 'id'=>$model->id, 'slug'=>$model->slug, 'categorySlug'=>$model->qaCategory->slug]), ['class' => 'border-link']) ?></h3>

    <?= Yii::$app->formatter->asHtml($model->getContentPreview()) ?>

    <br/>
    <br/>

    <ul class="b-publication-meta list-unstyled">
        <li>
            <img class="b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($model, ['w' => 50, 'h' => 50]) ?>">
            <?php if($model->userId):?>
                <a target="_blank" href="<?= Url::to(['profile/view', 'id'=>$model->user->id]) ?>"><?= $model->present()->userName() ?></a>
            <?php else:?>
                <?= $model->present()->userName() ?>
            <?php endif;?>
        </li>
        <li>
            <span class="fa fa-fw fa-clock-o"></span>
            <?= $model->present()->timeCreated() ?>
        </li>
        <li>
            <span class="fa fa-fw fa-bookmark"></span>
            <a href="<?= Url::to(['question-answer/category', 'slug'=>$model->qaCategory->slug]) ?>"><?= Html::encode($model->qaCategory->name) ?></a>
        </li>
        <li>
            <span class="fa fa-fw fa-comment-o"></span>
            <a href="<?= Url::to(['question-answer/view', 'id'=>$model->id, 'slug'=>$model->slug, 'categorySlug'=>$model->qaCategory->slug]) ?>">
                <?= $model->present()->commentCount() ?>
            </a>
        </li>
    </ul>
</div>