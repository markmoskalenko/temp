<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model      app\models\QuestionAnswerForm */
/* @var $this       yii\web\View */
/* @var $questionAnswerCategoryMap array*/

$this->registerJsFile('@web/js/frontend-qa-photos.js', [
    'depends' => [
        'app\assets\AppAsset',
        'app\assets\FileApiAsset',
    ],
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'qa-form',
    'options' => ['autocomplete' => 'off'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ]
    ],
]) ?>

    <div id="snapshot-widget"></div>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'question')->textarea() ?>
    <?= $form->field($model, 'qaCategoryId')->dropDownList($questionAnswerCategoryMap) ?>
    <?= $form->field($model, 'isMailNotification')->checkbox() ?>

    <div class="form-group field-boastform-photos">
        <?= Html::activeLabel($model, 'photos', ['class' => 'control-label']) ?>
        <?= Html::hiddenInput(Html::getInputName($model, 'photos')) ?>


        <div class="row js-uploaded-photos">
            <?php foreach ($model->photos as $photoId): ?>
                <?= Html::hiddenInput(Html::getInputName($model, 'photos[]'), $photoId) ?>
            <?php endforeach ?>
        </div>

        <p class="help-block" data-fileapi="active.show" style="display: none">
            <span class="fa fa-spinner fa-spin"></span>
            Идёт загрузка фотографий
        </p>

        <label class="b-upload-btn b-upload-btn_style_link">
            <span class="b-upload-btn__label">Добавить больше фотографий</span>
            <input type="file" name="file" class="b-upload-btn__input" id="<?= Html::getInputId($model, 'photos') ?>" />
        </label>
    </div>
    <br>

    <button type="submit" class="btn btn-lg btn-primary">Опубликовать вопрос</button>

<?php $form->end() ?>

<script type="text/html" id="qa-photo-template">
    <div class="col-xs-6 col-sm-3 col-md-2">
        <a href="#" class="thumbnail b-upload-thumb b-upload-thumb_state_hoverable js-photo-delete" onclick="return false">
            <input type="hidden" name="<?= Html::getInputName($model, 'photos[]') ?>" value="<%= id %>" />
            <span class="b-upload-thumb__image" style="background-image: url(<%= url %>)"></span>

            <span class="b-upload-thumb__hover">
                <span class="b-upload-thumb__hover-inner">
                    <span class="b-upload-thumb__hover-icon fa fa-trash-o"></span>
                    <span class="b-upload-thumb__hover-text">Нажмите, чтобы&nbsp;удалить</span>
                </span>
            </span>
        </a>
    </div>
</script>