<?php
use app\widgets\QuestionAnswerListView;
use yii\helpers\Html;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Вопрос-Ответ AliExpress';
$this->registerMetaTag(['name' => 'description', 'content' => 'Вам нужна помощь по Алиэкспресс? Задавайте свои вопросы и получайте оперативные ответы на тему Алиэкспресс.']);
$this->params['showQaCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

<?= QuestionAnswerListView::widget([
    'dataProvider' => $dataProvider,
    'viewParams' => compact('slug')
]) ?>

<?php $this->endContent() ?>