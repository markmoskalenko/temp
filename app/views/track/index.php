<?php
use app\assets\TrackerAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Отслеживание посылок алиэкспресс. Где посылка – быстро найти посылку';
$this->registerMetaTag(['name' => 'description', 'content' => 'С нашим сервисом отслеживание посылок с Aliexpress вы всегда будете в курсе местоположения вашей посылки. Быстро узнавайте где сейчас ваша посылка.']);
$this->params['disableSearchArea'] = true;

TrackerAsset::register($this);
?>

<div class="page-header">
    <h1>Отслеживание посылок</h1>
</div>

<div class="row" data-ng-controller="TrackCtrl">
    <div class="col-md-4 col-md-push-8">
        <div class="b-tracker-history">
            <h3 class="b-tracker-history__header">История отслеживаний</h3>
            <div id="tracker-history">
                <div data-ng-repeat="track in trackingStorage | orderBy:'time':true" data-ng-cloak>
                    <a class="b-tracker-history__track-code border-link border-link-dashed" data-ng-click="onDetectCourier(track.trackingNumber)">{{track.trackingNumber}}</a>
                    <span class="b-tracker-history__action-remove fa fa-times" data-ng-click="onDeleteTrackingNumberStorage(track.trackingNumber);"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-md-pull-4">
        <form class="b-tracker-form form-inline">
            <div class="form-group">
                <input type="text" data-ng-model="trackingNumber" data-ng-change="onReset()" class="b-tracker-form__input form-control input-lg" placeholder="Введите свой трек-код">
            </div>
            <button type="submit" data-ng-disabled="disabled" data-ng-click="onSubmit()" class="b-tracker-form__submit btn btn-lg btn-inverse" data-ng-cloak>{{buttonTitle}}</button>
        </form>

        <div data-ng-if="couriers.length > 1" data-ng-cloak>
            <form>
                <label>Выберите курьерную службу</label>
                <select id="select-couriers" data-ng-options="courier.name for courier in couriers track by courier.slug" data-ng-change="onSelectCourier(courier);" data-ng-model="courier"></select>
            </form>
            <br/>
        </div>

        <div class="error" style="color: red" data-ng-cloak>{{error}}</div>
        <div class="notice" style="color: #55dd0d;" data-ng-cloak ng-bind-html="notice"></div>
        <br/>
        <div class="b-tracker__loader" data-ng-class="{'hidden': !loader}" data-ng-cloak>
            <div class="fa fa-circle-o-notch fa-spin fa-5x"></div>
        </div>

        <table class="table table-bordered">
            <tr data-ng-repeat="item in info" data-ng-cloak>
                <td>
                    <div>{{item.date}}</div>
                    <div>{{item.time}}</div>
                </td>
                <td>
                    <div>{{item.statusName}}</div>
                    <div>{{item.location}}</div>
                </td>
                <td>
                    <img width="50" ng-src="/uploads/track/{{item.courierSlug}}.png"/>
                </td>
            </tr>
        </table>

        <div id="tracker-country-destination"></div>
        <div id="tracker-country-origin"></div>

        <p>С нашим сервисом отслеживание посылок с Aliexpress вы всегда будете в курсе местоположения вашей посылки.</p>
        <p>Теперь у вас не возникнет вопроса как отследить посылку с Алиэкспресс, т.к. вас отделяют считанные секунды, чтобы узнать где она находится. Для этого необходимо взять номер отслеживания, который передал вам продавец после оплаты и отправления заказа, и вставить в форму выше. Через некоторое время у вас на экране появится информация по местонахождению вашей посылки, начиная от отправителя из Китая и заканчивая вашим местным почтовым отделением. </p>
        <p>Хороших вам покупок и пусть посылки с Алиэкспресс не теряются!</p>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

        <script type="text/javascript">
            VK.init({apiId: 4780725, onlyWidgets: true});
        </script>
        <!-- Put this div tag to the place, where the Comments block will be -->
        <div id="vk_comments"></div>
        <script type="text/javascript">
            VK.Widgets.Comments("vk_comments", {limit: 10, attach: "photo"}, 1000);
        </script>
    </div>
</div>