<?php
use app\widgets\CommentRatingGridView;
use yii\helpers\Html;

/* @var $currentMonthDataProvider yii\data\DataProviderInterface */
/* @var $lastMonthDataProvider    yii\data\DataProviderInterface */
/* @var $overallDataProvider      yii\data\DataProviderInterface */
/* @var $this                     yii\web\View */

$this->title = 'Рейтинг комментаторов';

$currentMonth = date('Y-m-15');
$lastMonth = strtotime('-1 month', strtotime($currentMonth));
?>

<div class="page-header">
    <h1 class="page-header__title"><?= Html::encode($this->title) ?></h1>
</div>

<div role="tabpanel">
    <ul class="nav nav-pills b-rating-nav">
        <li role="presentation" class="b-rating-nav__tab active">
            <a role="tab" data-toggle="tab" href="#rating-current-month">
                За <span class="text-lowercase"><?= Yii::$app->formatter->asDate($currentMonth, 'LLLL') ?></span>
            </a>
        </li>
        <li role="presentation" class="b-rating-nav__tab">
            <a role="tab" data-toggle="tab" href="#rating-overall">За всё время</a>
        </li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="rating-current-month">
            <div class="table-responsive">
                <?= CommentRatingGridView::widget([
                    'dataProvider' => $currentMonthDataProvider,
                    'showPrize' => true,
                ]) ?>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="rating-last-month">
            <div class="table-responsive">
                <?= CommentRatingGridView::widget([
                    'dataProvider' => $lastMonthDataProvider,
                ]) ?>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="rating-overall">
            <div class="table-responsive">
                <?= CommentRatingGridView::widget([
                    'dataProvider' => $overallDataProvider,
                ]) ?>
            </div>
        </div>
    </div>
</div>


