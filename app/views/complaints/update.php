<?php
use yii\helpers\Html;

/* @var $complaint app\db\records\Complaint */
/* @var $model     app\models\ComplaintForm */
/* @var $this      yii\web\View */

$this->title = 'Редактирование жалобы на продавца';
$this->params['breadcrumbs'][] = ['label' => 'Мои жалобы', 'url' => ['mine']];
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render('_form', ['model' => $model]) ?>
