<?php
use app\assets\AutosizeAsset;
use app\db\records\ComplaintPhoto;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\ComplaintForm */
/* @var $this  yii\web\View */

AutosizeAsset::register($this);
$this->registerJs('autosize($("textarea"))');
?>

<?php $form = ActiveForm::begin([
    'id' => 'complaint-form',
    'options' => [
        'class' => 'js-loadable-form',
        'autocomplete' => 'off',
    ],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ],
    ],
]) ?>

    <?= $form->field($model, 'link')->textInput(['placeholder' => 'Скопируйте сюда ссылку на вашего продавца или его товар']) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6, 'placeholder' => 'Расскажите нам о вашем опыте работы с данным продавцом']) ?>

    <?= $form->field($model, 'photos')->widget('app\widgets\ImagesUploader', [
        'uploadUrl' => Url::to(['upload-photo']),
        'imageUrl' => function ($id) {
            $image = ComplaintPhoto::findOne($id);
            return Url::uploaded($image);
        }
    ]) ?>

    <button class="btn btn-primary btn-lg" type="submit" data-toggle="disable">
        <span class="fa fa-circle-o-notch fa-spin js-loadable-hunky hidden"></span>
        Опубликовать жалобу
    </button>

    <span class="text-muted" style="margin-left: 20px">
        Нажимая кнопку "Опубликовать жалобу", вы подтверждаете свое согласие с
        <a href="http://alitrust.ru/forum/theme/380" target="_blank">правилами публикации жалоб</a>.
    </span>

<?php $form->end() ?>