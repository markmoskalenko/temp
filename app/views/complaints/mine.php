<?php
use app\helpers\Url;
use app\widgets\ComplaintList;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Мои жалобы на продавцов';
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

<div class="page-header">
    <div class="page-header__title">Мои жалобы</div>

    <div class="page-header__controls">
        <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['create']) ?>">
            <span class="fa fa-plus"></span> Добавить жалобу
        </a>
    </div>
</div>

<?= ComplaintList::widget([
    'dataProvider' => $dataProvider,
    'showAuthor' => false,
    'showLikeButton' => false,
    'showOwnerControls' => true,
    'showSellerInfo' => true,
]) ?>

<?php $this->endContent() ?>
