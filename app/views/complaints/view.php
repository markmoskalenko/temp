<?php
use app\widgets\Comments;
use app\widgets\LikeButton;
use app\widgets\SliderGallery;
use app\widgets\SellerScore;
use yii\helpers\Html;
use app\helpers\Url;

/* @var $complaint app\db\records\Complaint */
/* @var $this      yii\web\View */

$seller = $complaint->seller;
$author = $complaint->user;
$photos = $complaint->getPhotos()->visible()->all();

$this->title = 'Жалоба от ' . $author->name . ' на магазин ' . $seller->storeName . ' на Алиэкспресс';
$this->registerMetaTag(['name' => 'description', 'content' => mb_substr($complaint->getFormattedText(), 0, 240) . '...']);
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>
    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

    <div class="b-complaint">
        <div class="b-complaint-seller">
            <div class="b-complaint-seller__name">
                <a class="border-link" href="<?= Url::entity($seller) ?>">
                    <?= Html::encode($seller->storeName) ?>
                </a>
            </div>

            <div class="b-complaint-seller__info">
                Рейтинг магазина:
                <?= SellerScore::widget(['seller' => $seller]) ?>
            </div>
        </div>

        <div class="b-complaint__text">
            <?= $complaint->getFormattedText() ?>
        </div>

        <?= SliderGallery::widget([
            'images' => $photos,
        ]) ?>

        <div class="b-complaint__meta b-meta">
            <div class="b-meta__item">
                <img class="b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($author, ['w' => 50]) ?>" />
                <span class="text-capitalize"><a target="_blank" href="<?= Url::to(['profile/view', 'id'=>$author->id]) ?>"><?= Html::encode($author->present()->name()) ?></a></span>
            </div>

            <div class="b-meta__item">
                <span class="b-meta__icon fa fa-calendar"></span>
                <?= Yii::$app->formatter->asDate($complaint->timeCreated, 'long') ?>
            </div>

            <div class="b-meta__item">
                <?= LikeButton::widget([
                    'entity' => $complaint,
                    'label' => 'сказать спасибо',
                    'url' => Url::to(['vote', 'id' => $complaint->id]),
                ]) ?>
            </div>
        </div>

        <div class="b-complaint__meta b-meta pull-right">
            <?php if (Yii::$app->user->can('updateComplaint', ['complaint' => $complaint])): ?>
                <a class="b-meta__item" href="<?= Url::to(['update', 'id' => $complaint->id]) ?>">
                    <span class="b-meta__icon fa fa-pencil"></span>
                </a>
            <?php endif ?>

            <?php if (Yii::$app->user->can('deleteComplaint', ['complaint' => $complaint])): ?>
                <a class="b-meta__item" href="<?= Url::to(['delete', 'id' => $complaint->id]) ?>"
                   data-method="post" data-confirm="Вы уверены, что хотите удалить эту жалобу?">
                    <span class="b-meta__icon fa fa-trash-o"></span></a>
            <?php endif ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <?= Comments::widget([
        'url' => Url::to(['comments', 'id' => $complaint->id]),
    ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
