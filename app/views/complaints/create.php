<?php
/* @var $model app\models\ComplaintForm */
/* @var $this  yii\web\View */

$this->title = 'Пожаловаться на продавца алиэкспресс';
$this->params['breadcrumbs'][] = ['label' => 'Мои жалобы', 'url' => ['mine']];
$this->registerMetaTag(['name' => 'description', 'content' => 'Оставьте жалобу на продавца алиэкспресс. Добавить продавца в черный список. Пожаловаться на продавца']);
?>

<div class="page-header">
    <h1>Пожаловаться на продавца</h1>
</div>

<?= $this->render('_form', ['model' => $model]) ?>
