<?php
use app\db\records\ComplaintPhoto;
use app\helpers\Url;
use app\widgets\LikeButton;
use app\widgets\SellerScore;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model  app\db\records\Complaint */
/* @var $widget app\widgets\ComplaintList */
/* @var $this   yii\web\View */

$complaint = $model;
$seller = $complaint->seller;
$photos = $complaint->getPhotos()->visible()->all();
$user = $complaint->user;

$dataPhotos = ArrayHelper::toArray($photos, [
    ComplaintPhoto::className() => [
        'id',
        'complaintId',
        'url' => function ($photo) {
            return Url::uploaded($photo);
        },
    ],
]);
?>

<a class="b-complaint__background-link" href="<?= Url::entity($complaint) ?>"></a>

<?php if ($model->rejected): ?>
    <div class="b-complaint__reject-message">
        Отзыв отклонен:
        <a href="#" onclick="return false" class="b-complaint__reject-reason" data-toggle="popover" data-placement="bottom" data-content="<?= Html::encode($model->rejectReason) ?>">причина</a>.
    </div>
<?php endif ?>

<?php if ($widget->showSellerInfo): ?>
    <div class="b-complaint-seller">
        <div class="b-complaint-seller__name">
            <?= Html::encode($seller->storeName) ?>
        </div>

        <div class="b-complaint-seller__info">
            Рейтинг магазина:
            <?= SellerScore::widget(['seller' => $seller]) ?>
        </div>
    </div>
<?php endif ?>

<div class="b-complaint__body <?= ($widget->showSellerInfo ? 'b-complaint__body_style_blockquote' : '') ?>">
    <div class="b-complaint__text b-complaint__text_state_collapsed">
        <div class="b-complaint__text-inner"><?= $complaint->getFormattedText() ?></div>
    </div>

    <div class="b-complaint__meta b-meta">
        <?php if ($widget->showAuthor): ?>
            <div class="b-meta__item">
                <img class="b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($user, ['w' => 50]) ?>" />
                <span class="text-capitalize"><a target="_blank" href="<?= Url::to(['profile/view', 'id'=>$user->id]) ?>"><?= Html::encode($user->present()->name()) ?></a></span>
            </div>
        <?php endif ?>

        <div class="b-meta__item">
            <span class="b-meta__icon fa fa-calendar"></span>
            <?= Yii::$app->formatter->asDate($complaint->timeCreated, 'long') ?>
        </div>

        <a class="b-meta__item border-link border-link-dashed js-gallery <?= ($photos) ? '' : 'hidden' ?>"
           href="#" onclick="return false"
           <?= Html::renderTagAttributes(['data-photos' => $dataPhotos]) ?>>
            <span class="b-meta__icon fa fa-camera"></span>
            <span class="js-photos-counter"><?= count($photos) ?></span> фото
        </a>

        <div class="b-meta__item text-info">
            <span class="b-meta__icon fa fa-comment-o"></span>
            <?= $complaint->commentCount; ?>
        </div>

        <?php if ($widget->showLikeButton): ?>
            <a class="b-meta__item">
                <?= LikeButton::widget([
                    'entity' => $model,
                    'url' => Url::to(['complaints/vote', 'id' => $model->id]),
                    'label' => 'сказать спасибо',
                ]) ?>
            </a>
        <?php else: ?>
            <div class="b-meta__item text-primary">
                <span class="b-meta__icon fa fa-heart"></span>
                <?= $complaint->votesCount; ?>
            </div>
        <?php endif ?>


        <a class="b-meta__item border-link border-link-dashed js-expand hidden" href="#" onclick="return false">
            <span class="b-meta__icon fa fa-angle-down"></span>
            Развернуть жалобу
        </a>

        <a class="b-meta__item border-link border-link-dashed js-collapse hidden" href="#" onclick="return false">
            <span class="b-meta__icon fa fa-angle-up"></span>
            Свернуть жалобу
        </a>

        <?php if ($widget->showOwnerControls): ?>
            <?php if (Yii::$app->user->can('updateComplaint', ['complaint' => $complaint])): ?>
                <a class="b-meta__item border-link" href="<?= Url::to(['update', 'id' => $complaint->id]) ?>">
                    <span class="b-meta__icon fa fa-pencil"></span>
                    Изменить текст
                </a>
            <?php endif ?>

            <?php if (Yii::$app->user->can('deleteComplaint', ['complaint' => $complaint])): ?>
                <a class="b-meta__item border-link" href="<?= Url::to(['delete', 'id' => $complaint->id]) ?>"
                   data-method="post" data-confirm="Вы уверены, что хотите удалить эту жалобу?">
                    <span class="b-meta__icon fa fa-trash-o"></span>
                    Удалить жалобу
                </a>
            <?php endif ?>
        <?php endif ?>
    </div>
</div>
