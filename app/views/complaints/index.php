<?php
use app\widgets\ComplaintList;
use yii\helpers\Url;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$dataProvider->prepare();

$complaintsCount = $dataProvider->getTotalCount();
$complaintsCount = round($complaintsCount, -2);

$this->title = 'Черный список магазинов и продавцов алиэкспресс';
$this->registerMetaTag(['name' => 'description', 'content' => "Более {$complaintsCount} жалоб о продавцах и магазинах алиэкспресс. Плохие отзывы о магазинах aliexpress. Кидалы алиэкспресс"]);

if (Yii::$app->request->get($dataProvider->pagination->pageParam)) {
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
}
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <div class="page-header__title">Жалобы на продавцов AliExpress</div>

        <div class="page-header__controls">
            <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['create']) ?>">
                <span class="fa fa-plus"></span> Добавить жалобу
            </a>
        </div>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?= ComplaintList::widget([
            'dataProvider' => $dataProvider,
            'showSellerInfo' => true,
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
