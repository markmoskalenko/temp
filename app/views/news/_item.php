<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $model app\db\records\News */
/* @var $this  app\components\View */
?>

<?php if ($model->imageId): ?>
    <div class="publication-label-header">
        <div class="publication-label-header__container">
            <a class="publication-label-header__background publication-label-header__link" href="<?= Url::entity($model) ?>">
                <span class="publication-label-header__title"><?= Html::encode($model->title) ?></span>
            </a>
        </div>

        <a class="publication-label-header__image" href="<?= Url::entity($model) ?>" style="background-image: url(<?= Url::uploaded($model->mainImage) ?>)"></a>
    </div>
<?php else: ?>
    <h1 class="publication-header">
        <a href="<?= Url::entity($model) ?>" class="publication-header__title publication-header__link"><?= Html::encode($this->title) ?></a>
    </h1>
<?php endif ?>

<div class="b-publication-teaser">
    <?= Yii::$app->formatter->asHtml($model->getContentPreview()) ?>
</div>

<div class="b-publication-meta">
    <div class="meta-info-group pull-left">
        <div class="meta-info-group__item meta-info">
            <span class="fa fa-fw fa-clock-o"></span>
            <?= Yii::$app->formatter->asDate($model->timePublished) ?>
        </div>
    </div>

    <div class="meta-info-group pull-right">
        <div class="meta-info-group__item meta-info">
            <span class="publication-widget__meta-icon meta-info__icon fa fa-eye"></span>
            <span class="meta-info__text"><?= $model->viewCount ?></span>
        </div>

        <a class="meta-info-group__item meta-info" href="<?= Url::entity($model) ?>#comments">
            <span class="publication-widget__meta-icon meta-info__icon fa fa-comment-o"></span>
            <span class="meta-info__text"><?= $model->commentCount ?></span>
        </a>
    </div>
</div>
