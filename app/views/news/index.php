<?php
use app\widgets\PublicationList;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         app\components\View */

$this->title = 'Новости и события - АлиТраст';
$this->registerMetaTag(['name' => 'description', 'content' => 'Новости и события о АлиЭкспресс. Последние новости проекта АлиТраст']);
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1>Новости и события</h1>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?= PublicationList::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/news/_item',
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
