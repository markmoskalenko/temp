<?php
use app\widgets\Comments;
use app\helpers\RedactorHtmlPurifier;
use app\helpers\Url;
use yii\helpers\Html;

/* @var $news app\db\records\News */
/* @var $this app\components\View */

$this->title = $news->seoTitle ?: $news->title;
$this->title = $this->title . ' - новости Алиэкспресс';

if ($news->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $news->seoDescription]);
}

$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];

$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => ($news->seoDescription) ? $news->seoDescription : $news->contentPreview]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => 'AliTrust.ru']);
$this->registerMetaTag(['property' => 'og:image', 'content' => ($news->imageId) ? Url::uploaded($news->mainImage, true) : '']);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([], true)]);
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>
    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?php if ($news->imageId): ?>
            <div class="publication-label-header">
                <div class="publication-label-header__container">
                    <div class="publication-label-header__background">
                        <h1 class="publication-label-header__title"><?= Html::encode($news->title) ?></h1>
                    </div>
                </div>

                <a class="publication-label-header__image" href="<?= Url::entity($news) ?>" style="background-image: url(<?= Url::uploaded($news->mainImage) ?>)"></a>
            </div>

            <br>
        <?php else: ?>
            <h1 class="publication-header">
                <a href="<?= Url::entity($news) ?>" class="publication-header__title publication-header__link"><?= Html::encode($news->title) ?></a>
            </h1>
        <?php endif ?>

        <div class="b-publication">
            <?= RedactorHtmlPurifier::process($news->content) ?>
        </div>

        <?= Comments::widget([
            'id' => 'comments',
            'url' => Url::to(['comments', 'id' => $news->id]),
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
