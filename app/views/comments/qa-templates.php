<?php
/* @var $this       yii\web\View */
/* @var $content    string */
/* @var $formAction string */
?>

<script type="text/html" id="comment-widget-template">
    <div class="b-comment-widget">
        <h3 class="b-comment-widget__content-header">Ответы</h3>
        <div class="b-comment-widget__content" data-region="content"></div>
        <h3 class="b-comment-widget__form-header">Добавить ответ</h3>
        <div class="b-comment-widget__form" data-region="form"></div>
    </div>
</script>

<script type="text/html" id="comment-root-item-template">
    <div class="b-comment-list__item">
        <div data-region="content"></div>
        <div data-region="list"></div>
        <div data-region="form"></div>
    </div>
</script>

<script type="text/html" id="comment-item-template">
    <div class="b-comment <%= (isAuthor) ? 'b-comment_style_author' : ((isModer) ? 'b-comment_style_moderator' : '') %>">
        <div class="b-comment__anchor" id="comment_<%= id %>"></div>

        <div class="b-comment__content"><%= content %></div>

        <div class="b-comment__meta b-meta">
            <div class="b-meta__item">
                <img class="b-meta__icon b-meta__icon_type_avatar" src="<%= authorAvatarUrl %>" />
                <span class="text-capitalize"><a target="_blank" href="/user/<%= authorId %>"><%= authorName %></a></span>
            </div>

            <div class="b-meta__item">
                <span class="b-meta__icon fa fa-calendar"></span>
                <%= timeCreated %>
            </div>

            <div class="b-meta__item <% if (canVote) { %>border-link border-link-dashed<% } %>"
                 data-action="vote" data-title="Сказать спасибо">
                <span class="b-meta__icon fa fa-thumbs-up"></span>
                <%= votesCount %>
            </div>

            <a class="b-meta__item border-link border-link-dashed" data-action="reply" href="#" onclick="return false">
                <span class="b-meta__icon fa fa-reply"></span>
                Ответить
            </a>

            <% if (canDelete) { %>
                <a class="b-meta__item border-link border-link-dashed" data-action="delete" href="#" onclick="return false">
                    <span class="b-meta__icon fa fa-trash-o"></span>
                    Удалить
                </a>
            <% } %>
        </div>
    </div>
</script>

<script type="text/html" id="comment-form-template">
    <form class="b-comment-form" method="post">
        <div class="form-group">
            <textarea class="b-comment-form__content-input form-control" name="content" rows="5"></textarea>
        </div>

        <div class="form-group">
            <button type="submit" class="b-comment-form__submit btn btn-primary">Отправить</button>

            <% if (parentId) { %>
                <button type="button" class="btn btn-default" data-action="cancel">Отмена</button>
            <% } %>
        </div>
    </form>
</script>