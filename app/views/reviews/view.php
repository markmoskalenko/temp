<?php
use app\helpers\Url;
use app\widgets\Comments;
use app\widgets\LikeButton;
use yii\helpers\Html;

/* @var $review app\db\records\Review */
/* @var $this   yii\web\View */

$this->title = $review->title;
$this->params['breadcrumbs'][] = ['label' => 'Обзоры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $review->category->name, 'url' => ['category', 'slug' => $review->category->slug]];
$this->params['showReviewCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>
    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?php if ($review->present()->contentStartsWithImage()): ?>
            <div class="publication-label-header">
                <div class="publication-label-header__container">
                    <div class="publication-label-header__background">
                        <h1 class="publication-label-header__title"><?= Html::encode($review->title) ?></h1>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="publication-header">
                <h1 class="publication-header__title"><?= Html::encode($review->title) ?></h1>
            </div>
        <?php endif ?>

        <div class="b-publication">
            <?= $review->present()->content() ?>
        </div>

        <div class="meta-info-group text-muted pull-left">
            <div class="meta-info-group__item meta-info">
                <img class="meta-info__icon b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($review->user, ['w' => 50]) ?>" />
                <span class="text-capitalize"><a target="_blank" href="<?= Url::to(['profile/view', 'id'=>$review->user->id]) ?>"><?= Html::encode($review->user->present()->name()) ?></a></span>
            </div>

            <div class="meta-info-group__item meta-info">
                <span class="meta-info__icon fa fa-calendar"></span>
                <?= $review->present()->timeCreated() ?>
            </div>

            <div class="meta-info-group__item meta-info">
                <?= LikeButton::widget([
                    'entity' => $review,
                    'url' => Url::to(['vote', 'id' => $review->id]),
                ]) ?>
            </div>
        </div>

        <div class="meta-info-group pull-right">
            <?php if (Yii::$app->user->can('updateReview', ['review' => $review])): ?>
                <div class="meta-info-group__item meta-info">
                    <a href="<?= Url::to(['update', 'id' => $review->id]) ?>">
                        <span class="meta-info__icon fa fa-fw fa-pencil"></span>
                    </a>
                </div>
            <?php endif ?>

            <?php if (Yii::$app->user->can('deleteReview', ['review' => $review])): ?>
                <div class="meta-info-group__item meta-info">
                    <a href="<?= Url::to(['delete', 'id' => $review->id]) ?>" data-method="post" data-confirm="Вы уверены?">
                        <span class="meta-info__icon fa fa-fw fa-trash"></span>
                    </a>
                </div>
            <?php endif ?>
        </div>

        <div class="clearfix"></div>
        <div class="page-divider page-divider_style_border"></div>

        <?= Comments::widget([
            'url' => Url::to(['comments', 'id' => $review->id]),
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
