<?php
use app\assets\ImagesUploaderAsset;
use app\helpers\Angular;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\ReviewForm */
/* @var $this  yii\web\View */

ImagesUploaderAsset::register($this);

$this->params['disableSearchArea'] = true;

$this->registerJs(<<<JS
    $("[images-uploader]").on("add.imageUploader select.imageUploader", function (event, image) {
        if (image.url) {
            redactor.insertTag($("#reviewform-content"), '[img]' + image.url + '[/img] \\n', '');
        }
    });

    $('.js-preview').on('click', function () {
        var btn = $(this);

        $.ajax({
            url: '/reviews/preview',
            type: 'POST',
            data: $('#reviewform-content').val(),
            beforeSend: function () {
                btn.prop('disabled', true);
            },
            complete: function () {
                btn.prop('disabled', false);
            },
            success: function (html) {
                $(".b-publication-preview").remove();
                var preview = $(".breadcrumb").before('<div class="b-publication-preview"></div>').prev();
                preview.append('<div class="page-header"><h1>' + $('#reviewform-title').val() + '</h1></div>');
                preview.append('<div class="b-publication">' + html + '</div>');
                $("html, body").animate({ scrollTop: 0 });
            }
        })
    });
JS
);
?>

<?php $form = ActiveForm::begin([
    'id' => 'boast-form',
    'options' => ['autocomplete' => 'off'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ]
    ],
]) ?>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'categoryId')->widget('app\widgets\Categorize', [
                'options' => [
                    'class' => 'form-control input-lg',
                    'prompt' => '',
                ],
            ]) ?>
        </div>
    </div>

    <?= $form->field($model, 'content', [
        'template' => "{label}\n{input}\n{hint}\n{error}\n{images}",
        'parts' => [
            '{images}' => Angular::directive('images-uploader', [
                'upload-url' => Url::to(['reviews/upload-image']),
                'template' => '#images-uploader-redactor-template',
            ]),
        ],
    ])->widget('app\widgets\Redactor', [
        'options' => ['rows' => 15],
    ]) ?>

    <p class="text-muted">
        Нажимая кнопку "Опубликовать обзор", вы подтверждаете свое согласие с
        <a href="http://alitrust.ru/forum/theme/392" target="_blank">правилами публикации обзоров</a>.
    </p>

    <button type="submit" class="btn btn-lg btn-primary">Опубликовать обзор</button>
    <button type="button" class="btn btn-link js-preview">Предпросмотр</button>

<?php $form->end() ?>
