<?php
use yii\helpers\Html;

/* @var $review app\db\records\Review */
/* @var $model  app\models\ReviewForm */
/* @var $this   yii\web\view */

$this->title = 'Редактирование обзора';
$this->params['breadcrumbs'][] = ['label' => 'Обзоры', 'url' => ['index']];
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render('_form', compact('model')) ?>
