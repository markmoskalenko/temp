<?php
use app\helpers\Url;
use app\widgets\PublicationList;

/* @var $category     app\db\records\Category */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         app\components\View */

$this->title = $category->name . ' - обзоры Алиэкспресс';
$this->params['breadcrumbs'][] = ['label' => 'Обзоры', 'url' => ['index']];

$this->params['showReviewCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1 class="page-header__title"><?= $category->name ?></h1>

        <div class="page-header__controls">
            <a class="page-header__btn btn btn-rounded btn-primary js-login-required" href="<?= Url::to(['create']) ?>">
                <span class="fa fa-fw fa-plus"></span>
                Добавить обзор
            </a>
        </div>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?= PublicationList::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/reviews/_item',
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
