<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $model app\db\records\Review */
/* @var $this  yii\web\View */

$review = $model;
?>

<?php if ($imageUrl = $review->present()->firstImage()): ?>
    <div class="publication-label-header">
        <div class="publication-label-header__container">
            <a class="publication-label-header__background publication-label-header__link" href="<?= Url::entity($review) ?>">
                <span class="publication-label-header__title"><?= Html::encode($review->title) ?></span>
            </a>
        </div>

        <a class="publication-label-header__image" href="<?= Url::entity($review) ?>" style="background-image: url(<?= $imageUrl ?>)"></a>
    </div>
<?php else: ?>
    <h1 class="publication-header">
        <a href="<?= Url::entity($review) ?>" class="publication-header__title publication-header__link"><?= Html::encode($review->title) ?></a>
    </h1>
<?php endif ?>

<?php if ($review->isRejected): ?>
    <div class="alert alert-danger">
        <h4>Данный обзор был отклонён модератором.</h4>
        <p>
            Вы можете отредактировать его или удалить. Наши модераторы периодически
            просматривают отклонённый контент и опубликуют его на сайте как только он станет удовлетворять
            <a class="alert-link" href="/forum/theme/392" target="_blank">правилам публикации обзоров</a>.
        </p>

        <p>
            Если вы не согласны с чем-либо,
            то всегда можете написать там через
            <a class="alert-link" href="<?= Url::to(['feedback/index', 'subject' => 'moderators']) ?>" target="_blank">форму обратной связи</a>.
        </p>

        <?php if ($review->rejectReason): ?>
            <br>
            <h4>Причина отклонения:</h4>
            <p><?= Yii::$app->formatter->asNtext($review->rejectReason) ?></p>
        <?php endif ?>
    </div>
<?php endif ?>

<div class="b-publication-teaser">
    <?= $review->present()->contentPreview() ?>
</div>

<div class="b-publication-meta">
    <div class="meta-info-group text-muted pull-left">
        <div class="meta-info-group__item meta-info">
            <img class="meta-info__icon b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($review->user, ['w' => 50]) ?>" />
            <a class="text-capitalize" href="<?= Url::to(['profile/view', 'id'=>$review->user->id]) ?>"><?= Html::encode($review->user->present()->name()) ?></a>
        </div>

        <div class="meta-info-group__item meta-info">
            <span class="meta-info__icon fa fa-calendar"></span>
            <?= Yii::$app->formatter->asDate($review->timeCreated, 'long') ?>
        </div>

        <div class="meta-info-group__item meta-info">
            <span class="meta-info__icon fa fa-heart text-primary"></span>
            <?= $review->present()->votesCount() ?>
        </div>

        <div class="meta-info-group__item meta-info">
            <span class="meta-info__icon fa fa-comment-o text-info"></span>
            <?= $review->present()->commentsCount() ?>
        </div>
    </div>

    <div class="meta-info-group pull-right">
        <?php if (Yii::$app->user->can('updateReview', ['review' => $review])): ?>
            <div class="meta-info-group__item meta-info">
                <a href="<?= Url::to(['update', 'id' => $review->id]) ?>">
                    <span class="meta-info__icon fa fa-fw fa-pencil"></span>
                </a>
            </div>
        <?php endif ?>

        <?php if (Yii::$app->user->can('deleteReview', ['review' => $review])): ?>
            <div class="meta-info-group__item meta-info">
                <a href="<?= Url::to(['delete', 'id' => $review->id]) ?>" data-method="post" data-confirm="Вы уверены?">
                    <span class="meta-info__icon fa fa-fw fa-trash"></span>
                </a>
            </div>
        <?php endif ?>
    </div>

    <div class="clearfix"></div>
</div>
