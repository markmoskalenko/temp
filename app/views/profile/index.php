<?php
use app\db\records\Notifications;
use app\models\ProfileForm;
use app\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $updatePasswordModel app\models\UpdatePasswordForm */
/* @var $changeEmailModel    app\models\ChangeEmailForm */
/* @var $profileModel        app\models\ProfileForm */
/* @var $this                yii\web\View */

$user = Yii::$app->getUser()->getIdentity();

$this->params['disableSearchArea'] = true;
$this->title = 'Профиль';

$this->registerJsFile('@web/js/frontend-profile-avatar.js', [
    'depends' => ['app\assets\AppAsset', 'app\assets\FileApiAsset'],
]);
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group field-profileform-avatarurl">
            <label for="user-avatarurl">Аватар</label>

            <label class="thumbnail b-upload-btn b-upload-thumb b-upload-thumb_state_hoverable" for="profileform-avatarurl">
                <span class="b-upload-thumb__hover">
                    <span class="b-upload-thumb__hover-inner">
                        <span class="b-upload-thumb__hover-icon fa fa-camera"></span>
                        <span class="b-upload-thumb__hover-text">Нажмите, чтобы&nbsp;изменить</span>
                    </span>
                </span>

                <img class="b-upload-thumb__image" src="<?= Url::avatar($user) ?>">
                <input class="b-upload-btn__input" type="file" name="file" id="profileform-avatarurl"/>
            </label>
        </div>
    </div>

    <div class="col-sm-8">
        <?= $form->field($profileModel, 'name')->textInput() ?>
        <?= $form->field($profileModel, 'city')->textInput() ?>

        <?= $form->field($profileModel, 'notifications')->checkboxList(Notifications::getTypes()); ?>

        <?php if ($profileModel->scenario === ProfileForm::SCENARIO_EMAILABLE): ?>
            <?= $form->field($profileModel, 'email')->input('email') ?>
        <?php endif ?>

        <?php if ($profileModel->scenario !== ProfileForm::SCENARIO_EMAILABLE): ?>
            <div class="form-group has-feedback profile-feedback-email">
                <label class="control-label">Электронная почта</label>
                <input class="form-control" disabled value="<?= Html::encode($user->email) ?>">
                <?php if ($user->emailConfirmation): ?>
                    <span class="form-control-feedback text-success"><i class="fa fa-check"></i></span>
                <?php else: ?>
                    <span class="form-control-feedback text-warning"><i class="fa fa-warning"></i> Не подтверждён</span>
                    <a href="<?= Url::to(['resend-email-confirmation']) ?>" class="profile-feedback-email-link">Отправить письмо ещё раз</a>
                <?php endif ?>
            </div>
        <?php endif ?>
    </div>
</div>

<hr>

<div class="profile-actions">
    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#change-email-modal">Сменить почту</button>
    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#change-password-modal">Сменить пароль</button>
    <button type="submit" class="btn btn-primary">Сохранить изменения</button>
</div>

<?php $form->end(); ?>

<div class="modal fade" id="change-email-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Смена электронной почты</h4>
            </div>

            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'options' => ['novalidate' => true],
            ]) ?>

            <div class="modal-body">
                <?= $form->field($changeEmailModel, 'email')->label('Введите новый адрес')->input('email') ?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-inverse" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
            </div>

            <?php $form->end() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="change-password-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Смена пароля</h4>
            </div>

            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
            ]) ?>

            <div class="modal-body">
                <?= $form->field($updatePasswordModel, 'password')->passwordInput() ?>
                <?= $form->field($updatePasswordModel, 'passwordRepeat')->passwordInput() ?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-inverse" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
            </div>

            <?php $form->end() ?>
        </div>
    </div>
</div>