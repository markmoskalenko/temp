<?php
use app\helpers\Url;
use yii\bootstrap\Nav;

/* @var $user         app\db\records\User */
/* @var $this         yii\web\View */

$this->title = 'Профиль пользователя - '.$user->name;
?>

<div class="page-header">
    <h1><?= $user->name ?></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <img src="<?= Url::avatar($user, ['w' => 150]) ?>" alt="" style="margin: 0 15px 15px 0; width: 150px"/>
            <br/>
            <?php if(!Yii::$app->user->isGuest && $user->id != Yii::$app->user->identity->id):?>
                <?php if($user->isSubscribe):?>
                    <a href="<?= Url::to(['profile/un-subscribe','id'=>$user->id]) ?>" class="btn btn-success">Отписаться</a>
                <?php else:?>
                    <a href="<?= Url::to(['profile/subscribe','id'=>$user->id]) ?>" class="btn btn-success">Подписаться</a>
                <?php endif;?>
            <?php endif;?>
        </div>
        <div class="pull-left">
            <div class="about_item"><span>Дата регистрации:</span> <?= Yii::$app->formatter->asDate($user->timeCreated) ?></div>
            <?php if ($user->city): ?>
                <div class="about_item"><span>Откуда:</span> <?= $user->city ?></div>
            <?php endif; ?>
            <div class="about_item"><span>Последний раз был:</span> <?= Yii::$app->formatter->asDate($user->timeVisited, 'dd.MM.yyyy HH:mm') ?></div>

            <br>

            <div class="statistic_item"><span>Жалоб:</span> <?= $user->countComplaint ?></div>
            <div class="statistic_item"><span>Хвастов:</span> <?= $user->countBoast ?></div>
            <div class="statistic_item"><span>Комментариев:</span> <?= $user->countComment ?></div>
        </div>
    </div>
</div>

<div class="page-divider page-divider_style_border"></div>

<?= Nav::widget([
    'options' => ['class' => 'nav-pills'],
    'items' => [
        [
            'url' => Url::to(['profile/boasts', 'id' => $user->id]),
            'active' => (Yii::$app->requestedRoute === 'profile/boasts' || Yii::$app->requestedRoute === 'profile/view'),
            'label' => 'Хвасты',
        ],
        [
            'url' => Url::to(['profile/complaints', 'id' => $user->id]),
            'active' => (Yii::$app->requestedRoute === 'profile/complaints'),
            'label' => 'Жалобы',
        ],
    ],
]) ?>

<br>