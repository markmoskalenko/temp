<?php
use app\assets\LikeButtonAsset;
use app\widgets\ComplaintList;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $user         app\db\records\User */
/* @var $this         yii\web\View */

LikeButtonAsset::register($this);
?>

<?= $this->render('view', compact('user')) ?>

<div class="page-header">
    <h2 class="page-header__title">Жалобы пользователя</h2>
</div>

<?= ComplaintList::widget([
    'dataProvider' => $dataProvider,
    'showSellerInfo' => true,
]) ?>