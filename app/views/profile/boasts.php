<?php
use app\assets\LikeButtonAsset;
use app\helpers\Url;
use app\widgets\BoastListView;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $user         app\db\records\User */
/* @var $this         yii\web\View */

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view', 'id' => $user->id])]);

LikeButtonAsset::register($this);
?>

<?= $this->render('view', compact('user')) ?>

<div class="page-header">
    <h2 class="page-header__title">Хвасты пользователя</h2>
</div>

<?= BoastListView::widget([
    'dataProvider' => $dataProvider,
]) ?>