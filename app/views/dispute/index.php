<?php
/* @var $this yii\web\View */

$this->title = 'Открытие спора на Алиэкспресс – генератор фотографий для спора';
$this->registerMetaTag(['name' => 'description', 'content' => 'Генератор фотографий для открытия спора на AliExpress. Создавайте фото для споров на Алиэкспресс с легкостью, а так же выделяйте моменты требующие особого внимания.' ]);
$this->params['disableSearchArea'] = true;

$this->registerJsFile('@web/js/frontend-dispute.js', [
    'depends' => ['yii\web\JqueryAsset', 'app\assets\AutosizeAsset', 'app\assets\Html2CanvasAsset', 'app\assets\SocialLikesAsset'],
]);
?>
<div class="page-header">
    <h1>Генератор фотографий для спора</h1>
</div>

<div class="b-dispute-text">
    <p>Выберите фотографии, которые вы хотите показать в споре.</p>
    <p>Слева прикрепите фото товара из магазина, каким его описывал продавец. Справа — фото товара, который вы получили.<br>
        С помощью мышки вы сможете выделить красным овалом область, заслуживающую особого внимания.</p>
</div>

<div class="b-dispute-social">
    <div class="social-likes social-likes_light">
        <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
        <div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
        <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
        <div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
    </div>
</div>

<table class="b-dispute-table table table-bordered">
    <tr>
        <th class="b-dispute-table__header">Described</th>
        <th class="b-dispute-table__header">Received</th>
    </tr>
    <tr>
        <td class="b-dispute-table__easel">
            <canvas class="b-dispute-table__canvas" width="550" style="display: none"></canvas>

            <div class="b-dispute-table__file-uploader">
                <span class="btn btn-default btn-file">
                    Выберите изображение <input type="file">
                </span>
            </div>
        </td>

        <td class="b-dispute-table__easel">
            <canvas class="b-dispute-table__canvas" width="550" style="display: none"></canvas>

            <div class="b-dispute-table__file-uploader">
                <span class="btn btn-default btn-file">
                    Выберите изображение <input type="file">
                </span>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="b-dispute-table__editor">
            <textarea class="b-dispute-table__editor-input" rows="1" placeholder="Введите ваши замечания по внешнему виду товара"></textarea>
        </td>
    </tr>
</table>

<div class="b-dispute-help help-block">
    Администрация AliExpress требует прикладывать замечания на английском языке. В этом вам помогут переводчики от
    <a href="https://translate.google.ru/" target="_blank">Google</a> или
    <a href="https://translate.yandex.ru" target="_blank">Яндекс</a>.
</div>

<div class="text-center">
    <button class="btn btn-primary" id="create" onclick="ga('send', 'event', 'dispute', 'click', 'create')">Создать изображение</button>
</div>

<div class="modal fade" id="dispute-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Фотография готова!</h4>
            </div>

            <div class="modal-body">
                <img class="img-responsive" src="data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <a class="btn btn-primary" id="save" href="#" onclick="ga('send', 'event', 'dispute', 'click', 'download')">Скачать фотографию</a>
            </div>
        </div>
    </div>
</div>
<br /><br />
<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

<script type="text/javascript">
    VK.init({apiId: 4780725, onlyWidgets: true});
</script>

<!-- Put this div tag to the place, where the Comments block will be -->
<div id="vk_comments"></div>
<script type="text/javascript">
    VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"});
</script>