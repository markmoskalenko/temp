<?php
use yii\helpers\Url;
/* @var $this   yii\web\View */
?>

<div class="modal fade" id="creator-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Добавить...</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <a class="btn btn-lg btn-block btn-default" href="<?= Url::to(['/boasts/create']) ?>">
                            Фото-хваст
                        </a>

                        <a class="btn btn-lg btn-block btn-default" href="<?= Url::to(['/complaints/create']) ?>">
                            Жалобу на продавца
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>