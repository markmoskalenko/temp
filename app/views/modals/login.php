<?php
use app\widgets\AuthChoice;
use yii\helpers\Url;

/* @var $widget app\widgets\LoginModal */
/* @var $this   yii\web\View */
?>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Это действие требует авторизацию</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= AuthChoice::widget([
                            'prefix' => 'Войти через ',
                            // 'redirect' => ['/complaints/create'],
                        ]) ?>

                        <div class="auth-clients">
                            <a class="btn btn-default" href="<?= Url::to(['@login']) ?>">
                                <i class="auth-icon fa fa-at fa-fw"></i>
                                <span class="auth-title">Войти через E-mail</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>