<?php
/* @var $widget app\widgets\ChromeExtensionModal */
/* @var $this   yii\web\View */
?>

<div class="modal fade b-chrome-ext-modal" id="chrome-extension-modal">
    <div class="modal-dialog b-chrome-ext-modal__dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Полезное расширение AliExpress для Chrome</h4>
            </div>

            <div class="b-chrome-ext-modal__body modal-body">
                <div class="b-chrome-ext-modal__steps">
                    <div class="b-chrome-ext-modal__step b-chrome-ext-modal__step_no_1">
                        <div>Проверка продавцов</div>
                        <div class="text-muted">Моментальная проверка продавцов по базе отзывов AliTrust.ru уведомит вас о найденных отзывах</div>
                    </div>

                    <div class="b-chrome-ext-modal__step b-chrome-ext-modal__step_no_2">
                        <div>Удобный поиск и быстрое меню</div>
                        <div class="text-muted">Моментальный поиск по товарам, а так же быстрый доступ к самым популярным разделам на AliExpress</div>
                    </div>
                </div>
            </div>

            <div class="b-chrome-ext-modal__body modal-body text-center">
                <a href="https://chrome.google.com/webstore/detail/aliexpress-tools/eenflijjbchafephdplkdmeenekabdfb"
                   class="b-chrome-ext-modal__install b-chrome-ext-modal__install_browser_chrome btn btn-lg btn-success js-install"
                   target="_blank">
                    Установить для Chrome
                </a>

                <div class="b-chrome-ext-modal__refuse">
                    <a class="border-link border-link-dashed js-refuse" href="#" data-dismiss="modal">Нет, я не хочу крутых возможностей!</a>
                </div>
            </div>
        </div>
    </div>
</div>