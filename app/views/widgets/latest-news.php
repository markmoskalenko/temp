<?php
/* @var $this yii\web\View */
?>

<div class="b-latest-news" ng-controller="LatestNewsListCtrl">
    <div class="panel panel-default">
        <div class="panel-body">
            <h4 class="b-latest-news__header">ЧТО У НАС НОВОГО</h4>

            <div class="b-latest-news__control">
                <ul class="nav nav-pills nav-sm">
                    <li ng-class="{active:activeType==1}">
                        <a href="javascript:void(0)" ng-click="onSetActiveType(1)">Публикации</a>
                    </li>

                    <li ng-class="{active:activeType==2}">
                        <a href="javascript:void(0)" ng-click="onSetActiveType(2)">Комментарии</a>
                    </li>
                </ul>
            </div>

            <ul class="b-latest-news-list ng-cloak" ng-show="ready">
                <li class="b-latest-news-list__item" ng-repeat="item in data">
                    <strong ng-bind-html="item.title" class="user ng-cloak" ng-cloak></strong>
                    <em class="right_arrow">→</em>
                    <span ng-bind-html="item.text" class="text ng-cloak" ng-cloak></span>
                </li>
            </ul>

            <div class="b-latest-news__empty ng-cloak" ng-show="!data.length && ready">Нет записей.</div>

            <div class="text-center">
                <img ng-show="!ready" width="150" src="/images/loading.gif" alt="Загрузка"/>
            </div>
        </div>
    </div>
</div>