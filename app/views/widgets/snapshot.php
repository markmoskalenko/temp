<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $snapshot app\db\records\Snapshot */
/* @var $this     yii\web\View */
?>

<a class="b-product-widget" target="_blank" href="<?= Url::cpa('http://www.aliexpress.com/snapshot/'.$snapshot->snapshotId.'.html', 'products') ?>">
    <img class="b-product-widget__image" src="<?= Html::encode($snapshot->imageUrl) ?>">
    <h3 class="b-product-widget__title"><?= Html::encode($snapshot->productName) ?></h3>
    <div class="b-product-widget__info">
        <span class="b-product-widget__price">US $<?= Html::encode($snapshot->price) ?></span>
        <span class="b-product-widget__unit"><?= Html::encode($snapshot->unit) ?></span>
    </div>
</a>
