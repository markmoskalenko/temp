<?php
/* @var $this      yii\web\View */
/* @var $inputName string */
?>

<script type="text/html" id="images-uploader-template">
    <div class="b-upload-dnd js-dnd ng-cloak" ng-class="{ 'b-upload-dnd_state_hover': dropHover }" ng-hide="images.length">
        <div class="b-upload-dnd__text" ng-hide="dropHover">Перетащите сюда свои фотографии</div>
        <div class="b-upload-dnd__text ng-cloak" ng-show="dropHover">Отпустите для начала загрузки</div>
        <div class="b-upload-dnd__divider">или</div>

        <label class="b-upload-btn btn btn-primary">
            <span class="b-upload-btn__label">Выберите файлы</span>
            <input type="file" name="file" class="b-upload-btn__input" />
        </label>
    </div>

    <div class="b-upload-thumb-list ng-cloak" ng-show="images.length">
        <div class="b-upload-thumb-list__item" ng-repeat="image in images">
            <a href="#" class="thumbnail b-upload-thumb b-upload-thumb_state_hoverable" ng-click="remove(image)" onclick="return false">
                <input type="hidden" name="<?= $inputName.'[]' ?>" value="{{ image.id }}" />
                <span class="b-upload-thumb__image" ng-style="{'background-image':'url('+image.url+')'}"></span>

                <span class="b-upload-thumb__hover">
                    <span class="b-upload-thumb__hover-inner">
                        <span class="b-upload-thumb__hover-icon fa fa-trash-o"></span>
                        <span class="b-upload-thumb__hover-text">Нажмите, чтобы&nbsp;удалить</span>
                    </span>
                </span>
            </a>
        </div>
    </div>

    <p class="help-block ng-cloak" ng-show="uploading">
        <span class="fa fa-spinner fa-spin"></span>
        Идёт загрузка фотографий
    </p>

    <label class="b-upload-btn b-upload-btn_style_link" ng-hide="uploading || images.length == 0">
        <span class="b-upload-btn__label">Добавить фотографии</span>
        <input type="file" name="file" class="b-upload-btn__input" />
    </label>
</script>