<?php
/* @var $widget app\widgets\Redactor */
/* @var $this   yii\web\View */
/* @var $input  string */
?>

<div class="wysiwyg">
    <div class="wysiwyg-panel">
        <div class="wysiwyg-wrapper">
            <div class="wysiwyg-button wysiwyg-dropdown">
                <span class="fa fa-header"></span>
                <select class="with-title" name="h" onchange="redactor.insertTagFromDropBox(this);" tabindex="-1">
                    <option value="" class="title">Заголовки:</option>
                    <option value="h1">Заголовок</option>
                    <option value="h2">Подзаголовок</option>
                    <option value="h3">Подподзаголовок</option>
                </select>
            </div>

            <a class="wysiwyg-button" title="Жирный" onclick="return redactor.insertTagWithText(this, 'b');" href="#" tabindex="-1">
                <span class="fa fa-bold"></span>
            </a>

            <a class="wysiwyg-button" title="Курсив" onclick="return redactor.insertTagWithText(this, 'i');" href="#" tabindex="-1">
                <span class="fa fa-italic"></span>
            </a>

            <a class="wysiwyg-button" title="Подчёркнутый" onclick="return redactor.insertTagWithText(this, 'u');" href="#" tabindex="-1">
                <span class="fa fa-underline"></span>
            </a>

            <a class="wysiwyg-button" title="Зачёркнутый" onclick="return redactor.insertTagWithText(this, 's');" href="#" tabindex="-1">
                <span class="fa fa-strikethrough"></span>
            </a>

            <a class="wysiwyg-button" title="Цитата" onclick="return redactor.insertTagWithText(this, 'quote');" href="#" tabindex="-1">
                <span class="fa fa-quote-left"></span>
            </a>

            <div class="wysiwyg-button wysiwyg-dropdown">
                <span class="fa fa-list"></span>
                <select name="list" onchange="redactor.insertList(this);" onclick="return false;" tabindex="-1">
                    <option value="" class="title">Список:</option>
                    <option value="ul">UL LI</option>
                    <option value="ol">OL LI</option>
                </select>
            </div>

            <a class="wysiwyg-button" title="Вставить ссылку" onclick="return redactor.insertLink(this);" href="#" tabindex="-1">
                <span class="fa fa-link"></span>
            </a>

            <a class="wysiwyg-button" title="Вставить видео" onclick="return redactor.insertTagWithText(this, 'video');" href="#" tabindex="-1">
                <span class="fa fa-video-camera"></span>
            </a>

            <a class="wysiwyg-button" title="Вставить спойлер" onclick="return redactor.insertSpoiler(this);" href="#" tabindex="-1">
                <span class="fa fa-certificate"></span>
            </a>

            <a class="wysiwyg-button" title="Кат" onclick="return redactor.insertCut(this);" href="#" tabindex="-1">
                <span class="fa fa-cut"></span>
            </a>
        </div>
    </div>

    <div class="wysiwyg-input">
        <?= $input ?>
    </div>
</div>

<script type="text/html" id="images-uploader-redactor-template">
    <div class="b-upload-thumb-list ng-cloak" ng-show="images.length">
        <div class="b-upload-thumb-list__item" ng-repeat="image in images">
            <a href="#" class="thumbnail b-upload-thumb b-upload-thumb_state_hoverable" ng-click="select(image)" onclick="return false">
                <span class="b-upload-thumb__image" ng-style="{'background-image':'url('+image.url+')'}"></span>

                <span class="b-upload-thumb__hover">
                    <span class="b-upload-thumb__hover-inner">
                        <span class="b-upload-thumb__hover-icon fa fa-plus"></span>
                        <span class="b-upload-thumb__hover-text">Нажмите, чтобы добавить в&nbsp;текст</span>
                    </span>
                </span>
            </a>
        </div>
    </div>

    <p class="help-block ng-cloak" ng-show="uploading">
        <span class="fa fa-spinner fa-spin"></span>
        Идёт загрузка фотографий
    </p>

    <p>
        <label class="b-upload-btn b-upload-btn_style_link" ng-hide="uploading">
            <span class="b-upload-btn__label">Добавить фотографии</span>
            <input type="file" name="file" class="b-upload-btn__input" />
        </label>

        <span class="b-upload-btn-text">только JPG, JPEG, PNG, GIF до 10 мб</span>
    </p>
</script>
