<?php
use app\db\records\Seller;
use app\widgets\ComparingRating;
use app\widgets\HelpIcon;
use app\widgets\SellerScore;
use app\widgets\StarsRating;
use yii\helpers\Html;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">Информация о продавце</div>
    </div>
    <div class="panel-body">
        <dl class="dl-seller dl-horizontal" style="margin-bottom: 0">
            <dt>Общий рейтинг продавца</dt>
            <dd><?= SellerScore::widget(['seller' => $seller]) ?></dd>

            <dt>Продавец работает с</dt>
            <dd><?= Yii::$app->formatter->asDate($seller->registerDate, 'd MMMM Y г.') ?></dd>

            <dt>Местоположение продавца</dt>
            <dd><?= Html::encode($seller->getFullAddress()) ?></dd>
        </dl>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= HelpIcon::widget([
            'text' => 'Суммарные рейтинги продавца на основе всех отзывов покупателей за последние 6 месяцев.',
        ]) ?>

        <div class="panel-title">Детальный рейтинг продавца</div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Рейтинг соответствия полученного товара его описанию. Основан на отзывах реальных покупателей.',
                    ]) ?>

                    Соответствие описанию
                </td>
                <td>
                    <?= StarsRating::widget([
                        'options' => ['tag' => 'span', 'class' => 'rating'],
                        'value' => $seller->getDetailed(Seller::CATEGORY_ITEM_AS_DESCRIBED, Seller::LABEL_VALUE),
                    ]) ?>

                    <strong>
                        <?= $seller->getDetailed(Seller::CATEGORY_ITEM_AS_DESCRIBED, Seller::LABEL_VALUE) ?>
                    </strong>

                    <span>(<?= $seller->getDetailed(Seller::CATEGORY_ITEM_AS_DESCRIBED, Seller::LABEL_REVIEW_COUNT, 'integer') ?>)</span>
                </td>
                <td>
                    <?= ComparingRating::widget([
                        'value' => $seller->getDetailed(Seller::CATEGORY_ITEM_AS_DESCRIBED, Seller::LABEL_COMPARING),
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Рейтинг общительности продавца. Основан на отзывах покупателей о том, как продавец охотно идет на связь со своими покупателями.',
                    ]) ?>

                    Общительность продавца
                </td>
                <td>
                    <?= StarsRating::widget([
                        'options' => ['tag' => 'span', 'class' => 'rating'],
                        'value' => $seller->getDetailed(Seller::CATEGORY_COMMUNICATION, Seller::LABEL_VALUE),
                    ]) ?>

                    <strong>
                        <?= $seller->getDetailed(Seller::CATEGORY_COMMUNICATION, Seller::LABEL_VALUE) ?>
                    </strong>

                    <span>
                        (<?= $seller->getDetailed(Seller::CATEGORY_COMMUNICATION, Seller::LABEL_REVIEW_COUNT, 'integer') ?>)
                    </span>
                </td>
                <td>
                    <?= ComparingRating::widget([
                        'value' => $seller->getDetailed(Seller::CATEGORY_COMMUNICATION, Seller::LABEL_COMPARING),
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Рейтинг скорости отправки товара продавцом с момента заказа этого товара покупателем. Основан на отзывах реальных покупателей.',
                    ]) ?>

                    Скорость отправки
                </td>
                <td>
                    <?= StarsRating::widget([
                        'options' => ['tag' => 'span', 'class' => 'rating'],
                        'value' => $seller->getDetailed(Seller::CATEGORY_SHIPPING_SPEED, Seller::LABEL_VALUE),
                    ]) ?>

                    <strong>
                        <?= $seller->getDetailed(Seller::CATEGORY_SHIPPING_SPEED, Seller::LABEL_VALUE) ?>
                    </strong>

                    <span>
                        (<?= $seller->getDetailed(Seller::CATEGORY_SHIPPING_SPEED, Seller::LABEL_REVIEW_COUNT, 'integer') ?>)
                    </span>
                </td>
                <td>
                    <?= ComparingRating::widget([
                        'value' => $seller->getDetailed(Seller::CATEGORY_SHIPPING_SPEED, Seller::LABEL_COMPARING),
                    ]) ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= HelpIcon::widget([
            'options' => ['data-html' => 'true'],
            'text' => '<p>Рейтинг отзывов от покупателей о магазине с момента регистрации продавца на AliExpress.</p>
                       <p>Если покупатель в течении 10 дней оставил несколько отзывов, то они считаются за один.</p>',
        ]) ?>

        <div class="panel-title">История продавца</div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th>Отзывы</th>
                <th>1 месяц</th>
                <th>3 месяца</th>
                <th>6 месяцев</th>
                <th>12 месяцев</th>
                <th>Всего</th>
            </tr>
            <tr>
                <th>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Если покупатель указал 4-5 звезд',
                    ]) ?>

                    Позитивные
                </th>
                <td><?= $seller->getHistory(Seller::HISTORY_POSITIVE, Seller::PERIOD_MONTH, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_POSITIVE, Seller::PERIOD_3_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_POSITIVE, Seller::PERIOD_6_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_POSITIVE, Seller::PERIOD_12_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_POSITIVE, Seller::PERIOD_OVERALL, 'integer') ?></td>
            </tr>
            <tr>
                <th>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Если покупатель указал 3 звезды',
                    ]) ?>

                    Хорошие
                </th>
                <td><?= $seller->getHistory(Seller::HISTORY_NEUTRAL, Seller::PERIOD_MONTH, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEUTRAL, Seller::PERIOD_3_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEUTRAL, Seller::PERIOD_6_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEUTRAL, Seller::PERIOD_12_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEUTRAL, Seller::PERIOD_OVERALL, 'integer') ?></td>
            </tr>
            <tr>
                <th>
                    <?= HelpIcon::widget([
                        'options' => ['class' => 'help-icon-sm'],
                        'text' => 'Если покупатель указал 1-2 звезды',
                    ]) ?>

                    Негативные
                </th>
                <td><?= $seller->getHistory(Seller::HISTORY_NEGATIVE, Seller::PERIOD_MONTH, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEGATIVE, Seller::PERIOD_3_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEGATIVE, Seller::PERIOD_6_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEGATIVE, Seller::PERIOD_12_MONTHS, 'integer') ?></td>
                <td><?= $seller->getHistory(Seller::HISTORY_NEGATIVE, Seller::PERIOD_OVERALL, 'integer') ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
