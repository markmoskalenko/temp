<?php
use app\models\SellerSearchForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Проверка продавца AliExpress. Продавцы на алиэкспресс. Seller Check';
$this->registerMetaTag(['name' => 'description', 'content' => 'Быстрая и удобная проверка продавцов на алиэкспресс. Достаточно просто вставить ссылку в Seller Check на товар или продавца. Черный список алиэкспресс.']);

$this->params['disableSearchArea'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>
    <div class="page-header">
        <h1 class="page-header__title">Проверка продавца AliExpress (Seller Check)</h1>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'form-seller-search',
        'action' => ['/site/search'],
        'options' => ['class' => 'js-loadable-form clearfix', 'autocomplete' => 'off'],
        'enableAjaxValidation' => true,
        'validateOnChange' => false,
    ]) ?>

    <?= $form->field(new SellerSearchForm(), 'link', [
        'options' => ['class' => 'search-form-group form-group'],
        'enableLabel' => false,
        'inputOptions' => [
            'class' => 'form-control input-lg',
            'placeholder' => 'Скопируйте сюда ссылку на вашего продавца или товар',
        ],
    ]) ?>

        <button class="search-form-btn btn btn-block btn-lg btn-inverse" type="submit" data-toggle="disable">
            <span class="fa fa-circle-o-notch fa-spin js-loadable-hunky hidden"></span>
            <span class="js-loadable-idler">Проверить</span>
        </button>

    <?php $form->end() ?>

    <div class="b-publication">
        <p>Вы часто совершаете покупки на сайте Aliexpress (Алиэкспресс) и задумываетесь о надёжности продавца? Теперь это в прошлом! Специально для Вас мы подготовили бесплатный сервис Seller Check, который поможет Вам совершить проверку продавца на Алиэкспресс и оставить о нем отзыв.</p>
        <p>Как можно воспользоваться сервисом? Всё что от вас требуется это перед покупкой зайти к нам на сайт, скопировать ссылку на товар (или же продавца) и вставить в форму выше, после чего наша система проанализировав множество факторов (статус продавца, количество сделок, срок регистрации, отзывы о продавце и т.д.) выдаст Вам факты о продавце Aliexpress в удобном виде. Факты продавца подскажут насколько можно доверять тому или иному продавцу, приходит ли заявленный товар, нет ли брака. Согласитесь, лучше перестраховаться сейчас, чем с ужасом открывать полученную на почте посылку с АлиЭкспресс и жалеть о впустую потраченных деньгах.</p>
        <p>Также не забывайте оставлять отзывы о продавцах на нашем сайте, этим вы поможете предотвратить обман со стороны продавца! Выведем всех продавцов на "чистую воду"! Соберем огромную базу отзывов Алиэкспресс и тем самым обезопасим свои будущие покупки.</p>
        <p>Удачных Вам покупок и пусть шоппинг на Aliexpress будет только в радость!</p>

        <div class="b-chrome-ext-modal__body modal-body text-center">
            <a href="https://chrome.google.com/webstore/detail/aliexpress-tools/eenflijjbchafephdplkdmeenekabdfb"
               class="b-chrome-ext-modal__install b-chrome-ext-modal__install_browser_chrome btn btn-lg btn-success js-install"
               target="_blank">
                Установить Seller Check для Chrome
            </a>
        </div>

    </div>

<?php $this->endContent() ?>