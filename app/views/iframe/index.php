<?php
use app\widgets\Magic;
/* @var $this yii\web\View */
?>

<!-- VK Retargeting -->
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=UFx1*4MLikFMfVbxBoGUi1WwfO2F5JElfzznX8cLK*2w7yCN5afG0Zr4Pmmnf9Ec23NrLoFWkqoTQPdgctXW20UlsQEog2EkCr0JSLVP0Js0BpFHPFRbFsnVmiaOff4Xg8jH5SZzmnKUrfW/TI9abxQde*m67oSpGuXjcqEZ2qY-';</script>
<!-- /VK Retargeting -->

<!-- FB Retargeting -->
<script>
    (function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
        _fbq.push(['addPixelId', '337696726405147']);
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=337696726405147&amp;ev=PixelInitialized" /></noscript>
<!-- /FB Retargeting -->

<?php if (Yii::$app->request->referrer): ?>
    <?= Magic::widget() ?>
<?php endif ?>