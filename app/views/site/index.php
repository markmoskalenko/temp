<?php
use app\assets\FotoramaAsset;
use app\db\records\Article;
use app\db\records\Complaint;
use app\db\records\News;
use app\db\records\Review;
use app\helpers\Url;
use app\widgets\BoastListView;
use app\widgets\SellerScore;
use app\widgets\StarsRating;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;

/* @var $boastDataProvider     yii\data\DataProviderInterface */
/* @var $complaintDataProvider yii\data\DataProviderInterface */
/* @var $this                  yii\web\View */

$this->title = 'АлиТраст – мы делимся доверием. Сообщество покупателей АлиЭкспресс';
$this->registerMetaTag(['name' => 'description', 'content' => 'Мы объединяем всех, кто покупает на AliExpress, в одном месте собираем покупательский опыт, информацию о тех, кому стоит доверять, а кому не стоит']);

FotoramaAsset::register($this);
?>

<div class="stripe">
    <div class="container">
        <?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

        <div class="page-header">
            <a class="page-header__title" href="<?= Url::to(['news/index']) ?>">Новости и события</a>

            <div class="page-header__controls">
                <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['feedback/index', 'subject' => 'content']) ?>">
                    <span class="fa fa-fw fa-file-text-o"></span>
                    Предложить новость
                </a>
            </div>
        </div>

        <div class="publication-widget-group">
            <?php if ($news = News::find()->random()->published()->featured()->one()): ?>
                <?php /* @var $news News */ ?>
                <a class="publication-widget-group__item publication-widget publication-widget_size_big"
                   href="<?= Url::entity($news) ?>" style="background-image: url(<?= Url::uploaded($news->mainImage) ?>)">
                    <div class="publication-widget__title"><?= Html::encode($news->title) ?></div>

                    <!--
                    <div class="publication-widget__description">Posti Finland Economy популярен тем, что предлагает пользователям доставку товаров «до двери». Это очень удобно, т.к. позволяет экономить время</div>
                    -->

                    <div class="publication-widget__meta meta-info-group">
                        <div class="meta-info-group__item meta-info meta-info_style_danger">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-eye"></span>
                            <span class="meta-info__text"><?= $news->viewCount ?></span>
                        </div>
                        <div class="meta-info-group__item meta-info meta-info_style_warning">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-comment"></span>
                            <span class="meta-info__text"><?= $news->commentCount ?></span>
                        </div>
                    </div>
                </a>
            <?php endif ?>
        </div>

        <div class="publication-widget-group publication-widget-group_columns_3">
            <?php foreach (News::find()->last()->published()->notFeatured()->limit(3)->all() as $publication): ?>
                <?php /* @var $publication News */ ?>
                <a class="publication-widget-group__item publication-widget publication-widget_size_small"
                   href="<?= Url::entity($publication) ?>" style="background-image: url(<?= Url::uploaded($publication->mainImage) ?>)">
                    <div class="publication-widget__title"><?= Html::encode($publication->title) ?></div>
                    <div class="publication-widget__date"><?= Yii::$app->formatter->asDate($publication->timePublished, 'dd MMMM YYYY') ?></div>
                    <div class="publication-widget__meta meta-info-group">
                        <div class="meta-info-group__item meta-info meta-info_style_danger">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-eye"></span>
                            <span class="meta-info__text"><?= $publication->viewCount ?></span>
                        </div>
                        <div class="meta-info-group__item meta-info meta-info_style_warning">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-comment"></span>
                            <span class="meta-info__text"><?= $publication->commentCount ?></span>
                        </div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>

        <div class="page-divider page-divider_style_border"></div>

        <div class="page-header">
            <a class="page-header__title" href="<?= Url::to(['reviews/index']) ?>">Обзоры</a>

            <div class="page-header__controls">
                <a class="page-header__btn btn btn-rounded btn-primary js-login-required" href="<?= Url::to(['reviews/create']) ?>">
                    <span class="fa fa-fw fa-plus"></span>
                    Добавить обзор
                </a>
            </div>
        </div>

        <div class="review-widget-group review-widget-group--columns-3">
            <?php foreach (Review::find()->last()->featured()->visible()->limit(3)->each() as $review): ?>
                <?php /* @var $review Review */ ?>

                <div class="review-widget-group__item">
                    <a class="review-widget" href="<?= Url::entity($review) ?>" style="background-image: url(<?= $review->present()->firstImage() ?>)">
                        <img class="review-widget__avatar" src="<?= Url::avatar($review->user, ['w' => 80, 'h' => 80, 'fit' => 'crop']) ?>" alt="<?= Html::encode($review->user->name) ?>"/>
                        <div class="review-widget__content">
                            <div class="review-widget__title"><?= Html::encode($review->title) ?></div>
                            <div class="review-widget__author"><?= Html::encode($review->user->name) ?></div>
                        </div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>

        <div class="page-divider page-divider_style_border"></div>

        <div class="page-header">
            <a class="page-header__title" href="<?= Url::to(['boasts/index']) ?>">Хвасты</a>

            <div class="page-header__nav">
                <ul class="list-inline list-inline_style_wide">
                    <li><a class="page-header__nav-link" href="http://alitrust.ru/boasts/odezhda-i-obuv">Одежда и обувь</a></li>
                    <li class="visible-lg-inline-block"><a class="page-header__nav-link" href="http://alitrust.ru/boasts/bizhuteriya-i-chasy">Бижутерия и часы</a></li>
                    <li>
                        <div class="dropdown">
                            <a class="page-header__nav-link" href="#" data-toggle="dropdown">Категории <span class="fa fa-angle-down"></span></a>
                            <?= Dropdown::widget([
                                'options' => ['style' => 'margin-top: 10px'],
                                'items' => [
                                    ['label' => 'Одежда и обувь', 'url' => 'http://alitrust.ru/boasts/odezhda-i-obuv'],
                                    ['label' => 'Бижутерия и часы', 'url' => 'http://alitrust.ru/boasts/bizhuteriya-i-chasy'],
                                    ['label' => 'Телефоны и компьютеры', 'url' => 'http://alitrust.ru/boasts/telefony-i-kompyutery'],
                                    ['label' => 'Сумки и аксессуары', 'url' => 'http://alitrust.ru/boasts/sumki-i-aksessuary'],
                                    ['label' => 'Спорт, хобби и развлечения', 'url' => 'http://alitrust.ru/boasts/sport-khobbi-i-razvlecheniya'],
                                    ['label' => 'Для взрослых (18+)', 'url' => 'http://alitrust.ru/boasts/adult18'],
                                    ['label' => 'Другое', 'url' => 'http://alitrust.ru/boasts/other'],
                                ],
                            ]) ?>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="page-header__controls">
                <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['boasts/create']) ?>">
                    <span class="fa fa-fw fa-camera"></span>
                    Добавить фото-хваст
                </a>
            </div>
        </div>

        <div class="block-fog" style="height: 650px">
            <?= BoastListView::widget([
                'dataProvider' => $boastDataProvider,
                'layout' => '{items}',
            ]) ?>
        </div>

        <br>

        <div class="text-center">
            <a class="btn btn-rounded btn-primary" href="<?= Url::to(['boasts/index']) ?>">Ещё хвасты</a>
        </div>

        <div class="page-divider page-divider_style_border"></div>

        <div class="page-header">
            <a class="page-header__title" href="<?= Url::to(['articles/index']) ?>">Статьи</a>

            <div class="page-header__nav">
                <ul class="list-inline list-inline_style_wide">
                    <li><a class="page-header__nav-link" href="http://alitrust.ru/articles/znakomstvo-s-aliexpress">Новичкам</a></li>
                    <li><a class="page-header__nav-link" href="http://alitrust.ru/articles/gid-po-razmeram">Гид по размерам</a></li>
                    <li><a class="page-header__nav-link" href="http://alitrust.ru/articles/otslezhivanie-posylok">Отслеживание посылок</a></li>
                </ul>
            </div>
        </div>

        <div class="publication-widget-group publication-widget-group_columns_3">
            <?php foreach (Article::find()->last()->published()->limit(3)->all() as $publication): ?>
                <?php /* @var $publication News */ ?>
                <a class="publication-widget-group__item publication-widget publication-widget_size_small"
                   href="<?= Url::entity($publication) ?>" style="background-image: url(<?= Url::uploaded($publication->mainImage) ?>)">
                    <div class="publication-widget__title"><?= Html::encode($publication->title) ?></div>
                    <div class="publication-widget__date"><?= Yii::$app->formatter->asDate($publication->timePublished, 'dd MMMM YYYY') ?></div>
                    <div class="publication-widget__meta meta-info-group">
                        <div class="meta-info-group__item meta-info meta-info_style_danger">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-eye"></span>
                            <span class="meta-info__text"><?= $publication->viewCount ?></span>
                        </div>
                        <div class="meta-info-group__item meta-info meta-info_style_warning">
                            <span class="publication-widget__meta-icon meta-info__icon fa fa-comment"></span>
                            <span class="meta-info__text"><?= $publication->commentCount ?></span>
                        </div>
                    </div>
                </a>
            <?php endforeach ?>
        </div>

        <div class="page-divider page-divider_style_border"></div>

        <div class="page-header">
            <a class="page-header__title" href="<?= Url::to(['complaints/index']) ?>">Жалобы</a>

            <div class="page-header__controls">
                <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['complaints/create']) ?>">
                    <span class="fa fa-plus"></span>
                    Добавить жалобу
                </a>
            </div>
        </div>

        <?php /* @var $complaint Complaint */ ?>
        <div class="fotorama" data-width="100%" data-height="285" data-arrows="false" data-loop="true" data-autoplay="true">
            <?php foreach ($complaintDataProvider->getModels() as $complaint): ?>
                <div>
                    <a class="complaint-widget" href="<?= Url::entity($complaint) ?>" style="height: 285px">
                        <div class="complaint-widget__title"><?= Html::encode($complaint->seller->storeName) ?></div>
                        <div class="complaint-widget__rating">Рейтинг: <?= SellerScore::widget(['seller' => $complaint->seller]) ?></div>
                        <?= StarsRating::widget(['options' => ['class' => 'complaint-widget__stars'], 'value' => $complaint->seller->getSummaryRating()]) ?>
                        <div class="complaint-widget__content"><?= Html::encode($complaint->text) ?></div>
                        <div class="complaint-widget__author">
                            <img class="complaint-widget__avatar" src="<?= Url::avatar($complaint->user, ['w' => 40, 'h' => 40, 'fit' => 'crop']) ?>">
                            <?= Html::encode($complaint->user->name) ?>
                        </div>
                        <div class="complaint-widget__cover complaint-widget__map"></div>
                        <div class="complaint-widget__cover complaint-widget__bg-black"></div>
                        <div class="complaint-widget__cover complaint-widget__bg-red complaint-widget__hoverable"></div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>

        <br>

        <div class="text-center">
            <a class="btn btn-rounded btn-primary" href="<?= Url::to(['complaints/index']) ?>">Ещё жалобы</a>
        </div>

        <?php $this->endContent() ?>
    </div>
</div>

<div class="stripe stripe-gray stripe-spy">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="page-header">
                    <h1 class="page-header__title">Что такое AliTrust?</h1>
                </div>

                <p>
                    Каждый человек делает что-то лучше других. Мы, например, лучше других разбираемся в покупках на
                    AliExpress.com. Что купить, как, а главное, у кого – на эти вопросы мы отвечаем каждый день, чтобы
                    вы не волновались и не рисковали, а совершали только выгодные и честные покупки.
                </p>

                <p>
                    Суть AliTrust в том, что его создают реальные покупатели. Мы заказываем товары, тестируем их,
                    изучаем продавцов, ищем выгодные сделки и предложения. Наша цель – развивать не просто сайт, где
                    можно хвастаться приобретенными вещами, рассказывать о неудачах, советоваться и задавать вопросы. Мы
                    объединяем всех, кто покупает на AliExpress, в одном месте собираем покупательский опыт,
                    информацию о тех, кому стоит доверять, а кому не стоит.
                </p>
            </div>
        </div>
    </div>
</div>





