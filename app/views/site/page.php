<?php
use app\helpers\RedactorHtmlPurifier;
use yii\helpers\Html;

/* @var $page app\db\records\Page */
/* @var $this yii\web\View */

$this->params['disableSearchArea'] = true;
$this->title = $page->seoTitle ?: $page->name;
if ($page->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $page->seoDescription]);
}
?>

<div class="page-header">
    <h1><?= Html::encode($page->name) ?></h1>
</div>

<?= RedactorHtmlPurifier::process($page->content) ?>