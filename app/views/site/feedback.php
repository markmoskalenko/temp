<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\FeedbackForm */
/* @var $this  yii\web\View */

$this->title = 'Связь с администрацией сайта';
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
    <div class="alert alert-success">
        Мы получили ваше сообщение и скоро ответим на него. Спасибо!
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-lg-8">
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form',
                'options' => ['autocomplete' => 'off'],
                'fieldConfig' => [
                    'inputOptions' => [
                        'class' => 'form-control input-lg',
                    ]
                ],
            ]); ?>

            <?= $form->field($model, 'userName')->textInput() ?>
            <?= $form->field($model, 'userEmail')->input('email') ?>
            <?= $form->field($model, 'subject')->dropDownList($model->subjectLabels()) ?>
            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

            <?php if ($model->scenario == $model::SCENARIO_GUEST): ?>
                <?= $form->field($model, 'verifyCode')->label('Введите код')->widget('yii\captcha\Captcha', [
                    'options' => ['class' => 'form-control input-lg', 'style' => 'display: inline-block; width: 100px'],
                ]) ?>
            <?php endif ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-lg btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php endif ?>
