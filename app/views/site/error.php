<?php

use yii\helpers\Html;
use yii\web\HttpException;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->params['disableSearchArea'] = true;
$this->title = $name;
?>

<div class="site-error text-center">
    <div class="page-header">
        <h1>
            <?= Html::encode($this->title) ?>
            <p><small><?= Html::encode($message) ?></small></p>
        </h1>
    </div>

    <?php if ( ! ($exception instanceof HttpException)): ?>
        <p>Мы уже уведомлены о проблеме и решаем её.</p>
    <?php endif ?>

    <p>
        <?= Html::a('Перейти на главную', ['@home']) ?>
    </p>
    <?php if (Yii::$app->user->isGuest): ?>
        <p>
            <?= Html::a('Войти на сайт', ['@login']) ?>
        </p>
        <p>
            <?= Html::a('Восстановить пароль', ['@forgot']) ?>
        </p>
    <?php endif ?>
</div>