<?php
/* @var $this yii\web\View */
/* @var $url  string */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="REFRESH" content="1; URL=<?= $url ?>">
    <title>Page Moved</title>
</head>
<body>
    <script type="text/javascript">window.location = "<?= $url ?>";</script>
</body>
</html>