<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $productSearch app\models\ProductSearch */
/* @var $this          app\components\View */

$this->title = 'Поиск товаров на Алиэкспресс. Товары из китая aliexpress';
$this->registerMetaTag(['name' => 'description', 'content' => 'Воспользуйтесь быстрым и удобным поиском, чтобы найти интересующие вас товары в Китае на сайте aliexpress.']);
$this->params['disableSearchArea'] = true;
?>

<div class="page-header">
    <h1>Поиск товаров на AliExpress</h1>
</div>

<?= Html::beginForm(['search']) ?>
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <?= Html::activeLabel($productSearch, 'searchQuery', ['label' => 'Поиск']) ?>
                <?= Html::activeTextInput($productSearch, 'searchQuery', ['class' => 'form-control', 'placeholder' => 'Товар для поиска']) ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <?= Html::activeLabel($productSearch, 'categoryId', ['label' => 'Категория']) ?>
                <?= Html::activeDropDownList($productSearch, 'categoryId', $productSearch->getCategories(), ['class' => 'form-control', 'prompt' => 'Все категории']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 col-lg-5">
            <div class="form-group">
                <?= Html::activeLabel($productSearch, 'priceFrom', ['label' => 'Цена']) ?>
                <div class="row">
                    <div class="col-xs-6">
                        <?= Html::activeTextInput($productSearch, 'priceFrom', ['class' => 'form-control', 'placeholder' => 'от']) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::activeTextInput($productSearch, 'priceTo', ['class' => 'form-control', 'placeholder' => 'до']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-lg-5">
            <div class="form-group">
                <?= Html::activeLabel($productSearch, 'orderBy', ['label' => 'Сортировать']) ?>
                <?= Html::activeDropDownList($productSearch, 'orderBy', $productSearch->getOrders(), ['class' => 'form-control']) ?>
            </div>
        </div>

        <div class="col-sm-4 col-lg-2">
            <?= Html::label('&nbsp;', null, ['class' => 'hidden-xs']) ?>
            <?= Html::submitButton('Искать', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
<?= Html::endForm() ?>

<?= ListView::widget([
    'dataProvider' => $productSearch->dataProvider(),
    'layout' => '<p>{summary}</p> <div class="row">{items}</div> {pager}',
    'emptyTextOptions' => ['class' => 'b-product-list__empty'],
    'emptyText' => $productSearch->isEmpty ? '<p class="fa fa-search fa-5x"></p><p>Укажите запрос для поиска</p>' : '<p class="fa fa-meh-o fa-5x"></p><p>Не найдено ни одного товара.</p>',
    'itemOptions' => ['class' => 'col-sm-6 col-md-3'],
    'itemView' => '_item',
]) ?>

