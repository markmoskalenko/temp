<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $model array */
/* @var $this  app\components\View */

$salePrice = str_replace('US ', '', $model['salePrice']);
$originalPrice = str_replace('US ', '', $model['originalPrice']);
$hasDiscount = ($originalPrice > $salePrice);
?>

<a href="<?= Url::cpa($model['productUrl'], 'products') ?>" class="b-product-thumb thumbnail" target="_blank">
    <div class="b-product-thumb__image">
        <img src="<?= Html::encode($model['imageUrl']) ?>" class="img-responsive">
    </div>

    <div class="b-product-thumb__title"><?= Html::encode($model['productTitle']) ?></div>

    <?php if ($hasDiscount): ?>
        <div class="b-product-thumb__discount"><?= Html::encode($model['discount']) ?></div>
    <?php endif ?>

    <div class="b-product-thumb__price">
        <?php if ($hasDiscount): ?>
            <span class="b-product-thumb__price-original"><?= Html::encode($originalPrice) ?></span>
        <?php endif ?>

        <span class="b-product-thumb__price-current"><?= Html::encode($salePrice) ?></span>

        <?php if ($model['lotNum'] > 1): ?>
            <span class="b-product-thumb__count">(<?= Html::encode($model['lotNum']) ?> шт.)</span>
        <?php endif ?>
    </div>
</a>
