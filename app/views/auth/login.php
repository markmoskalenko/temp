<?php
use app\widgets\AuthChoice;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model app\models\LoginForm */
/* @var $this  app\components\View */
?>

<div class="page-header">
    <h1><?= $this->t('header-social') ?></h1>
</div>

<?= AuthChoice::widget() ?>

<br><br>

<div class="page-header">
    <h1><?= $this->t('header-email') ?></h1>
</div>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control input-lg', 'autocomplete' => 'off'],
        'labelOptions' => ['class' => 'sr-only'],
    ],
]) ?>

<?= $form->field($model, 'email')->input('email', [
    'placeholder' => $model->getAttributeLabel('email'),
]) ?>

<?= $form->field($model, 'password')->passwordInput([
    'placeholder' => $model->getAttributeLabel('password'),
]) ?>

<?= Html::submitButton($this->t('button-sign-in'), ['class' => 'btn btn-primary btn-lg']) ?>
<?= Html::a($this->t('button-sign-up'), ['sign-up'], ['class' => 'btn btn-link btn-lg']) ?>
<?= Html::a($this->t('button-reset-password'), ['request-password-reset-token'], ['class' => 'btn btn-link btn-lg']) ?>

<?php $form->end() ?>
