<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this  app\components\View */
/* @var $model app\models\PasswordResetRequestForm */
/* @var $form  yii\bootstrap\ActiveForm */
?>

<div class="page-header">
    <h1><?= $this->t('header') ?></h1>
</div>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control input-lg', 'autocomplete' => 'off'],
        'labelOptions' => ['class' => 'sr-only'],
    ],
]) ?>

<p><?= $this->t('text') ?></p>

<?= $form->field($model, 'email')->input('email', [
    'placeholder' => $model->getAttributeLabel('email'),
]) ?>

<div class="form-group">
    <?= Html::submitButton($this->t('button-request-token'), ['class' => 'btn btn-primary btn-lg']) ?>
    <?= Html::a($this->t('button-sign-in'), ['login'], ['class' => 'btn btn-link btn-lg']) ?>
    <?= Html::a($this->t('button-sign-up'), ['sign-up'], ['class' => 'btn btn-link btn-lg']) ?>
</div>

<?php $form->end() ?>
