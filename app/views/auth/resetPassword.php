<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $model app\models\ResetPasswordForm */
/* @var $this  app\components\View */
?>

<div class="page-header">
    <h1><?= $this->t('header') ?></h1>
</div>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control input-lg', 'autocomplete' => 'off'],
        'labelOptions' => ['class' => 'sr-only'],
    ],
]) ?>

<?= $form->field($model, 'password')->passwordInput([
    'placeholder' => $model->getAttributeLabel('password'),
]) ?>
<?= $form->field($model, 'passwordRepeat')->passwordInput([
    'placeholder' => $model->getAttributeLabel('passwordRepeat'),
]) ?>

    <div class="form-group">
        <?= Html::submitButton($this->t('action'), ['class' => 'btn btn-primary btn-lg']) ?>
    </div>

<?php $form->end() ?>