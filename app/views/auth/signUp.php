<?php
use app\widgets\AuthChoice;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model app\models\SignUpForm */
/* @var $this  app\components\View */
?>

<div class="page-header">
    <h1><?= $this->t('header-social') ?></h1>
</div>

<?= AuthChoice::widget() ?>

<br><br>

<div class="page-header">
    <h1><?= $this->t('header-email') ?></h1>
</div>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'inputOptions' => ['class' => 'form-control input-lg', 'autocomplete' => 'off'],
        'labelOptions' => ['class' => 'sr-only'],
    ],
]) ?>

<?= $form->field($model, 'name')->textInput([
    'placeholder' => $model->getAttributeLabel('name'),
]) ?>

<?= $form->field($model, 'email')->input('email', [
    'placeholder' => $model->getAttributeLabel('email'),
]) ?>

<?= Html::submitButton($this->t('button-sign-up'), ['class' => 'btn btn-primary btn-lg']) ?>
<?= Html::a($this->t('button-sign-in'), ['login'], ['class' => 'btn btn-link btn-lg']) ?>

<?php $form->end() ?>
