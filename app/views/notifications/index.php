<?php
use app\widgets\NotificationsListView;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Уведомления';
?>

<div ng-controller="NotificationListCtrl">
    <div class="page-header">
        <h1 class="page-header__title">Уведомления</h1>

        <div class="page-header__controls">
            <a href="javascript:void(0)" ng-click="onReadAll();" class="btn btn-success">Отметить все уведомления прочитанными</a>
        </div>
    </div>

    <?= NotificationsListView::widget([
        'dataProvider' => $dataProvider,
    ]) ?>
</div>