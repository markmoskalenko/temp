<?php
use app\db\records\Notifications;

/* @var $model app\db\records\Notifications */
/* @var $this  app\components\View */

$read = $model->isRead == Notifications::NOTIFICATION_READ;
?>

<div ng-click="onRead(<?= $read ? 1 : 0 ?>, <?= $model->id ?>);" title="Перейти"  class="alert alert-<?= $read ? 'success' : 'warning' ;?>" style="display:block;">
    <?= $model->getTitle() ?>
    <div class="pull-right"><?= $model->timeCreated ?></div>
</div>