<?php
use app\assets\LikeButtonAsset;
use app\widgets\BoastListView;
use app\helpers\Url;
use yii\bootstrap\Dropdown;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Хвасты Алиэкспресс, отзывы о покупках в Китае, фотографии покупок aliexpress';
$this->registerMetaTag(['name' => 'description', 'content' => 'Хвасты Алиэкспресс – это яркие впечатления пользователей от товаров, заказанных в Китае. Добавляйте свои хвасты и делитесь эмоциями.']);

LikeButtonAsset::register($this);
?>

<div class="page-header">
    <h1 class="page-header__title">Хвасты Алиэкспресс</h1>

    <div class="page-header__nav">
        <ul class="list-inline list-inline_style_wide">
            <li><a class="page-header__nav-link" href="http://alitrust.ru/boasts/odezhda-i-obuv">Одежда и обувь</a></li>
            <li class="visible-lg-inline-block"><a class="page-header__nav-link" href="http://alitrust.ru/boasts/bizhuteriya-i-chasy">Бижутерия и часы</a></li>
            <li>
                <div class="dropdown">
                    <a class="page-header__nav-link" href="#" data-toggle="dropdown">Категории <span class="fa fa-angle-down"></span></a>
                    <?= Dropdown::widget([
                        'options' => ['style' => 'margin-top: 10px'],
                        'items' => [
                            ['label' => 'Одежда и обувь', 'url' => 'http://alitrust.ru/boasts/odezhda-i-obuv'],
                            ['label' => 'Бижутерия и часы', 'url' => 'http://alitrust.ru/boasts/bizhuteriya-i-chasy'],
                            ['label' => 'Все для детей', 'url' => 'http://alitrust.ru/boasts/vse-dlya-detyei'],
                            ['label' => 'Все для дома', 'url' => 'http://alitrust.ru/boasts/home'],
                            ['label' => 'Телефоны и компьютеры', 'url' => 'http://alitrust.ru/boasts/telefony-i-kompyutery'],
                            ['label' => 'Сумки и аксессуары', 'url' => 'http://alitrust.ru/boasts/sumki-i-aksessuary'],
                            ['label' => 'Спорт, хобби и развлечения', 'url' => 'http://alitrust.ru/boasts/sport-khobbi-i-razvlecheniya'],
                            ['label' => 'Автотовары', 'url' => 'http://alitrust.ru/boasts/avtotovary'],
                            ['label' => 'Электроника', 'url' => 'http://alitrust.ru/boasts/elektronika'],
                            ['label' => 'Для взрослых (18+)', 'url' => 'http://alitrust.ru/boasts/adult18'],
                            ['label' => 'Другое', 'url' => 'http://alitrust.ru/boasts/other'],
                        ],
                    ]) ?>
                </div>
            </li>
        </ul>
    </div>

    <div class="page-header__controls">
        <a class="page-header__btn btn btn-rounded btn-inverse" href="<?= Url::to(['rating/boasts/competition']) ?>" title="Рейтинг">
            <span class="fa fa-fw fa-bar-chart"></span>
            Рейтинг
        </a>

        <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['create']) ?>">
            <span class="fa fa-fw fa-camera"></span>
            Добавить фото-хваст
        </a>
    </div>
</div>

<?= BoastListView::widget([
    'dataProvider' => $dataProvider,
]) ?>
