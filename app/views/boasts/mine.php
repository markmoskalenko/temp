<?php
use app\assets\LikeButtonAsset;
use app\widgets\BoastListView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = 'Мои хвасты';

LikeButtonAsset::register($this);
?>

<div class="page-header">
    <div class="page-header__title"><?= Html::encode($this->title) ?></div>

    <div class="page-header__controls">
        <a class="page-header__btn btn btn-rounded btn-inverse" href="<?= Url::to(['rating/index']) ?>" title="Рейтинг">
            <span class="fa fa-fw fa-bar-chart"></span>
            Конкурс
        </a>

        <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['create']) ?>">
            <span class="fa fa-fw fa-camera"></span>
            Добавить фото-хваст
        </a>
    </div>
</div>

<?= BoastListView::widget([
    'dataProvider' => $dataProvider,
]) ?>