<?php
use app\assets\FotoramaAsset;
use app\db\records\Snapshot;
use app\helpers\Url;
use app\widgets\Comments;
use app\widgets\LikeButton;
use app\widgets\SnapshotWidget;
use app\widgets\StarsRating;
use yii\helpers\Html;

/* @var $boast app\db\records\Boast */
/* @var $this  yii\web\View */

$this->title = $boast->title . ' — хвасты Алиэкспресс';

$this->registerMetaTag(['name' => 'description', 'content' => $this->title ]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $boast->title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $boast->text]);
$this->registerMetaTag(['property' => 'og:image', 'content' => ($boast->photos) ? Url::uploaded($boast->photos[0], true) : '']);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => 'AliTrust.ru']);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([], true)]);

$this->params['breadcrumbs'][] = ['label' => 'Хвасты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($boast->category->name), 'url' => ['category', 'slug' => $boast->category->slug]];

FotoramaAsset::register($this);
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1><?= Html::encode($boast->title) ?></h1>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <div class="fotorama" data-nav="thumbs">
            <?php foreach ($boast->photos as $photo): ?>
                <img src="<?= Url::uploaded($photo) ?>" alt="<?= Html::encode($boast->title) ?>">
            <?php endforeach ?>
        </div>

        <br>

        <p><?= Yii::$app->formatter->asNtext($boast->text) ?></p>

        <div class="b-meta">
            <div class="b-meta__item">
                <img class="b-meta__icon b-meta__icon_type_avatar" src="<?= Url::avatar($boast->user, ['w' => 50]) ?>" />
                <span class="text-capitalize"><a target="_blank" href="<?= Url::to(['profile/view', 'id'=>$boast->user->id]) ?>"><?= Html::encode($boast->user->present()->name()) ?></a></span>
            </div>

            <div class="b-meta__item">
                Оценка товара:
                <?= StarsRating::widget([
                    'options' => ['tag' => 'span', 'class' => 'rating'],
                    'value' => $boast->score,
                ]) ?>
            </div>

            <div class="b-meta__item">
                Куплено за:
                <span class="b-meta__icon fa fa-usd"></span><?= Html::encode($boast->price) ?>
            </div>

            <div class="b-meta__item">
                <?= LikeButton::widget([
                    'entity' => $boast,
                    'url' => Url::to(['vote', 'id' => $boast->id]),
                ]) ?>
            </div>

            <?php if (Yii::$app->user->can('updateBoast', ['boast' => $boast])): ?>
                <a href="<?= Url::to(['update', 'id' => $boast->id]) ?>" class="b-meta__item">
                    <span class="b-meta_icon fa fa-fw fa-pencil"></span>Редактировать
                </a>
            <?php endif ?>

            <?php if (Yii::$app->user->can('deleteBoast', ['boast' => $boast])): ?>
                <a href="<?= Url::to(['delete', 'id' => $boast->id]) ?>" class="b-meta__item" data-method="post" data-confirm="Вы уверены?">
                    <span class="b-meta_icon fa fa-fw fa-trash"></span>Удалить
                </a>
            <?php endif ?>
        </div>

        <h3>Где это купить?</h3>

        <?= SnapshotWidget::widget([
            'snapshot' => Snapshot::findOne(['snapshotId' => $boast->snapshotId]),
        ]) ?>

        <?= Comments::widget([
            'url' => Url::to(['boasts/comments', 'id' => $boast->id]),
        ]) ?>
    <?php $this->endContent() ?>
<?php $this->endContent() ?>