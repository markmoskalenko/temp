<?php
use yii\helpers\Html;

/* @var $model      app\models\BoastForm */
/* @var $boast      app\db\records\Boast */
/* @var $this       yii\web\View */

$this->title = $boast->title;
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render('_form', compact('model')) ?>
