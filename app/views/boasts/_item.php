<?php
use app\helpers\Url;
use app\widgets\StarsRating;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $model app\db\records\Boast */
/* @var $this  yii\web\View */

$user = $model->user;

$thumbWidth = 350;
$thumbHeight = round($thumbWidth / $model->mainPhoto->width * $model->mainPhoto->height);
?>

<div class="b-boast" ng-controller="LikeableController" ng-init="isVoted=<?= $model->isVoted ? 'true' : 'false' ?>; votesCount=<?= (int) $model->votesCount ?>; voteUrl='<?= Url::to(['/boasts/vote', 'id' => $model->id]) ?>'">
    <div class="b-boast__controls">
        <button type="button" class="b-boast__controls-btn btn" ng-click="like()" ng-class="isVoted ? 'btn-primary': 'btn-default'">
            <span class="fa fa-heart"></span>
        </button>
    </div>

    <?php if ($model->mainPhoto): ?>
        <a class="b-boast__image-wrapper" href="<?= Url::entity($model) ?>">
            <img class="b-boast__image" src="<?= Url::thumb($model->mainPhoto, ['w' => 350]) ?>"
                 width="<?= $thumbWidth ?>" height="<?= $thumbHeight ?>" alt="<?= Html::encode($model->title) ?>">
        </a>
    <?php endif ?>

    <div class="b-boast__content-wrapper">
        <ul class="b-boast__meta list-inline pull-left">
            <li>
                <?= StarsRating::widget([
                    'value' => $model->score,
                ]) ?>
            </li>
        </ul>

        <ul class="b-boast__meta list-inline pull-right">
            <li>
                <span class="b-meta__icon fa fa-heart text-primary"></span>
                <span ng-bind="votesCount"><?= $model->votesCount ?></span>
            </li>
            <li>
                <span class="b-meta__icon fa fa-comment-o text-info"></span>
                <?= $model->commentCount ?>
            </li>
        </ul>
    </div>

    <hr class="b-boast__divider">

    <div class="b-boast__content-wrapper">
        <div class="b-boast__user">
            <img class="img-circle" src="<?= Url::avatar($user, ['w' => 40, 'h' => 40, 'fit' => 'crop']) ?>" width="40" height="40">
            <a href="<?= Url::entity($user) ?>"><?= $user->name ?></a>
        </div>

        <a class="b-boast__content" href="<?= Url::entity($model) ?>">
            <?= StringHelper::truncateWords($model->text, 20) ?>
        </a>
    </div>
</div>