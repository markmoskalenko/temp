<?php
use app\assets\AutosizeAsset;
use app\db\records\BoastPhoto;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $model app\models\BoastForm */
/* @var $this  yii\web\View */

AutosizeAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'boast-form',
    'options' => ['autocomplete' => 'off'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ]
    ],
]) ?>

    <?= $form->field($model, 'externalUrl')->widget('app\widgets\SnapshotField', [
        'clientOptions' => [
            'price' => '#' . Html::getInputId($model, 'price'),
        ],
        'options' => [
            'class' => 'form-control input-lg',
            'placeholder' => 'Скопируйте сюда ссылку на снэпшот вашего товара',
        ],
    ]) ?>

    <div id="snapshot-widget"></div>

    <?= $form->field($model, 'title')->textInput() ?>

    <div class="row">
        <div class="col-md-7">
            <?= $form->field($model, 'categoryId')->widget('app\widgets\Categorize', [
                'options' => [
                    'class' => 'form-control input-lg',
                    'prompt' => '',
                ],
            ]) ?>
        </div>
        <div class="col-xs-6 col-md-2">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>
        <div class="col-xs-6 col-md-3">
            <?= $form->field($model, 'score')->widget('app\widgets\Raty') ?>
        </div>
    </div>

    <?= $form->field($model, 'photos')->widget('app\widgets\ImagesUploader', [
        'uploadUrl' => Url::to(['upload-photo']),
        'imageUrl' => function ($id) {
            $photo = BoastPhoto::findOne($id);
            return Url::uploaded($photo);
        }
    ]) ?>

    <?= $form->field($model, 'text')->textarea(['class' => 'form-control input-lg autosize', 'rows' => 3]) ?>

    <br>

    <button type="submit" class="btn btn-lg btn-primary">Опубликовать хваст</button>

    <span class="text-muted" style="margin-left: 20px">
        Нажимая кнопку "Опубликовать хваст", вы подтверждаете свое согласие с
        <a href="http://alitrust.ru/forum/theme/373" target="_blank">правилами публикации хвастов</a>.
    </span>

<?php $form->end() ?>
