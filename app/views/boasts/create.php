<?php
use yii\helpers\Html;

/* @var $model      app\models\BoastForm */
/* @var $this       yii\web\View */

$this->title = 'Добавить свой хваст Алиэкспресс';
?>

<div class="page-header">
    <h1>Новый хваст</h1>
</div>

<?= $this->render('_form', compact('model')) ?>
