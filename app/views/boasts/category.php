<?php
use app\widgets\BoastListView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $category     app\db\records\Category */
/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         yii\web\View */

$this->title = $category->seoTitle ?: $category->name;
$this->title = $this->title . ' – хвасты Алиэкспресс';

if ($category->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $category->seoDescription]);
} else {
    $this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
}

$this->params['breadcrumbs'][] = ['label' => 'Хвасты', 'url' => ['index']];
?>

<div class="page-header">
    <div class="page-header__title"><?= Html::encode($category->name) ?></div>

    <div class="page-header__controls">
        <a class="page-header__btn btn btn-rounded btn-primary" href="<?= Url::to(['create']) ?>">
            <span class="fa fa-fw fa-camera"></span>
            Добавить фото-хваст
        </a>
    </div>
</div>

<?= BoastListView::widget([
    'dataProvider' => $dataProvider,
]) ?>