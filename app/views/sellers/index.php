<?php
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $sellerSearch app\search\SellerSearch */
/* @var $this         yii\web\View */

$dataProvider = $sellerSearch->dataProvider();
$dataProvider->prepare();

$sellersCount = $dataProvider->getTotalCount();
$sellersCount = round($sellersCount, -2);

$this->title = 'Все магазины алиэкспресс - отзывы о магазинах, магазины aliexpress';
$this->registerMetaTag(['name' => 'description', 'content' => "Более {$sellersCount} магазинов Aliexpress. Рейтинг магазинов алиэкспресс, отзывы о магазинах алиэкспресс, магазины аликидалы, список недобросовестных продавцов aliexpress"]);

if ($dataProvider->pagination->getPage() !== 0) {
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
}
?>

<div class="page-header">
    <h1>Магазины Алиэкспресс</h1>
</div>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'layout' => '{items} {pager}',
]) ?>

<?php $this->endContent() ?>
