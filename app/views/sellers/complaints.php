<?php
use app\widgets\ComplaintList;
use app\widgets\SellerPjax;

/* @var $seller          app\db\records\Seller */
/* @var $complaintSearch app\search\ComplaintSearch */
/* @var $this            yii\web\View */

$dataProvider = $complaintSearch->dataProvider();
$dataProvider->pagination = false;

$complaintsCount = $dataProvider->getTotalCount();
$complaintsText = Yii::$app->i18n->format('{n, plural, =0{} one{# жалоба} few{# жалобы} many{# жалоб} other{# жалобы}}', ['n' => $complaintsCount], Yii::$app->language);

$this->title = ($complaintsCount <= 10) ? "Жалобы на магазин {$seller->storeName}. Отзывы о продавце алиэкспресс {$seller->sellerName}" : "{$complaintsText} на магазин {$seller->storeName}. Отзывы о продавце алиэкспресс {$seller->sellerName}" ;

$complaintsCount = ($complaintsCount < 10) ? $complaintsCount : round($complaintsCount, -1);
$complaintsText = Yii::$app->i18n->format('{n, plural, =0{} one{# жалоба} few{# жалобы} many{Более # жалоб} other{# жалобы}}', ['n' => $complaintsCount], Yii::$app->language);

$this->registerMetaTag(['name' => 'description', 'content' => "{$complaintsText} на продавца {$seller->sellerName} и его магазин на алиэкспресс {$seller->storeName}. Номер магазина: $seller->originalStoreId"]);
?>

<?php SellerPjax::begin([
    'seller' => $seller,
]) ?>

<?= $this->render('_header', [
    'seller' => $seller,
    'complaintsCount' => $dataProvider->getTotalCount(),
]) ?>

<?= ComplaintList::widget([
    'dataProvider' => $dataProvider,
    'showSellerInfo' => false,
]) ?>

<?php SellerPjax::end() ?>
