<?php
use app\helpers\Url;
use app\modules\forum\db\ThemeQuery;
use app\modules\forum\entities\Comment;
use app\widgets\StarsRating;
use yii\bootstrap\Nav;
use yii\helpers\Html;

/* @var $seller          app\db\records\Seller */
/* @var $this            yii\web\View */
/* @var $complaintsCount integer */

$forumCommentsCount = Comment::find()->active()->joinWith(['theme' => function (ThemeQuery $query) use ($seller) {
    $query->andWhere(['sellerId' => $seller->id]);
}])->count();

$this->registerJs('$(".js-data-url").each(function () { $(this).attr("href", $(this).data("url")) });');
?>

<div class="seller-info">
    <div class="seller-info-header">
        <h1 class="seller-info-title"><?= Html::encode($seller->storeName) ?></h1>
        <div class="seller-info-updated">Информация обновлена: <?= Yii::$app->formatter->asDatetime($seller->timeUpdated) ?></div>
    </div>

    <script type="text/html" id="seller-info-loading-template">
        <span class="fa fa-spinner fa-spin"></span>
        идёт обновление информации о продавце
    </script>

    <div class="seller-info-text">
        <?php if ($seller->sellerName): ?>
            <p>Продавец: <?= Html::encode($seller->sellerName) ?></p>
        <?php endif ?>
        <p>Номер магазина: <?= Html::a(Html::encode($seller->originalStoreId),  Url::cpa($seller->getOriginalStoreUrl() , 'storesales'), ['class' => 'border-link', 'rel' => 'nofollow', 'target' => '_blank']) ?></p>
    </div>

    <div class="seller-info-right-top">
        <?= StarsRating::widget([
            'options' => ['class' => 'rating seller-info-rating'],
            'value' => $seller->getSummaryRating(),
        ]) ?>
    </div>

    <div class="seller-info-right-bottom">
        <?php
        $btnOptions = ['class' => 'btn btn-success btn-lg'];
        $btnContent = 'Добавить жалобу';

        if (Yii::$app->user->isGuest) {
            $btnOptions['class'] .= ' js-analytic-event';
            $btnOptions['data'] = ['toggle' => 'modal', 'target' => '#login-modal', 'analytic-category' => 'complaints.button', 'analytic-label' => 'seller'];
            echo Html::button($btnContent, $btnOptions);
        } else {
            echo Html::a($btnContent, ['complaints/create'], $btnOptions);
        } ?>
    </div>
</div>

<hr>

<?= Nav::widget([
    'options' => ['class' => 'nav-pills'],
    'encodeLabels' => false,
    'items' => [
        [
            'url' => Url::toSeller($seller, ['#' => 'content']),
            'active' => (Yii::$app->requestedRoute === 'sellers/view'),
            'label' => '<span class="visible-xs-inline">Информация</span>
                        <span class="hidden-xs">Общая информация</span>',
        ],
        [
            'url' => Url::toSellerComplaints($seller, ['#' => 'content']),
            'active' => (Yii::$app->requestedRoute === 'sellers/complaints'),
            'label' => '<span class="visible-xs-inline">Жалобы</span>
                        <span class="hidden-xs">Жалобы на продавца</span>
                        <span class="badge">' . $complaintsCount . '</span>',
        ],
        [
            'url' => '#',
            'linkOptions' => ['target' => '_blank', 'class'=> 'js-analytic-event js-data-url', 'data-analytic-category' => 'seller.tabs', 'data-analytic-label' => 'sales', 'data-url' => Url::cpa($seller->getOriginalStoreSaleUrl(), 'storesales')],
            'visible' => (bool) $seller->saleItemsCount,
            'label' => '<span class="visible-xs-inline">Акции</span>
                        <span class="hidden-xs">Товары по акции</span>
                        <span class="badge">' . $seller->saleItemsCount . '</span>',
        ],
        [
            'url' => '#',
            'linkOptions' => ['class' => 'js-data-url', 'data-url' => Url::to(['/forum/themes/seller', 'id' => $seller->id])],
            'label' => 'Обсудить на форуме <span class="badge">' . $forumCommentsCount . '</span>',
        ],
    ],
]) ?>

<br>