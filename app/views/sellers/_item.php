<?php
use app\helpers\Url;
use app\widgets\StarsRating;
use yii\helpers\Html;

/* @var $model app\db\records\Seller */
/* @var $this  yii\web\View */

$complaintsCount = $model->getComplaints()->visible()->count();
$complaintsCount = Yii::$app->i18n->format('{n, plural, =0{ни одной жалобы} one{найдена # жалоба} few{найдено # жалобы} many{найдено # жалоб} other{найдено # жалобы}}', ['n' => $complaintsCount], Yii::$app->language);
?>

<div class="seller-info">
    <h3 class="seller-info-title">
        <?= Html::a(Html::encode($model->storeName), Url::toSeller($model), ['class' => 'border-link']) ?>
    </h3>

    <div class="seller-info-text">
        <?php if ($model->sellerName): ?>
            <p>Продавец: <?= Html::encode($model->sellerName) ?></p>
        <?php endif ?>
        <p>Номер магазина: <?= Html::encode($model->originalStoreId) ?></p>
    </div>

    <div class="seller-info-right-top">
        <?= StarsRating::widget([
            'options' => ['class' => 'rating seller-info-rating'],
            'value' => $model->getSummaryRating(),
        ]) ?>
    </div>

    <div class="seller-info-right-bottom">
        <?= Html::a($complaintsCount, Url::toSellerComplaints($model), ['class' => 'border-link']) ?>
    </div>
</div>

<hr>