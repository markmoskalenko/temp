<?php
use app\assets\CouponsAsset;
use yii\widgets\ListView;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Купоны AliExpress';

CouponsAsset::register($this);
?>

<div class="page-header">
    <h1 class="page-header__title">Купоны AliExpress</h1>
</div>

<div class="coupon-settings well" data-ng-controller="CouponsSettingsController">
    <div class="row">
        <div class="col-xs-4">
            <strong>Настройки:</strong>
        </div>

        <div class="col-xs-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" data-ng-model="enableNotifications" checked> показывать уведомления
                </label>
                <a href="#" data-ng-click="checkNotifications()" onclick="return false">проверка</a>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" data-ng-model="enableAudio" checked> издавать звуки
                </label>
                <a href="#" data-ng-click="checkAudio()" onclick="return false">проверка</a>
            </div>
        </div>
    </div>
</div>

<div data-ng-controller="CouponsController">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '<div class="coupon-list">{items}</div> {pager}',
        'itemOptions' => ['tag' => false],
        'itemView' => '_item',
    ]) ?>
</div>

