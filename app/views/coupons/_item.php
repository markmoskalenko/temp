<?php
use app\db\records\Coupon;
use app\helpers\Url;

/* @var $model app\db\records\Coupon */
/* @var $this  yii\web\View */

$coupon = $model;

if ($coupon->status === Coupon::STATUS_NOT_IN_STORE) {
    $statusClass = 'coupon--warning';
} elseif ($coupon->status === Coupon::STATUS_REMOVED) {
    $statusClass = 'coupon--danger';
} else {
    $statusClass = '';
}
?>


<div class="coupon-list__item">
    <div class="coupon <?= $statusClass ?>" data-id="<?= $coupon->id ?>" data-hash="<?= $coupon->hash ?>">
        <?php if ($coupon->imageUrl): ?>
            <img class="coupon__image" src="<?= $coupon->imageUrl ?>" />
        <?php else: ?>
            <img class="coupon__image" src="<?= Url::to('/images/sale-shopping-bag.png') ?>" />
        <?php endif ?>


        <div class="coupon__content">
            <div class="coupon__header">
                <div class="coupon__store"><?= $coupon->storeName ?></div>

                <?php if ($coupon->categoryId): ?>
                    <div class="coupon__category"><?= $coupon->category->name ?></div>
                <?php endif ?>
            </div>

            <div class="coupon__right">
                <p>
                    <span class="fa fa-clock-o"></span>
                    <time class="timeago" datetime="<?= $coupon->timeCreated ?>"><?= Yii::$app->formatter->asTime($coupon->timeCreated) ?></time>
                </p>

                <?php if ($coupon->status === Coupon::STATUS_NOT_IN_STORE): ?>
                    <p class="text-warning">Купон пока не появился в магазине</p>
                <?php endif ?>

                <?php if ($coupon->status === Coupon::STATUS_AVAILABLE): ?>
                    <p class="text-success">Купон в магазине</p>
                <?php endif ?>

                <?php if ($coupon->status === Coupon::STATUS_REMOVED): ?>
                    <p class="text-danger">Купон уже разобрали</p>
                <?php endif ?>
            </div>

            <div class="coupon__meta meta-info-group">
                <div class="coupon-meta meta-info-group__item">
                    <div class="coupon-meta__header">Скидка / Минимальный заказ</div>
                    <div class="coupon-meta__content">
                        <span class="coupon-meta__lg">$<?= $coupon->value ?></span>
                        <span class="coupon-meta__sm">$<?= $coupon->cost ?></span>
                    </div>
                </div>

                <?php if ( ! ($coupon->status === Coupon::STATUS_REMOVED || $coupon->status === Coupon::STATUS_NOT_IN_STORE)): ?>
                    <div class="coupon-meta meta-info-group__item">
                        <div class="coupon-meta__header">Купонов разобрано</div>
                        <div class="coupon-meta__content">
                            <?php if ($coupon->totalCount): ?>
                                <span class="coupon-meta__lg"><?= (int) $coupon->issusedCount ?></span>
                                <span class="coupon-meta__sm"> / <?= (int) $coupon->totalCount ?></span>
                            <?php else: ?>
                                <span class="coupon-meta__lg">—</span>
                            <?php endif ?>
                        </div>
                    </div>
                <?php endif ?>

                <div class="coupon-meta meta-info-group__item">
                    <div class="coupon-meta__header">Товаров под купон</div>
                    <div class="coupon-meta__content">
                        <?php if ($coupon->availableProducts === null): ?>
                            <div class="coupon-meta__lg">—</div>
                        <?php else: ?>
                            <div class="coupon-meta__lg"><?= $coupon->availableProducts ?></div>
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <div class="coupon__actions">
                <a class="btn btn-rounded btn-primary" href="<?= Url::to(['store', 'id' => $coupon->id]) ?>" target="_blank">Перейти в магазин</a>
                <a class="btn btn-rounded btn-inverse" href="<?= Url::to(['products', 'id' => $coupon->id]) ?>" target="_blank">Открыть подходящие товары</a>
            </div>
        </div>
    </div>
</div>