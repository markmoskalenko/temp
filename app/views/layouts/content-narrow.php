<?php
use app\widgets\Alert;

/* @var $this    yii\web\View */
/* @var $content string */

$this->params['disableSearchArea'] = true;
?>

<?php $this->beginContent('@app/views/layouts/main.php') ?>

<div class="stripe" id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<?php $this->endContent() ?>