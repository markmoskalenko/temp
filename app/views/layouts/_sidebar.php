<?php
use app\db\records\ArticleCategory;
use app\db\records\Category;
use app\helpers\Url;
use app\widgets\BannerRotator;
use yii\helpers\Html;

/* @var $this    yii\web\View */
/* @var $content string */
?>

<div class="row">
    <div class="col-xs-9">
        <?= $content ?>
    </div>

    <div class="col-xs-3">
        <div class="social-groups text-center" style="margin: 5px 0 19px">
            <a class="social-groups__link social-groups__link_type_facebook" href="<?= Yii::$app->params['socials.facebook'] ?>" title="Facebook" target="_blank">
                <span class="social-groups__icon fa fa-facebook"></span>
            </a>

            <a class="social-groups__link social-groups__link_type_twitter" href="<?= Yii::$app->params['socials.twitter'] ?>" title="Twitter" target="_blank">
                <span class="social-groups__icon fa fa-twitter"></span>
            </a>

            <a class="social-groups__link social-groups__link_type_vk" href="<?= Yii::$app->params['socials.vk'] ?>" title="Вконтакте" target="_blank">
                <span class="social-groups__icon fa fa-vk"></span>
            </a>

            <a class="social-groups__link social-groups__link_type_instagram" href="<?= Yii::$app->params['socials.instagram'] ?>" title="Instagram" target="_blank">
                <span class="social-groups__icon fa fa-instagram"></span>
            </a>

            <a class="social-groups__link social-groups__link_type_youtube" href="<?= Yii::$app->params['socials.youtube'] ?>" title="YouTube" target="_blank">
                <span class="social-groups__icon fa fa-youtube"></span>
            </a>
        </div>

        <?php if (isset($this->params['showReviewCategoriesBlock'])): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Обзоры по категориям</div>
                </div>

                <?php $categories = Category::find()->hasReviews()->all() ?>

                <div class="list-group">
                    <?php foreach ($categories as $category): ?>
                        <a class="list-group-item" href="<?= Url::to(['reviews/category', 'slug' => $category->slug]) ?>"><?= Html::encode($category->name) ?></a>
                    <?php endforeach ?>
                </div>
            </div>
        <?php elseif (isset($this->params['showCategoriesBlock'])): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Статьи по категориям</div>
                </div>

                <?php $categories = ArticleCategory::find()->all() ?>

                <div class="list-group">
                    <?php foreach ($categories as $category): ?>
                        <a class="list-group-item" href="<?= $category->url ?>"><?= Html::encode($category->name) ?></a>
                    <?php endforeach ?>
                </div>
            </div>
        <?php else: ?>
            <?= BannerRotator::widget([
                'options' => ['style' => 'display: block; margin-bottom: 22px;'],
                'imageOptions' => ['class' => 'img-responsive'],
            ]) ?>
        <?php endif ?>

        <?= \app\widgets\LatestNews::widget() ?>
    </div>
</div>