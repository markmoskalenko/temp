<?php
use app\widgets\Alert;
use yii\widgets\Breadcrumbs;

/* @var $this    yii\web\View */
/* @var $content string */
?>

<?php $this->beginContent('@app/views/layouts/main.php') ?>

<div class="anchor" id="content"></div>

<div class="stripe">
    <div class="container">
        <?= Alert::widget() ?>

        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
    </div>
</div>

<?php $this->endContent() ?>