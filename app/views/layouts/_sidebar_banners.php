<?php
use app\db\records\ArticleCategory;
use app\db\records\Category;
use app\helpers\Url;
use app\widgets\CategoriesBlock;
use app\widgets\VkGroup;
use yii\helpers\Html;

/* @var $this    yii\web\View */
/* @var $content string */
?>

<div class="row">
    <div class="col-xs-9">
        <?= $content ?>
    </div>

    <div class="col-xs-3">
        <a href="<?= Url::to(['/boasts/index']) ?>">
            <img class="img-responsive" src="/images/temp/banner_top.png">
        </a>

        <br>

        <a href="<?= Url::cpa('http://activities.aliexpress.com/ru/cell_phone.php', 'products') ?>">
            <img class="img-responsive" src="/images/temp/banner_mobile.png">
        </a>

        <br>

        <a href="<?= Url::cpa('http://activities.aliexpress.com/ru/everythingforkids.php', 'products') ?>">
            <img class="img-responsive" src="/images/temp/banner_children.png">
        </a>
    </div>
</div>