<?php
use app\models\SellerSearchForm;
use app\widgets\ChromeExtensionModal;
use app\widgets\CreatorModal;
use app\widgets\LoginModal;
use app\widgets\MainMenuNav;
use app\widgets\MultiLogin;
use yii\widgets\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use app\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;

/* @var $this    app\components\View */
/* @var $content string */

$user = Yii::$app->user->getIdentity();

AppAsset::register($this);

$this->registerJs('app.user.set("id", '.json_encode(Yii::$app->user->id).'); app.start();', View::POS_END);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>

    <meta name="google-site-verification" content="ISqD_uPICpN_twI-_iMPHT6ygcFkb5aL1kzO7d8b_SE" />
    <meta name="google-site-verification" content="4-1D8qa6OruknCVgiYKngzyZ_m82FDJLZ1tM57bL-qk" />
    <?= Html::csrfMetaTags() ?>

    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/eenflijjbchafephdplkdmeenekabdfb">
    <link rel="apple-touch-icon" href="<?= Url::to('/apple-touch-icon.png') ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?= Url::to('/favicon.ico') ?>">
    <link rel="icon" type="image/png" href="<?= Url::to('/favicon.png') ?>" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">

    <?php $this->head() ?>
</head>
<body class="body body_fixed_footer" ng-app="AngularApp" <?= (Yii::$app->user->isGuest) ? '' : 'ng-controller="NotificatorCtrl"' ?>>
    <?php $this->beginBody() ?>

    <script type="text/javascript">
        var BASE_URL = '<?= Url::home(true) ?>',
            API_URL = BASE_URL.replace('alikidala', 'api.alikidala');
    </script>

    <div class="main-menu">
        <div class="container">
            <div class="main-menu__container">
                <a class="main-menu__logo main-menu-link" href="/">AliTrust</a>

                <div class="main-menu__left-corner">
                    <a class="main-menu-link main-menu-link--sm" target="_blank" onclick="ga('send', 'event', 'browser-extension', 'main-menu')"
                       href="https://chrome.google.com/webstore/detail/aliexpress-tools/eenflijjbchafephdplkdmeenekabdfb?hl=ru">
                        <span class="main-menu-link__svg-icon"><?= file_get_contents(Yii::getAlias('@webroot/images/chrome.svg')) ?></span>
                        <span class="main-menu-link__text">Расширение для&nbsp;Chrome</span>
                    </a>
                </div>

                <?= MainMenuNav::widget([
                    'options' => ['class' => 'main-menu__left main-menu-nav'],
                    'itemOptions' => ['class' => 'main-menu-nav__item'],
                    'items' => [
                        ['label' => 'Отследить посылку', 'url' => ['/track/index'], 'options'=>['onClick'=>'document.location.href = "/track";']],
                        ['label' => 'Проверка продавца', 'url' => ['/check/index']],
                    ],
                ]) ?>

                <?= MainMenuNav::widget([
                    'options' => ['class' => 'main-menu__right main-menu-nav'],
                    'itemOptions' => ['class' => 'main-menu-nav__item'],
                    'items' => [
                        ['label' => 'Хвасты', 'url' => ['/boasts/index']],
                        ['label' => 'Жалобы', 'url' => ['/complaints/index']],
                        ['label' => 'Новости', 'url' => ['/news/index']],
                        ['label' => 'Форум', 'url' => ['/forum/index'], 'active' => Yii::$app->controller->module->id === 'forum'],
                    ],
                ]) ?>

                <div class="main-menu__right-corner">
                    <?php if (Yii::$app->user->isGuest): ?>
                        <a class="main-menu-link" href="<?= Url::to(['/auth/login']) ?>">
                            <span class="main-menu__icon fa fa-user-secret"></span>
                            Войти
                        </a>
                    <?php else: ?>
                        <div class="dropdown">
                            <a href="#" class="main-menu-link" data-toggle="dropdown">
                                <span class="main-menu__username"><?= Yii::$app->user->identity->name ?></span>
                                <img class="main-menu__avatar" src="<?= Url::avatar(Yii::$app->user->identity, ['w' => 40, 'h' => 40, 'fit' => 'crop']) ?>">
                            </a>

                            <?= Dropdown::widget([
                                'options' => ['class' => 'main-menu__dropdown'],
                                'items' => [
                                    ['label' => 'Уведомления', 'url' => '/notifications/index'],
                                    ['label' => 'Профиль', 'url' => ['/profile/index']],
                                    ['label' => 'Админка', 'url' => '/admin', 'visible' => Yii::$app->user->can('redactor')],
                                    Html::tag('li', '', ['class' => 'divider']),
                                    ['label' => 'Мои хвасты', 'url' => '/boasts/mine'],
                                    ['label' => 'Мои жалобы', 'url' => '/complaints/mine'],
                                    ['label' => 'Мои обзоры', 'url' => '/reviews/mine'],
                                    ['label' => 'Мои посылки', 'url' => '/track'],
                                    Html::tag('li', '', ['class' => 'divider']),
                                    ['label' => 'Выход', 'url' => ['@logout'], 'linkOptions' => ['data-method' => 'post']],
                                ],
                            ]) ?>
                        </div>
                    <?php endif ?>
                </div>

                <a class="main-menu__notifications ng-cloak" href="<?= Url::to(['/notifications/index']) ?>"
                   ng-init="newNotifications=<?= $this->countNewNotification ?>"
                   ng-show="newNotifications > 0">{{newNotifications}}</a>
            </div>

            <div class="main-menu-search">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-seller-search',
                    'action' => ['/site/search'],
                    'options' => ['autocomplete' => 'off'],
                    'enableAjaxValidation' => true,
                ]) ?>

                    <?= $form->field(new SellerSearchForm(), 'link', [
                        'template' => "{input}\n{error}",
                        'options' => [],
                        'inputOptions' => [
                            'class' => 'main-menu-search__input form-control',
                            'placeholder' => 'Для проверки продавца вставьте сюда ссылку на него или один из его товаров',
                        ],
                        'errorOptions' => [
                            'class' => 'main-menu-search__error',
                        ],
                    ]) ?>

                    <button type="submit" class="main-menu-search__submit">
                        <span class="main-menu-search__icon fa fa-search" data-icon="default"></span>
                        <span class="main-menu-search__icon fa fa-spinner fa-spin hidden" data-icon="loading"></span>
                    </button>
                <?php $form->end() ?>
            </div>
        </div>
    </div>

    <?= MultiLogin::widget() ?>

    <?= $content ?>

    <?php if (Yii::$app->user->isGuest): ?>
        <?= LoginModal::widget() ?>
    <?php else: ?>
        <?= CreatorModal::widget() ?>
    <?php endif ?>

    <?= ChromeExtensionModal::widget() ?>

    <iframe src="<?= Url::to(['/iframe/index']) ?>" width="0" height="0" frameborder="0" style="position: absolute"></iframe>

    <footer class="body__footer stripe stripe-inverse stripe-flatten">
        <div class="container">
            <div class="footer">
                <div class="footer__header row">
                    <div class="col-xs-9">
                        <ul class="list-inline">
                            <li class="visible-lg-inline-block"><?= Html::a('Генератор фото для споров', ['/dispute/index'], ['class' => 'footer__link']) ?></li>
                            <li><?= Html::a('Поиск товаров', ['/products/index'], ['class' => 'footer__link']) ?></li>
                            <li><?= Html::a('Проверка продавца', ['/check/index'], ['class' => 'footer__link']) ?></li>
                            <li>|</li>
                            <li><?= Html::a('Статьи', ['/articles/index'], ['class' => 'footer__link']) ?></li>
                            <li><?= Html::a('Хвасты', ['/boasts/index'], ['class' => 'footer__link']) ?></li>
                            <li><?= Html::a('Жалобы', ['/complaints/index'], ['class' => 'footer__link']) ?></li>
                            <li><?= Html::a('Форум', '/forum', ['class' => 'footer__link']) ?></li>
                        </ul>
                    </div>

                    <div class="col-xs-3 text-right">
                        <div class="social-groups">
                            <a class="social-groups__link social-groups__link_type_inverse" href="<?= Yii::$app->params['socials.facebook'] ?>" title="Facebook" target="_blank">
                                <span class="social-groups__icon fa fa-facebook"></span>
                            </a>

                            <a class="social-groups__link social-groups__link_type_inverse" href="<?= Yii::$app->params['socials.twitter'] ?>" title="Twitter" target="_blank">
                                <span class="social-groups__icon fa fa-twitter"></span>
                            </a>

                            <a class="social-groups__link social-groups__link_type_inverse" href="<?= Yii::$app->params['socials.vk'] ?>" title="Вконтакте" target="_blank">
                                <span class="social-groups__icon fa fa-vk"></span>
                            </a>

                            <a class="social-groups__link social-groups__link_type_inverse" href="<?= Yii::$app->params['socials.instagram'] ?>" title="Instagram" target="_blank">
                                <span class="social-groups__icon fa fa-instagram"></span>
                            </a>

                            <a class="social-groups__link social-groups__link_type_inverse" href="<?= Yii::$app->params['socials.youtube'] ?>" title="YouTube" target="_blank">
                                <span class="social-groups__icon fa fa-youtube"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        &copy; AliTrust — эксперт в области покупок из Китая, 2012&ndash;<?= date('Y') ?>
                    </div>

                    <div class="col-xs-6 text-right">
                        <ul class="list-inline">
                            <li><?= Html::a('Политика конфиденциальности', ['@page', 'slug' => 'policy'], ['class' => 'link-muted']) ?></li>
                            <li><?= Html::a('Обратная связь', ['/feedback/index'], ['class' => 'link-muted']) ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter25691963 = new Ya.Metrika({id:25691963, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, ut:"noindex"}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/25691963?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-53233668-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- /Google Analytics -->

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
