<?php
use app\widgets\PublicationList;

/* @var $category     app\db\records\ArticleCategory */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         app\components\View */

$this->title = $category->seoTitle ?: $category->name;
$this->title = $this->title . ' - статьи Алиэкспресс';
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];

if ($category->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $category->seoDescription]);
}

$this->params['showCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1><?= $category->name ?></h1>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?php if ($category->content): ?>
            <div class="publication">
                <?= $category->content ?>
            </div>

            <hr>
        <?php endif ?>

        <?= PublicationList::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/articles/_item',
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
