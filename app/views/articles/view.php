<?php
use app\db\records\Article;
use app\widgets\Comments;
use app\helpers\Url;
use app\helpers\RedactorHtmlPurifier;
use yii\helpers\Html;

/* @var $article app\db\records\Article */
/* @var $this    app\components\View */

$this->title = $article->seoTitle ?: $article->title;

$cat_title = $article->category->seoTitle ?: $article->category->name;

$this->title = $this->title . ' - ' . mb_strtolower($cat_title);

$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $article->category->name, 'url' => ['category', 'slug' => $article->category->slug]];

$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => ($article->seoDescription) ? $article->seoDescription : $article->contentPreview]);
$this->registerMetaTag(['property' => 'og:site_name', 'content' => 'AliTrust.ru']);
$this->registerMetaTag(['property' => 'og:image', 'content' => ($article->mainImage) ? Url::uploaded($article->mainImage, true) : '']);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([], true)]);

if ($article->seoDescription) {
    $this->registerMetaTag(['name' => 'description', 'content' => $article->seoDescription]);
}

$this->params['showCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>
    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?php if ($article->imageId): ?>
            <div class="publication-label-header">
                <div class="publication-label-header__container">
                    <div class="publication-label-header__background">
                        <h1 class="publication-label-header__title"><?= Html::encode($article->title) ?></h1>
                    </div>
                </div>

                <a class="publication-label-header__image" href="<?= Url::entity($article) ?>" style="background-image: url(<?= Url::uploaded($article->mainImage) ?>)"></a>
            </div>

            <br>
        <?php else: ?>
            <h1 class="publication-header">
                <a href="<?= Url::entity($article) ?>" class="publication-header__title publication-header__link"><?= Html::encode($article->title) ?></a>
            </h1>
        <?php endif ?>

        <div class="b-publication">
            <?= RedactorHtmlPurifier::process($article->content) ?>
        </div>

        <div class="page-divider page-divider_style_border"></div>

        <?php if ($similar = Article::find()->random()->published()->category($article->categoryId)->limit(2)->all()): ?>
            <?php /* @var $similar Article[] */ ?>
            <h3>Похожие статьи</h3>

            <div class="publication-widget-group publication-widget-group_columns_2">
                <?php foreach ($similar as $publication): ?>
                    <a href="<?= Url::entity($publication) ?>"
                       class="publication-widget-group__item publication-widget publication-widget_size_small"
                       style="background-image: url(<?= Url::uploaded($publication->mainImage) ?>)">
                        <div class="publication-widget__title"><?= Html::encode($publication->title) ?></div>
                        <div class="publication-widget__date"><?= Yii::$app->formatter->asDate($publication->timePublished, 'dd MMMM YYYY') ?></div>
                        <div class="publication-widget__meta meta-info-group">
                            <div class="meta-info-group__item meta-info meta-info_style_danger">
                                <span class="publication-widget__meta-icon meta-info__icon fa fa-eye"></span>
                                <span class="meta-info__text"><?= $publication->viewCount ?></span>
                            </div>
                            <div class="meta-info-group__item meta-info meta-info_style_warning">
                                <span class="publication-widget__meta-icon meta-info__icon fa fa-comment"></span>
                                <span class="meta-info__text"><?= $publication->commentCount ?></span>
                            </div>
                        </div>
                    </a>
                <?php endforeach ?>
            </div>
        <?php endif ?>

        <?= Comments::widget([
            'id' => 'comments',
            'url' => Url::to(['comments', 'id' => $article->id]),
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
