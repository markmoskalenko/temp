<?php
use app\widgets\PublicationList;

/* @var $dataProvider yii\data\BaseDataProvider */
/* @var $this         app\components\View */

$this->title = 'Статьи и полезная информация - АлиТраст';
$this->registerMetaTag(['name' => 'description', 'content' => 'Статьи и полезная информация на тему покупок на торговой площадке АлиЭкспресс']);

$this->params['showCategoriesBlock'] = true;
?>

<?php $this->beginContent('@app/views/layouts/_sidebar.php') ?>

    <div class="page-header">
        <h1 class="page-header__title">Статьи и информация</h1>
    </div>

    <?php $this->beginContent('@app/views/layouts/_sidebar_banners.php') ?>

        <?= PublicationList::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/articles/_item',
        ]) ?>

    <?php $this->endContent() ?>
<?php $this->endContent() ?>
