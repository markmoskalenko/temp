<?php
namespace app\components;

use Raven_Client;
use Raven_ErrorHandler;
use Yii;
use yii\base\Component;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Raven extends Component
{
    /**
     * @var string
     */
    public $dsn;
    /**
     * @var array
     */
    public $options = [];
    /**
     * @var boolean
     */
    public $registerErrorHandler = false;
    /**
     * @var boolean
     */
    public $registerExceptionHandler = false;

    /**
     * @var Raven_Client
     */
    private $_client;
    /**
     * @var Raven_ErrorHandler
     */
    private $_errorHandler;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->registerErrorHandler) {
            $this->getErrorHandler()->registerErrorHandler();
        }

        if ($this->registerExceptionHandler) {
            $this->getErrorHandler()->registerExceptionHandler();
        }
    }

    /**
     * @return Raven_Client
     */
    public function getClient()
    {
        if ($this->_client === null) {
            $this->_client = new Raven_Client($this->dsn, $this->options);
        }

        return $this->_client;
    }

    /**
     * @return Raven_ErrorHandler
     */
    public function getErrorHandler()
    {
        if ($this->_errorHandler === null) {
            $this->_errorHandler = new Raven_ErrorHandler($this->getClient());
        }

        return $this->_errorHandler;
    }

    /**
     * @param \Exception $exception
     * @param array      $options
     * @param mixed      $logger
     * @param mixed      $vars
     *
     * @return mixed
     */
    public function captureException($exception, $options = null, $logger = null, $vars = null)
    {
        return $this->getClient()->captureException($exception, $options, $logger, $vars);
    }

    /**
     * @param string  $message
     * @param array   $params
     * @param array   $levelOrOptions
     * @param boolean $stack
     * @param mixed   $vars
     *
     * @return integer event id
     */
    public function captureMessage($message, $params = [], $levelOrOptions = [], $stack = false, $vars = null)
    {
        return $this->getClient()->captureMessage($message, $params, $levelOrOptions, $stack, $vars);
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public function extraContext($data)
    {
        $this->getClient()->extra_context($data);
        return $this;
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public function tagsContext($data)
    {
        $this->getClient()->tags_context($data);
        return $this;
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public function userContext($data)
    {
        $this->getClient()->user_context($data);
        return $this;
    }
}