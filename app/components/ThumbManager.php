<?php
namespace app\components;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Glide\ServerFactory;
use Yii;
use yii\base\Component;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ThumbManager extends Component
{
    /**
     * @var string
     */
    public $sourcePath = '@webroot/uploads';
    /**
     * @var string
     */
    public $thumbsPath = '@webroot/thumbs';

    /**
     * @var \League\Glide\Api\Api
     */
    protected $api;
    /**
     * @var \League\Flysystem\FilesystemInterface
     */
    protected $source;
    /**
     * @var \League\Flysystem\FilesystemInterface
     */
    protected $thumbs;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = Yii::getAlias($this->sourcePath);
        $this->thumbsPath = Yii::getAlias($this->thumbsPath);

        $server = new ServerFactory();

        $this->api = $server->getApi();
        $this->source = new Filesystem(new Local($this->sourcePath));
        $this->thumbs = new Filesystem(new Local($this->thumbsPath));
    }

    /**
     * @param string $filename
     * @param array  $params
     * @return boolean
     */
    public function exists($filename, $params)
    {
        $thumb = $this->thumbFilename($filename, $params);
        return $this->thumbs->has($thumb);
    }

    /**
     * @param string $filename
     * @param array  $params
     * @return boolean
     */
    public function create($filename, $params)
    {
        $source = $this->source->read($filename);
        $thumb = $this->api->run($source, $params);

        $this->thumbs->write($this->thumbFilename($filename, $params), $thumb);
    }

    /**
     * @param string $filename
     * @param array  $params
     * @return string
     */
    public function thumbFilename($filename, $params)
    {
        return $this->hash($params) . '/' . $filename;
    }

    /**
     * Generate a CRC32 hash for the directory path. Collisions are higher
     * than MD5 but generates a much smaller hash string.
     *
     * @param array $params thumb parameters to be hashed.
     * @return string hashed string.
     */
    public function hash($params)
    {
        $string = json_encode($params);
        return sprintf('%x', crc32($string));
    }
}