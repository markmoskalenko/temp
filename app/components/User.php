<?php
namespace app\components;

use app\db\records\User as UserRecord;
use Yii;
use yii\web\IdentityInterface;

/**
 * @property UserRecord $identity
 * @property boolean    $isEmailConfirmed
 * @property boolean    $hasSocialProfile
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class User extends \yii\web\User
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ( ! $this->isGuest) {
            $this->getIdentity()->touch('timeVisited');
        }
    }

    /**
     * @param boolean $autoRenew
     * @return UserRecord
     */
    public function getIdentity($autoRenew = true)
    {
        return parent::getIdentity($autoRenew);
    }

    /**
     * @param IdentityInterface $identity
     * @param integer           $duration
     * @return boolean
     */
    public function login(IdentityInterface $identity, $duration = null)
    {
        if ($duration === null) {
            $duration = Yii::$app->params['user.loginDuration'];
        }
        return parent::login($identity, $duration);
    }

    /**
     * @return boolean
     */
    public function getIsEmailConfirmed()
    {
        return ($this->getIdentity() && $this->getIdentity()->getAttribute('emailConfirmation'));
    }

    /**
     * @return boolean
     */
    public function getHasSocialProfile()
    {
        return ($this->getIdentity() && $this->getIdentity()->getSocialProfiles()->count() > 0);
    }
}