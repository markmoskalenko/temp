<?php
namespace app\components;

use Yii;
use yii\base\Component;

/**
 * @property array $allowed
 * @property array $ietfTags
 * @property array $labels
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Polyglot extends Component
{
    const LANG_RU = 'ru';
    const LANG_PT = 'pt';
    const LANG_ES = 'es';

    /**
     * @return array
     */
    public function getAllowed()
    {
        return [
            static::LANG_RU,
            static::LANG_PT,
            static::LANG_ES,
        ];
    }

    /**
     * @return array
     */
    public function getIetfTags()
    {
        return [
            static::LANG_RU => 'ru-RU',
            static::LANG_PT => 'pt-BR',
            static::LANG_ES => 'es-ES',
        ];
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return [
            static::LANG_RU => 'Русский',
            static::LANG_PT => 'Португальский',
            static::LANG_ES => 'Испанский',
        ];
    }

    /**
     * @param mixed  $params
     * @param string $language
     * @return string
     */
    public function createUrl($params, $language = null)
    {
        $language = $this->normalizeLanguage($language);

        $host = Yii::$app->params['host.frontend.' . $language];
        $url = Yii::$app->urlManager->createUrl($params);

        return 'http://' . $host . $url;
    }

    /**
     * @param string $language
     * @return string
     */
    public function getSupportEmail($language = null)
    {
        $language = $this->normalizeLanguage($language);

        if ($language === static::LANG_RU) {
            return 'support@' . Yii::$app->params['host.frontend.ru'];
        } else {
            return 'support@' . Yii::$app->params['host.frontend'];
        }
    }

    /**
     * @param string $language
     * @return string
     */
    public function normalizeLanguage($language)
    {
        $language = substr($language, 0, 2);

        if ( ! in_array($language, $this->allowed)) {
            $language = substr(Yii::$app->language, 0, 2);
        }

        return $language;
    }
}