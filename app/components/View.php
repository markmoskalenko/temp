<?php
namespace app\components;

use app\behaviors\TranslatableViewBehavior;
use app\repositories\NotificationRepository;
use yii\helpers\StringHelper;
use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin TranslatableViewBehavior
 */
class View extends \yii\web\View
{
    /**
     * @var integer
     */
    public $countNewNotification = 0;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if( ! \Yii::$app->user->isGuest) {
            $this->countNewNotification = (new NotificationRepository())->getCountNew();
        }
    }

    /**
     * Returns a list of behaviors that this component should behave as.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'i18n' => TranslatableViewBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function registerJsFile($url, $options = [], $key = null)
    {
        if ( ! StringHelper::startsWith($url, '@web/')) {
            parent::registerJsFile($url, $options, $key);
            return;
        }

        $url = str_replace('@web/', '', $url);

        $bundle = new AssetBundle(array_merge($options, [
            'baseUrl' => '@web',
            'basePath' => '@webroot',
            'js' => [$url],
        ]));

        if ($key === null) {
            $key = $url;
        }

        $this->assetManager->bundles[ $key ] = $bundle;
        $this->registerAssetBundle($key);
    }
}