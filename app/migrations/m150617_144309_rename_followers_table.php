<?php

use yii\db\Migration;

class m150617_144309_rename_followers_table extends Migration
{
    public function up()
    {
        $this->renameTable('followers', 'user_follower');
    }

    public function down()
    {
        $this->renameTable('user_follower', 'followers');
    }
}
