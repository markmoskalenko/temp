<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_013849_add_favorited_column_to_users extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'isFavorited', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('user', 'isFavorited');
    }
}
