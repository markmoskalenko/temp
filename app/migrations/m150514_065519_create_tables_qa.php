<?php

use yii\db\Schema;
use yii\db\Migration;

class m150514_065519_create_tables_qa extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `qa_category` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(250) NOT NULL DEFAULT '',
              `slug` varchar(250) NOT NULL DEFAULT '',
              `seoTitle` varchar(250) DEFAULT NULL,
              `seoDescription` varchar(250) DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
          CREATE TABLE `qa` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `qaCategoryId` int(11) unsigned NOT NULL,
          `userId` int(11) unsigned DEFAULT NULL,
          `title` varchar(255) NOT NULL,
          `slug` varchar(255) NOT NULL,
          `email` varchar(255) NOT NULL,
          `firstName` varchar(255) DEFAULT NULL,
          `question` text NOT NULL,
          `isMailNotification` tinyint(1) unsigned NOT NULL DEFAULT '1',
          `status` smallint(6) NOT NULL DEFAULT '10',
          `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `timeUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          PRIMARY KEY (`id`),
          KEY `qaCategoryId` (`qaCategoryId`),
          KEY `userId` (`userId`),
          CONSTRAINT `fk_qa_qa_category` FOREIGN KEY (`qaCategoryId`) REFERENCES `qa_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `fk_qa_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
          CREATE TABLE `qa_photo` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `userId` int(11) unsigned DEFAULT NULL,
          `qaId` int(11) unsigned DEFAULT NULL,
          `filename` varchar(255) DEFAULT NULL,
          `rejected` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `timeCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`),
          KEY `boastId` (`qaId`)
        ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->execute("DROP TABLE IF EXISTS `qa_category`;");
        $this->execute("DROP TABLE IF EXISTS `qa`;");
        $this->execute("DROP TABLE IF EXISTS `qa_photo`;");
    }
}
