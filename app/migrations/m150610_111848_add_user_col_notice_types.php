<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_111848_add_user_col_notice_types extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'notificationsJson', Schema::TYPE_STRING . ' NOT NULL DEFAULT "[]"');
        $notifications = \yii\helpers\Json::encode([
            \app\db\records\Notifications::TYPE_NOTIFICATION_COMPLAINT_COMMENT,
            \app\db\records\Notifications::TYPE_NOTIFICATION_COMPLAINT,
            \app\db\records\Notifications::TYPE_NOTIFICATION_BOAST_COMMENT,
            \app\db\records\Notifications::TYPE_NOTIFICATION_BOAST,
            \app\db\records\Notifications::TYPE_NOTIFICATION_QA_COMMENT,
            \app\db\records\Notifications::TYPE_NOTIFICATION_QA,
            \app\db\records\Notifications::TYPE_NOTIFICATION_VOTE_BOAST,
            \app\db\records\Notifications::TYPE_NOTIFICATION_VOTE_COMMENT,
            \app\db\records\Notifications::TYPE_NOTIFICATION_VOTE_COMPLAINT
        ]);
        \app\db\records\User::updateAll(['notificationsJson'=>$notifications]);
    }

    public function down()
    {
        echo "m150610_111848_add_user_col_notice_types cannot be reverted.\n";

        return false;
    }
}