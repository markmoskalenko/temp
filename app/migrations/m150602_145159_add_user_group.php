<?php

use yii\db\Schema;
use yii\db\Migration;

class m150602_145159_add_user_group extends Migration
{
    public function up()
    {
        $this->alterColumn('user', 'group', "enum('user','admin','redactor','forumThemeModerator','forumTopicModerator','forumModerator') NOT NULL DEFAULT 'user'");
    }

    public function down()
    {
        echo "m150602_145159_add_user_group cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
