<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_133546_add_seller_id_column_to_forum_themes extends Migration
{
    public function up()
    {
        $this->addColumn('forum_board_theme', 'sellerId', 'INT(11) UNSIGNED DEFAULT NULL');
        $this->addForeignKey('fk_forum_board_theme_seller', 'forum_board_theme', 'sellerId', 'seller', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn('forum_board_theme', 'sellerId');
    }
}
