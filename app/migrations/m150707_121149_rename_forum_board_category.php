<?php

use yii\db\Schema;
use yii\db\Migration;

class m150707_121149_rename_forum_board_category extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_forum_topic_category', 'forum_board');
        $this->renameColumn('forum_board', 'category', 'categoryId');
        $this->addForeignKey('fk_forum_board_category', 'forum_board', 'categoryId', 'forum_category', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_forum_board_category', 'forum_board');
        $this->renameColumn('forum_board', 'categoryId', 'category');
        $this->addForeignKey('fk_forum_topic_category', 'forum_board', 'category', 'forum_category', 'id', 'CASCADE', 'CASCADE');
    }
}
