<?php

use yii\db\Schema;
use yii\db\Migration;

class m150710_114702_add_closed_column_to_forum_boards extends Migration
{
    public function up()
    {
        $this->addColumn('forum_board', 'isClosed', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('forum_board', 'isClosed');
    }
}
