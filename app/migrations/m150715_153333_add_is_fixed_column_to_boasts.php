<?php

use yii\db\Migration;

class m150715_153333_add_is_fixed_column_to_boasts extends Migration
{
    public function up()
    {
        $this->addColumn('boast', 'isFixed', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('boast', 'isFixed');
    }
}
