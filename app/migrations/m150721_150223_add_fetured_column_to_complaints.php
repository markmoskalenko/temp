<?php

use yii\db\Schema;
use yii\db\Migration;

class m150721_150223_add_fetured_column_to_complaints extends Migration
{
    public function up()
    {
        $this->addColumn('complaint', 'isFeatured', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('complaint', 'isFeatured');

    }
}
