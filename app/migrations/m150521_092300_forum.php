<?php

use yii\db\Schema;
use yii\db\Migration;

class m150521_092300_forum extends Migration
{
    public function safeUp()
    {
        $this->execute("
        CREATE TABLE `forum_category` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;");

        $this->execute("
            CREATE TABLE `forum_topic` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `title` varchar(255) NOT NULL,
              `description` text NOT NULL,
              `img` varchar(255) DEFAULT NULL,
              `slug` varchar(255) NOT NULL,
              `seoTitle` varchar(255) DEFAULT NULL,
              `seoDescription` varchar(255) DEFAULT NULL,
              `category` int(11) unsigned NOT NULL,
              PRIMARY KEY (`id`),
              KEY `category` (`category`),
              CONSTRAINT `fk_forum_topic_category` FOREIGN KEY (`category`) REFERENCES `forum_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->execute("DROP TABLE IF EXISTS `forum_category`;");
        $this->execute("DROP TABLE IF EXISTS `forum_topic`;");
        return false;
    }
}
