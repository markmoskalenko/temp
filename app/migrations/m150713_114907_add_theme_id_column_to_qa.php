<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_114907_add_theme_id_column_to_qa extends Migration
{
    public function up()
    {
        $this->addColumn('qa', 'themeId', 'INT(11) UNSIGNED');
    }

    public function down()
    {
        $this->dropColumn('qa', 'themeId');
    }
}
