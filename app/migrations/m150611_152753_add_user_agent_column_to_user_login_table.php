<?php

use yii\db\Schema;
use yii\db\Migration;

class m150611_152753_add_user_agent_column_to_user_login_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_login', 'userAgent', 'VARCHAR(500) NOT NULL AFTER ip');
    }
    
    public function safeDown()
    {
        $this->dropColumn('user_login', 'userAgent');
    }
}
