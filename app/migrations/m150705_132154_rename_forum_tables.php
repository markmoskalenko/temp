<?php

use yii\db\Schema;
use yii\db\Migration;

class m150705_132154_rename_forum_tables extends Migration
{
    public function up()
    {
        $this->renameTable('forum_topic', 'forum_board');
        $this->renameTable('forum_topic_theme', 'forum_board_theme');
        $this->renameTable('forum_topic_theme_comment', 'forum_comment');
        $this->renameTable('forum_topic_theme_history', 'forum_comment_history');
    }

    public function down()
    {
        $this->renameTable('forum_board', 'forum_topic');
        $this->renameTable('forum_board_theme', 'forum_topic_theme');
        $this->renameTable('forum_comment', 'forum_topic_theme_comment');
        $this->renameTable('forum_comment_history', 'forum_topic_theme_history');
    }
}
