<?php

use yii\db\Migration;

class m150701_124150_create_forum_image_table extends Migration
{
    public function safeUp()
    {
        $sql = 'CREATE TABLE `forum_image` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `userId` int(11) unsigned NOT NULL,
            `filename` varchar(255) DEFAULT NULL,
            `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

        $this->execute($sql);
    }
    
    public function safeDown()
    {
        $this->dropTable('forum_image');
    }
}
