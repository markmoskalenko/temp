<?php

use yii\db\Schema;
use yii\db\Migration;

class m150801_143910_add_featured_column_to_reviews extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'isFeatured', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('review', 'isFeatured');
    }
}
