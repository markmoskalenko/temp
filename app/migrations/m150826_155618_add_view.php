<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_155618_add_view extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE VIEW comment_likes AS SELECT
	`b`.`id` AS `id`,
	count(`v`.`id`) AS `likes`
FROM
	(
		(
			`comment` `b`
			LEFT JOIN `vote` `v` ON (
				(
					(`v`.`entityId` = `b`.`id`)
					AND (`v`.`entityName` = 'comment')
					AND (`v`.`userId` <> `b`.`authorId`)
				)
			)
		)
		LEFT JOIN `user` `u` ON ((`u`.`id` = `v`.`userId`))
	)
WHERE
	(
		(`u`.`isAffectRating` <> 0)
		AND (`u`.`emailConfirmation` = 1)
	)
GROUP BY
	`b`.`id`
        ");
    }

    public function down()
    {
        echo "m150826_155618_add_view cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
