<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_095811_forum_update_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('forum_topic_theme_history', 'themeId', Schema::TYPE_INTEGER.' UNSIGNED DEFAULT NULL');
        $this->addColumn('forum_topic_theme_history', 'commentId', Schema::TYPE_INTEGER.' UNSIGNED DEFAULT NULL');
    }

    public function safeDown()
    {
        echo "m150527_095811_forum_update_table cannot be reverted.\n";

        return false;
    }
}
