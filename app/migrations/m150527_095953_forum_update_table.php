<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_095953_forum_update_table extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_forum_history_comment', 'forum_topic_theme_history', 'commentId', 'forum_topic_theme_comment', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150527_095953_forum_update_table cannot be reverted.\n";

        return false;
    }
}
