<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_084342_forum_add_col extends Migration
{
    public function safeUp()
    {
        $this->addColumn('forum_topic_theme', 'bb_description', Schema::TYPE_TEXT . ' NOT NULL');
        $this->addColumn('forum_topic_theme_comment', 'bb_comment', Schema::TYPE_TEXT . ' NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('forum_topic_theme', 'bb_description');
        $this->dropColumn('forum_topic_theme_comment', 'bb_comment');
    }
}
