<?php

use yii\db\Schema;
use yii\db\Migration;

class m150721_150204_rename_boats_fixed_column_to_featured extends Migration
{
    public function up()
    {
        $this->renameColumn('boast', 'isFixed', 'isFeatured');
    }

    public function down()
    {
        $this->renameColumn('boast', 'isFeatured', 'isFixed');
    }
}
