<?php

use yii\db\Schema;
use yii\db\Migration;

class m150602_110023_add_col_forum extends Migration
{
    public function up()
    {
        $this->addColumn('forum_topic_theme', 'isClose', 'tinyint(1) not null default 0');
    }

    public function down()
    {
        echo "m150602_110023_add_col_forum cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
