<?php

use yii\db\Schema;
use yii\db\Migration;

class m150703_002850_rename_forum_comment_hide_column extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('forum_topic_theme_comment', 'isHide', 'isHidden');
    }
    
    public function safeDown()
    {
        $this->renameColumn('forum_topic_theme_comment', 'isHidden', 'isHide');
    }
}
