<?php

use yii\db\Schema;
use yii\db\Migration;

class m150815_122219_create_proxies_table extends Migration
{
    public function up()
    {
        $this->createTable('proxy', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'ip' => Schema::string(20)->notNull(),
            'port' => Schema::string(10)->notNull(),
            'requests' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'isValid' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
            'timeFailed' => 'TIMESTAMP NULL DEFAULT NULL',
            'timeCreated' => 'TIMESTAMP NULL DEFAULT NULL',
            'timeUpdated' => 'TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'PRIMARY KEY (id)',
        ]);
    }

    public function down()
    {
        $this->dropTable('proxy');
    }
}
