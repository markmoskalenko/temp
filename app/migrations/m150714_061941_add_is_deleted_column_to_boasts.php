<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_061941_add_is_deleted_column_to_boasts extends Migration
{
    public function up()
    {
        $this->addColumn('boast', 'isDeleted', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('boast', 'isDeleted');
    }
}
