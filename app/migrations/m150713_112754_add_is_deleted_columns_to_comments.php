<?php

use yii\db\Migration;

class m150713_112754_add_is_deleted_columns_to_comments extends Migration
{
    public function up()
    {
        $this->addColumn('comment', 'isDeleted', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('comment', 'isDeleted');
    }
}
