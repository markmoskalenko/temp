<?php

use yii\db\Schema;
use yii\db\Migration;

class m150707_125841_add_priority_to_forum_categories_and_boards extends Migration
{
    public function up()
    {
        $this->addColumn('forum_category', 'priority', 'TINYINT(3) NOT NULL DEFAULT 0');
        $this->addColumn('forum_board', 'priority', 'TINYINT(3) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('forum_category', 'priority');
        $this->dropColumn('forum_board', 'priority');
    }
}
