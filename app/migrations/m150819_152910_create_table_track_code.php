<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_152910_create_table_track_code extends Migration
{
    public function up()
    {
        $this->execute('
DROP TABLE IF EXISTS `tracking_number`;
CREATE TABLE `tracking_number` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trackingNumber` varchar(255) NOT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tracking_number_user`;
CREATE TABLE `tracking_number_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL,
  `trackingNumberId` int(10) unsigned NOT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT "0000-00-00 00:00:00",
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

');
    }

    public function down()
    {
        echo "m150819_152910_create_table_track_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
