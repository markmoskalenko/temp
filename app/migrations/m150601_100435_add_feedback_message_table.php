<?php

use yii\db\Migration;

class m150601_100435_add_feedback_message_table extends Migration
{
    public function up()
    {
        $sql = 'CREATE TABLE `feedback_message` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `userId` int(11) unsigned DEFAULT NULL,
          `userIp` varchar(20) DEFAULT NULL,
          `userAgent` varchar(250) DEFAULT NULL,
          `userName` varchar(250) DEFAULT NULL,
          `userEmail` varchar(250) DEFAULT NULL,
          `subject` varchar(250) DEFAULT NULL,
          `body` text,
          `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

        $this->execute($sql);
    }

    public function down()
    {
        $this->dropTable('feedback_message');
    }
}
