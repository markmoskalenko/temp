<?php

use yii\db\Schema;
use yii\db\Migration;

class m150802_235923_add_rejection_columns_to_reviews extends Migration
{
    public function up()
    {
        $this->addColumn('review', 'isRejected', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
        $this->addColumn('review', 'rejectReason', 'TEXT');
    }

    public function down()
    {
        $this->dropColumn('review', 'isRejected');
        $this->dropColumn('review', 'rejectReason');
    }
}
