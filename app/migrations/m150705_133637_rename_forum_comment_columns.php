<?php

use yii\db\Schema;
use yii\db\Migration;

class m150705_133637_rename_forum_comment_columns extends Migration
{
    public function up()
    {
        $this->renameColumn('forum_comment', 'comment', 'content');
    }

    public function down()
    {
        $this->renameColumn('forum_comment', 'content', 'comment');
    }
}
