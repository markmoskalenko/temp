<?php

use yii\db\Schema;
use yii\db\Migration;

class m150705_132828_rename_forum_theme_columns extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_forum_theme_topic', 'forum_board_theme');
        $this->renameColumn('forum_board_theme', 'topicId', 'boardId');
        $this->addForeignKey('fk_forum_theme_board', 'forum_board_theme', 'boardId', 'forum_board', 'id', 'CASCADE', 'CASCADE');

        $this->renameColumn('forum_board_theme', 'isHide', 'isHidden');
        $this->renameColumn('forum_board_theme', 'isClose', 'isClosed');
    }

    public function down()
    {
        $this->dropForeignKey('fk_forum_theme_board', 'forum_board_theme');
        $this->renameColumn('forum_board_theme', 'boardId', 'topicId');
        $this->addForeignKey('fk_forum_theme_topic', 'forum_board_theme', 'topicId', 'forum_board', 'id', 'CASCADE', 'CASCADE');


        $this->renameColumn('forum_board_theme', 'isHidden', 'isHide');
        $this->renameColumn('forum_board_theme', 'isClosed', 'isClose');
    }
}
