<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_134449_add_forum_category_column_to_qa_categories extends Migration
{
    public function up()
    {
        $this->addColumn('qa_category', 'boardId', 'INT(11) UNSIGNED');
    }

    public function down()
    {
        $this->dropColumn('qa_category', 'boardId');
    }
}
