<?php

use yii\db\Schema;
use yii\db\Migration;

class m150524_001128_add_views_columns_for_content extends Migration
{
    public function safeUp()
    {
        $this->addColumn('article', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER contentPreview');
        $this->createIndex('viewCount', 'article', 'viewCount');

        $this->addColumn('boast', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER score');
        $this->createIndex('viewCount', 'boast', 'viewCount');

        $this->addColumn('complaint', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER rejectReason');
        $this->createIndex('viewCount', 'complaint', 'viewCount');

        $this->addColumn('news', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER contentPreview');
        $this->createIndex('viewCount', 'news', 'viewCount');

        $this->addColumn('qa', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER status');
        $this->createIndex('viewCount', 'qa', 'viewCount');

        $this->addColumn('review', 'viewCount', 'INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER content');
        $this->createIndex('viewCount', 'review', 'viewCount');

    }

    public function safeDown()
    {
        $this->dropColumn('article', 'viewCount');
        $this->dropColumn('boast', 'viewCount');
        $this->dropColumn('complaint', 'viewCount');
        $this->dropColumn('news', 'viewCount');
        $this->dropColumn('qa', 'viewCount');
        $this->dropColumn('review', 'viewCount');
    }
}
