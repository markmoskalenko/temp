<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_132717_create_news_category_table extends Migration
{
    public function up()
    {
        $this->createTable('news_category', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'name' => Schema::string()->notNull(),
            'slug' => Schema::string()->notNull(),
            'content' => Schema::text(),
            'seoTitle' => Schema::string(),
            'seoDescription' => Schema::string(),
            'PRIMARY KEY (id)',
        ]);
    }

    public function down()
    {
        $this->dropTable('news_category');
    }
}
