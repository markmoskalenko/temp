<?php

use yii\db\Schema;
use yii\db\Migration;

class m150527_082006_forum extends Migration
{
    public function safeUp()
    {

        $this->execute("
        CREATE TABLE `forum_topic_theme` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `topicId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isFixed` tinyint(1) NOT NULL DEFAULT '0',
  `isHide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category` (`topicId`),
  KEY `userId` (`userId`),
  CONSTRAINT `fk_forum_theme_topic` FOREIGN KEY (`topicId`) REFERENCES `forum_topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_forum_theme_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
        ");

        $this->execute("
        CREATE TABLE `forum_topic_theme_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `themeId` int(11) unsigned NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isFixed` tinyint(1) NOT NULL DEFAULT '0',
  `isHide` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`themeId`),
  KEY `userId` (`userId`),
  CONSTRAINT `fk_comment_theme` FOREIGN KEY (`themeId`) REFERENCES `forum_topic_theme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_comment_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
");
        $this->execute("
        CREATE TABLE `forum_topic_theme_comment_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commentId` int(11) unsigned DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `timeCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `boastId` (`commentId`),
  CONSTRAINT `fk_image_forum_comment` FOREIGN KEY (`commentId`) REFERENCES `forum_topic_theme_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
");
        $this->execute("
        CREATE TABLE `forum_topic_theme_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` text NOT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) unsigned NOT NULL,
  `themeId` int(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `themeId` (`themeId`),
  KEY `userId` (`userId`),
  CONSTRAINT `fk_forum_history_theme` FOREIGN KEY (`themeId`) REFERENCES `forum_topic_theme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_forum_history_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
        $this->execute("
        CREATE TABLE `forum_topic_theme_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `themeId` int(11) unsigned DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `timeCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `boastId` (`themeId`),
  CONSTRAINT `fk_image_theme` FOREIGN KEY (`themeId`) REFERENCES `forum_topic_theme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
");
    }

    public function safeDown()
    {
        $this->execute("DROP TABLE IF EXISTS `forum_topic_theme`;");
        $this->execute("DROP TABLE IF EXISTS `forum_topic_theme_comment`;");
        $this->execute("DROP TABLE IF EXISTS `forum_topic_theme_comment_image`;");
        $this->execute("DROP TABLE IF EXISTS `forum_topic_theme_comment_image`;");
        $this->execute("DROP TABLE IF EXISTS `forum_topic_theme_image`;");
        return false;
    }
}
