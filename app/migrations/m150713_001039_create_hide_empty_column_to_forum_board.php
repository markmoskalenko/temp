<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_001039_create_hide_empty_column_to_forum_board extends Migration
{
    public function up()
    {
        $this->addColumn('forum_board', 'hideEmpty', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('forum_board', 'hideEmpty');
    }
}
