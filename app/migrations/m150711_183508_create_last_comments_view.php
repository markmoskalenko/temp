<?php

use yii\db\Schema;
use yii\db\Migration;

class m150711_183508_create_last_comments_view extends Migration
{
    public function up()
    {
        $sql = 'CREATE VIEW forum_comment_last AS
            SELECT * FROM forum_comment
            WHERE isDeleted = 0 AND isHidden = 0
            GROUP BY themeId
            ORDER BY timeUpdated';

        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP VIEW forum_comment_last');
    }
}
