<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_150107_add_inital_column_to_forum_comment_table extends Migration
{
    public function up()
    {
        $this->addColumn('forum_topic_theme_comment', 'isInitial', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('forum_topic_theme_comment', 'isInitial');
    }
}
