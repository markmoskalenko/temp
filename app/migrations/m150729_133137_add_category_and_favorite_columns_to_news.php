<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_133137_add_category_and_favorite_columns_to_news extends Migration
{
    public function up()
    {
        $this->addColumn('news', 'categoryId', 'INT(11) UNSIGNED DEFAULT NULL');
        $this->addForeignKey('news_categoryId_fk', 'news', 'categoryId', 'news_category', 'id', 'SET NULL', 'CASCADE');

        $this->addColumn('news', 'isFeatured', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropForeignKey('news_categoryId_fk', 'news');
        $this->dropColumn('news', 'categoryId');

        $this->dropColumn('news', 'isFeatured');
    }
}
