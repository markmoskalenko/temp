<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_151413_add_inRating_column_to_boasts_table extends Migration
{
    public function up()
    {
        $this->addColumn('boast', 'inRating', 'TINYINT(1) DEFAULT 1 AFTER visible');
    }

    public function down()
    {
        $this->dropColumn('boast', 'inRating');
    }
}
