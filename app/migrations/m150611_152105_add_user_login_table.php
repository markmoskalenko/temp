<?php

use yii\db\Schema;
use yii\db\Migration;

class m150611_152105_add_user_login_table extends Migration
{
    public function safeUp()
    {
        $sql = "CREATE TABLE `user_login` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `userId` int(11) unsigned NOT NULL,
          `ip` varchar(15) NOT NULL DEFAULT '',
          `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $this->execute($sql);
    }
    
    public function safeDown()
    {
        $this->dropTable('user_login');
    }
}
