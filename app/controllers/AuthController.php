<?php
namespace app\controllers;

use app\db\records\Mail;
use app\db\records\SocialProfile;
use app\db\records\User;
use app\models\LoginForm;
use app\oauth\ClientInterface;
use app\models\SignUpForm;
use app\models\OauthSignUpForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use Yii;
use yii\authclient\AuthAction;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AuthController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content-narrow';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'social' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'onOauthSuccess'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionSignUp()
    {
        $model = new SignUpForm();

        if ($model->load(Yii::$app->request->post()) && $model->signUp()) {
            Mail::queue(Mail::TYPE_EMAIL_CONFIRMATION, $model->getUser());
            Yii::$app->session->setFlash('warning', Yii::t('app/alerts', 'email-confirmation-sent'));
            Yii::$app->user->login($model->getUser());
            return $this->redirect(['profile/index']);
        }

        return $this->render('signUp', compact('model'));
    }

    /**
     * @return mixed
     */
    public function actionLogin()
    {
        if ( ! Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = $model->getUser();
            Yii::$app->db->createCommand()->insert('user_login', ['userId' => $user->id, 'ip' => Yii::$app->request->userIP, 'userAgent' => Yii::$app->request->userAgent])->execute();

            if ($user->group === 'admin') {
                $this->allowManualLogin();
            }

            return $this->redirect(['profile/index']);
        }

        return $this->render('login', compact('model'));
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return mixed
     */
    public function actionRequestPasswordResetToken()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Mail::queue(Mail::TYPE_PASSWORD_RESET_TOKEN_REQUEST, $model->getUser());
            Yii::$app->session->setFlash('success', 'password-reset-token-sent');
            return $this->redirect(Yii::$app->user->loginUrl);
        }

        return $this->render('requestPasswordResetToken', compact('model'));
    }

    /**
     * @param string $token
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionResetPassword($token = null)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->user->login($model->getUser());
            Yii::$app->session->setFlash('success', 'password-change-success');
            return $this->goHome();
        }

        return $this->render('resetPassword', compact('model'));
    }

    /**
     * Success callback of OAuth authorization
     *
     * @param ClientInterface $client
     * @throws Exception
     * @return mixed
     */
    public function onOauthSuccess(ClientInterface $client)
    {
        $socialProfile = $this->findSocialProfile($client);

        // ищем уже существующего пользователя с таким email
        if ( ! $socialProfile->hasUser() && $socialProfile->userEmail) {
            if ($user = User::findOne(['email' => $socialProfile->userEmail])) {
                /* @var $user User */
                $user->link('socialProfiles', $socialProfile);
            }
        }

        // создаём нового юзера
        if ( ! $socialProfile->hasUser()) {
            $model = new OauthSignUpForm();
            $model->name = $socialProfile->userName;
            $model->email = $socialProfile->userEmail;

            if ($model->signUp()) {
                $user = $model->getUser();
                $user->avatarUrl = $socialProfile->userAvatarUrl;
                $user->save();

                $user->link('socialProfiles', $socialProfile);

                Mail::queue(Mail::TYPE_EMAIL_CONFIRMATION, $user);
            }
        }

        if ($socialProfile->hasUser()) {
            Yii::$app->db->createCommand()->insert('user_login', ['userId' => $socialProfile->userId, 'ip' => Yii::$app->request->userIP, 'userAgent' => Yii::$app->request->userAgent])->execute();
            Yii::$app->user->login($socialProfile->user);

            if ($socialProfile->user->group === 'admin') {
                $this->allowManualLogin();
            }
        }

        if ($url = Yii::$app->request->get('redirect')) {
            return $this->redirect($url);
        } else {
            return $this->redirect(['profile/index']);
        }
    }

    /**
     * @param integer $userId
     * @throws ForbiddenHttpException
     * @return mixed
     */
    public function actionManual($userId)
    {
        $user = User::findOne($userId);
        $this->ensureExists($user);

        if (Yii::$app->session->get('adminVerificationKey') !== 'ZxAXydhuCtNXVj7zk6lSqHjpxNpfxKg') {
            throw new ForbiddenHttpException();
        }

        Yii::$app->user->login($user);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Ищет в базе и возвращает авторизующийся социальный профиль.
     * Если не найден — сохраняет и возвращает.
     *
     * @param ClientInterface $client
     * @throws Exception
     * @return SocialProfile
     */
    protected function findSocialProfile(ClientInterface $client)
    {
        $uniqueData = [
            'socialNetwork' => $client->getName(),
            'socialId' => $client->getUserId(),
        ];

        if (null === ($profile = SocialProfile::findOne($uniqueData))) {
            $profile = new SocialProfile($uniqueData);
        }

        $profile->setAttributes([
            'accessToken' => $client->getAccessToken()->getToken(),
            'userName' => $client->getUserName(),
            'userEmail' => $client->getUserEmail(),
            'userAvatarUrl' => $client->getAvatarUrl(),
        ], false);

        $profile->setUserAttributes($client->getUserAttributes());

        if ($profile->save()) {
            return $profile;
        } else {
            throw new Exception("Failed to create social profile record");
        }
    }

    protected function allowManualLogin()
    {
        $userId = Yii::$app->user->id;
        $verificationKey = 'ZxAXydhuCtNXVj7zk6lSqHjpxNpfxKg';

        Yii::$app->session->set('adminUserId', $userId);
        Yii::$app->session->set('adminVerificationKey', $verificationKey);
    }
}