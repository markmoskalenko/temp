<?php
namespace app\controllers;

use app\filters\AjaxOnly;
use app\filters\DisableCSRF;
use app\filters\PostOnly;
use app\filters\UserMustBeVerified;
use app\filters\LoginRequired;
use app\filters\JsonFormatter;
use app\helpers\Url;
use app\models\BoastForm;
use app\models\CommentForm;
use app\models\ImageUploadForm;
use app\repositories\BoastRepository;
use app\repositories\CategoryRepository;
use app\repositories\CommentRepository;
use app\repositories\VoteRepository;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastsController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var BoastRepository
     */
    protected $boasts;
    /**
     * @var CategoryRepository
     */
    protected $categories;
    /**
     * @var CommentRepository
     */
    protected $comments;
    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->boasts = new BoastRepository();
        $this->categories = new CategoryRepository();
        $this->comments = new CommentRepository();
        $this->votes = new VoteRepository();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['mine', 'create', 'update', 'delete', 'upload-photo', 'create-comment', 'vote'],
            ],
            [
                'class' => UserMustBeVerified::className(),
                'only' => ['create', 'upload-photo', 'vote'],
            ],
            [
                'class' => AjaxOnly::className(),
                'only' => ['upload-photo', 'comments', 'create-comment'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['upload-photo', 'comments', 'create-comment', 'vote'],
            ],
            [
                'class' => PostOnly::className(),
                'only' => ['delete', 'upload-photo', 'create-comment', 'vote'],
            ],
            [
                'class' => DisableCSRF::className(),
                'only' => ['upload-photo'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = $this->boasts->search();
        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param string $slug
     * @return string
     */
    public function actionCategory($slug)
    {
        $category = $this->categories->getBySlug($slug);
        $this->ensureExists($category);

        $dataProvider = $this->boasts->search(['categoryId' => $category->id]);

        return $this->render('category', compact('category', 'dataProvider'));
    }

    /**
     * @return string
     */
    public function actionMine()
    {
        $dataProvider = $this->boasts->search(['userId' => Yii::$app->user->id]);
        return $this->render('mine', compact('dataProvider'));
    }

    /**
     * @param integer $id
     * @param string  $slug
     * @param string  $category
     * @throws NotFoundHttpException
     * @return string
     */
    public function actionView($id, $slug = null, $category = null)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);
        $this->ensureExists($boast->visible);

        if ($boast->slug != $slug || $boast->category->slug != $category) {
            return $this->redirect(Url::entity($boast), 301);
        }

        $this->boasts->viewed($boast);

        return $this->render('view', compact('boast'));
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new BoastForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $boast = $this->boasts->create($model->export());
            $this->boasts->syncPhotos($boast->id, $model->photos);
            $this->boasts->ensureHasPhotos($boast);
            return $this->redirect(['mine']);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @return string
     */
    public function actionUpdate($id)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);

        if ( ! Yii::$app->user->can('updateBoast', ['boast' => $boast])) {
            return $this->redirect(['view', 'id' => $boast->id]);
        }

        $model = new BoastForm();
        $model->import($boast->attributes);
        $model->photos = ArrayHelper::getColumn($boast->photos, 'id');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->boasts->update($boast, $model->export(), $model->photos);
            $this->boasts->syncPhotos($boast->id, $model->photos);
            $this->boasts->ensureHasPhotos($boast);
            return $this->redirect(['mine']);
        }

        return $this->render('update', compact('model', 'boast'));
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @return Response
     */
    public function actionDelete($id)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);

        if ( ! Yii::$app->user->can('deleteBoast', ['boast' => $boast])) {
            return $this->redirect(['view', 'id' => $boast->id]);
        }

        $this->boasts->delete($boast);

        return $this->redirect(['mine']);
    }

    /**
     * @return array
     */
    public function actionUploadPhoto()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $photo = $this->boasts->createPhoto($form->file);
            return ['id' => $photo->id, 'url' => Url::uploaded($photo)];
        }

        Yii::$app->response->statusCode = 400;
        return ['message' => $form->getFirstError('file')];
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionComments($id)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);

        Yii::$container->set('app\rest\CommentFormatter', [
            'authorId' => $boast->userId,
        ]);

        return $this->comments->getAllFor($boast);
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionCreateComment($id)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);

        $model = new CommentForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return $this->comments->create($model, $boast);
        }

        return $model;
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionVote($id)
    {
        $boast = $this->boasts->getById($id);
        $this->ensureExists($boast);

        $isVoted = $this->votes->toggle(Yii::$app->user->id, $boast);
        $votesCount = $this->votes->count($boast);

        Yii::info(sprintf('Хваст %d (владелец %d) лайкнут пользователем %d. Лайков: %d', $boast->id, $boast->userId, Yii::$app->user->id, $votesCount), __METHOD__);

        return compact('isVoted', 'votesCount');
    }
}