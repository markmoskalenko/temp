<?php
namespace app\controllers;

use app\repositories\TrackingNumberRepository;
use app\tracking\Client;
use app\tracking\Response\Formatter;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class TrackController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var Client
     */
    public $api;

    /**
     * @var Formatter
     */
    public $formatter;

    /**
     * @var TrackingNumberRepository
     */
    public $repository;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->api = new Client(Yii::$app->params['tracking.apiKey']);
        $this->formatter = new Formatter();
        $this->repository = new TrackingNumberRepository();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'history' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $trackingNumber
     * @return mixed
     */
    public function actionAjaxDetectCourier($trackingNumber)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = $this->api->detectCourier($trackingNumber);

        return $this->formatter->courier( $response );
    }

    /**
     * @param $courierSlug
     * @param $trackingNumber
     * @return array
     * @throws \GdePosylka\Client\Exception\EmptyCourierSlug
     * @throws \GdePosylka\Client\Exception\EmptyTrackingNumber
     */
    public function actionAjaxAddTrackingNumber($courierSlug, $trackingNumber)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $this->api->addTracking($courierSlug, $trackingNumber);

        $this->repository->create(['trackingNumber'=>$trackingNumber]);
    }

    /**
     * @return array
     */
    public function actionAjaxGetCouriers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = $this->api->getCouriers();

        return $this->formatter->couriers( $response );
    }

    /**
     * @param $courierSlug
     * @param $trackingNumber
     * @return array
     * @throws \GdePosylka\Client\Exception\EmptyCourierSlug
     * @throws \GdePosylka\Client\Exception\EmptyTrackingNumber
     */
    public function actionAjaxGetTrackingInfo($courierSlug, $trackingNumber)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = $this->api->getTrackingInfo($courierSlug, $trackingNumber);

        return $this->formatter->info( $response );
    }

    /**
     * @return mixed
     */
    public function actionHistory()
    {
        $code = Yii::$app->request->post('code');
        $userId = Yii::$app->user->id;
        $ip = null;

        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        }

        $command = Yii::$app->db->createCommand()->insert('track_history', compact('userId', 'ip', 'code'));
        $command->sql = $command->rawSql . ' ON DUPLICATE KEY UPDATE `count` = `count` + 1, `timeUpdated` = NOW()';
        $command->execute();
    }

    /**
     * @param $name
     */
    public function actionGetImage($name){
        $url = "http://gdeposylka.ru/img/icons/128x128/{$name}";
        $path = Yii::getAlias('@webroot/uploads/track/'.$name);
        file_put_contents($path, file_get_contents($url));
        $this->refresh();
    }
}