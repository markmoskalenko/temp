<?php
namespace app\controllers;

use app\filters\JsonFormatter;
use app\filters\LoginRequired;
use app\helpers\Url;
use app\models\CommentForm;
use app\repositories\CommentRepository;
use app\repositories\NewsRepository;
use app\search\NewsSearch;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var NewsRepository
     */
    protected $news;
    /**
     * @var CommentRepository
     */
    protected $comments;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->news = new NewsRepository();
        $this->comments = new CommentRepository();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['create-comment'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['comments', 'create-comment'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $newsSearch = new NewsSearch();
        $dataProvider = $this->news->search($newsSearch);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param integer $id
     * @param string  $slug
     * @return mixed
     */
    public function actionView($id = null, $slug = null)
    {
        if ($id) {
            $news = $this->news->getById($id);
        } elseif ($slug) {
            $news = $this->news->getBySlug($slug);
        } else {
            $news = false;
        }

        $this->ensureExists($news);

        if ($news->id != $id || $news->slug !== $slug) {
            $this->redirect(Url::entity($news), 301);
        }

        $this->news->viewed($news);

        return $this->render('view', compact('news'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionComments($id)
    {
        $news = $this->news->getById($id);
        $this->ensureExists($news);

        return $this->comments->getAllFor($news);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionCreateComment($id)
    {
        $news = $this->news->getById($id);
        $this->ensureExists($news);

        $model = new CommentForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return $this->comments->create($model, $news);
        }

        return $model;
    }
}