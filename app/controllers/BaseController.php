<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseController extends Controller
{
    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     */
    public function ensureExists($condition)
    {
        if (!$condition) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @param string $permissionName
     * @param array $params
     * @param boolean $allowCaching
     * @throws ForbiddenHttpException
     */
    public function ensureAllowed($permissionName, $params = [], $allowCaching = true)
    {
        if (!Yii::$app->user->can($permissionName, $params, $allowCaching)) {
            throw new ForbiddenHttpException();
        }
    }

    public function ensureAjax()
    {

    }
}