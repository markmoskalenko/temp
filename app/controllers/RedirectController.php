<?php
namespace app\controllers;

use aliexpress\api\Client;
use aliexpress\api\Exception as ApiException;
use app\db\records\User;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RedirectController extends Controller
{
    /**
     * @param string $url
     * @return Response
     */
    public function actionIndex($url)
    {
        if (mb_strpos($url, 'aliexpress.com')) {
            return $this->actionCpa($url, 'trustlinks');
        }

        return $this->redirect($url);
    }

    /**
     * @param string $url
     * @param string $token
     * @return Response
     */
    public function actionAuth($url, $token)
    {
        if (Yii::$app->user->isGuest) {
            if ($user = User::findIdentityByAccessToken($token)) {
                Yii::$app->user->login($user);
            }
        }

        return $this->redirect($url);
    }

    /**
     * @param string $url
     * @param string $category
     * @throws Exception
     * @return Response
     */
    public function actionCpa($url, $category)
    {
        $api = new Client(Yii::$app->params['ali.apiKey']);

        $params = [
            'trackingId' => $category,
            'urls' => $url,
        ];

        try {
            $result = $api->getLinks($params);

            if (empty($result['promotionUrls'][0]['promotionUrl'])) {
                throw new ApiException('Failed to find a promotion url.');
            }

            $url = $result['promotionUrls'][0]['promotionUrl'];
        } catch (ApiException $e) {
            Yii::$app->raven->extraContext($params);
            Yii::$app->raven->captureException($e);
        }

        return $this->renderPartial('/site/redirect', compact('url'));
    }
}