<?php
namespace app\controllers;

use app\models\ProductSearch;
use Yii;
use yii\web\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductsController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->get(), '');
        $productSearch->validate();

        if (preg_match('/[а-яё]/', $productSearch->searchQuery) !== false) {
            $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query([
                'key' => 'trnsl.1.1.20150414T120819Z.b4059bd240e50b25.2fed295e2bf4a7399f573c284378693ab058505a',
                'text' => $productSearch->searchQuery,
                'lang' => 'en',
            ]);

            $result = @file_get_contents($url);
            $result = @json_decode($result, true);

            if (isset($result['text'][0])) {
                $productSearch->translatedSearchQuery = $result['text'][0];
            }
        }

        if ($attributes = array_filter($productSearch->attributes)) {
            if ( ! $productSearch->isEmpty) {
                $attributes['userId'] = Yii::$app->user->getId();
                Yii::$app->db->createCommand()->insert('product_search', $attributes)->execute();
            }
        }

        return $this->render('index', compact('productSearch'));
    }

    /**
     * @return mixed
     */
    public function actionSearch()
    {
        $productSearch = new ProductSearch();
        $productSearch->load(Yii::$app->request->post());
        return $this->redirect(['index'] + array_filter($productSearch->attributes));
    }
}