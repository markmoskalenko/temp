<?php
namespace app\controllers;

use app\db\records\Seller;
use app\models\SellerSearchForm;
use app\search\ComplaintSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * WidgetApi controller
 */
class WidgetApiController extends \yii\rest\Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ContentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $request = Yii::$app->request;
                            $permissions = Yii::$app->params['widget.api.access'];
                            $apiKey = $request->post('apiKey', null);
                            $checkSum = $request->post('checkSum', null);
                            $checkSumValue = $request->post('checkSumValue', null);
                            $host = $request->getReferrer();
                            $permission = isset($permissions[$apiKey]) ? $permissions[$apiKey] : false;

                            $controlSum = crypt($host . $checkSumValue, $permission['salt']);
                            if ($permission && $permission['host'] === $host && $controlSum == $checkSum) {
                                return true;
                            } else {
                                throw new ForbiddenHttpException('У вас нет доступа к этой странице');
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @return mixed
     */
    public function actionSearch()
    {
        $model = new SellerSearchForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $seller = Seller::findOr404(['originalStoreId' => $model->seller->originalStoreId]);

            $complaintSearch = new ComplaintSearch();
            $complaintSearch->sellerId = $seller->id;
            $complaintSearch->visible = true;

            $html = $this->renderPartial('search', [
                'seller' => $seller,
                'complaintSearch' => $complaintSearch
            ]);

            return compact('html');
        } else {
            Yii::$app->response->setStatusCode(422, 'Ошибка валидации данных.');

            return $model->getErrors();
        }
    }
}
