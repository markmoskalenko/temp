<?php
namespace app\controllers;

use app\filters\LoginRequired;
use app\filters\JsonFormatter;
use app\filters\UserMustBeVerified;
use app\repositories\CommentRepository;
use app\repositories\VoteRepository;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentsController extends BaseController
{
    /**
     * @var CommentRepository
     */
    protected $comments;
    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['delete', 'vote'],
            ],
            [
                'class' => UserMustBeVerified::className(),
                'only' => ['vote'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['vote'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->comments = new CommentRepository();
        $this->votes = new VoteRepository();
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $comment = $this->comments->getById($id);
        $this->ensureExists($comment);

        if ( ! Yii::$app->user->can('deleteComment', ['comment' => $comment])) {
            throw new ForbiddenHttpException();
        }

        $this->comments->delete($comment);
        Yii::$app->end(204);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionVote($id)
    {
        $comment = $this->comments->getById($id);
        $this->ensureExists($comment);

        $userId = Yii::$app->user->id;

        if ($this->votes->exists($userId, $comment)) {
            return ['status' => false, 'message' => 'Вы уже голосовали за этот комментарий.'];
        } else {
            $this->votes->create($userId, $comment);
            return ['status' => true, 'votesCount' => $this->votes->count($comment)];
        }
    }
}