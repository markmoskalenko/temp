<?php
namespace app\controllers;

use app\filters\LoginRequired;
use app\filters\JsonFormatter;
use app\repositories\LatestNewsRepository;
use app\repositories\NotificationRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class LatestNewsController
 * @package app\controllers
 */
class LatestNewsController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var LatestNewsRepository
     */
    protected $repository;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'rest' => [
                'class' => JsonFormatter::className(),
                'only' => ['index'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->repository = new LatestNewsRepository();
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($type)
    {
        return $this->repository->getLast($type);
    }
}