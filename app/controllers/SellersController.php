<?php
namespace app\controllers;

use app\db\records\Seller;
use app\helpers\Url;
use app\search\ComplaintSearch;
use app\search\SellerSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellersController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'content';


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $sellerSearch = new SellerSearch();

        return $this->render('index', [
            'sellerSearch' => $sellerSearch
        ]);
    }

    /**
     * @param integer $originalStoreId
     * @param string  $slug
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionView($originalStoreId, $slug)
    {
        $seller = Seller::findOr404(['originalStoreId' => $originalStoreId]);

        if ($seller->slug !== $slug) {
            return $this->redirect(Url::toSeller($seller));
        }

        $complaintSearch = new ComplaintSearch();
        $complaintSearch->sellerId = $seller->id;
        $complaintSearch->visible = true;

        return $this->render('view', [
            'seller' => $seller,
            'complaintSearch' => $complaintSearch,
        ]);
    }

    /**
     * @param integer $originalStoreId
     * @param string  $slug
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionComplaints($originalStoreId, $slug)
    {
        $seller = Seller::findOr404(['originalStoreId' => $originalStoreId]);

        if ($seller->slug !== $slug) {
            return $this->redirect(Url::toSellerComplaints($seller));
        }

        $complaintSearch = new ComplaintSearch();
        $complaintSearch->sellerId = $seller->id;
        $complaintSearch->visible = true;

        return $this->render('complaints', [
            'seller' => $seller,
            'complaintSearch' => $complaintSearch,
        ]);
    }

    /**
     * @param integer $originalStoreId
     * @param string  $slug
     * @return mixed
     */
    public function actionReviews($originalStoreId, $slug)
    {
        $seller = Seller::findOr404(['originalStoreId' => $originalStoreId]);
        return $this->redirect(Url::toSellerComplaints($seller), 301);
    }
}