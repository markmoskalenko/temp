<?php
namespace app\controllers;

use app\filters\AjaxOnly;
use app\filters\DisableCSRF;
use app\filters\PostOnly;
use app\filters\UserMustBeVerified;
use app\filters\JsonFormatter;
use app\filters\LoginRequired;
use app\helpers\BBCodeParser;
use app\helpers\Url;
use app\models\CommentForm;
use app\models\ImageUploadForm;
use app\repositories\CategoryRepository;
use app\repositories\CommentRepository;
use app\repositories\ReviewImageRepository;
use app\models\ReviewForm;
use app\repositories\ReviewRepository;
use app\repositories\VoteRepository;
use Yii;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public $layout = 'content';

    /**
     * @var CategoryRepository
     */
    protected $categories;
    /**
     * @var ReviewRepository
     */
    protected $reviews;
    /**
     * @var ReviewImageRepository
     */
    protected $images;
    /**
     * @var CommentRepository
     */
    protected $comments;
    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->categories = new CategoryRepository();
        $this->reviews = new ReviewRepository();
        $this->images = new ReviewImageRepository();
        $this->comments = new CommentRepository();
        $this->votes = new VoteRepository();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['mine', 'create', 'update', 'delete', 'upload-image', 'create-comment', 'vote'],
            ],
            [
                'class' => UserMustBeVerified::className(),
                'only' => ['create', 'upload-image', 'vote'],
            ],
            [
                'class' => AjaxOnly::className(),
                'only' => ['upload-image', 'comments', 'create-comment'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['upload-image', 'comments', 'create-comment', 'vote'],
            ],
            [
                'class' => PostOnly::className(),
                'only' => ['delete', 'upload-image', 'create-comment', 'vote'],
            ],
            [
                'class' => DisableCSRF::className(),
                'only' => ['upload-image'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->reviews->search(['visible' => 1]);
        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function actionCategory($slug)
    {
        $category = $this->categories->getBySlug($slug);
        $this->ensureExists($category);

        $dataProvider = $this->reviews->search(['categoryId' => $category->id, 'visible' => 1]);

        return $this->render('category', compact('dataProvider', 'category'));
    }

    /**
     * @return mixed
     */
    public function actionMine()
    {
        $dataProvider = $this->reviews->search(['userId' => Yii::$app->user->id]);
        return $this->render('mine', compact('dataProvider'));
    }

    /**
     * @param integer $id
     * @param string $slug
     * @return mixed
     */
    public function actionView($id, $slug = null)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        if ($review->slug !== $slug) {
            return $this->redirect(['view', 'id' => $review->id, 'slug' => $review->slug]);
        }

        $this->reviews->viewed($review);

        return $this->render('view', compact('review'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReviewForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->reviews->create($model->export());
            return $this->redirect(['mine']);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);
        $this->ensureAllowed('updateReview', ['review' => $review]);

        $model = new ReviewForm();
        $model->import($review);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->reviews->update($review, $model->export());
            return $this->redirect(['mine']);
        }

        return $this->render('update', compact('review', 'model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);
        $this->ensureAllowed('deleteReview', ['review' => $review]);

        $this->reviews->delete($review);

        return $this->redirect(['mine']);
    }

    /**
     * @return string
     */
    public function actionPreview()
    {
        $content = Yii::$app->request->rawBody;
        return BBCodeParser::process($content);
    }

    /**
     * @return array
     */
    public function actionUploadImage()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $image = $this->images->create($form->file);
            return ['id' => $image->id, 'url' => Url::uploaded($image)];
        }

        Yii::$app->response->statusCode = 400;
        return ['message' => $form->getFirstError('file')];
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionComments($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        return $this->comments->getAllFor($review);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionCreateComment($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        $model = new CommentForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return $this->comments->create($model, $review);
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionVote($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        $isVoted = $this->votes->toggle(Yii::$app->user->id, $review);
        $votesCount = $this->votes->count($review);
        return compact('isVoted', 'votesCount');
    }
}