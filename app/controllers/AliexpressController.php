<?php
namespace app\controllers;

use app\db\records\Seller;
use app\db\records\Snapshot;
use app\filters\AjaxOnly;
use app\filters\JsonFormatter;
use app\models\SnapshotForm;
use app\parser\entities\SellerEntity;
use app\parser\entities\StoreEntity;
use app\parser\handlers\StoreHandler;
use app\widgets\SnapshotWidget;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AliexpressController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AjaxOnly::className(),
                'only' => ['update-seller'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['check-url', 'update-seller'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionCheckUrl()
    {
        $response = ['status' => false];

        $form = new SnapshotForm();
        $form->externalUrl = Yii::$app->request->post('url');

        if ($form->validate() && $snapshot = $form->getSnapshot()) {
            $response['status'] = true;
            $response['snapshot'] = $snapshot;
        } else {
            $response['error'] = $form->getFirstError('externalUrl');
        }

        return $response;
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionUpdateSeller($id)
    {
        Yii::$app->session->close();

        /** @var $seller Seller */
        $seller = Seller::findOne($id);

        if (strtotime($seller->timeUpdated) > strtotime('4 hours ago')) {
            return ['updated' => false];
        }

        if ( ! $seller->originalStoreId) {
            return ['updated' => false];
        }

        $storeEntity = new StoreEntity($seller->originalStoreId);
        $contactsPage = $storeEntity->downloadContactsPage();

        if ($seller->originalSellerId) {
            $sellerEntity = new SellerEntity($seller->originalSellerId);
        } else {
            $sellerEntity = new SellerEntity($contactsPage->getSellerId());
        }

        $handler = new StoreHandler();
        $handler->importSellerRecord(
            $seller,
            $contactsPage,
            $sellerEntity->downloadStoreMobilePage(),
            $sellerEntity->downloadFeedbackPage(),
            $sellerEntity->downloadWishlistPage()
        );

        return ['updated' => true];
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionSnapshot($id)
    {
        $snapshot = Snapshot::findOne(['snapshotId' => $id]);
        $this->ensureExists($snapshot);

        return SnapshotWidget::widget(compact('snapshot'));
    }
}