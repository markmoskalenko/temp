<?php
namespace app\controllers;

use app\db\records\Notifications;
use app\filters\LoginRequired;
use app\filters\JsonFormatter;
use app\repositories\NotificationRepository;
use Yii;

class NotificationsController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var NotificationRepository
     */
    protected $notifications;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => LoginRequired::className(),
                'only' => ['index', 'new-notifications', 'notification-read'],
            ],
            'rest' => [
                'class' => JsonFormatter::className(),
                'only' => ['new-notifications', 'notification-read'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->notifications = new NotificationRepository();
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->notifications->search();

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionNewNotifications()
    {
        return $this->notifications->getCountNew();
    }

    /**
     * @param null $id
     * @param bool $all
     */
    public function actionNotificationRead($id = null, $all = false)
    {
        $this->notifications->setRead($id, $all);
    }
}