<?php
namespace app\controllers;

use app\db\records\FeedbackMessage;
use app\db\records\Page;
use app\models\FeedbackForm;
use app\models\SellerSearchForm;
use app\helpers\Url;
use app\repositories\BoastRepository;
use app\repositories\ComplaintRepository;
use app\search\ComplaintSearch;
use Yii;
use yii\base\UserException;
use yii\captcha\CaptchaAction;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'content';


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => CaptchaAction::className(),
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main';

        $complaints = new ComplaintRepository();
        $complaintDataProvider = $complaints->search(['isFeatured' => 1], 4);

        $boasts = new BoastRepository();
        $boastDataProvider = $boasts->searchRandom(['isFeatured' => 1], 8);

        return $this->render('index', compact('complaintDataProvider', 'boastDataProvider'));
    }

    /**
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            throw new NotFoundHttpException();
        }

        if ($exception instanceof HttpException) {
            $name = 'Ошибка ' . $exception->statusCode;
        } elseif ($exception instanceof UserException) {
            $name = $exception->getName();
        } else {
            $name = 'Упс...';
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'Возникла внутренняя ошибка сервера.');
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->render('error', [
                'name' => $name,
                'message' => $message,
                'exception' => $exception,
            ]);
        }
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function actionPage($slug)
    {
        $page = Page::findOr404(['slug' => $slug]);
        return $this->render('page', compact('page'));
    }

    /**
     * @return mixed
     */
    public function actionSearch()
    {
        $model = new SellerSearchForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->seller->updateCounters(['searchCount' => 1]);
            return $this->redirect(Url::toSeller($model->seller));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deprecated.
     *
     * @param string $url
     * @param string $token
     * @return mixed
     */
    public function actionRedirect($url, $token = null)
    {
        return $this->redirect(['redirect/auth', 'url' => $url, 'token' => $token]);
    }

    /**
     * @return mixed
     */
    public function actionFeedback()
    {
        $model = new FeedbackForm();
        $model->subject = Yii::$app->request->get('subject');

        if ( ! Yii::$app->user->isGuest) {
            $user = Yii::$app->user->getIdentity();
            $model->userName = $user->name;
            $model->userEmail = $user->email;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $feedback = new FeedbackMessage();
            $feedback->setAttributes($model->attributes);
            $feedback->subject = ArrayHelper::getValue($model->subjectLabels(), $model->subject);
            $feedback->save();

            Yii::$app->mailer->compose(['html' => 'feedback-html'], ['feedback' => $feedback])
                ->setTo('feedback@alitrust.ru')
                ->setFrom([$feedback->userEmail => $feedback->userName])
                ->setSubject(sprintf('Обращение с сайта alitrust.ru #%06d', $feedback->id))
                ->send();

            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

        return $this->render('feedback', compact('model'));
    }
}
