<?php
namespace app\controllers;

use app\helpers\Url;
use app\modules\forum\repositories\ThemeRepository;
use app\repositories\QuestionAnswerCategoryRepository;
use app\repositories\QuestionAnswerRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class QuestionAnswerController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var QuestionAnswerRepository
     */
    protected $qa;
    /**
     * @var QuestionAnswerCategoryRepository
     */
    protected $categories;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->qa = new QuestionAnswerRepository();
        $this->categories = new QuestionAnswerCategoryRepository();
    }

    /**
     * @param string $slug
     * @return string
     */
//    public function actionIndex($slug = null)
//    {
//        $dataProvider = $this->qa->search();
//        return $this->render('index', compact('dataProvider', 'slug'));
//    }

    /**
     * @param string $slug
     * @return string
     * @throws NotFoundHttpException
     */
//    public function actionCategory($slug = null)
//    {
//        $category = $this->categories->getBySlugOr404($slug);
//        $dataProvider = $this->qa->search(['qaCategoryId' => $category->id]);
//
//        return $this->render('category', compact('dataProvider', 'slug', 'category'));
//    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $qa = $this->qa->getById($id);
        $this->ensureExists($qa);

        $themes = new ThemeRepository();
        $theme = $themes->byId($qa->themeId);
        $this->ensureExists($theme);

        return $this->redirect(Url::entity($theme), 301);
    }
}