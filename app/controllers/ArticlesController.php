<?php
namespace app\controllers;

use app\filters\JsonFormatter;
use app\filters\LoginRequired;
use app\helpers\Url;
use app\models\CommentForm;
use app\repositories\ArticleRepository;
use app\repositories\CommentRepository;
use app\search\ArticleSearch;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticlesController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var ArticleRepository
     */
    protected $articles;
    /**
     * @var CommentRepository
     */
    protected $comments;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->articles = new ArticleRepository();
        $this->comments = new CommentRepository();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['create-comment'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['comments', 'create-comment'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $this->articles->search($searchModel);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function actionCategory($slug)
    {
        $category = $this->articles->getCategoryBySlug($slug);
        $this->ensureExists($category);

        $searchModel = new ArticleSearch(['categoryId' => $category->id]);
        $dataProvider = $this->articles->search($searchModel);

        return $this->render('category', compact('dataProvider', 'category'));
    }

    /**
     * @param integer $id
     * @param string  $slug
     * @param string  $category
     * @return mixed
     */
    public function actionView($id = null, $slug = null, $category = null)
    {
        if ($id) {
            $article = $this->articles->getById($id);
        } elseif ($slug) {
            $article = $this->articles->getBySlug($slug);
        } else {
            $article = false;
        }

        $this->ensureExists($article);

        if ($article->id != $id || $article->slug !== $slug || $article->category->slug !== $category) {
            return $this->redirect(Url::entity($article), 301);
        }

        $this->articles->viewed($article);

        return $this->render('view', compact('article'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionComments($id)
    {
        $article = $this->articles->getById($id);
        $this->ensureExists($article);

        return $this->comments->getAllFor($article);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionCreateComment($id)
    {
        $article = $this->articles->getById($id);
        $this->ensureExists($article);

        $model = new CommentForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return $this->comments->create($model, $article);
        }

        return $model;
    }
}