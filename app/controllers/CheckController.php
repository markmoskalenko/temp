<?php
namespace app\controllers;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CheckController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
