<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class IframeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->renderPartial('index');
    }
}