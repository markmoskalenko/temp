<?php
namespace app\controllers;

use app\filters\AjaxOnly;
use app\filters\DisableCSRF;
use app\filters\PostOnly;
use app\filters\UserMustBeVerified;
use app\filters\LoginRequired;
use app\filters\JsonFormatter;
use app\helpers\Url;
use app\models\CommentForm;
use app\models\ComplaintForm;
use app\models\ImageUploadForm;
use app\models\UploadTrait;
use app\repositories\CommentRepository;
use app\repositories\ComplaintRepository;
use app\repositories\VoteRepository;
use app\search\ComplaintSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintsController extends BaseController
{
    use UploadTrait;

    /**
     * @var string
     */
    public $layout = 'content';

    /**
     * @var ComplaintRepository
     */
    protected $complaints;
    /**
     * @var CommentRepository
     */
    protected $comments;
    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->complaints = new ComplaintRepository();
        $this->comments = new CommentRepository();
        $this->votes = new VoteRepository();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['mine', 'create', 'update', 'delete', 'create-comment', 'upload-photo', 'vote'],
            ],
            [
                'class' => UserMustBeVerified::className(),
                'only' => ['create', 'upload-photo', 'create-comment', 'vote'],
            ],
            [
                'class' => AjaxOnly::className(),
                'only' => ['upload-photo', 'comments', 'create-comment'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['upload-photo', 'comments', 'create-comment', 'vote'],
            ],
            [
                'class' => PostOnly::className(),
                'only' => ['delete', 'upload-photo', 'create-comment', 'vote'],
            ],
            [
                'class' => DisableCSRF::className(),
                'only' => ['upload-photo'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $complaintSearch = new ComplaintSearch();
        $complaintSearch->approved = true;

        return $this->render('index', [
            'dataProvider' => $complaintSearch->dataProvider(),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionMine()
    {
        $user = Yii::$app->user->getIdentity();

        $complaintSearch = new ComplaintSearch();
        $complaintSearch->userId = $user->id;

        return $this->render('mine', [
            'dataProvider' => $complaintSearch->dataProvider(),
        ]);
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     * @return string
     */
    public function actionView($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);

        if ($complaint->rejected && $complaint->userId !== Yii::$app->user->id) {
            throw new NotFoundHttpException();
        }

        $this->complaints->viewed($complaint);

        return $this->render('view', compact('complaint'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ComplaintForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->complaints->create($model->attributes, $model->photos);
            Yii::$app->session->setFlash('success', Yii::t('app/alerts', 'complaint-create-success'));
            return $this->redirect(['mine']);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionUpdate($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);
        $this->ensureAllowed('updateComplaint', ['complaint' => $complaint]);

        $model = new ComplaintForm();
        $model->setAttributes($complaint->attributes);
        $model->photos = ArrayHelper::getColumn($complaint->photos, 'id');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $complaint->setAttributes(['moderated' => 0, 'rejected' => 0], $safeOnly = false);
            $this->complaints->update($complaint, $model->attributes, $model->photos);
            return $this->redirect(['mine']);
        }

        return $this->render('update', compact('model', 'complaint'));
    }

    /**
     * @param integer $id
     * @throws ForbiddenHttpException
     * @return mixed
     */
    public function actionDelete($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);
        $this->ensureAllowed('deleteComplaint', ['complaint' => $complaint]);

        Yii::$app->session->setFlash('success', Yii::t('app/alerts', 'complaint-delete-success'));
        $complaint->delete();

        return $this->redirect(['mine']);
    }

    /**
     * @return array
     */
    public function actionUploadPhoto()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $photo = $this->complaints->createPhoto($form->file);
            return ['id' => $photo->id, 'url' => Url::uploaded($photo)];
        }

        Yii::$app->response->statusCode = 400;
        return ['message' => $form->getFirstError('file')];
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionComments($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);

        Yii::$container->set('app\rest\CommentFormatter', [
            'authorId' => $complaint->userId,
            'enableSort' => true,
        ]);

        return $this->comments->getAllFor($complaint);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionCreateComment($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);

        $model = new CommentForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return $this->comments->create($model, $complaint);
        }

        return $model;
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionVote($id)
    {
        $complaint = $this->complaints->getById($id);
        $this->ensureExists($complaint);

        $isVoted = $this->votes->toggle(Yii::$app->user->id, $complaint);
        $votesCount = $this->votes->count($complaint);
        return compact('isVoted', 'votesCount');
    }
}