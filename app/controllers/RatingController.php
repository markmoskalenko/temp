<?php
namespace app\controllers;

use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RatingController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $query = (new Query)->select('u.*, COUNT(bl.id) as boasts_total, SUM(bl.likes) as likes_total, COUNT(br.id) as boasts_rating, SUM(br.likes) as likes_rating, SUM(br.rating) AS rating ')
            ->from('user AS u')
            ->andWhere(['b.visible' => 1, 'b.inRating' => 1, 'u.group' => 'user'])
            ->leftJoin('boast AS b', 'b.userId = u.id')
            ->leftJoin('boast_likes AS bl', 'bl.id = b.id')
            ->leftJoin('boast_rating AS br', 'br.id = b.id')
            ->limit(50)
            ->groupBy('u.id')
            ->orderBy('rating DESC, likes_total DESC, boasts_total DESC')
            ->having('rating IS NOT NULL');

        $firstDayOfCurrentMonth = date('Y-m-01 00:00:00');
        $lastDayOfLastMonth = date('Y-m-d 23:59:59', strtotime('last day of last month'));
        $firstDayOfLastMonth = date('Y-m-d 00:00:00', strtotime('first day of last month'));

        $currentMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['>=', 'b.timeCreated', $firstDayOfCurrentMonth]),
            'pagination' => false,
        ]);

        $lastMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['between', 'b.timeCreated', $firstDayOfLastMonth, $lastDayOfLastMonth]),
            'pagination' => false,
        ]);

        $overallDataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('index', compact('currentMonthDataProvider', 'lastMonthDataProvider', 'overallDataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionBoasts()
    {
        $query = (new Query)->select('u.*, COUNT(bl.id) as boasts_total, SUM(bl.likes) as likes_total, COUNT(br.id) as boasts_rating, SUM(br.likes) as likes_rating, SUM(br.rating) AS rating ')
            ->from('user AS u')
            ->andWhere(['b.visible' => 1, 'b.inRating' => 1, 'u.group' => 'user'])
            ->leftJoin('boast AS b', 'b.userId = u.id')
            ->leftJoin('boast_likes AS bl', 'bl.id = b.id')
            ->leftJoin('boast_rating AS br', 'br.id = b.id')
            ->limit(50)
            ->groupBy('u.id')
            ->orderBy('rating DESC, likes_total DESC, boasts_total DESC')
            ->having('rating IS NOT NULL');

        $firstDayOfCurrentMonth = date('Y-m-01 00:00:00');
        $lastDayOfLastMonth = date('Y-m-d 23:59:59', strtotime('last day of last month'));
        $firstDayOfLastMonth = date('Y-m-d 00:00:00', strtotime('first day of last month'));

        $currentMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['>=', 'b.timeCreated', $firstDayOfCurrentMonth]),
            'pagination' => false,
        ]);

        $lastMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['between', 'b.timeCreated', $firstDayOfLastMonth, $lastDayOfLastMonth]),
            'pagination' => false,
        ]);

        $overallDataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('boast', compact('currentMonthDataProvider', 'lastMonthDataProvider', 'overallDataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionComments()
    {
        $query = (new Query)->select('u.*, COUNT(bl.id) as comment_total, SUM(bl.likes) as likes_total')
            ->from('user AS u')
            ->leftJoin('comment AS b', 'b.authorId = u.id')
            ->leftJoin('comment_likes AS bl', 'bl.id = b.id')
            ->limit(50)
            ->andWhere(['b.isDeleted' => 0, 'u.group' => 'user'])
            ->groupBy('u.id')
            ->orderBy('likes_total DESC, comment_total DESC');


        $firstDayOfCurrentMonth = date('Y-m-01 00:00:00');
        $lastDayOfLastMonth = date('Y-m-d 23:59:59', strtotime('last day of last month'));
        $firstDayOfLastMonth = date('Y-m-d 00:00:00', strtotime('first day of last month'));

        $currentMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['>=', 'b.timeCreated', $firstDayOfCurrentMonth]),
            'pagination' => false,
        ]);

        $lastMonthDataProvider = new ActiveDataProvider([
            'query' => Query::create($query)->andWhere(['between', 'b.timeCreated', $firstDayOfLastMonth, $lastDayOfLastMonth]),
            'pagination' => false,
        ]);

        $overallDataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('comment', compact('currentMonthDataProvider', 'lastMonthDataProvider', 'overallDataProvider'));
    }
}