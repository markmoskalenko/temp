<?php
namespace app\controllers;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DisputeController extends BaseController
{
    /**
     * @var string
     */
    public $layout = 'content';


    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}