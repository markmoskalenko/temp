<?php
namespace app\controllers;

use app\db\records\Mail;
use app\db\records\User;
use app\filters\JsonFormatter;
use app\models\ImageUploadForm;
use app\models\UpdatePasswordForm;
use app\models\ChangeEmailForm;
use app\models\ProfileForm;
use app\models\UploadTrait;
use app\repositories\BoastRepository;
use app\repositories\ComplaintRepository;
use app\repositories\FollowerRepository;
use app\repositories\UserRepository;
use app\services\ImageUploadService;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProfileController extends BaseController
{
    use UploadTrait;

    /**
     * @var string
     */
    public $layout = 'content-narrow';

    /**
     * @var UserRepository
     */
    protected $user;
    /**
     * @var FollowerRepository
     */
    protected $followers;


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'resend-email-confirmation'],
                'rules' => [
                    [
                        'actions' => ['index', 'resend-email-confirmation'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'format' => [
                'class' => JsonFormatter::className(),
                'only' => ['upload-avatar'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->user = new UserRepository();
        $this->followers = new FollowerRepository();
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->actionBoasts($id);
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionBoasts($id)
    {
        $this->layout = 'content';

        $user = $this->user->getById($id);
        $this->ensureExists($user);

        $boasts = new BoastRepository();
        $dataProvider = $boasts->search(['userId' => $user->id]);

        return $this->render('boasts', compact('user', 'dataProvider'));
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionComplaints($id)
    {
        $this->layout = 'content';

        $user = $this->user->getById($id);
        $this->ensureExists($user);

        $boasts = new ComplaintRepository();
        $dataProvider = $boasts->search(['userId' => $user->id, 'rejected' => 0]);

        return $this->render('complaints', compact('user', 'dataProvider'));
    }

    /**
     * @param $id
     */
    public function actionSubscribe($id)
    {
        $this->followers->create($id);
        $this->redirect(['view', 'id'=>$id]);
    }

    /**
     * @param $id
     */
    public function actionUnSubscribe($id)
    {
        $this->followers->delete($id);

        $this->redirect(['view', 'id'=>$id]);
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->getUser()->getIdentity();
        $data = Yii::$app->getRequest()->post();
        $isAjax = Yii::$app->getRequest()->getIsAjax();

        $profileModel = new ProfileForm($user);
        if ($profileModel->load($data) && $profileModel->save()) {
            if ($profileModel->scenario === ProfileForm::SCENARIO_EMAILABLE && $user->email) {
                Mail::queue(Mail::TYPE_EMAIL_CONFIRMATION, $user);
                Yii::$app->session->setFlash('warning', Yii::t('app/alerts', 'email-confirmation-sent'));
            }

            return $this->redirect(['index']);
        }

        $changeEmailModel = new ChangeEmailForm($user);
        if ($changeEmailModel->load($data)) {
            if ($isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($changeEmailModel);
            } elseif ($changeEmailModel->save()) {
                Mail::queue(Mail::TYPE_EMAIL_CONFIRMATION, $user);
                Yii::$app->session->setFlash('warning', Yii::t('app/alerts', 'email-confirmation-sent'));

                return $this->redirect(['index']);
            }
        }

        $updatePasswordModel = new UpdatePasswordForm($user);
        if ($updatePasswordModel->load($data)) {
            if ($isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($updatePasswordModel);
            } elseif ($updatePasswordModel->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app/alerts', 'password-change-success'));

                return $this->redirect(['index']);
            }
        }

        return $this->render('index', compact('profileModel', 'changeEmailModel', 'updatePasswordModel'));
    }

    /**
     * @param string $token
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionEmailConfirmation($token)
    {
        if (null === ($user = User::findOne(['emailConfirmationToken' => $token]))) {
            throw new BadRequestHttpException(Yii::t('app/alerts', 'email-confirmation-token-wrong'));
        }

        /* @var $user User */
        $user->setEmailConfirmed();
        $user->save(false);

        if (!$user->passwordHash) {
            Mail::queue(Mail::TYPE_PASSWORD_DELIVERY, $user);
        }

        Yii::$app->session->setFlash('success', Yii::t('app/alerts', 'email-confirmation-success'));

        Yii::$app->user->login($user);

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionResendEmailConfirmation()
    {
        $user = Yii::$app->user->getIdentity();

        if ($user->email && !Yii::$app->user->isEmailConfirmed) {
            $previous = Mail::find()
                ->where(['email' => $user->email, 'type' => Mail::TYPE_EMAIL_CONFIRMATION])
                ->andWhere(['>', 'timeCreated', date('Y-m-d H:i:s', strtotime('5 minutes ago'))])
                ->count();

            if ($previous) {
                Yii::$app->session->setFlash('error', Yii::t('app/alerts', 'email-confirmation-resend-timeout'));
            } else {
                Mail::queue(Mail::TYPE_EMAIL_CONFIRMATION, $user);
                Yii::$app->session->setFlash('success', Yii::t('app/alerts', 'email-confirmation-sent'));
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionUploadAvatar()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $filename = $this->createUniqueFileName($form->file);
            $destination = Yii::getAlias('@webroot/uploads/' . $filename);
            $url = Yii::getAlias('@web/uploads/' . $filename);

            $uploader = new ImageUploadService();
            $uploader->save($form->file->tempName, $destination);

            $user = Yii::$app->user->getIdentity();
            $user->updateAttributes(['avatarUrl' => $url]);

            return ['url' => $url];
        }

        Yii::$app->response->statusCode = 400;

        return ['errors' => $form->getErrors('file')];
    }
}