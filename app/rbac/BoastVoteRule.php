<?php
namespace app\rbac;

use app\db\records\Boast;
use app\repositories\VoteRepository;
use DateInterval;
use DateTime;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class BoastVoteRule
 * @package app\rbac
 */
class BoastVoteRule extends Rule
{
    public $name = 'BoastVoteRule';

    /**
     * @var VoteRepository
     */
    public $vote;

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $this->vote = new VoteRepository();

        $time = ArrayHelper::getValue($params, 'user.timeCreated');
        $userId = ArrayHelper::getValue($params, 'user.id');

        if (!$time) {
            return false;
        }

        $now = new DateTime();

        $compare = new \DateTime($time);
        $compare->add(DateInterval::createFromDateString('10 day'));

        if ($compare < $now) {
            return true;
        }

        $count = $this->vote->getCountByUserOfDay($userId, new Boast());

        if ($count == 10) {
            return false;
        }

        return true;
    }
}