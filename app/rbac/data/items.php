<?php
return [
    'user' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'voteBoast',
            'updateOwnBoast',
            'deleteComment',
            'voteComment',
            'updateCreatedComplaint',
            'updateRejectedComplaint',
            'updateOwnReview',
            'updateRejectedReview',
            'createForumThemeInOpenBoard',
            'updateOwnForumTheme',
            'createForumCommentInOpenTheme',
            'updateOwnForumComment',
        ],
    ],
    'forumThemeModerator' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'user',
        ],
    ],
    'forumTopicModerator' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'forumThemeModerator',
        ],
    ],
    'forumModerator' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'forumTopicModerator',
        ],
    ],
    'redactor' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'user',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'redactor',
            'forumModerator',
            'updateBoast',
            'updateComplaint',
            'updateReview',
            'createForumTheme',
            'updateForumTheme',
            'deleteForumTheme',
            'moderateForumTheme',
            'pinForumTheme',
            'closeForumTheme',
            'moveForumTheme',
            'createForumComment',
            'updateForumComment',
            'deleteForumComment',
            'pinForumComment',
            'hideForumComment',
        ],
    ],
    'voteBoast' => [
        'type' => 2,
        'ruleName' => 'BoastVoteRule',
    ],
    'updateBoast' => [
        'type' => 2,
        'children' => [
            'deleteBoast',
        ],
    ],
    'updateOwnBoast' => [
        'type' => 2,
        'ruleName' => 'updateOwnBoastRule',
        'children' => [
            'updateBoast',
        ],
    ],
    'deleteBoast' => [
        'type' => 2,
    ],
    'deleteComment' => [
        'type' => 2,
        'ruleName' => 'deleteCommentRule',
    ],
    'voteComment' => [
        'type' => 2,
        'ruleName' => 'voteCommentRule',
    ],
    'updateComplaint' => [
        'type' => 2,
        'children' => [
            'deleteComplaint',
        ],
    ],
    'updateCreatedComplaint' => [
        'type' => 2,
        'ruleName' => 'updateCreatedComplaintRule',
        'children' => [
            'updateComplaint',
        ],
    ],
    'updateRejectedComplaint' => [
        'type' => 2,
        'ruleName' => 'updateRejectedComplaintRule',
        'children' => [
            'updateComplaint',
        ],
    ],
    'deleteComplaint' => [
        'type' => 2,
    ],
    'updateReview' => [
        'type' => 2,
        'children' => [
            'deleteReview',
        ],
    ],
    'updateOwnReview' => [
        'type' => 2,
        'ruleName' => 'updateOwnReviewRule',
        'children' => [
            'updateReview',
        ],
    ],
    'updateRejectedReview' => [
        'type' => 2,
        'ruleName' => 'updateRejectedReviewRule',
        'children' => [
            'updateReview',
        ],
    ],
    'deleteReview' => [
        'type' => 2,
    ],
    'createForumTheme' => [
        'type' => 2,
    ],
    'createForumThemeInOpenBoard' => [
        'type' => 2,
        'ruleName' => 'openBoardRule',
        'children' => [
            'createForumTheme',
        ],
    ],
    'updateForumTheme' => [
        'type' => 2,
    ],
    'updateOwnForumTheme' => [
        'type' => 2,
        'ruleName' => 'updateOwnForumThemeRule',
        'children' => [
            'updateForumTheme',
        ],
    ],
    'deleteForumTheme' => [
        'type' => 2,
    ],
    'moderateForumTheme' => [
        'type' => 2,
    ],
    'pinForumTheme' => [
        'type' => 2,
    ],
    'closeForumTheme' => [
        'type' => 2,
    ],
    'moveForumTheme' => [
        'type' => 2,
    ],
    'createForumComment' => [
        'type' => 2,
    ],
    'createForumCommentInOpenTheme' => [
        'type' => 2,
        'ruleName' => 'openThemeRule',
        'children' => [
            'createForumComment',
        ],
    ],
    'updateForumComment' => [
        'type' => 2,
    ],
    'updateOwnForumComment' => [
        'type' => 2,
        'ruleName' => 'updateOwnForumCommentRule',
        'children' => [
            'updateForumComment',
        ],
    ],
    'deleteForumComment' => [
        'type' => 2,
    ],
    'pinForumComment' => [
        'type' => 2,
    ],
    'hideForumComment' => [
        'type' => 2,
    ],
];
