<?php
namespace app\rbac;

use app\repositories\VoteRepository;
use yii\rbac\Rule;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NotVotedRule extends Rule
{
    /**
     * @var string attribute with voteable entity
     */
    public $attribute;


    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (empty($params[$this->attribute])) {
            return false;
        }

        $entity = $params[$this->attribute];
        $votes = new VoteRepository();

        if ($votes->exists($user, $entity)) {
            return false;
        }

        return true;
    }
}