<?php
namespace app\rbac;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NotOwnerRule extends OwnerRule
{
    /**
     * @var boolean
     */
    public $inverse = true;
}