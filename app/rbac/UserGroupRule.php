<?php
namespace app\rbac;

use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Checks if user group matches.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserGroupRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'userGroup';


    /**
     * @param integer $user
     * @param Item    $item
     * @param array   $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        $group = Yii::$app->user->identity->group;

        if ($item->name === 'admin') {
            return $group == 'admin';
        }

        if ($item->name === 'forumModerator') {
            return $group == 'admin' || $group == 'forumModerator';
        }

        if ($item->name === 'forumTopicModerator') {
            return $group == 'admin' || $group == 'forumTopicModerator' || $group == 'forumModerator';
        }

        if ($item->name === 'forumThemeModerator') {
            return $group == 'admin' || $group == 'forumTopicModerator' || $group == 'forumModerator' || $group == 'forumThemeModerator';
        }

        if ($item->name === 'redactor') {
            return $group == 'admin' || $group == 'redactor';
        }

        if ($item->name === 'user') {
            return $group == 'admin' || $group == 'redactor'|| $group == 'user' || $group == 'forumThemeModerator' || $group == 'forumTopicModerator' || $group == 'forumModerator';
        }

        return false;
    }
}