<?php
namespace app\rbac;

use app\db\records\Comment;
use yii\rbac\Rule;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NotCommentedRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'notCommented';


    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (empty($params['comment'])) {
            return false;
        }

        $children = Comment::find()
            ->where(['parentId' => $params['comment']->id])
            ->count();

        if ($children) {
            return false;
        }

        return true;
    }
}