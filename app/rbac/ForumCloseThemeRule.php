<?php
namespace app\rbac;

use app\modules\forum\entities\Moderator;
use app\modules\forum\entities\Theme;
use yii\rbac\Rule;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ForumCloseThemeRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if(\Yii::$app->user->isGuest) return false;

        $group = \Yii::$app->user->identity->group;
        $type = Moderator::getTypeByGroup($group);
        $comment = $params['comment'];

        if ($type == Moderator::TYPE_MODERATOR_FORUM) {
            return true;
        } elseif ($type == Moderator::TYPE_MODERATOR_TOPIC) {
            if ($comment instanceof Theme) {
                $topicId = $comment->boardId;
            } else {
                $topicId = $comment->theme->topic->id;
            }

            return Moderator::find()
                ->byOwner()
                ->byType(Moderator::TYPE_MODERATOR_TOPIC)
                ->byObject($topicId)
                ->exists();
        } else {
            return false;
        }

    }
}