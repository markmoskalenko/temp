<?php
namespace app\rbac;

use app\modules\forum\entities\Moderator;
use yii\rbac\Rule;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ForumCommentDeleteRule extends Rule
{
    /**
     * @var string attribute in passed array of params that contains time to compare.
     */
    public $attribute;
    /**
     * @var string DateInterval specification or time in relative format
     * @see http://php.net/manual/ru/dateinterval.construct.php
     * @see http://php.net/manual/ru/dateinterval.createfromdatestring.php
     */
    public $type;

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if(\Yii::$app->user->isGuest) return false;

        $group = \Yii::$app->user->identity->group;
        $type = Moderator::getTypeByGroup($group);
        if ($type == Moderator::TYPE_MODERATOR_FORUM) {
            return true;
        } else {
            return false;
        }

    }
}