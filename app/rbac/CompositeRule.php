<?php
namespace app\rbac;

use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CompositeRule extends Rule
{
    /**
     * @var Rule[]
     */
    public $rules = [];


    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        foreach ($this->rules as $rule) {
            if ( ! $rule->execute($user, $item, $params)) {
                return false;
            }
        }

        return true;
    }
}