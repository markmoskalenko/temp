<?php
return [
    'auth.login.title' => 'Вход на сайт',
    'auth.login.header-email' => 'Войти через E-mail',
    'auth.login.header-social' => 'Войти через',
    'auth.login.button-sign-in' => 'Войти',
    'auth.login.button-sign-up' => 'Регистрация',
    'auth.login.button-reset-password' => 'Забыли пароль?',

    'auth.request-password-reset-token.title' => 'Восстановление пароля',
    'auth.request-password-reset-token.header' => 'Восстановление пароля',
    'auth.request-password-reset-token.text' => 'Для восстановления пароля укажите ваш E-mail',
    'auth.request-password-reset-token.button-request-token' => 'Восстановить',
    'auth.request-password-reset-token.button-sign-in' => 'Войти на сайт',
    'auth.request-password-reset-token.button-sign-up' => 'Зарегистрироваться',

    'auth.reset-password.title' => 'Восстановление пароля',
    'auth.reset-password.header' => 'Восстановление пароля',
    'auth.reset-password.action' => 'Изменить пароль',

    'auth.sign-up.title' => 'Регистрация',
    'auth.sign-up.header-email' => 'Регистрация через E-mail',
    'auth.sign-up.header-social' => 'Войти через',
    'auth.sign-up.button-sign-up' => 'Зарегистрироваться',
    'auth.sign-up.button-sign-in' => 'Войти',

    'articles.index.title' => 'Статьи',
    'articles.index.header' => 'Статьи',

    'boasts.index.title' => 'Фото-хвасты товаров с AliExpress',
    'boasts.mine.title' => 'Мои фото-хвасты',
    'boasts.create.title' => 'Добавление нового фото-хваста',

    'news.index.title' => 'Новости',
    'news.index.header' => 'Новости',

    'products.index.title' => 'Поиск товаров',
];