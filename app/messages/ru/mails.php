<?php
return [
    'email-confirmation.subject' => 'Подтверждение E-mail',
    'email-confirmation.header' => 'Добро пожаловать',
    'email-confirmation.body-html' => 'Всего один шаг отделяет вас от подтверждения почтового адреса.<br />Нажмите на кнопку ниже, чтобы...',
    'email-confirmation.body-text' => 'Для подтвеждения электронного ящика перейдите по ссылке ниже.',
    'email-confirmation.action' => 'Активировать аккаунт',

    'password-reset-token.subject' => 'Восстановление пароля',
    'password-reset-token.header' => 'Восстановление пароля',
    'password-reset-token.body-html' => 'Для подтверждения получения забытого пароля<br /> просто нажмите на кнопку ниже.',
    'password-reset-token.body-text' => 'Для подтверждения получения забытого пароля просто перейдите по ссылке ниже.',
    'password-reset-token.action' => 'Восстановить пароль',

    'password-delivery.subject' => 'Данные вашего аккаунта',
    'password-delivery.header' => 'Спасибо за регистрацию',
    'password-delivery.body' => 'Ваши данные для входа:',
    'password-delivery.field-email' => 'E-mail:',
    'password-delivery.field-password' => 'Пароль:',
    'password-delivery.action' => 'Перейти на сайт',

    'review-rejected.subject' => 'Ваш отзыв был отклонён',
    'review-rejected.summary' => 'Ваш отзыв был отклонен модератором. Исправьте его пожалуйста для публикации.',
    'review-rejected.header' => 'Ваш отзыв отклонён модератором',
    'review-rejected.action' => 'Исправить отзыв',

    'footer.rights' => 'Все права защищены.',
];