<?php
return [
    'change-email-form.email' => 'Введите новый адрес',

    'complaint.link' => 'Ссылка на продавца или товар',
    'complaint.text' => 'Текст отзыва',

    'update-password-form.password' => 'Новый пароль',
    'update-password-form.password-repeat' => 'Повторите пароль',

    'user.email' => 'Электронная почта',
    'user.name' => 'Ваше имя',
    'user.city' => 'Город',
    'user.password' => 'Пароль',
];