<?php
return [
    'required' => 'Это поле обязательно для заполнения',
    'required-short' => 'Обзяательное поле',

    'ali-express-form.link-required' => 'Необходимо указать ссылку на продавца или товар.',
    'ali-express-form.link-incorrect-url' => 'Указан некорректный URL.',
    'ali-express-form.link-incorrect-format' => 'Такая ссылка не поддерживается.',
    'ali-express-form.link-store-removed' => 'Этот магазин больше не доступен на AliExpress.',
    'ali-express-form.link-product-removed' => 'Этот товар больше не доступен на AliExpress.',
    'ali-express-form.link-http-error' => 'Ошибка получения данных. Попрбойте позже.',

    'login-form.login-failed' => 'Неверный логин или пароль.',

    'update-password-form.password-repeat-not-match' => 'Введённые пароли не совпадают.',

    'user.email-required' => 'Необходимо указать адрес электронной почты.',
    'user.email-incorrect-format' => 'Такой адрес электронной почты не поддерживается.',
    'user.name-required' => 'Укажите своё имя, чтобы мы знали как к вам обращаться.',
    'user.password-required' => 'Необходимо указать пароль.',
    'user.password-too-short' => 'Пароль должен быть длинее {min, number} {min, plural, one{символа} few{символов} many{символов} other{символов}}.',
];