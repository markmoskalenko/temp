<?php
namespace app\search;

use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewSearch extends Model
{
    /**
     * @var integer
     */
    public $userId;
    /**
     * @var integer
     */
    public $visible;
}