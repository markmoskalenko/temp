<?php
namespace app\search;

use app\db\records\Page;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PageSearch extends Page
{
    /**
     * @var string
     */
    public $language;


    /**
     * @return ActiveDataProvider
     */
    public function dataProvider()
    {
        $query = Page::find();

        if ($this->language) {
            $query->speaking($this->language);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}