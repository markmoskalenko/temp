<?php
namespace app\search;

use app\db\records\User;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserSearch extends User
{
    /**
     * @return ActiveDataProvider
     */
    public function dataProvider()
    {
        $query = User::find();

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
