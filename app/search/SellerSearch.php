<?php
namespace app\search;

use app\db\records\Seller;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerSearch extends Seller
{
    /**
     * @return ActiveDataProvider
     */
    public function dataProvider()
    {
        $query = Seller::find()
            ->closed(false)
            ->hasComplaints()
            ->orderByComplaintsCount()
            ->last();

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}