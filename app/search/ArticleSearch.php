<?php
namespace app\search;

use app\db\records\Article;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticleSearch extends Article
{
    /**
     * @var string
     */
    public $language;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if (empty($this->language)) {
            $this->language = Yii::$app->language;
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }
}