<?php
namespace app\search;

use app\db\records\Complaint;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintSearch extends Complaint
{
    /**
     * @var boolean
     */
    public $approved;
    /**
     * @var boolean
     */
    public $moderatable;
    /**
     * @var boolean
     */
    public $rejected;
    /**
     * @var boolean
     */
    public $visible;


    /**
     * @return ActiveDataProvider
     */
    public function dataProvider()
    {
        $query = Complaint::find()
            ->with(['seller', 'photos', 'user']);

        $query->andFilterWhere([
            'sellerId' => $this->sellerId,
        ]);

        if ($this->approved) {
            $query->approved();
        }

        if ($this->moderatable) {
            $query->moderatable();
        }

        if ($this->rejected) {
            $query->rejected();
        }

        if ($this->visible) {
            $query->visible();
        }

        if ($this->userId) {
            $query->andWhere(['userId' => $this->userId]);
            $query->orderBy('rejected DESC');
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'timeCreated' => SORT_DESC,
                ],
                'attributes' => [
                    'timeCreated' => [
                        'asc' => ['timeCreated' => SORT_ASC],
                        'desc' => ['timeCreated' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'дате создания',
                    ],
                ],
            ],
        ]);
    }
}