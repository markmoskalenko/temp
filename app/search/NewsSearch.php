<?php
namespace app\search;

use app\db\records\News;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsSearch extends News
{
    /**
     * @var string
     */
    public $language;


    /**
     * @inheritdoc
     */
    public function init()
    {
        if (empty($this->language)) {
            $this->language = Yii::$app->language;
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }
}