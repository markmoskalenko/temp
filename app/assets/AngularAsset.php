<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AngularAsset
 */
class AngularAsset extends AssetBundle
{
    public $sourcePath = '@bower/angular';

    public $js = [
        'angular.min.js',
    ];

    public $css = [
        'angular-csp.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}