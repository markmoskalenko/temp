<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class TrackerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-categories-selectize.js',
        'js/frontend-track.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\SelectizePluginAsset',
        'app\assets\AngularLocalStorageAsset',
    ];
}
