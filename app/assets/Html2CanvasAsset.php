<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Html2CanvasAsset extends AssetBundle
{
    public $sourcePath = '@bower/html2canvas/build';

    public $js = [
        'html2canvas.min.js',
    ];
}
