<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MasonryAsset extends AssetBundle
{
    public $sourcePath = '@bower/masonry/dist';
    public $js = [
        'masonry.pkgd.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
