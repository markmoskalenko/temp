<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CouponsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-coupons.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\AngularLocalStorageAsset',
        'app\assets\AngularSocketIoAsset',
        'app\assets\TimeAgoAsset',
        'app\assets\Html5WebNotificationAsset',
    ];
}
