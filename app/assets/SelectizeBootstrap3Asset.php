<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SelectizeBootstrap3Asset extends AssetBundle
{
    public $sourcePath = '@bower/selectize/dist';

    public $css = [
        'css/selectize.bootstrap3.css',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
