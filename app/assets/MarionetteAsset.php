<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MarionetteAsset extends AssetBundle
{
    public $sourcePath = '@bower/marionette/lib';

    public $js = [
        'backbone.marionette.min.js'
    ];

    public $depends = [
        'app\assets\BackboneAsset',
    ];
}