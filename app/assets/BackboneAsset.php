<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BackboneAsset extends AssetBundle
{
    public $sourcePath = '@bower/backbone';

    public $js = [
        'backbone.js',
    ];

    public $depends = [
        'app\assets\UnderscoreAsset',
        'yii\web\JqueryAsset',
    ];
}