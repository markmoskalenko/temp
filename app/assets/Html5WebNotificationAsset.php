<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Html5WebNotificationAsset extends AssetBundle
{
    public $sourcePath = '@bower/HTML5-Desktop-Notifications';

    public $js = [
        'desktop-notify-min.js',
    ];
}
