<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/frontend.css',
    ];

    public $js = [
        'js/frontend-application.js',
        'js/frontend.js',
        'js/frontend-notification.js'
    ];

    public $depends = [
        'app\assets\MarionetteAsset',
        'app\assets\AngularAsset',
        'app\assets\AngularLocalStorageAsset',
        'app\assets\AngularSanitizeAsset',
        'app\assets\AngularSocketIoAsset',
        'app\assets\AutosizeAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
