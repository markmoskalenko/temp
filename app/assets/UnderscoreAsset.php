<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UnderscoreAsset extends AssetBundle
{
    public $sourcePath = '@bower/underscore';

    public $js = [
        'underscore.js',
    ];
}