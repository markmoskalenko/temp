<?php
namespace app\assets;

use yii\web\AssetBundle;

class RedactorCutPluginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/redactor-cut.js',
    ];

    public $depends = [
        'vova07\imperavi\Asset',
    ];
}