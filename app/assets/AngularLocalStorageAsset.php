<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AngularLocalStorageAsset extends AssetBundle
{
    public $sourcePath = '@bower/angular-local-storage/dist';

    public $js = [
        'angular-local-storage.min.js',
    ];

    public $depends = [
        'app\assets\AngularAsset',
    ];
}
