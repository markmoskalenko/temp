<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastListAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-boast-list.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\MasonryAsset',
        'app\assets\ImagesLoadedAsset',
        'app\assets\InfiniteScrollAsset',
        'app\assets\LikeButtonAsset',
    ];
}
