<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImagesUploaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/common-images-uploader.js',
    ];

    public $depends = [
        'app\assets\AngularAsset',
        'app\assets\FileApiAsset',
        'app\assets\AppAsset',
    ];
}
