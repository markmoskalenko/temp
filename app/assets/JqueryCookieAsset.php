<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class JqueryCookieAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery.cookie';

    public $js = [
        'jquery.cookie.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
