<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImagesLoadedAsset extends AssetBundle
{
    public $sourcePath = '@bower/imagesloaded';

    public $js = [
        'imagesloaded.pkgd.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
