<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-complaint.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\FancyboxAsset',
    ];
}