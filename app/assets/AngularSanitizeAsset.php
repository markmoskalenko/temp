<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AngularAsset
 */
class AngularSanitizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/angular-sanitize';

    public $js = [
        'angular-sanitize.min.js',
    ];

    public $depends = [
        'app\assets\AngularAsset',
    ];
}