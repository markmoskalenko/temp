<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FreewallAsset extends AssetBundle
{
    public $sourcePath = '@bower/freewall';

    public $js = [
        'freewall.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}