<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SocialLikesAsset extends AssetBundle
{
    public $sourcePath = '@bower/social-likes';

    public $css = [
        'social-likes_birman.css',
    ];

    public $js = [
        'social-likes.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
