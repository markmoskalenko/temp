<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SocketIoAsset extends AssetBundle
{
    public $sourcePath = '@bower/socketio-client';

    public $js = [
        'socket.io.js',
    ];
}