<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LikeButtonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-like-button.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'yii\web\JqueryAsset',
    ];
}
