<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BemAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-bem';

    public $js = [
        'jquery-bem.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
