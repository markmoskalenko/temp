<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AutosizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/autosize/dist';

    public $js = [
        'autosize.min.js',
    ];
}
