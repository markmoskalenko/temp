<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class InfiniteScrollAsset extends AssetBundle
{
    public $sourcePath = '@vendor/webcreate/jquery-ias/src';

    public $js = [
        'callbacks.js',
        'jquery-ias.js',
        'extension/noneleft.js',
        'extension/spinner.js',
        'extension/paging.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
