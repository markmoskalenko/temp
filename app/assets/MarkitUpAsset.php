<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MarkitUpAsset extends AssetBundle
{
    public $sourcePath = '@bower/markitup/markitup';

    public $js = [
        'jquery.markitup.js',
    ];

    public $css = [
        'skins/markitup/style.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}