<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AngularSocketIoAsset extends AssetBundle
{
    public $sourcePath = '@bower/angular-socket-io';

    public $js = [
        'socket.min.js',
    ];

    public $depends = [
        'app\assets\AngularAsset',
        'app\assets\SocketIoAsset',
    ];
}
