<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MorrisAsset extends AssetBundle
{
    public $sourcePath = '@bower/morris.js';

    public $css = [
        'morris.css',
    ];

    public $js = [
        'morris.min.js',
    ];

    public $depends = [
        'app\assets\RaphaelAsset',
        'yii\web\JqueryAsset',
    ];
}
