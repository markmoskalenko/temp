<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LightSliderAsset extends AssetBundle
{
    public $sourcePath = '@bower/lightslider/dist';

    public $css = [
        'css/lightslider.min.css',
    ];

    public $js = [
        'js/lightslider.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
