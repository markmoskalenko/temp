<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SelectizePluginAsset extends AssetBundle
{
    public $sourcePath = '@bower/selectize/dist';

    public $js = [
        'js/standalone/selectize.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\JqueryAsset',
    ];
}
