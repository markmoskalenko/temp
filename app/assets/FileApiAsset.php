<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FileApiAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery.fileapi';

    public $js = [
        'FileAPI/FileAPI.min.js',
        'jquery.fileapi.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}