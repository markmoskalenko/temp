<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RedactorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/common-redactor.js',
    ];

    public $depends = [
        'app\assets\AngularAsset',
        'yii\web\JqueryAsset',
    ];
}
