<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BackboneLocalStorageAsset extends AssetBundle
{
    public $sourcePath = '@bower/backbone.localStorage';

    public $js = [
        'backbone.localStorage-min.js',
    ];

    public $depends = [
        'app\assets\BackboneAsset',
    ];
}
