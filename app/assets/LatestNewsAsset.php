<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class LatestNewsAsset
 */
class LatestNewsAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $js = [
        'js/frontend-latest-news.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\AngularAsset',
    ];
}
