<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FancyboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/fancybox/source';

    public $css = [
        'jquery.fancybox.css',
        'helpers/jquery.fancybox-thumbs.css'
    ];

    public $js = [
        'jquery.fancybox.pack.js',
        'helpers/jquery.fancybox-thumbs.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}