<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RatyAsset extends AssetBundle
{
    public $sourcePath = '@bower/raty/lib';

    public $css = [
        'jquery.raty.css',
    ];

    public $js = [
        'jquery.raty.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
