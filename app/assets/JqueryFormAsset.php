<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class JqueryFormAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-form';

    public $js = [
        'jquery.form.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
