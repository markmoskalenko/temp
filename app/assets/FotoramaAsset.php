<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FotoramaAsset extends AssetBundle
{
    public $sourcePath = '@bower/fotorama';

    public $css = [
        'fotorama.css',
    ];

    public $js = [
        'fotorama.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}