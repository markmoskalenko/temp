<?php
namespace app\assets;

use yii\web\AssetBundle;

class TimeAgoAsset extends AssetBundle
{
    public $sourcePath = '@bower/timeago';

    public $js = [
        'jquery.timeago.js',
        'locales/jquery.timeago.ru.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
