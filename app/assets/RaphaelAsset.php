<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RaphaelAsset extends AssetBundle
{
    public $sourcePath = '@bower/raphael';

    public $js = [
        'raphael-min.js',
    ];
}
