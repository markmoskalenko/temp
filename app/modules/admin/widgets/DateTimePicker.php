<?php
namespace app\modules\admin\widgets;

use app\modules\admin\assets\DateTimePickerAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DateTimePicker extends InputWidget
{
    /**
     * @var array
     * @see https://github.com/Eonasdan/bootstrap-datetimepicker
     */
    public $clientOptions = [];
    /**
     * @var array
     */
    public $options = [
        'class' => 'form-control',
    ];


    /**
     * Executes the widget.
     */
    public function run()
    {
        DateTimePickerAsset::register($this->view);

        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->value, $this->options);
        }

        $selector = '#' . $this->options['id'];
        $options = Json::encode($this->clientOptions);

        $this->view->registerJs("jQuery('$selector').datetimepicker($options);");
    }
}