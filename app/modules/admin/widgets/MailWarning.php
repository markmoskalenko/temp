<?php
namespace app\modules\admin\widgets;

use app\db\records\Mail;
use yii\base\Widget;
use yii\bootstrap\Alert;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MailWarning extends Widget
{
    public function run()
    {
        $lastMail = Mail::find()->orderBy('timeSendSucceed DESC')->one();

        if ( ! $lastMail) {
            return null;
        }

        if (strtotime($lastMail->timeSendSucceed) > strtotime('1 day ago')) {
            return null;
        }

        return Alert::widget([
            'options' => ['class' => 'alert-warning'],
            'body' => 'Последнее письмо было отправлено ' . Html::tag('strong', \Yii::$app->formatter->asDatetime($lastMail->timeSendSucceed)),
        ]);
    }
}