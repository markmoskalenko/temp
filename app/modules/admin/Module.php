<?php
namespace app\modules\admin;

use yii\helpers\Url;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\admin\controllers';


    /**
     * Initializes the module.
     */
    public function init()
    {
        parent::init();

        Yii::$app->errorHandler->errorAction = 'admin/site/error';
    }

    /**
     * This method is invoked right before an action within this module is executed.
     *
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( ! parent::beforeAction($action)) {
            return false;
        }

        Yii::$container->set('app\modules\admin\widgets\DateTimePicker', [
            'clientOptions' => [
                'format' => 'YYYY-MM-DD HH:mm',
            ],
        ]);

        Yii::$container->set('vova07\imperavi\Widget', [
            'settings' => [
                'imageUpload' => Url::to(['site/upload-image']),
                'imageManagerJson' => Url::to(['site/last-images']),
                'plugins' => ['fullscreen', 'imagemanager'],
            ],
        ]);

        Yii::$container->set('yii\widgets\LinkPager', [
            'disabledPageCssClass' => 'hidden',
            'firstPageLabel' => 'Первая',
            'prevPageLabel' => 'Предыдущая',
            'nextPageLabel' => 'Следующаяя',
            'lastPageLabel' => 'Последняя',
        ]);

        return true;
    }
}