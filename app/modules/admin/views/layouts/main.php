<?php
use app\db\records\Complaint;
use app\modules\admin\assets\ModuleAsset;
use app\modules\admin\widgets\MailWarning;
use otsec\yii2\fladmin\MainMenu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this    yii\web\View */
/* @var $content string */

ModuleAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <section id="container">
        <!--header start-->
        <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
            </div>

            <a href="/" class="logo">
                <span>Ali</span>Trust
            </a>

            <div class="top-nav">
                <ul class="nav pull-right top-menu">
                    <li class="dropdown language">
                        <a href="<?= Url::to(['auth/logout']) ?>">
                            <span class="fa fa-unlock-alt"></span>
                            <span class="username">Выход</span>
                        </a>
                    </li>
                </ul>
            </div>
        </header>
        <!--header end-->

        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse">
                <?= MainMenu::widget([
                    'encodeLabels' => false,
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '<i class="fa fa-fw fa-dashboard"></i> Дашборд',
                            'url' => ['dashboard/index'],
                            'active' => (Yii::$app->controller->id === 'dashboard'),
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-sitemap"></i> Категории',
                            'url' => ['categories/index'],
                            'active' => (Yii::$app->controller->id === 'categories'),
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-thumbs-down"></i> Жалобы',
                            'url' => ['complaints/index'],
                            'active' => (Yii::$app->controller->id === 'complaints'),
                            'items' => [
                                [
                                    'label' => 'На модерации <span class="badge">'.Complaint::find()->where(['moderated' => 0])->count().'</span>',
                                    'url' => ['complaints/new'],
                                ],
                                [
                                    'label' => 'Проверенные <span class="badge">'.Complaint::find()->where(['moderated' => 1])->count().'</span>',
                                    'url' => ['complaints/approved'],
                                ],
                                [
                                    'label' => 'Отклонённые <span class="badge">'.Complaint::find()->where(['rejected' => 1])->count().'</span>',
                                    'url' => ['complaints/rejected'],
                                ],
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-newspaper-o"></i> Новости',
                            'url' => ['news/index'],
                            'items' => [
                                [
                                    'label' => 'Публикации',
                                    'url' => ['news/index'],
                                    'active' => (Yii::$app->controller->id === 'news'),
                                ],
                                [
                                    'label' => 'Категории',
                                    'url' => ['news-categories/index'],
                                    'active' => (Yii::$app->controller->id === 'news-categories'),
                                ],
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-files-o"></i> Статьи',
                            'url' => ['articles/index'],
                            'items' => [
                                [
                                    'label' => 'Публикации',
                                    'url' => ['articles/index'],
                                    'active' => (Yii::$app->controller->id === 'articles'),
                                ],
                                [
                                    'label' => 'Категории',
                                    'url' => ['article-categories/index'],
                                    'active' => (Yii::$app->controller->id === 'article-categories'),
                                ],
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-file"></i> Страницы',
                            'url' => ['pages/index'],
                            'active' => (Yii::$app->controller->id === 'pages'),
                        ],
                        [
                            'label' => '<i class="fa fa-fw fa-users"></i> Пользователи',
                            'url' => ['users/index'],
                            'active' => (Yii::$app->controller->id === 'users'),
                        ],
                    ],
                ]); ?>
            </div>
        </aside>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => ['label' => '<i class="fa fa-home"></i>', 'url' => ['site/index']],
                    'encodeLabels' => false,
                ]) ?>
                
                <?= MailWarning::widget() ?>
                
                <?= $content; ?>
            </section>
        </section>
        <!--main content end-->

        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                <?= date('Y') ?> &copy; <?= Yii::powered(); ?>.
                <a href="#" class="go-top">
                    <i class="fa fa-angle-up"></i>
                </a>
            </div>
        </footer>
        <!--footer end-->
    </section>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
