<?php
/* @var $category app\db\records\ArticleCategory */
/* @var $this     yii\web\View */

$this->title = 'Новая категория';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', compact('category')) ?>