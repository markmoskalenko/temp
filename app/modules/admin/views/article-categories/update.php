<?php
/* @var $category app\db\records\ArticleCategory */
/* @var $this     yii\web\View */

$this->title = $category->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $category->name;
?>

<?= $this->render('_form', compact('category')) ?>