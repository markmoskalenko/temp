<?php
use app\assets\MorrisAsset;

/* @var $this yii\web\View */
/* @var $contentData array */
/* @var $userData array */
/* @var $counters array */
/* @var $votesData array */
/* @var $commentsData array */
/* @var $tracksData array */

$this->title = 'Панель управления';
$this->params['breadcrumbs'][] = $this->title;

$contentDataJson = json_encode($contentData);
$userDataJson = json_encode($userData);
$votesDataJson = json_encode($votesData);
$commentsDataJson = json_encode($commentsData);
$emailsDataJson = json_encode($emailsData);
$tracksDataJson = json_encode($tracksData);

MorrisAsset::register($this);
?>

<div class="row state-overview">
    <div class="col-lg-3 col-sm-6">
        <section class="panel">
            <div class="symbol terques">
                <i class="fa fa-user"></i>
            </div>
            <div class="value">
                <h1 class="count"><?= $counters['users'] ?></h1>
                <p><?= Yii::$app->i18n->format('{n, plural, =0{Пользователей} one{Пользователь} few{Пользователя} many{Пользователей} other{Пользователя}}', ['n' => $counters['users']], Yii::$app->language) ?></p>
            </div>
        </section>
    </div>
    <div class="col-lg-3 col-sm-6">
        <section class="panel">
            <div class="symbol red">
                <i class="fa fa-bullhorn"></i>
            </div>
            <div class="value">
                <h1 class=" count2"><?= $counters['complaints'] ?></h1>
                <p><?= Yii::$app->i18n->format('{n, plural, =0{Жалоб} one{Жалоба} few{Жалобы} many{Жалоб} other{Жалобы}}', ['n' => $counters['complaints']], Yii::$app->language) ?></p>
            </div>
        </section>
    </div>
    <div class="col-lg-3 col-sm-6">
        <section class="panel">
            <div class="symbol yellow">
                <i class="fa fa-camera"></i>
            </div>
            <div class="value">
                <h1 class=" count3"><?= $counters['boasts'] ?></h1>
                <p><?= Yii::$app->i18n->format('{n, plural, =0{Хвастов} one{Хваст} few{Хваста} many{Хвастов} other{Хваста}}', ['n' => $counters['boasts']], Yii::$app->language) ?></p>
            </div>
        </section>
    </div>
    <div class="col-lg-3 col-sm-6">
        <section class="panel">
            <div class="symbol blue">
                <i class="fa fa-comments-o"></i>
            </div>
            <div class="value">
                <h1 class=" count4"><?= $counters['forum_themes'] ?></h1>
                <p><?= Yii::$app->i18n->format('{n, plural, =0{Тем} one{Тема} few{Темы} many{Тем} other{Темы}}', ['n' => $counters['forum_themes']], Yii::$app->language) ?></p>
            </div>
        </section>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Динамика пользовательского контента</div>
    <div class="panel-body">
        <div id="user-content-area"></div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Динамика трек-кодов</div>
    <div class="panel-body">
        <div id="user-track-area"></div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Динамика комментариев</div>
    <div class="panel-body">
        <div id="user-comments-area"></div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Динамика лайков</div>
    <div class="panel-body">
        <div id="user-votes-area"></div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Динамика регистраций</div>
    <div class="panel-body">
        <div id="user-area"></div>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Транзакционные E-mail</div>
    <div class="panel-body">
        <div id="user-emails-area"></div>
    </div>
</div>

<?php

$js = <<<JS
Morris.Area({
    element: 'user-content-area',
    data: {$contentDataJson},
    xkey: 'dateCreated',
    ykeys: ['complaints', 'boasts', 'forum_theme'],
    labels: ['Жалобы', 'Хвасты', 'Темы на форуме'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60', '#4a8bc2', '#a9d86e'],
    fillOpacity: 0.3,
    behaveLikeLine: true,
    smooth: true,
    resize: true
});

Morris.Area({
    element: 'user-track-area',
    data: {$tracksDataJson},
    xkey: 'dateCreated',
    ykeys: ['track'],
    labels: ['Количество'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60'],
    fillOpacity: 0.3,
    smooth: true,
    resize: true
});

Morris.Area({
    element: 'user-votes-area',
    data: {$votesDataJson},
    xkey: 'dateCreated',
    ykeys: ['complaint', 'comment', 'boast'],
    labels: ['Жалобы', 'Комментарии', 'Хвасты'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60', '#4a8bc2', '#a9d86e'],
    fillOpacity: 0.3,
    behaveLikeLine: true,
    smooth: true,
    resize: true
});

Morris.Area({
    element: 'user-comments-area',
    data: {$commentsDataJson},
    xkey: 'dateCreated',
    ykeys: ['complaint', 'article', 'boast', 'forum_comments', 'news'],
    labels: ['Жалобы', 'Статьи', 'Хвасты', 'Форум', 'Новости'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60', '#4a8bc2', '#a9d86e', '#e3247e', '#a85d00'],
    fillOpacity: 0.3,
    behaveLikeLine: true,
    smooth: true,
    resize: true
});

Morris.Area({
    element: 'user-area',
    data: {$userDataJson},
    xkey: 'dateCreated',
    ykeys: ['email', 'social'],
    labels: ['E-mail', 'Соцсети'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60', '#4a8bc2'],
    fillOpacity: 0.3,
    smooth: true,
    resize: true
});

Morris.Area({
    element: 'user-emails-area',
    data: {$emailsDataJson},
    xkey: 'dateCreated',
    ykeys: ['email'],
    labels: ['E-mail'],
    hideHover: 'auto',
    lineWidth: 1,
    pointSize: 5,
    parseTime: true,
    lineColors: ['#ff6c60'],
    fillOpacity: 0.3,
    behaveLikeLine: true,
    smooth: true,
    resize: true
});

JS;

$this->registerJs($js);
