<?php
/* @var app\modules\admin\models\CategoryForm $model */
/* @var app\db\records\Category[] $categories */
/* @var yii\web\View $this */

$this->title = 'Новая категория';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Новая';
?>

<div class="category-create">
    <?= $this->render('_form', compact('model', 'categories')) ?>
</div>
