<?php
use yii\helpers\Html;
/* @var yii\web\View */
?>

<div class="panel">
    <div class="panel-body text-right">
        <?= Html::a('<span class="fa fa-plus"></span> добавить новую', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </div>

    <div class="panel-body">
        {items}
    </div>
</div>