<?php
/* @var app\modules\admin\models\CategoryForm $model */
/* @var app\db\records\Category[] $categories */
/* @var yii\web\View $this */

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="category-update">
    <?= $this->render('_form', compact('model', 'categories')) ?>
</div>
