<?php
use app\assets\SelectizeBootstrap3Asset;
use app\assets\SelectizePluginAsset;
use app\db\records\Category;
use otsec\yii2\bootstrap\ActiveForm;
use otsec\yii2\fladmin\FlAdmin;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var app\modules\admin\models\CategoryForm $model */
/* @var app\db\records\Category[] $categories */
/* @var yii\web\View $this */

$listCategories = ArrayHelper::map($categories, 'id', function (Category $category) {
    return $category->present()->namesPath();
});

SelectizeBootstrap3Asset::register($this);
SelectizePluginAsset::register($this);
?>

<div class="category-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= FlAdmin::beginPanel('Основная информация') ?>
        <?= $form->field($model, 'parentId')->dropDownList($listCategories, ['prompt' => 'Корневая категория']); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 250]) ?>
        <?= $form->field($model, 'synonyms')->textarea() ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel('SEO параметры') ?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => 250]) ?>
        <?= $form->field($model, 'seoTitle')->textInput(['maxlength' => 250]) ?>
        <?= $form->field($model, 'seoDescription')->textarea(['rows' => 2]) ?>
    <?= FlAdmin::endPanel() ?>

    <?= FlAdmin::beginPanel() ?>
        <?= $form->beginWrapper() ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Вернуться назад', ['index'], ['class' => 'btn btn-default']) ?>
        <?= $form->endWrapper() ?>
    <?= FlAdmin::endPanel() ?>

    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<<JS
$('#categoryform-synonyms').selectize({
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});
JS;

$this->registerJs($script);
