<?php
use app\db\records\Category;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var $dataProvider yii\data\DataProviderInterface
 * @var $this         yii\web\View
 */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="category-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => $this->render('_grid'),
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function(Category $category) {
                    $levelPrefix = str_repeat(str_repeat('&nbsp', 10), $category->depth);
                    $categoryName = Html::a($category->name, ['update', 'id' => $category->id]);
                    return trim($levelPrefix . ' ' . $categoryName);
                },
            ],
            [
                'attribute' => 'slug',
                'headerOptions' => ['width' => '25%'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => 70],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
