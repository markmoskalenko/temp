<?php
use otsec\yii2\bootstrap\ActiveForm;
use otsec\yii2\fladmin\FlAdmin;
use yii\helpers\Html;

/* @var $userModel           app\db\records\User */
/* @var $signUpModel         app\models\SignUpForm */
/* @var $updatePasswordModel app\models\UpdatePasswordForm */
/* @var $this                yii\web\View */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= FlAdmin::beginPanel($userModel->isNewRecord ? 'Новый пользователь' : 'Основная информания') ?>
        <?php if (isset($signUpModel)): ?>
            <?= $form->field($signUpModel, 'name')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($signUpModel, 'login')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($signUpModel, 'password')->passwordInput() ?>
        <?php else: ?>
            <?= $form->field($userModel, 'name')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($userModel, 'login')->textInput(['maxlength' => 100]) ?>
        <?php endif ?>
    <?= FlAdmin::endPanel() ?>

    <?php if (isset($updatePasswordModel)): ?>
        <?= FlAdmin::beginPanel('Сменить пароль') ?>
            <?= $form->field($updatePasswordModel, 'password')->passwordInput() ?>
            <?= $form->field($updatePasswordModel, 'passwordRepeat')->passwordInput() ?>
        <?= FlAdmin::endPanel() ?>
    <?php endif ?>

    <?= FlAdmin::beginPanel() ?>
        <?= $form->beginWrapper() ?>
            <?= Html::submitButton($userModel->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $userModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Вернуться назад', ['index'], ['class' => 'btn btn-default']) ?>
        <?= $form->endWrapper() ?>
    <?= FlAdmin::endPanel() ?>

    <?php $form->end() ?>
</div>
