<?php
use yii\grid\GridView;

/* @var $searchModel app\search\UserSearch */
/* @var $this        yii\web\View */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <?= GridView::widget([
        'dataProvider' => $searchModel->dataProvider(),
        'layout' => $this->render('_grid'),
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => 70],
            ],
            [
                'attribute' => 'email',
                'headerOptions' => ['width' => 300],
            ],
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['width' => 70],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
