<?php

/* @var $userModel           app\db\records\User */
/* @var $updatePasswordModel app\models\UpdatePasswordForm */
/* @var $this                yii\web\View */

$this->title = 'Редактирование пользователя: ' . ' ' . $userModel->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="user-update">
    <?= $this->render('_form', [
        'userModel' => $userModel,
        'updatePasswordModel' => $updatePasswordModel,
    ]) ?>
</div>
