<?php

/* @var $userModel   app\db\records\User */
/* @var $signUpModel app\models\SignUpForm */
/* @var $this        yii\web\View */

$this->title = 'Новый пользователь';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавление';
?>

<div class="user-create">
    <?= $this->render('_form', [
        'userModel' => $userModel,
        'signUpModel' => $signUpModel,
    ]) ?>
</div>

