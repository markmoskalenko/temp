<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $complaint app\db\records\Complaint */
/* @var $model     app\modules\admin\models\ComplaintRejectForm */
/* @var $this      yii\web\View */

$this->title = 'Отклонение отзыва';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Отклонение';
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
        ],
    ],
]) ?>

<div class="panel">
    <div class="panel-heading">Уведомление пользователя</div>
    <div class="panel-body">
        <?= $form->field($model, 'reason')->textarea(['rows' => 10]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>
