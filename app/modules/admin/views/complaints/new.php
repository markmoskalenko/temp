<?php
use yii\widgets\ListView;

/* @var $complaintSearch app\search\ComplaintSearch */
/* @var $this            yii\web\View */

$this->title = 'Новые отзывы';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Новые';
?>

<?= ListView::widget([
    'dataProvider' => $complaintSearch->dataProvider(),
    'itemView' => '_item',
    'layout' => $this->render('_layout'),
    'sorter' => [
        'options' => ['class' => 'sorter sorter-inline', 'style' => 'margin-left: 10px'],
        'attributes' => ['timeCreated'],
    ],
]); ?>

