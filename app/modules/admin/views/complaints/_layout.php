<?php
/* @var $this yii\web\View */
?>

<div class="panel">
    <div class="panel-body">
        <div class="pull-left">
            {summary}
        </div>

        <div class="pull-right">
            Сортировка:
            {sorter}
        </div>
    </div>

    <div class="panel-body">
        {items}
        {pager}
    </div>
</div>