<?php
use app\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $complaint app\db\records\Complaint */
/* @var $photos    app\db\records\ComplaintPhoto[] */
/* @var $model     app\modules\admin\models\ComplaintUpdateForm */
/* @var $this      yii\web\View */

$this->title = 'Модерирование отзыва';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Модерирование';
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
            'error' => '',
            'hint' => '',
        ],
    ],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($model, 'mood')->radioList($complaint->moodLabels()) ?>
        <?= $form->field($model, 'text')->textarea(['rows' => 10]) ?>

        <div class="form-group" id="b1">
            <div class="col-sm-offset-3 col-sm-8">
                <a href="#" onclick="$('#b1').hide(); $('#b2').show(); return false;">Показать оригинальный текст</a>
            </div>
        </div>

        <div class="form-group" id="b2" style="display: none;">
            <label class="control-label col-sm-3">Оригинальный текст</label>
            <div class="col-sm-8">
                <p class="form-control-static"><?= Yii::$app->formatter->asNtext($complaint->originalText) ?></p>
                <p>
                    <a href="#" onclick="$('#b2').hide(); $('#b1').show(); return false;">Скрыть оригинальный текст</a>
                </p>
            </div>
        </div>
    </div>
</div>

<?php if (count($photos)): ?>
    <div class="panel js-photo-container">
        <div class="panel-heading">Прикреплённые фотографии</div>
        <div class="panel-body">
            <div class="row">
                <?php foreach ($photos as $photo): ?>
                    <div class="col-sm-3 js-photo-item">
                        <a href="#" data-href="<?= Url::to(['reject-photo', 'id' => $photo->id]) ?>"
                           class="thumbnail thumbnail-uploadable js-delete-photo-link"
                           onclick="return false">
                        <span class="thumbnail-uploadable-delete">
                            <span class="thumbnail-uploadable-delete-icon fa fa-trash-o"></span>
                        </span>
                            <span class="thumbnail-uploadable-image" style="background-image: url(<?= Url::uploaded($photo) ?>)"></span>
                        </a>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
    var gallery = $('.js-photo-container');

    gallery.photos = function () {
        return this.find('.js-photo-item');
    };

    gallery.photos().each(function () {
        var block = $(this),
            link = block.find('.js-delete-photo-link');

        link.on('click', function () {
            if ( ! confirm('Вы уверены, что хотите удалить это фото?')) {
                return false;
            }

            $.ajax({
                url: link.data('href'),
                type: 'post',
                beforeSend: function () {
                    block.hide();
                },
                success: function () {
                    block.remove();
                    gallery.trigger('photo:deleted');
                },
                error: function () {
                    alert('Не удалось удалить фото');
                    block.show();
                }
            })
        });
    });

    gallery.on('photo:deleted', function () {
        if ( ! gallery.photos().length) {
            gallery.hide();
        }
    });
JS
);
