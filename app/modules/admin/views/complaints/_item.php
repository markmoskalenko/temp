<?php
use app\db\records\Complaint;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model app\db\records\Complaint */
/* @var $this  yii\web\View */

$photoCount = $model->getPhotos()->visible()->count();
?>

<div class="well clearfix">
    <p><?= Yii::$app->formatter->asNtext($model->text) ?></p>

    <ul class="complaint-controls list-unstyled list-inline pull-left">
        <li>
            <span class="fa fa-clock-o"></span>
            <?= Yii::$app->formatter->asDatetime($model->timeCreated, 'short') ?>
        </li>

        <li>
            <span class="fa fa-user"></span>
            <?= Html::encode($model->user->present()->name()) ?>
        </li>

        <?php if ($photoCount): ?>
            <li>
                <span class="fa fa-camera"></span>
                <?= $photoCount ?> фото
            </li>
        <?php endif ?>

        <li>
            <?php if ($model->mood === Complaint::MOOD_POSITIVE): ?>
                <span class="text-info">
                    <span class="fa fa-smile-o"></span>
                    Позитивный отзыв
                </span>
            <?php elseif ($model->mood === Complaint::MOOD_NEGATIVE): ?>
                <span class="text-danger">
                    <span class="fa fa-frown-o"></span>
                    Негативный отзыв
                </span>
            <?php else: ?>
                <span class="fa fa-meh-o"></span>
                Нейтральный отзыв
            <?php endif ?>
        </li>
    </ul>

    <ul class="complaint-controls list-unstyled list-inline pull-right">
        <li>
            <a href="<?= Url::to(['update', 'id' => $model->id]) ?>">
                <span class="fa fa-fw fa-pencil"></span> Редактировать
            </a>
        </li>

        <?php if (Yii::$app->controller->action->id !== 'approved'): ?>
            <li>
                <a href="<?= Url::to(['approve', 'id' => $model->id]) ?>">
                    <span class="fa fa-fw fa-thumbs-o-up"></span> Одобрить
                </a>
            </li>
        <?php endif ?>

        <?php if (Yii::$app->controller->action->id !== 'rejected'): ?>
            <li>
                <a href="<?= Url::to(['reject', 'id' => $model->id]) ?>">
                    <span class="fa fa-fw fa-thumbs-o-down"></span> Отклонить
                </a>
            </li>
        <?php endif ?>
    </ul>
</div>