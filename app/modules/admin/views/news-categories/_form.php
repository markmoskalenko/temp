<?php
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $category app\db\records\NewsCategory */
/* @var $this     yii\web\View */
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($category, 'name')->textInput() ?>
        <?= $form->field($category, 'content')->widget(Redactor::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($category, 'slug')->textInput() ?>
        <?= $form->field($category, 'seoTitle')->textInput() ?>
        <?= $form->field($category, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>