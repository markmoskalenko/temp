<?php
use yii\grid\GridView;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        'name',
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>