<?php
/* @var $page app\db\records\Page */
/* @var $this yii\web\View */

$this->title = $page->name;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', compact('page')) ?>