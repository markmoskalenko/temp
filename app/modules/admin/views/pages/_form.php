<?php
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $page app\db\records\Page */
/* @var $this yii\web\View */
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($page, 'name')->textInput() ?>
        <?= $form->field($page, 'language')->dropDownList(Yii::$app->polyglot->getLabels()) ?>
        <?= $form->field($page, 'content')->widget(Redactor::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($page, 'slug')->textInput() ?>
        <?= $form->field($page, 'seoTitle')->textInput() ?>
        <?= $form->field($page, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>