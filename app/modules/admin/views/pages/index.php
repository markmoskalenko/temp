<?php
use app\db\records\Page;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $pageSearch app\search\PageSearch */
/* @var $this       yii\web\View */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $pageSearch->dataProvider(),
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function (Page $page) {
                $title = Html::a($page->name, ['update', 'id' => $page->id], ['class' => 'btn-block']);
                $language = Html::tag('span', substr($page->language, 0, 2), ['class' => 'label label-info text-uppercase']);
                return Html::tag('div', $language, ['class' => 'pull-right']) . $title;
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
