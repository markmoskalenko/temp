<?php
/* @var $model app\models\ReviewForm */
/* @var $this  yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Обзоры', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<?= $this->render('_form', compact('model')) ?>