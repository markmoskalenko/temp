<?php
use app\assets\SelectizeBootstrap3Asset;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $article app\db\records\Article */
/* @var $this    yii\web\View */

SelectizeBootstrap3Asset::register($this);
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'externalUrl')->textInput() ?>

        <?= $form->field($model, 'categoryId')->widget('app\widgets\Categorize', [
            'options' => ['prompt' => ''],
        ]) ?>

        <?= $form->field($model, 'content')->widget('vova07\imperavi\Widget', [
            'plugins' => [
                'cut' => 'app\assets\RedactorCutPluginAsset',
            ],
            'settings' => [
                'buttons' => ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'image', 'video', 'link'],
                'formatting' => ['p', 'blockquote', 'h2'],
                'imageUpload' => Url::to(['upload-image']),
                'imageManagerJson' => Url::to(['images']),
                'plugins' => ['imagemanager', 'video'],
            ],
        ]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($model, 'slug')->textInput() ?>
        <?= $form->field($model, 'seoTitle')->textInput() ?>
        <?= $form->field($model, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>