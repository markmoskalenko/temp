<?php
/* @var $model app\models\ReviewForm */
/* @var $this  yii\web\View */

$this->title = 'Новая статья';
$this->params['breadcrumbs'][] = ['label' => 'Обзоры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', compact('model')) ?>