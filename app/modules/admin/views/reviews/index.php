<?php
use app\db\records\Review;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Обзоры';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function (Review $review) {
                return Html::a($review->title, ['update', 'id' => $review->id], ['class' => 'btn-block']);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
