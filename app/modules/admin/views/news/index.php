<?php
use app\db\records\News;
use rmrevin\yii\fontawesome\FA;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function (News $news) {
                $title = Html::a($news->title, ['update', 'id' => $news->id], ['class' => 'btn-block']);
                $category = ($news->categoryId) ? Html::tag('span', $news->category->name, ['class' => 'label label-default']) : '';
                $featured = ($news->isFeatured) ? Html::tag('span', FA::icon('star'), ['class' => 'label label-warning']) : '';
                return Html::tag('div', $featured . PHP_EOL . $category, ['class' => 'pull-right']) . $title;
            },
        ],
        [
            'attribute' => 'timePublished',
            'format' => 'raw',
            'contentOptions' => ['class' => 'text-center h4'],
            'headerOptions' => ['width' => 150, 'class' => 'text-center'],
            'value' => function (News $news) {
                return Yii::$app->formatter->asDate($news->timePublished, 'dd MMM HH:mm');
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
