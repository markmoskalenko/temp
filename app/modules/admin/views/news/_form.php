<?php
use app\db\records\NewsCategory;
use app\helpers\Url;
use app\modules\admin\widgets\DateTimePicker;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $news app\db\records\News */
/* @var $this yii\web\View */

$categories = NewsCategory::find()->all();
$categories = ArrayHelper::map($categories, 'id', 'name');
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($news, 'title')->textInput() ?>
        <?= $form->field($news, 'categoryId')->dropDownList($categories, ['prompt' => '']) ?>
        <?= $form->field($news, 'language')->dropDownList(Yii::$app->polyglot->getLabels(), ['prompt' => '']) ?>
        <?= $form->field($news, 'timePublished')->widget(DateTimePicker::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Контент</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-3" for="<?= Html::getInputId($news, 'imageId') ?>">
                <?= $news->getAttributeLabel('imageId') ?>
            </label>

            <div class="col-sm-6">
                <?php if ($news->imageId): ?>
                    <div class="thumbnail">
                        <img src="<?= Url::uploaded($news->mainImage) ?>" class="img-responsive"/>
                    </div>
                <?php endif ?>

                <div style="margin-bottom: 20px">
                    <?= Html::activeFileInput($news, 'imageId') ?>
                </div>
            </div>
        </div>

        <?= $form->field($news, 'content')->widget(Redactor::className()) ?>
        <?= $form->field($news, 'contentPreview')->widget(Redactor::className()) ?>
        <?= $form->field($news, 'isFeatured')->checkbox() ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($news, 'slug')->textInput() ?>
        <?= $form->field($news, 'seoTitle')->textInput() ?>
        <?= $form->field($news, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>