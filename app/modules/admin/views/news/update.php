<?php
/* @var $news app\db\records\News */
/* @var $this yii\web\View */

$this->title = $news->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<?= $this->render('_form', compact('news')) ?>