<?php
/* @var $article  app\db\records\Article */
/* @var $this     yii\web\View */

$this->title = 'Новая статья';
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', compact('article')) ?>