<?php
use app\db\records\Article;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $this         yii\web\View */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => $this->render('_grid'),
    'columns' => [
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function (Article $article) {
                $title = Html::a($article->title, ['update', 'id' => $article->id], ['class' => 'btn-block']);
                $category = Html::tag('span', $article->category->name, ['class' => 'label label-default']);
                $language = Html::tag('span', substr($article->category->language, 0, 2), ['class' => 'label label-info text-uppercase']);
                return Html::tag('div', $category . PHP_EOL . $language, ['class' => 'pull-right']) . $title;
            },
        ],
        [
            'attribute' => 'timePublished',
            'format' => 'raw',
            'contentOptions' => ['class' => 'text-center h4'],
            'headerOptions' => ['width' => 150, 'class' => 'text-center'],
            'value' => function (Article $article) {
                return Yii::$app->formatter->asDate($article->timePublished, 'dd MMM HH:mm');
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['class' => 'text-center'],
            'headerOptions' => ['width' => 70],
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
