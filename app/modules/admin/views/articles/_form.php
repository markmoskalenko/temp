<?php
use app\helpers\Url;
use app\modules\admin\widgets\DateTimePicker;
use app\db\records\ArticleCategory;
use vova07\imperavi\Widget as Redactor;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $article app\db\records\Article */
/* @var $this    yii\web\View */

$categories = ArticleCategory::find()->all();
$categories = ArrayHelper::map($categories, 'id', 'name', function ($category) {
    if ($category->language === LANG_RU) {
        return 'На русском языке';
    } elseif ($category->language === LANG_PT) {
        return 'На португальском языке';
    } elseif ($category->language === LANG_ES) {
        return 'На испанском языке';
    } else {
        return $category->language;
    }
});
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => ['enctype' => 'multipart/form-data'],
]) ?>

<div class="panel">
    <div class="panel-heading">Основная информация</div>
    <div class="panel-body">
        <?= $form->field($article, 'title')->textInput() ?>
        <?= $form->field($article, 'categoryId')->dropDownList($categories, ['prompt' => '']) ?>
        <?= $form->field($article, 'timePublished')->widget(DateTimePicker::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">Контент</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-3" for="<?= Html::getInputId($article, 'imageId') ?>">
                <?= $article->getAttributeLabel('imageId') ?>
            </label>

            <div class="col-sm-6">
                <?php if ($article->imageId): ?>
                    <div class="thumbnail">
                        <img src="<?= Url::uploaded($article->mainImage) ?>" class="img-responsive"/>
                    </div>
                <?php endif ?>

                <div style="margin-bottom: 20px">
                    <?= Html::activeFileInput($article, 'imageId') ?>
                </div>
            </div>
        </div>

        <?= $form->field($article, 'content')->widget(Redactor::className()) ?>
        <?= $form->field($article, 'contentPreview')->widget(Redactor::className()) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-heading">SEO настройки</div>
    <div class="panel-body">
        <?= $form->field($article, 'slug')->textInput() ?>
        <?= $form->field($article, 'seoTitle')->textInput() ?>
        <?= $form->field($article, 'seoDescription')->textarea(['rows' => 2]) ?>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
</div>