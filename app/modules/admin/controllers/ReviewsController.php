<?php
namespace app\modules\admin\controllers;

use app\modules\admin\models\ReviewForm;
use app\repositories\ReviewImageRepository;
use app\repositories\ReviewRepository;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewsController extends BaseController
{
    /**
     * @var ReviewRepository
     */
    protected $reviews;
    /**
     * @var ReviewImageRepository
     */
    protected $images;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->reviews = new ReviewRepository();
        $this->images = new ReviewImageRepository();
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->reviews->search(['visible' => 1]);
        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionMine()
    {
        $dataProvider = $this->reviews->search(['visible' => 1, 'userId' => Yii::$app->user->id]);
        return $this->render('mine', compact('dataProvider'));
    }

    /**
     * @param integer $id
     * @param string $slug
     * @return mixed
     */
    public function actionView($id, $slug = null)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);
        $this->ensureExists($review->visible);

        if ($review->slug !== $slug) {
            return $this->redirect(['view', 'id' => $review->id, 'slug' => $review->slug]);
        }

        $this->reviews->viewed($review);

        return $this->render('view', compact('review'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReviewForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->reviews->create($model->attributes);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        $model = new ReviewForm();
        $model->setAttributes($review->attributes);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->reviews->update($review, $model->attributes);
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('review', 'model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $review = $this->reviews->getById($id);
        $this->ensureExists($review);

        $this->reviews->delete($review);

        return $this->redirect(['index']);
    }
}