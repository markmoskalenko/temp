<?php
namespace app\modules\admin\controllers;

use app\modules\admin\models\CategoryForm;
use app\repositories\CategoryRepository;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoriesController extends BaseController
{
    /**
     * @var CategoryRepository
     */
    protected $categories;


    /**
     * @param string             $id
     * @param \yii\base\Module   $module
     * @param CategoryRepository $categories
     * @param array              $config
     */
    public function __construct($id, $module, CategoryRepository $categories, $config = [])
    {
        $this->categories = $categories;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->categories->search();
        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CategoryForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->categories->create($model->export(), $model->parentId);
            $this->redirect(['index']);
        }

        $categories = $this->categories->getAll();

        return $this->render('create', compact('model', 'categories'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $category = $this->categories->getById($id);
        $this->ensureExists($category);

        $model = new CategoryForm();
        $model->import($category);
        $model->parentId = ArrayHelper::getValue($this->categories->getParent($category), 'id');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->categories->update($category, $model->export(), $model->parentId);
            $this->redirect('index');
        }

        $categories = $this->categories->getAll();

        return $this->render('create', compact('model', 'categories'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $category = $this->categories->getById($id);
        $this->ensureExists($category);

        $this->categories->delete($category);

        return $this->redirect(['index']);
    }
}
