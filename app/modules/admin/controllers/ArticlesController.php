<?php
namespace app\modules\admin\controllers;

use app\db\records\Article;
use app\db\records\PublicationImage;
use app\repositories\PublicationImageRepository;
use app\search\ArticleSearch;
use app\models\ImageUploadForm;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticlesController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Article::find(),
            'sort' => ['defaultOrder' => ['timePublished' => SORT_DESC]]
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $article = new Article();
        $article->timePublished = date('Y-m-d H:i');

        if ($image = $this->findImage($article, 'imageId')) {
            $article->imageId = $image->id;
        }

        if ($article->load(Yii::$app->request->post()) && $article->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('article'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $article = Article::findOr404($id);

        if ($image = $this->findImage($article, 'imageId')) {
            $article->imageId = $image->id;
        }

        if ($article->load(Yii::$app->request->post()) && $article->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('article'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Article::findOr404($id)->delete();
        return $this->redirect('index');
    }

    /**
     * @param Model $model
     * @param string $attribute
     * @return PublicationImage|null
     */
    protected function findImage($model, $attribute)
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstance($model, $attribute);

        if ($form->validate()) {
            $repository = new PublicationImageRepository();
            return $repository->create($form->file);
        }

        return null;
    }
}