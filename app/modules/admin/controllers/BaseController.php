<?php
namespace app\modules\admin\controllers;

use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'controllers' => ['auth'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin', 'redactor'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param mixed $condition
     * @throws NotFoundHttpException
     */
    public function ensureExists($condition)
    {
        if ( ! $condition) {
            throw new NotFoundHttpException();
        }
    }
}