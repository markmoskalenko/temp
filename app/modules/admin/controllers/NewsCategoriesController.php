<?php
namespace app\modules\admin\controllers;

use app\db\records\NewsCategory;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsCategoriesController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsCategory::find(),
            'pagination' => false,
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $category = new NewsCategory();

        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('category'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $category = NewsCategory::findOr404($id);

        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('category'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        NewsCategory::findOr404($id)->delete();
        return $this->redirect('index');
    }
}