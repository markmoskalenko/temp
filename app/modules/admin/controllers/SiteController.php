<?php
namespace app\modules\admin\controllers;

use app\filters\JsonFormatter;
use app\models\ImageUploadForm;
use app\repositories\PublicationImageRepository;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SiteController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'empty';


    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFormatter::className(),
                'only' => ['last-images', 'upload-image'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['dashboard/index']);
    }

    /**
     * Returns JSON with last uploaded images.
     *
     * @return mixed
     */
    public function actionLastImages()
    {
        $images = new PublicationImageRepository();
        return $images->last(50);
    }

    /**
     * @return mixed
     */
    public function actionUploadImage()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $images = new PublicationImageRepository();
            return $images->create($form->file);
        }

        return [];
    }
}
