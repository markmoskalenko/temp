<?php
namespace app\modules\admin\controllers;

use app\db\records\Boast;
use app\db\records\Complaint;
use app\db\records\User;
use app\db\records\QuestionAnswer;
use app\db\records\Vote;
use app\db\records\Comment;
use app\db\records\Mail;
use app\db\records\TrackingNumber;
use app\modules\forum\entities\Theme;
use app\modules\forum\entities\Comment AS ForumComments;
use Carbon\Carbon;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DashboardController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $counters = $this->getCounters();
        $contentData = $this->getContentData();
        $userData = $this->getUserData();
        $votesData = $this->getVotesData();
        $commentsData = $this->getCommentsData();
        $emailsData = $this->getEmailData();
        $tracksData = $this->getTrackData();

        return $this->render('index', compact('counters', 'contentData', 'userData', 'votesData', 'commentsData', 'emailsData', 'tracksData'));
    }

    /**
     * @return array
     */
    protected function getContentData()
    {
        $timeout = new Carbon('1 month ago');

        $complaints = (new Query)->select('COUNT(id) as complaints, DATE(timeCreated) AS dateCreated')
            ->from(Complaint::tableName())
            ->andwhere(['>', 'timeCreated', $timeout])
            ->andwhere(['=', 'rejected', 0])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $boasts = (new Query)->select('COUNT(id) as boasts, DATE(timeCreated) AS dateCreated')
            ->from(Boast::tableName())
            ->andwhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=','visible', 1])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $forum_theme = (new Query)->select('COUNT(id) as forum_theme, DATE(timeCreated) AS dateCreated')
            ->from(Theme::tableName())
            ->andwhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=','isDeleted', 0])
            ->andWhere(['=','isHidden', 0])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $data = ArrayHelper::merge($complaints, $boasts, $forum_theme);
        $data = array_values($data);

        $ensure = function ($item) {
            foreach (['complaints', 'boasts', 'forum_theme'] as $key) {
                if (empty($item[$key])) {
                    $item[$key] = 0;
                }
            }

            return $item;
        };

        return array_map($ensure, $data);
    }

    /**
     * @return array
     */
    protected function getUserData()
    {
        $timeout = new Carbon('1 month ago');

        $data = (new Query)
            ->select('DATE(user.timeCreated) AS dateCreated, COUNT(user.id) AS total, COUNT(`user_social`.id) AS social')
            ->from(User::tableName())
            ->leftJoin('user_social', 'user.id = user_social.userId')
            ->where(['>', 'user.timeCreated', $timeout])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->all();

        foreach ($data as $key => $group) {
            $data[ $key ]['email'] = $group['total'] - $group['social'];
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getCounters()
    {
        return [
            'boasts' => Boast::find()->where(['=', 'visible', 1])->count(),
            'complaints' => Complaint::find()->where(['=', 'rejected', 0])->count(),
            'forum_themes' => Theme::find()->andwhere(['=', 'isDeleted', 0])->andwhere(['=', 'isHidden', 0])->count(),
            'users' => User::find()->count(),
        ];
    }

    /**
     * @return array
     */
    protected function getVotesData()
    {
        $timeout = new Carbon('1 month ago');

        $boasts = (new Query)
            ->select('COUNT(id) as boast, DATE(timeCreated) AS dateCreated')
            ->from(Vote::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'boast'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $comments = (new Query)
            ->select('COUNT(id) as comment, DATE(timeCreated) AS dateCreated')
            ->from(Vote::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'comment'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $complaints = (new Query)
            ->select('COUNT(id) as complaint, DATE(timeCreated) AS dateCreated')
            ->from(Vote::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'complaint'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $data = ArrayHelper::merge($boasts, $comments, $complaints);
        $data = array_values($data);

        $ensure = function ($item) {
            foreach (['boast', 'comment', 'complaint'] as $key) {
                if (empty($item[$key])) {
                    $item[$key] = 0;
                }
            }

            return $item;
        };

        return array_map($ensure, $data);
    }

    /**
     * @return array
     */
    protected function getCommentsData()
    {
        $timeout = new Carbon('1 month ago');

        $boasts = (new Query)
            ->select('COUNT(id) as boast, DATE(timeCreated) AS dateCreated')
            ->from(Comment::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'boast'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $articles = (new Query)
            ->select('COUNT(id) as article, DATE(timeCreated) AS dateCreated')
            ->from(Comment::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'article'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $news = (new Query)
            ->select('COUNT(id) as news, DATE(timeCreated) AS dateCreated')
            ->from(Comment::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'news'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $forum_comments = (new Query)
            ->select('COUNT(id) as forum_comments, DATE(timeCreated) AS dateCreated')
            ->from(ForumComments::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'isDeleted', 0])
            ->andWhere(['=', 'isHidden', 0])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $complaints = (new Query)
            ->select('COUNT(id) as complaint, DATE(timeCreated) AS dateCreated')
            ->from(Comment::tableName())
            ->andWhere(['>', 'timeCreated', $timeout])
            ->andWhere(['=', 'entityName', 'complaint'])
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $data = ArrayHelper::merge($boasts, $articles, $news, $forum_comments, $complaints);
        $data = array_values($data);

        $ensure = function ($item) {
            foreach (['boast', 'article', 'news', 'forum_comments', 'complaint'] as $key) {
                if (empty($item[$key])) {
                    $item[$key] = 0;
                }
            }

            return $item;
        };

        return array_map($ensure, $data);
    }

    protected function getEmailData()
    {
        $timeout = new Carbon('1 month ago');

        $emails = (new Query)->select('COUNT(id) as email, DATE(timeSendSucceed) AS dateCreated')
            ->from(Mail::tableName())
            ->andwhere(['>', 'timeSendSucceed', $timeout])
            ->andwhere('timeSendSucceed IS NOT NULL')
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $data = array_values($emails);

        $ensure = function ($item) {
            foreach (['email'] as $key) {
                if (empty($item[$key])) {
                    $item[$key] = 0;
                }
            }

            return $item;
        };

        return array_map($ensure, $data);
    }

    protected function getTrackData()
    {
        $timeout = new Carbon('1 month ago');

        $tracks = (new Query)->select('COUNT(id) as track, DATE(timeCreated) AS dateCreated')
            ->from(TrackingNumber::tableName())
            ->andwhere(['>', 'timeCreated', $timeout])
            ->andwhere('timeCreated IS NOT NULL')
            ->groupBy('dateCreated')
            ->orderBy('dateCreated')
            ->indexBy('dateCreated')
            ->all();

        $data = array_values($tracks);

        $ensure = function ($item) {
            foreach (['track'] as $key) {
                if (empty($item[$key])) {
                    $item[$key] = 0;
                }
            }

            return $item;
        };

        return array_map($ensure, $data);
    }
}