<?php
namespace app\modules\admin\controllers;

use app\db\records\PublicationImage;
use app\db\records\News;
use app\repositories\PublicationImageRepository;
use app\search\NewsSearch;
use app\models\ImageUploadForm;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->last(),
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $news = new News();
        $news->timePublished = date('Y-m-d H:i');

        if ($image = $this->findImage($news, 'imageId')) {
            $news->imageId = $image->id;
        }

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('news'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $news = News::findOr404($id);

        if ($image = $this->findImage($news, 'imageId')) {
            $news->imageId = $image->id;
        }

        if ($news->load(Yii::$app->request->post()) && $news->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('news'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        News::findOr404($id)->delete();
        return $this->redirect('index');
    }

    /**
     * @param Model $model
     * @param string $attribute
     * @return PublicationImage|null
     */
    protected function findImage($model, $attribute)
    {
        $uploader = new ImageUploadForm();
        $uploader->file = UploadedFile::getInstance($model, $attribute);

        if ($uploader->validate()) {
            $repository = new PublicationImageRepository();
            return $repository->create($uploader->file);
        }

        return null;
    }
}