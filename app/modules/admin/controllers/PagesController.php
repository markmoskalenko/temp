<?php
namespace app\modules\admin\controllers;

use app\db\records\Page;
use app\search\PageSearch;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PagesController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $pageSearch = new PageSearch();
        return $this->render('index', compact('pageSearch'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $page = new Page();

        if ($page->load(Yii::$app->request->post()) && $page->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('page'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $page = Page::findOr404($id);

        if ($page->load(Yii::$app->request->post()) && $page->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('page'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Page::findOr404($id)->delete();
        return $this->redirect('index');
    }
}