<?php
namespace app\modules\admin\controllers;

use app\db\records\Mail;
use app\db\records\Complaint;
use app\db\records\ComplaintPhoto;
use app\search\ComplaintSearch;
use app\modules\admin\models\ComplaintRejectForm;
use app\modules\admin\models\ComplaintUpdateForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintsController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['new']);
    }

    /**
     * @return mixed
     */
    public function actionNew()
    {
        $complaintSearch = new ComplaintSearch();
        $complaintSearch->moderatable = true;

        return $this->render('new', compact('complaintSearch'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $complaint = Complaint::findOr404($id);
        $photos = $complaint->getPhotos()->visible()->all();

        $model = new ComplaintUpdateForm(['text' => $complaint->text, 'mood' => $complaint->mood]);
        if ($model->load(Yii::$app->request->post()) && $model->save($complaint)) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('complaint', 'photos', 'model'));
    }

    /**
     * @return mixed
     */
    public function actionApproved()
    {
        $complaintSearch = new ComplaintSearch();
        $complaintSearch->approved = true;

        return $this->render('approved', compact('complaintSearch'));
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionApprove($id)
    {
        $complaint = Complaint::findOr404($id);
        $complaint->approve();
        $complaint->saveOrFail();

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionRejected()
    {
        $complaintSearch = new ComplaintSearch();
        $complaintSearch->rejected = true;

        return $this->render('rejected', compact('complaintSearch'));
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionReject($id)
    {
        $complaint = Complaint::findOr404($id);

        $model = new ComplaintRejectForm();
        if ($model->load(Yii::$app->request->post()) && $model->save($complaint)) {
            Mail::queue(Mail::TYPE_REVIEW_REJECTED, $complaint->user, $complaint);
            return $this->redirect(['index']);
        }

        return $this->render('reject', compact('complaint', 'model'));
    }

    /**
     * @param integer $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionRejectPhoto($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $photo = ComplaintPhoto::findOr404($id);
        $photo->rejected = 1;
        $photo->saveOrFail();

        return [];
    }
}