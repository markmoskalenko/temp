<?php
namespace app\modules\admin\controllers;

use app\db\records\ArticleCategory;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticleCategoriesController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ArticleCategory::find(),
            'pagination' => false,
        ]);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $category = new ArticleCategory();

        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            return $this->redirect('index');
        }

        return $this->render('create', compact('category'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $category = ArticleCategory::findOr404($id);

        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            return $this->redirect('index');
        }

        return $this->render('update', compact('category'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        ArticleCategory::findOr404($id)->delete();
        return $this->redirect('index');
    }
}