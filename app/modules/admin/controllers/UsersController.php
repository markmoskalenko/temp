<?php
namespace app\modules\admin\controllers;

use app\db\records\User;
use app\search\UserSearch;
use app\models\SignUpForm;
use app\models\UpdatePasswordForm;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UsersController extends BaseController
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        return $this->render('index', compact('searchModel'));
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $signUpModel = new SignUpForm();

        $userModel = $signUpModel->getUser();
        $userModel->load(Yii::$app->request->post());

        if ($signUpModel->load(Yii::$app->request->post()) && $signUpModel->signUp()) {
            $userModel->saveOrFail();
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('signUpModel', 'userModel'));
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $userModel = User::findOr404($id);
        $updatePasswordModel = new UpdatePasswordForm($userModel);

        if ($userModel->load(Yii::$app->request->post()) && $userModel->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('userModel', 'updatePasswordModel'));
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        User::findOr404($id)->delete();
        return $this->redirect(['index']);
    }
}