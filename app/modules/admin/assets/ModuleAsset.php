<?php
namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ModuleAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/backend.css',
    ];

    public $js = [
    ];

    public $depends = [
        'app\assets\MarionetteAsset',
        'otsec\yii2\fladmin\DashboardAsset',
        'yii\web\YiiAsset',
    ];
}
