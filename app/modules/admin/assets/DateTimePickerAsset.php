<?php
namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DateTimePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/eonasdan-bootstrap-datetimepicker/build';

    public $css = [
        'css/bootstrap-datetimepicker.css',
    ];

    public $js = [
        'js/bootstrap-datetimepicker.min.js',
    ];

    public $depends = [
        'app\modules\admin\assets\MomentAsset',
        'yii\web\JqueryAsset',
    ];
}