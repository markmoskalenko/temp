<?php
namespace app\modules\admin\models;

use app\db\records\Complaint;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintUpdateForm extends Model
{
    /**
     * @var string
     */
    public $mood;
    /**
     * @var string
     */
    public $text;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['mood', 'string'],
            ['mood', 'trim'],
            ['mood', 'required'],
            ['mood', 'default', 'value' => Complaint::MOOD_NEUTRAL],

            ['text', 'string'],
            ['text', 'trim'],
            ['text', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'mood' => 'Тип отзыва',
            'text' => 'Текст отзыва',
        ];
    }

    /**
     * @param Complaint  $complaint
     * @param boolean $runValidation
     * @return bool
     */
    public function save(Complaint $complaint, $runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $complaint->setAttributes($this->getAttributes(), false);
        $complaint->approve();
        $complaint->saveOrFail();

        return true;
    }
}