<?php
namespace app\modules\admin\models;

use app\models\SnapshotForm;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewForm extends Model
{
    /**
     * @var integer
     */
    public $categoryId;
    /**
     * @var string
     */
    public $externalUrl;
    /**
     * @var integer
     */
    public $snapshotId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $content;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $seoTitle;
    /**
     * @var string
     */
    public $seoDescription;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'integer'],
            ['categoryId', 'required'],

            ['externalUrl', 'string'],
            ['externalUrl', 'trim'],
            ['externalUrl', 'required'],
            ['externalUrl', 'url', 'defaultScheme' => 'http', 'message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-url')],
            ['externalUrl', 'app\validators\AliexpressUrlValidator', 'entities' => ['app\parser\entities\SnapshotEntity'], 'message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-format')],
            ['externalUrl', 'parseUrl', 'params' => ['message' => Yii::t('app/validation', 'ali-express-form.link-incorrect-format')]],

            ['title', 'string'],
            ['title', 'trim'],
            ['title', 'required'],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'externalUrl' => 'Ссылка на снэпшот',
            'title' => 'Заголовок',
            'content' => 'Содержимое',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег Title',
            'seoDescription' => 'Тег Meta Description',
        ];
    }

    /**
     * @return array
     */
    public function attributeHints()
    {
        return [
            'content' => 'Кнопка "Разделитель" сокращает длинные записи. Тект после разделителя будет виден только на странице обзора.',
        ];
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ['categoryId', 'externalUrl', 'title', 'content', 'slug', 'seoTitle', 'seoDescription'];
    }

    /**
     * Inline validator for AliExpress URL.
     *
     * @param string $attribute
     * @param array  $params
     */
    public function parseUrl($attribute, $params)
    {
        $this->snapshotId = null;

        if ($this->hasErrors()) {
            return;
        }

        $form = new SnapshotForm();
        $form->externalUrl = $this->$attribute;

        if ( ! $form->validate()) {
            $this->addError($attribute, $form->getFirstError('externalUrl'));
            return;
        }

        if ($snapshot = $form->getSnapshot()) {
            $this->snapshotId = $snapshot->snapshotId;
        }

        if ( ! $this->snapshotId) {
            $this->addError($attribute, $params['message']);
        }
    }
}