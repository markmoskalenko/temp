<?php
namespace app\modules\admin\models;

use app\models\FillableTrait;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoryForm extends Model
{
    use FillableTrait;

    /**
     * @var integer
     */
    public $parentId;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var string
     */
    public $synonyms;
    /**
     * @var string
     */
    public $seoTitle;
    /**
     * @var string
     */
    public $seoDescription;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['parentId', 'integer'],

            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default', 'value' => null],

            ['synonyms', 'string'],
            ['synonyms', 'trim'],
            ['synonyms', 'default', 'value' => null],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default', 'value' => null],
        ];
    }

    /**
     * @return array
     */
    public function fillable()
    {
        return ['name', 'slug', 'synonyms', 'seoTitle', 'seoDescription'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parentId' => 'Родительская категория',
            'name' => 'Название',
            'synonyms' => 'Синонимы для поиска',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег Title',
            'seoDescription' => 'Тег Meta Description',
        ];
    }
}