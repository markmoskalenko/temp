<?php
namespace app\modules\admin\models;

use app\db\records\Complaint;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintRejectForm extends Model
{
    /**
     * @var string
     */
    public $reason;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['reason', 'string'],
            ['reason', 'trim'],
            ['reason', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'reason' => 'Причина отклонения отзыва',
        ];
    }

    /**
     * @param Complaint  $complaint
     * @param boolean $runValidation
     * @return bool
     */
    public function save(Complaint $complaint, $runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $complaint->reject($this->reason);
        $complaint->saveOrFail();

        return true;
    }
}