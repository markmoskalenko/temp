<?php
namespace app\modules\forum\helpers;

use app\bbcode\SpoilerDefinition;
use JBBCode\CodeDefinitionBuilder;
use JBBCode\Parser;

class BBCodeParser extends \app\helpers\BBCodeParser
{
    /**
     * @param Parser $parser
     */
    public static function configure(Parser $parser)
    {
        /* [h1] header tag */
        $builder = new CodeDefinitionBuilder('h1', '<div class="forum-comment-content__h1">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [h2] header tag */
        $builder = new CodeDefinitionBuilder('h2', '<div class="forum-comment-content__h2">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [h3] header tag */
        $builder = new CodeDefinitionBuilder('h3', '<div class="forum-comment-content__h3">{param}</div>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [b] bold tag */
        $builder = new CodeDefinitionBuilder('b', '<strong>{param}</strong>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [i] italics tag */
        $builder = new CodeDefinitionBuilder('i', '<em>{param}</em>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [u] underline tag */
        $builder = new CodeDefinitionBuilder('u', '<u>{param}</u>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [center] center tag */
        $builder = new CodeDefinitionBuilder('center', '<center>{param}</center>');
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        $urlValidator = new \JBBCode\validators\UrlValidator();

        /* [url] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{param}">{param}</a>');
        $builder->setParseContent(false)->setBodyValidator($urlValidator);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [url=http://example.com] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{option}">{param}</a>');
        $builder->setUseOption(true)->setParseContent(true)->setOptionValidator($urlValidator);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [img] image tag */
        $builder = new CodeDefinitionBuilder('img', '<img class="forum-comment-content__image" src="{param}" />');
        $builder->setUseOption(false)->setParseContent(false);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [video] video tag */
        // отдаём разбираться фильтру HtmlPurifier, из-за глюка с запрещением картинок с других ресурсов.
        $builder = new CodeDefinitionBuilder('video', '<video>{param}</video>');
        $builder->setUseOption(false)->setParseContent(false);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [quote] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="forum-comment-content__quote">{param}</div>');
        $builder->setParseContent(true);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [quote="Chuck Norris"] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<div class="forum-comment-content__quote"><div class="forum-comment-content__quote-header">{option}</div>{param}</div>');
        $builder->setUseOption(true)->setParseContent(true);
        $definition = $builder->build();
        $parser->addCodeDefinition($definition);

        /* [spoiler] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(false);
        $parser->addCodeDefinition($definition);

        /* [spoiler="Заголовок"] spoiler tag */
        $definition = new SpoilerDefinition();
        $definition->setUseOption(true);
        $parser->addCodeDefinition($definition);
    }

    /**
     * @param Parser $parser
     */
    public static function filter(Parser $parser)
    {
        $parser->accept(new \JBBCode\visitors\HTMLSafeVisitor());
        $parser->accept(new \app\bbcode\ParagraphsVisitor());
        $parser->accept(new \app\bbcode\TrimVisitor());
        $parser->accept(new \app\bbcode\LineBreakVisitor());
        $parser->accept(new \app\bbcode\SmileyVisitor());
    }
}