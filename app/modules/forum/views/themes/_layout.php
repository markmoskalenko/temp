<?php
use yii\helpers\Url;

/* @var $board app\modules\forum\entities\Board */
/* @var $this  yii\web\View */
?>

<div class="forum-panel">
    <?php if (Yii::$app->user->can('createForumTheme', ['board' => $board])): ?>
        <div style="position: absolute; top: 0; left: 0">
            <a href="<?= Url::to(['create', 'boardId' => $board->id]) ?>" class="forum-panel__control btn btn-inverse"><span class="fa fa-fw fa-plus"></span>Создать новую тему</a>
        </div>
    <?php endif ?>

    {pager}
</div>

<div class="forum-theme-list">
    {items}
</div>

<div class="forum-panel">
    <?php if (Yii::$app->user->can('createForumTheme', ['board' => $board])): ?>
        <div style="position: absolute; top: 0; left: 0">
            <a href="<?= Url::to(['create', 'boardId' => $board->id]) ?>" class="forum-panel__control btn btn-inverse"><span class="fa fa-fw fa-plus"></span>Создать новую тему</a>
        </div>
    <?php endif ?>

    {pager}
</div>
