<?php
use app\assets\AutosizeAsset;
use app\assets\ImagesUploaderAsset;
use app\helpers\Angular;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $model       app\modules\forum\models\ThemeForm */
/* @var $this        yii\web\View */
/* @var $isNewRecord boolean */

ImagesUploaderAsset::register($this);
AutosizeAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'forum-form',
    'options' => ['autocomplete' => 'off'],
    'fieldConfig' => [
        'inputOptions' => [
            'class' => 'form-control input-lg',
        ]
    ],
]) ?>
    <?= $form->field($model, 'title')->textInput() ?>

    <?php /* if( ! $newRecord && \Yii::$app->user->can('movieThemeForum', ['comment' => $theme])):?>
        <?= $form->field($model,'topicId')->dropDownList($forums, ['class' => 'control-label']) ?>
    <?php endif */ ?>

    <?= $form->field($model, 'comment', [
        'template' => "{label}\n{input}\n{hint}\n{error}\n{images}",
        'parts' => [
            '{images}' => Angular::directive('images-uploader', [
                'upload-url' => Url::to(['default/upload-image']),
                'template' => '#images-uploader-redactor-template',
            ]),
        ],
    ])->widget('app\widgets\Redactor', [
        'options' => ['class' => 'form-control autosize', 'rows' => 8],
    ]) ?>

    <button type="submit" class="btn btn-lg btn-primary">Опубликовать тему</button>
<?php $form->end() ?>
