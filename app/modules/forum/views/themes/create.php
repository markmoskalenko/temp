<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $board app\modules\forum\entities\Board */
/* @var $model app\modules\forum\models\ThemeForm */
/* @var $this  yii\web\View */

$this->params['breadcrumbs'][] = ['label' => 'Форум Алиэкспресс', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $board->title, 'url' => Url::entity($board)];
$this->title = 'Новая тема';
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render('_form', ['model' => $model, 'isNewRecord' => true]) ?>