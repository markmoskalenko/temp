<?php
use app\helpers\Url;

/* @var $model app\modules\forum\entities\Theme */
/* @var $this  yii\web\View */

$theme = $model;
?>

<div class="row">
    <div class="col-xs-8">
        <?php if ($theme->isClosed): ?>
            <span class="forum-theme-list__item-icon fa fa-lock"></span>
        <?php elseif ($theme->isFixed): ?>
            <span class="forum-theme-list__item-icon fa fa-thumb-tack"></span>
        <?php else: ?>
            <span class="forum-theme-list__item-icon fa fa-comments-o"></span>
        <?php endif ?>

        <a class="<?= $theme->isFixed ? 'text-bold' : '' ?>" href="<?= Url::entity($theme) ?>"><?= $theme->title ?></a>
        <a class="link-muted text-small text-nowrap" href="<?= Url::entity($theme->user) ?>"><?= $theme->user->name ?></a>
        <?= $theme->present()->pagination() ?>
    </div>

    <div class="col-xs-3">
        <?php if ($theme->lastComment): ?>
            <?php $comment = $theme->lastComment ?>

            <a class="link-muted text-small text-nowrap" href="<?= Url::entity($comment->user) ?>">
                <?= $comment->user->name ?>
            </a>

            <a class="text-small text-nowrap" href="<?= Url::entity($comment) ?>">
                <?= $comment->present()->timeRelative() ?>
            </a>
        <?php endif ?>
    </div>

    <div class="col-xs-1 text-right">
        <span class="text-small"><?= $theme->getCommentsCount() ?></span>
    </div>
</div>

