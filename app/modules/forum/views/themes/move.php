<?php
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $theme    app\modules\forum\entities\Theme */
/* @var $this     yii\web\View */
/* @var $boardMap array */

$this->params['breadcrumbs'][] = ['label' => 'Форум Алиэкспресс', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $theme->board->title, 'url' => Url::entity($theme->board)];
$this->params['breadcrumbs'][] = ['label' => $theme->title, 'url' => Url::entity($theme)];
$this->title = 'Перемещение темы ' . $theme->title;
?>

<div class="page-header">
    <h1>Перемещение темы</h1>
</div>

<?php $form = ActiveForm::begin([
    'options' => ['autocomplete' => 'off'],
]) ?>

    <?= $form->field($model, 'boardId')->dropDownList($boardMap) ?>

    <button type="submit" class="btn btn-primary">Сохранить</button>
<?php $form->end() ?>
