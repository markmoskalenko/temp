<?php
use app\helpers\Url;
use yii\helpers\Html;

/* @var $theme app\modules\forum\entities\Theme */
/* @var $model app\modules\forum\models\ThemeForm */
/* @var $this  yii\web\View */

$this->params['breadcrumbs'][] = ['label' => 'Форум Алиэкспресс', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $theme->board->title, 'url' => Url::entity($theme->board)];
$this->title = 'Редактирование темы';
?>

<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?= $this->render('_form', ['model' => $model, 'isNewRecord' => false]) ?>