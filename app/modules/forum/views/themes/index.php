<?php
use app\modules\forum\widgets\ThemeList;
use yii\helpers\Html;

/* @var $board        app\modules\forum\entities\Board */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $fixedThemes  app\modules\forum\entities\Theme[] */
/* @var $this         yii\web\View */

$this->title = $board->title . ' - Форум Алиэкспресс';
$this->params['breadcrumbs'][] = ['label' => 'Форум Алиэкспресс', 'url' => ['default/index']];
?>

<div class="page-header">
    <h1><?= Html::encode($board->title) ?></h1>
</div>

<?= ThemeList::widget([
    'dataProvider' => $dataProvider,
    'fixedThemes' => $fixedThemes,
    'board' => $board,
]) ?>