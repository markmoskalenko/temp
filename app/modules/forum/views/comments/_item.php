<?php
use app\modules\forum\helpers\BBCodeParser;
use app\modules\forum\helpers\HtmlPurifier;
use app\helpers\Url;

/* @var $model app\modules\forum\entities\Comment */
/* @var $this  yii\web\View */

$comment = $model;
$author = $comment->user;
?>

<div class="forum-comment-list__item" data-key="<?= $comment->id ?>">
    <div id="c<?= $comment->id ?>" class="anchor"></div>

    <div class="forum-comment-list__item-profile">
        <div class="forum-profile">
            <div class="forum-profile__username">
                <a href="#" title="Ответить" onclick="referUser(this); return false;"><?= $author->name ?></a>
            </div>

            <div class="forum-profile__avatar">
                <a href="<?= Url::entity($author) ?>">
                    <img class="img-responsive" src="<?= Url::avatar($author, ['w' => 100, 'h' => 100]) ?>" alt="<?= $author->name ?>"/>
                </a>
            </div>

            <div class="forum-profile__comments">
                <div class="fa fa-fw fa-comment-o"></div>
                <?= $author->present()->forumCommentCount() ?>
            </div>
        </div>
    </div>

    <div class="forum-comment-list__item-body">
        <div class="forum-comment">
            <div class="col-xs-10">
                <ul class="forum-comment__meta">
                    <li>
                        <a href="#c<?= $comment->id ?>" class="link-muted">#</a>
                    </li>

                    <li>
                        <?= $comment->present()->timeCreated() ?>
                    </li>
                </ul>

                <div class="forum-comment__content">
                    <?= BBCodeParser::process($comment->content) ?>
                </div>
            </div>

            <div class="col-xs-2">
                <div class="forum-comment__actions">
                    <?php if (Yii::$app->user->can('updateForumComment', ['comment' => $comment])): ?>
                        <a href="#" onclick="updateComment(this); return false;">Редактировать</a>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('deleteForumComment', ['comment' => $comment])): ?>
                        <a href="<?= Url::to(['delete', 'id' => $comment->id]) ?>" data-method="post">Удалить</a>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('pinForumComment', ['comment' => $comment])): ?>
                        <?php if ($comment->isFixed): ?>
                            <a href="<?= Url::to(['unpin', 'id' => $comment->id]) ?>">Открепить</a>
                        <?php else: ?>
                            <a href="<?= Url::to(['pin', 'id' => $comment->id]) ?>">Прикрепить</a>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('hideForumComment', ['comment' => $comment])): ?>
                        <?php if ($comment->isHidden): ?>
                            <a href="<?= Url::to(['show', 'id' => $comment->id]) ?>">Показать</a>
                        <?php else: ?>
                            <a href="<?= Url::to(['hide', 'id' => $comment->id]) ?>">Скрыть</a>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if ( ! Yii::$app->user->isGuest): ?>
                        <a href="#" onclick="quoteComment(this); return false;">Цитировать</a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
