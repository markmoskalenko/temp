<?php
use app\helpers\Angular;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $comment app\modules\forum\entities\Comment */
/* @var $model   app\modules\forum\models\CommentForm */
/* @var $this    yii\web\View */

$author = $comment->user;
?>

<?php $form = ActiveForm::begin([
    'id' => 'comment-form-update',
    'action' => ['update', 'id' => $comment->id],
    'options' => ['class' => 'forum-comment-list__item', 'autocomplete' => 'off'],
    'validateOnChange' => false,
    'validateOnBlur' => false,
]) ?>

<div class="forum-comment-list__item-profile">
    <div class="forum-profile">
        <div class="forum-profile__username">
            <a href="#" onclick="referUser(this); return false;"><?= $author->name ?></a>
        </div>

        <div class="forum-profile__avatar">
            <a href="<?= Url::entity($author) ?>">
                <img class="img-responsive" src="<?= Url::avatar($author, ['w' => 100, 'h' => 100]) ?>" alt="<?= $author->name ?>"/>
            </a>
        </div>

        <div class="forum-profile__comments">
            <div class="fa fa-fw fa-comment-o"></div>
            <?= $author->present()->forumCommentCount() ?>
        </div>
    </div>
</div>

<div class="forum-comment-list__item-body">
    <?= $form->field($model, 'content', [
        'template' => "{input}\n{hint}\n{error}\n{images}",
        'parts' => [
            '{images}' => Angular::directive('images-uploader', [
                'upload-url' => Url::to(['default/upload-image']),
                'template' => '#images-uploader-redactor-template',
            ]),
        ],
    ])->widget('app\widgets\Redactor', [
        'options' => ['class' => 'autosize', 'rows' => 3],
    ]) ?>
</div>

<div class="clearfix"></div>

<div class="forum-comment-list__item-footer">
    <button type="button" class="btn btn-primary" onclick="saveUpdatedComment()">Сохранить сообщение</button>
    или
    <a class="border-link border-link-dashed" href="#" onclick="cancelUpdate(); return false;">отменить</a>
</div>

<?php $form->end() ?>
