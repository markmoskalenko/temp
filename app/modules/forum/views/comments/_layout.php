<?php
use app\helpers\Url;

/* @var $theme app\modules\forum\entities\Theme */
/* @var $this  yii\web\View */
?>

<div class="forum-panel">
    <?php if ($theme->isClosed): ?>
        <span class="forum-panel__control btn btn-inverse" style="position: absolute; top: 0; left: 0">
            <span class="fa fa-lock"></span>
            Тема закрыта
        </span>
    <?php elseif (Yii::$app->user->can('createForumComment', ['theme' => $theme])): ?>
        <div style="position: absolute; top: 0; left: 0">
            <a href="#comment-form-create" class="forum-panel__control btn btn-inverse">Новое сообщение</a>
        </div>
    <?php endif ?>

    {pager}

    <?php if (Yii::$app->user->can('moderateForumTheme', ['theme' => $theme])): ?>
        <div class="dropdown" style="position: absolute; top: 0; right: 0">
            <button class="forum-panel__control btn btn-inverse dropdown-toggle" type="button" data-toggle="dropdown">
                Действия над темой
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                <?php if (Yii::$app->user->can('updateForumTheme', ['theme' => $theme])): ?>
                    <li><a href="<?= Url::to(['themes/update', 'id' => $theme->id]) ?>">Переименовать тему</a></li>
                <?php endif ?>

                <?php if (Yii::$app->user->can('moveForumTheme', ['theme' => $theme])): ?>
                    <li><a href="<?= Url::to(['themes/move', 'id' => $theme->id]) ?>">Переместить тему</a></li>
                <?php endif ?>

                <?php if (Yii::$app->user->can('pinForumTheme', ['theme' => $theme])): ?>
                    <?php if ($theme->isFixed): ?>
                        <li><a href="<?= Url::to(['themes/unpin', 'id' => $theme->id]) ?>">Открепить тему</a></li>
                    <?php else: ?>
                        <li><a href="<?= Url::to(['themes/pin', 'id' => $theme->id]) ?>">Прикрепить тему</a></li>
                    <?php endif ?>
                <?php endif ?>

                <?php if (Yii::$app->user->can('closeForumTheme', ['theme' => $theme])): ?>
                    <?php if ($theme->isClosed): ?>
                        <li><a href="<?= Url::to(['themes/open', 'id' => $theme->id]) ?>">Открыть тему</a></li>
                    <?php else: ?>
                        <li><a href="<?= Url::to(['themes/close', 'id' => $theme->id]) ?>">Закрыть тему</a></li>
                    <?php endif ?>
                <?php endif ?>
            </ul>
        </div>
    <?php endif ?>
</div>

<div class="forum-comment-list">
    {items}
</div>

<div class="forum-panel">
    <?php if ($theme->isClosed): ?>
        <span class="forum-panel__control btn btn-inverse" style="position: absolute; top: 0; left: 0">
            <span class="fa fa-lock"></span>
            Тема закрыта
        </span>
    <?php endif ?>

    {pager}

    <?php if (Yii::$app->user->can('moderateForumTheme', ['theme' => $theme])): ?>
        <div class="dropdown" style="position: absolute; top: 0; right: 0">
            <button class="forum-panel__control btn btn-inverse dropdown-toggle" type="button" data-toggle="dropdown">
                Действия над темой
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                <?php if (Yii::$app->user->can('updateForumTheme', ['theme' => $theme])): ?>
                    <li><a href="<?= Url::to(['themes/update', 'id' => $theme->id]) ?>">Переименовать тему</a></li>
                <?php endif ?>

                <?php if (Yii::$app->user->can('moveForumTheme', ['theme' => $theme])): ?>
                    <li><a href="<?= Url::to(['themes/move', 'id' => $theme->id]) ?>">Переместить тему</a></li>
                <?php endif ?>

                <?php if (Yii::$app->user->can('pinForumTheme', ['theme' => $theme])): ?>
                    <?php if ($theme->isFixed): ?>
                        <li><a href="<?= Url::to(['themes/unpin', 'id' => $theme->id]) ?>">Открепить тему</a></li>
                    <?php else: ?>
                        <li><a href="<?= Url::to(['themes/pin', 'id' => $theme->id]) ?>">Прикрепить тему</a></li>
                    <?php endif ?>
                <?php endif ?>

                <?php if (Yii::$app->user->can('closeForumTheme', ['theme' => $theme])): ?>
                    <?php if ($theme->isClosed): ?>
                        <li><a href="<?= Url::to(['themes/open', 'id' => $theme->id]) ?>">Открыть тему</a></li>
                    <?php else: ?>
                        <li><a href="<?= Url::to(['themes/close', 'id' => $theme->id]) ?>">Закрыть тему</a></li>
                    <?php endif ?>
                <?php endif ?>
            </ul>
        </div>
    <?php endif ?>
</div>
