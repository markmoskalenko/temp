<?php
use app\assets\AutosizeAsset;
use app\assets\ImagesUploaderAsset;
use app\helpers\Angular;
use app\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $theme       app\modules\forum\entities\Theme */
/* @var $model       app\modules\forum\models\CommentForm */
/* @var $this        yii\web\View */

ImagesUploaderAsset::register($this);
AutosizeAsset::register($this);
?>

<br>

<?php $form = ActiveForm::begin([
    'id' => 'comment-form-create',
    'action' => ['create', 'themeId' => $theme->id],
    'options' => ['autocomplete' => 'off'],
    'validateOnChange' => false,
    'validateOnBlur' => false,
]) ?>

    <?= $form->field($model, 'content', [
        'template' => "{input}\n{hint}\n{error}\n{images}",
        'parts' => [
            '{images}' => Angular::directive('images-uploader', [
                'upload-url' => Url::to(['default/upload-image']),
                'template' => '#images-uploader-redactor-template'
            ]),
        ],
    ])->widget('app\widgets\Redactor', [
        'options' => ['class' => 'autosize', 'rows' => 3],
    ]) ?>

    <button type="button" class="btn btn-lg btn-primary" onclick="saveCreatedComment()">Добавить сообщение</button>
<?php $form->end() ?>
