<?php
use app\helpers\Url;
use app\modules\forum\assets\ForumAsset;
use app\modules\forum\widgets\CommentList;
use yii\helpers\Html;

/* @var $theme         app\modules\forum\entities\Theme */
/* @var $fixedComments app\modules\forum\entities\Comment[] */
/* @var $commentModel  app\modules\forum\models\CommentForm */
/* @var $dataProvider  yii\data\DataProviderInterface */
/* @var $this          yii\web\View */

$this->params['breadcrumbs'][] = ['label' => 'Форум Алиэкспресс', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => $theme->board->title, 'url' => Url::entity($theme->board)];
$this->title = $theme->title . ' - форум Алиэкспресс';

ForumAsset::register($this);
?>

<div class="page-header">
    <h1><?= Html::encode($theme->title) ?></h1>
</div>

<?= CommentList::widget([
    'dataProvider' => $dataProvider,
    'fixedComments' => $fixedComments,
    'theme' => $theme,
]) ?>

<?php if ( ! Yii::$app->user->isGuest && ! $theme->isClosed) :?>
    <?= $this->render('_form-create', ['theme' => $theme, 'model' => $commentModel, 'isNewRecord' => true]) ?>
<?php endif ?>

<?php if (Yii::$app->user->isGuest): ?>
    <div class="forum-register-required">
        <a href="<?= Url::to(['@signup']) ?>">Зарегистрируйтесь</a> или <a href="<?= Url::to(['@login']) ?>">войдите</a> на сайт,
        чтобы начать общение на форуме.
    </div>
<?php endif ?>

<div class="clearfix"></div>