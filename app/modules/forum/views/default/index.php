<?php
use app\helpers\Url;

/* @var $categories app\modules\forum\entities\Category[] */
/* @var $this       yii\web\View */

$this->title = 'Форум Алиэкспресс - AliTrust.ru';
$this->registerMetaTag(['name' => 'description', 'content' => 'Форумы АлиЭкспресс – это то место, где Вас услышат, поймут и помогут решить любые проблемы']);
?>

<div class="page-header">
    <h1>Форум Алиэкспресс</h1>
</div>

<?php foreach($categories as $category): ?>
    <div class="forum-category">
        <div class="forum-category__header">
            <div class="forum-category__title"><?= $category->title ?></div>

            <div class="forum-category__info">
                <div class="text-bold">Тем</div>
                <div>Сообщений</div>
            </div>
        </div>

        <div class="forum-category__body">
            <div class="forum-boast-list">
                <?php foreach($category->boards as $board): ?>
                    <div class="forum-board-list__item">
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="forum-board-list__item-icon"></div>
                                <div class="forum-board-list__item-content">
                                    <a class="forum-board-list__item-name" href="<?= Url::entity($board) ?>"><?= $board->title ?></a>
                                    <div class="forum-board-list__item-description"><?= $board->description ?></div>
                                </div>
                            </div>

                            <div class="col-xs-2">
                                <?php if ($comment = $board->lastComment): ?>
                                    <a href="<?= Url::entity($comment->user) ?>" class="link-muted"><?= $comment->user->name ?></a>
                                    <br>
                                    <a charset="text-info" href="<?= Url::entity($comment) ?>"><?= $comment->present()->timeRelative() ?></a>
                                    <span class="fa fa-angle-double-right text-muted"></span>
                                <?php endif ?>
                            </div>

                            <div class="col-xs-2 text-right">
                                <div class="text-bold"><?= $board->getThemesCount() ?></div>
                                <div><?= $board->getCommentsCount() ?></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php endforeach ?>
