<?php
namespace app\modules\forum;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\forum\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
