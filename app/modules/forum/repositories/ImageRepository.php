<?php
namespace app\modules\forum\repositories;

use app\modules\forum\entities\Image;
use app\models\UploadTrait;
use app\repositories\ActiveRepository;
use app\services\ImageUploadService;
use Yii;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImageRepository extends ActiveRepository
{
    use UploadTrait;


    /**
     * @param UploadedFile $file
     * @return Image
     */
    public function create(UploadedFile $file)
    {
        $filename = $this->createFileName($file, true);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, Yii::getAlias('@webroot/uploads/'.$filename));

        $image = new Image();
        $image->filename = $filename;
        $this->saveOrFail($image);

        return $image;
    }
}