<?php
namespace app\modules\forum\repositories;

use app\modules\forum\entities\Board;
use app\repositories\ActiveRepository;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoardRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\modules\forum\entities\Board';


    /**
     * @return Board[]
     */
    public function all()
    {
        return Board::find()->all();
    }

    /**
     * @param $slug
     * @return \app\modules\forum\entities\Board
     */
    public function bySlug($slug)
    {
        return Board::find()->bySlug($slug)->one();
    }

    /**
     * @param $id
     * @return Board
     */
    public function getById($id)
    {
        return Board::find()->byId($id)->one();
    }

    /**
     * @param $id
     * @return bool
     */
    public function existsById($id)
    {
        return Board::find()->byId($id)->exists();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Board::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     * @return \app\modules\forum\entities\Board
     */
    public function create($data)
    {
        /* @var $model \app\modules\forum\entities\Board */
        $model = \Yii::createObject($this->recordClass);
        $model->setAttributes($data, false);
        $this->saveOrFail($model);

        return $model;
    }

    /**
     * @return array
     */
    public function getMap()
    {
        $boards = Board::find()
            ->joinWith('category')
            ->orderBy('forum_category.priority DESC, forum_board.priority DESC')
            ->all();

        return ArrayHelper::map($boards, 'id', 'title', 'category.title');
    }
}
