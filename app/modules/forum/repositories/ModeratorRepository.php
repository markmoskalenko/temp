<?php

namespace app\modules\forum\repositories;

use app\modules\forum\entities\Moderator;
use app\repositories\ActiveRepository;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class ModeratorRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\modules\forum\entities\Moderator';

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Moderator::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     * @return Moderator
     */
    public function create($data)
    {
        /* @var $model \app\modules\forum\entities\Moderator */
        $model = \Yii::createObject($this->recordClass);
        $model->setAttributes($data, false);
        $this->saveOrFail($model);

        return $model;
    }
}
