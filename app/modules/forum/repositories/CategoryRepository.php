<?php
namespace app\modules\forum\repositories;

use app\modules\forum\entities\Category;
use app\repositories\ActiveRepository;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class CategoryRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\modules\forum\entities\Category';


    /**
     * @return Category[]
     */
    public function all()
    {
        return Category::find()->orderBy('priority DESC')->all();
    }

    /**
     * @param $slug
     * @return Category
     */
    public function bySlug($slug)
    {
        return Category::find()->bySlug($slug)->one();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Category::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     * @return Category
     */
    public function create($data)
    {
        /* @var $model Category */
        $model = \Yii::createObject($this->recordClass);
        $model->setAttributes($data, false);
        $this->saveOrFail($model);

        return $model;
    }

    /**
     * @return array
     */
    public function getForumsMap()
    {
        $data = Category::find()
            ->select(['title', 'id'])
            ->with([
                'forumTopics' => function ($q) {
                    return $q->select(['id', 'title', 'category']);
                }
            ])
            ->asArray()
            ->all();
        $result = [];
        foreach($data as $forum){
            $resultTopic = [];
            foreach($forum['forumTopics'] as $topic){
                $resultTopic[$topic['id']] = $topic['title'];
            }
            $result[$forum['title']] = $resultTopic;
        }
        return $result;
    }
}
