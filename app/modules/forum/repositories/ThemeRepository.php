<?php
namespace app\modules\forum\repositories;

use app\models\UploadTrait;
use app\modules\forum\entities\Theme;
use app\repositories\ActiveRepository;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class ThemeRepository extends ActiveRepository
{
    use UploadTrait;

    /**
     * @inheritdoc
     */
    public $recordClass = 'app\modules\forum\entities\Theme';


    /**
     * @param integer $id
     * @return Theme
     */
    public function byId($id)
    {
        return Theme::find()->byId($id)->active()->one();
    }

    /**
     * @param integer $id
     * @return Theme
     */
    public function getBySellerId($id)
    {
        return Theme::findOne(['sellerId' => $id]);
    }

    /**
     * @param integer $topicId
     * @return Theme
     */
    public function findAllFixedByTopicId($topicId)
    {
        return Theme::find()->fixed()->active()->byBoard($topicId)->all();
    }

    /**
     * @param array   $params
     * @param boolean $hideEmpty
     * @return ActiveDataProvider
     */
    public function search($params = [], $hideEmpty = false)
    {
        $query = Theme::find();

        $query->notFixed();
        $query->active();

        if ($params) {
            $query->andWhere($params);
        }

        $query->leftJoin('forum_board_theme_last_comment AS fcl', 'fcl.themeId = forum_board_theme.id');
        $query->orderBy('fcl.timeUpdated DESC, forum_board_theme.timeCreated DESC');

        if ($hideEmpty) {
            $query->andWhere('fcl.id IS NOT NULL');
        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);

        return $provider;
    }

    /**
     * @param $id
     */
    public function setUpdateTimeById($id){
        $model = Theme::find()->byId($id)->active()->one();
        $model->save();
    }

    /**
     * @param array $data
     * @return Theme
     */
    public function create($data)
    {
        $theme = new Theme();
        $theme->setAttributes($data, false);
        $this->saveOrFail($theme);

        return $theme;
    }

    /**
     * @param \app\db\records\Seller $seller
     * @return Theme
     */
    public function createForSeller($seller)
    {
        $theme = new Theme();
        $theme->boardId = \Yii::$app->params['forum.sellersBoardId'];
        $theme->sellerId = $seller->id;
        $theme->title = 'Магазин #' . $seller->originalStoreId . '. ' . $seller->storeName;
        $this->saveOrFail($theme);

        return $theme;
    }

    /**
     * @param Theme $theme
     * @param array $data
     */
    public function update($theme, $data)
    {
        $theme->setAttributes($data);
        $this->saveOrFail($theme);
    }

    /**
     * @param Theme   $theme
     * @param integer $boardId
     */
    public function move($theme, $boardId)
    {
        $theme->updateAttributes(['boardId' => $boardId]);
    }

    /**
     * @param Theme $theme
     */
    public function pin($theme)
    {
        $theme->updateAttributes(['isFixed' => 1]);
    }

    /**
     * @param Theme $theme
     */
    public function unpin($theme)
    {
        $theme->updateAttributes(['isFixed' => 0]);
    }

    /**
     * @param Theme $theme
     */
    public function close($theme)
    {
        $theme->updateAttributes(['isClosed' => 1]);
    }

    /**
     * @param Theme $theme
     */
    public function open($theme)
    {
        $theme->updateAttributes(['isClosed' => 0]);
    }
}