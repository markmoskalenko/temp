<?php
namespace app\modules\forum\repositories;

use app\modules\forum\db\BoardQuery;
use app\modules\forum\db\ThemeQuery;
use app\modules\forum\entities\Comment;
use app\repositories\ActiveRepository;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class CommentRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\modules\forum\entities\Comment';


    /**
     * @param integer $id
     * @return Comment
     */
    public function byId($id)
    {
        return Comment::find()->byId($id)->one();
    }

    /**
     * @param integer $themeId
     * @return Comment
     */
    public function getInitial($themeId)
    {
        return Comment::findOne(['themeId' => $themeId, 'isInitial' => $themeId]);
    }

    /**
     * @param $themeId
     * @param $theme
     * @return array|\yii\db\ActiveRecord[]
     */
    public function findAllFixedByThemeId($themeId, $theme)
    {
        if (Yii::$app->user->can('hideForumComment', ['theme' => $theme])) {
            return Comment::find()->fixed()->active(false)->byTheme($themeId)->all();
        }else{
            return Comment::find()->fixed()->active()->byTheme($themeId)->all();
        }
    }

    /**
     * @param $userId
     * @return int|string
     */
    public function getCountByUser($userId)
    {
        return Comment::find()->byUser($userId)->countable()->count();
    }

    /**
     * @param integer $boardId
     * @return integer
     */
    public function getCountByBoardId($boardId)
    {
        return Comment::find()
            ->active()
            ->joinWith([
                'theme' => function (ThemeQuery $tq) {
                    $tq->active();
                    $tq->joinWith([
                        'board' => function (BoardQuery $bq) {
                            $bq->from('forum_board t2');
                        }
                    ]);
                }
            ])
            ->andWhere(['t2.id' => $boardId])->count();
    }

    public function getLastByBoardId($boardId)
    {
        return Comment::find()->last()->active()->joinWith(['board' => function (BoardQuery $query) use ($boardId) {
            $query->byId($boardId);
        }]);
    }

    /**
     * @param array $params
     * @param $theme
     * @return ActiveDataProvider
     */
    public function search($params = [], $theme)
    {
        $query = Comment::find();

        $query->notFixed();

        if (Yii::$app->user->can('hideForumComment', ['theme' => $theme])) {
            $query->active(false);
        }else{
            $query->active();
        }

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 20],
            'sort' => false,
        ]);
    }

    /**
     * @param Comment $comment
     * @param ActiveDataProvider $dataProvider
     * @return integer
     */
    public function calcPage($comment, $dataProvider)
    {
        $query = clone $dataProvider->query;
        $query->andWhere(['<=', 'id', $comment->id]);

        $commentsBefore = $query->count();
        $pageSize = $dataProvider->pagination->pageSize;

        return ceil($commentsBefore / $pageSize);
    }

    /**
     * @param array $data
     * @param boolean $isInitial
     * @return Comment
     */
    public function create($data, $isInitial = false)
    {
        $comment = new Comment();
        $comment->setAttributes($data, false);
        $comment->isInitial = $isInitial;
        $this->saveOrFail($comment);

        return $comment;
    }

    /**
     * @param Comment $comment
     * @param array $data
     */
    public function update($comment, $data)
    {
        $comment->setAttributes($data, false);
        $this->saveOrFail($comment);
    }

    /**
     * @param Comment $comment
     */
    public function delete($comment)
    {
        if ($comment->isInitial) {
            $comment->theme->delete();
        } else {
            $comment->updateAttributes(['isDeleted' => 1]);
        }
    }

    /**
     * @param Comment $comment
     */
    public function pin($comment)
    {
        Comment::updateAll(['isFixed' => 0], ['themeId' => $comment->themeId]);
        $comment->updateAttributes(['isFixed' => 1]);
    }

    /**
     * @param Comment $comment
     */
    public function unpin($comment)
    {
        $comment->updateAttributes(['isFixed' => 0]);
    }

    /**
     * @param Comment $comment
     */
    public function hide($comment)
    {
        $comment->updateAttributes(['isHidden' => 1]);
    }

    /**
     * @param Comment $comment
     */
    public function show($comment)
    {
        $comment->updateAttributes(['isHidden' => 0]);
    }

    /**
     * @param Comment $comment
     * @param integer $userId
     */
    public function replicate($comment, $userId)
    {
        $columns = [
            'commentId' => $comment->id,
            'userId' => $userId,
            'data' => Json::encode($comment),
        ];

        Yii::$app->db->createCommand()
            ->insert('forum_comment_history', $columns)
            ->execute();
    }
}
