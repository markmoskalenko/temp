<?php
namespace app\modules\forum\entities;

use app\db\records\Seller;
use app\db\records\User;
use app\modules\forum\presenters\ThemePresenter;
use app\presenters\PresentableBehavior;
use Yii;
use app\db\ActiveRecord;
use app\modules\forum\db\ThemeQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $boardId
 * @property integer $sellerId
 * @property string  $title
 * @property string  $slug
 * @property string  $description
 * @property integer $isDeleted
 * @property integer $isFixed
 * @property integer $isHidden
 * @property integer $isClosed
 * @property string  $timeCreated
 * @property string  $timeUpdated

 * @property Board   $board
 * @property Comment $lastComment
 * @property Seller  $seller
 * @property User    $user

 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class Theme extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_board_theme';
    }

    /**
     * @return ThemeQuery
     */
    public static function find()
    {
        return new ThemeQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'presenter' => [
                'class' => PresentableBehavior::className(),
                'presenter' => ThemePresenter::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated', 'timeUpdated'],
                    static::EVENT_BEFORE_UPDATE => ['timeUpdated'],
                ],
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
                'value' => function () {
                    $user = Yii::$app->get('user', false);
                    return ($user && !$user->isGuest) ? $user->id : Yii::$app->params['forum.defaultUserId'];
                }
            ],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => ['topicId', 'title'],
        ];
    }

    /**
     * @return boolean
     */
    public function allowUpdate()
    {
        return strtotime($this->timeCreated) > strtotime('-1 hour') && !Yii::$app->user->isGuest && $this->userId == Yii::$app->user->id;
    }

    /**
     * @return \app\modules\forum\db\BoardQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::className(), ['id' => 'boardId']);
    }

    /**
     * @return \app\db\queries\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \app\modules\forum\db\CommentQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['themeId' => 'id']);
    }

    /**
     * @return \app\db\queries\SellerQuery
     */
    public function getSeller()
    {
        return $this->hasOne(Seller::className(), ['id' => 'sellerId']);
    }

    /**
     * @return \app\modules\forum\db\CommentQuery
     */
    public function getLastComment()
    {
        $query = $this->getComments()->active()->orderBy(['timeCreated' => SORT_DESC]);
        $query->multiple = false;
        return $query;
    }

    /**
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->getComments()->active()->count();
    }
}
