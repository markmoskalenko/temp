<?php
namespace app\modules\forum\entities;

use app\db\ActiveRecord;
use app\modules\forum\db\CategoryQuery;
use Yii;

/**
 * @property integer $id
 * @property string  $title
 *
 * @property Board[] $boards
 *
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class Category extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_category';
    }
    
    /**
    * @return CategoryQuery
    */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoards()
    {
        return $this->hasMany(Board::className(), ['categoryId' => 'id'])->orderBy('priority DESC');
    }
}
