<?php
namespace app\modules\forum\entities;

use app\db\ActiveRecord;
use app\db\records\User;
use app\modules\forum\db\ModeratorQuery;
use Yii;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $objectId
 * @property integer $type
 * @property User    $user
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class Moderator extends ActiveRecord
{
    const TYPE_MODERATOR_FORUM = 1;
    const TYPE_MODERATOR_TOPIC = 2;
    const TYPE_MODERATOR_THEME = 3;

    /**
     * @var array
     */
    private static $_groupType = [
        'admin' => self::TYPE_MODERATOR_FORUM,
        'forumModerator' => self::TYPE_MODERATOR_FORUM,
        'forumTopicModerator' => self::TYPE_MODERATOR_TOPIC,
        'forumThemeModerator' => self::TYPE_MODERATOR_THEME
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_moderators';
    }

    /**
     * @return ModeratorQuery
     */
    public static function find()
    {
        return new ModeratorQuery(get_called_class());
    }

    /**
     * @param $group
     * @return mixed
     */
    public static function getTypeByGroup($group)
    {
        return isset(static::$_groupType[ $group ]) ? static::$_groupType[ $group ] : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
