<?php
namespace app\modules\forum\entities;

use app\interfaces\UploadableModel;
use app\db\ActiveRecord;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $userId
 * @property string  $filename
 * @property string  $timeCreated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Image extends ActiveRecord implements UploadableModel
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'forum_image';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'userId'],
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'timeCreated'],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[1];
    }
}
