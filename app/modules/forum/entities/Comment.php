<?php
namespace app\modules\forum\entities;

use app\db\records\User;
use app\modules\forum\presenters\CommentPresenter;
use app\presenters\PresentableBehavior;
use Yii;
use app\db\ActiveRecord;
use app\modules\forum\db\CommentQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $themeId
 * @property integer $userId
 * @property string  $content
 * @property integer $isDeleted
 * @property integer $isFixed
 * @property integer $isHidden
 * @property integer $isInitial
 * @property string  $timeCreated
 * @property string  $timeUpdated
 *
 * @property Theme   $theme
 * @property User    $user
 *
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_comment';
    }

    /**
     * @return CommentQuery
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'presenter' => [
                'class' => PresentableBehavior::className(),
                'presenter' => CommentPresenter::className(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated', 'timeUpdated'],
                ],
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function allowUpdate()
    {
        return strtotime($this->timeCreated) > strtotime('-1 hour') && !Yii::$app->user->isGuest && $this->userId == Yii::$app->user->id;
    }

    /**
     * @return \app\modules\forum\db\ThemeQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'themeId']);
    }

    /**
     * @return \app\modules\forum\db\BoardQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::className(), ['id' => 'boardId'])->via('theme');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
