<?php
namespace app\modules\forum\entities;

use Yii;
use app\db\ActiveRecord;
use app\modules\forum\db\BoardQuery;
use yii\behaviors\SluggableBehavior;

/**
 * @property integer $id
 * @property integer $categoryId
 * @property string  $title
 * @property string  $description
 * @property string  $img
 * @property string  $slug
 * @property integer $priority
 * @property string  $seoTitle
 * @property string  $seoDescription
 * @property integer $isSilent
 * @property integer $isClosed
 * @property integer $hideEmpty
 *
 * @property Category $category
 * @property Theme[]  $themes
 * @property Comment  $lastComment
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @author Mark Moskalenko <office@it-yes.com>
 */
class Board extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_board';
    }

    /**
     * @return BoardQuery
     */
    public static function find()
    {
        return new BoardQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
        ];
    }

    /**
     * @return \app\modules\forum\db\CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \app\modules\forum\db\ThemeQuery
     */
    public function getThemes()
    {
        return $this->hasMany(Theme::className(), ['boardId' => 'id']);
    }

    /**
     * @return integer
     */
    public function getThemesCount()
    {
        return $this->getThemes()->active()->count();
    }

    /**
     * @return \app\modules\forum\db\CommentQuery
     */
    public function getLastComment()
    {
        /* @var $comments \app\modules\forum\repositories\CommentRepository */
        $comments = Yii::createObject('\app\modules\forum\repositories\CommentRepository');
        return $comments->getLastByBoardId($this->id);
    }

    /**
     * @return integer
     */
    public function getCommentsCount()
    {
        /* @var $comments \app\modules\forum\repositories\CommentRepository */
        $comments = Yii::createObject('\app\modules\forum\repositories\CommentRepository');
        return $comments->getCountByBoardId($this->id);
    }
}