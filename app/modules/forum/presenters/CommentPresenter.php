<?php
namespace app\modules\forum\presenters;

use app\modules\forum\entities\Comment;
use app\presenters\BasePresenter;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class CommentPresenter extends BasePresenter
{
    /**
     * @var Comment
     */
    public $owner;

    /**
     * @return string
     */
    public function timeCreated()
    {
        return Yii::$app->formatter->asDate($this->owner->timeCreated, 'dd MMM Y HH:mm');
    }

    /**
     * @return string
     */
    public function timeRelative()
    {
        return Yii::$app->formatter->asRelativeTime($this->owner->timeCreated, strtotime('+3 hours'));
    }
}
