<?php
namespace app\modules\forum\presenters;

use app\presenters\BasePresenter;
use app\widgets\TopicLinkPager;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class ThemePresenter extends BasePresenter
{
    /**
     * @var \app\modules\forum\entities\Theme
     */
    public $owner;

    /**
     * @return string
     */
    public function timeCreated()
    {
        return Yii::$app->formatter->asDate($this->owner->timeCreated, 'dd MMM Y HH:mm');
    }

    /**
     * @return string
     */
    public function pagination()
    {
        /* @var $repository \app\modules\forum\repositories\CommentRepository */
        $repository = Yii::createObject('app\modules\forum\repositories\CommentRepository');

        $dataProvider = $repository->search(['themeId' => $this->owner->id], $this->owner);

        $pagination = $dataProvider->getPagination();
        $pagination->totalCount = $dataProvider->getTotalCount();
        $pagination->route = 'forum/comments/index';
        $pagination->params = ['themeId' => $this->owner->id];

        return TopicLinkPager::widget([
            'pagination' => $pagination,
        ]);

    }
}
