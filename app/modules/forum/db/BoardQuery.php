<?php
namespace app\modules\forum\db;

use app\modules\forum\entities\Board;
use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class BoardQuery extends ActiveQuery
{
    /**
     * @param integer $id
     * @return static
     */
    public function byId($id)
    {
        return $this->andWhere([
            Board::tableName().'.id' => $id,
        ]);
    }

    /**
     * @param boolean $isSilent
     * @return static
     */
    public function silent($isSilent = true)
    {
        return $this->andWhere([
            Board::tableName().'.isSilent' => ($isSilent) ? 1 : 0,
        ]);
    }
}