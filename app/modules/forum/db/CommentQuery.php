<?php
namespace app\modules\forum\db;

use app\modules\forum\entities\Comment;
use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class CommentQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function fixed()
    {
        return $this->andWhere([Comment::tableName() . '.isFixed' => 1]);
    }

    /**
     * @return static
     */
    public function notFixed()
    {
        return $this->andWhere([Comment::tableName() . '.isFixed' => 0]);
    }

    /**
     * @param $themeId
     * @return static
     */
    public function byTheme($themeId)
    {
        return $this->andWhere([Comment::tableName() . '.themeId' => $themeId]);
    }

    /**
     * @param $userId
     * @return static
     */
    public function byUser($userId)
    {
        return $this->andWhere([Comment::tableName() . '.userId' => $userId]);
    }

    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy([
            Comment::tableName().'.timeUpdated' => SORT_DESC,
        ]);
    }

    /**
     * @param bool $hide
     * @return static
     */
    public function active($hide = true)
    {
        if ($hide) {
            $this->andWhere([Comment::tableName() . '.isHidden' => 0]);
        }

        return $this->andWhere([Comment::tableName() . '.isDeleted' => 0]);
    }

    /**
     * @param $id
     * @return static
     */
    public function byId($id)
    {
        return $this->andWhere([Comment::tableName() . '.id' => $id]);
    }

    /**
     * @return static
     */
    public function owner()
    {
        return $this->andWhere([Comment::tableName() . '.userId' => Yii::$app->user->id]);
    }

    /**
     * @return static
     */
    public function edit()
    {
        return $this->andWhere("timeCreated > STR_TO_DATE(:date, '%Y-%m-%d %H:%i:%s')",
            [':date' => date('Y-m-d H:i:s', strtotime('-1 hour'))]);
    }

    /**
     * @return static
     */
    public function countable()
    {
        return $this->joinWith(['board' => function (BoardQuery $query) {
            $query->silent(false);
        }]);
    }
}
