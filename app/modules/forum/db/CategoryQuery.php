<?php
namespace app\modules\forum\db;

use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class CategoryQuery extends ActiveQuery
{
    /**
     * @param $slug
     * @return static
     */
    public function bySlug($slug)
    {
        return $this->andWhere(['slug'=>$slug]);
    }
}
