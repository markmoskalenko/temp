<?php
namespace app\modules\forum\db;

use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class ModeratorQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function byOwner()
    {
        return $this->andWhere(['userId' => Yii::$app->user->id]);
    }

    /**
     * @param $type
     * @return static
     */
    public function byType($type)
    {
        return $this->andWhere(['type' => $type]);
    }

    /**
     * @param $objectId
     * @return static
     */
    public function byObject($objectId)
    {
        return $this->andWhere(['objectId' => $objectId]);
    }
}
