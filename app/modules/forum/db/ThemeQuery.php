<?php

namespace app\modules\forum\db;

use app\modules\forum\entities\Theme;
use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class ThemeQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function fixed()
    {
        return $this->andWhere([Theme::tableName() . '.isFixed' => 1]);
    }

    /**
     * @return static
     */
    public function notFixed()
    {
        return $this->andWhere([Theme::tableName() . '.isFixed' => 0]);
    }

    /**
     * @return static
     */
    public function active()
    {
        return $this->andWhere([Theme::tableName() . '.isDeleted' => 0]);
    }

    /**
     * @param integer $boardId
     * @return static
     */
    public function byBoard($boardId)
    {
        return $this->andWhere([Theme::tableName() . '.boardId' => $boardId]);
    }

    /**
     * @param $id
     * @return static
     */
    public function byId($id)
    {
        return $this->andWhere([Theme::tableName() . '.id' => $id]);
    }

    /**
     * @return static
     */
    public function owner()
    {
        return $this->andWhere([Theme::tableName() . '.userId' => Yii::$app->user->id]);
    }

    /**
     * @param integer $sellerId
     * @return static
     */
    public function bySeller($sellerId)
    {
        return $this->andWhere(['sellerId' => $sellerId]);
    }
}