<?php
namespace app\modules\forum\widgets;

use app\modules\forum\entities\Board;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ThemeList extends ListView
{
    /**
     * @inheritdoc
     */
    public $itemOptions = ['class' => 'forum-theme-list__item'];
    /**
     * @inheritdoc
     */
    public $itemView = '/themes/_item';
    /**
     * @inheritdoc
     */
    public $emptyText = 'Внутри этого раздела ещё не содано ни одной темы.';
    /**
     * @inheritdoc
     */
    public $emptyTextOptions = ['class' => 'forum-theme-list__empty'];
    /**
     * @var array
     */
    public $fixedThemes = [];
    /**
     * @var Board
     */
    public $board;

    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0 || count($this->fixedThemes) > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {

            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                if($matches[0] == '{items}'){
                    $content = $this->renderEmpty();
                }else{
                    $content = '';
                }
                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        }
        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        echo Html::tag($tag, $content, $options);
    }

    /**
     * Инициализация
     */
    public function init()
    {
        $this->layout = $this->render('/themes/_layout', [
            'board' => $this->board,
        ]);

        $this->pager = [
            'options' => [
                'class' => 'forum-pagination pagination pagination-sm',
            ],
        ];
    }

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderItems()
    {
        $rows = [];

        foreach ($this->fixedThemes as $item) {
            $rows[] = $this->renderItem($item, uniqid(), uniqid());
        }

        $models = $this->dataProvider->getModels();
        $keys = $this->dataProvider->getKeys();

        foreach (array_values($models) as $index => $model) {
            $rows[] = $this->renderItem($model, $keys[$index], $index);
        }

        return implode($this->separator, $rows);
    }
}