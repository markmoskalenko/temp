<?php
namespace app\modules\forum\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentList extends ListView
{
    /**
     * @inheritdoc
     */
    public $itemOptions = ['tag' => false];
    /**
     * @inheritdoc
     */
    public $itemView = '/comments/_item';
    /**
     * @inheritdoc
     */
    public $emptyText = 'Внутри этой темы пока нет ни одного сообщения.<br> Вы можете стать первым!';
    /**
     * @inheritdoc
     */
    public $emptyTextOptions = ['class' => 'forum-comment-list__empty'];
    /**
     * @var array
     */
    public $fixedComments = [];
    /**
     * @var array
     */
    public $theme = [];



    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->layout = $this->render('/comments/_layout', [
            'theme' => $this->theme,
        ]);

        $this->pager = [
            'options' => ['class' => 'forum-pagination pagination pagination-sm'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->showOnEmpty || $this->dataProvider->getCount() > 0 || count($this->fixedComments) > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);

                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {

            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                if($matches[0] == '{items}'){
                    $content = $this->renderEmpty();
                }else{
                    $content = '';
                }
                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        }
        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        echo Html::tag($tag, $content, $options);
    }

    /**
     * Renders all data models.
     *
     * @return string the rendering result
     */
    public function renderItems()
    {
        return $this->renderFixedComments() . $this->separator . parent::renderItems();
    }

    /**
     * @return string
     */
    public function renderFixedComments()
    {
        $rows = [];

        foreach ($this->fixedComments as $item) {
            $rows[] = $this->renderItem($item, uniqid(), uniqid());
        }

        return implode($this->separator, $rows);
    }
}