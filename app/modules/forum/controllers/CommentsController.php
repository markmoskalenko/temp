<?php
namespace app\modules\forum\controllers;

use app\filters\AjaxOnly;
use app\filters\JsonFormatter;
use app\filters\PostOnly;
use app\helpers\Url;
use app\modules\forum\models\CommentForm;
use app\modules\forum\repositories\CommentRepository;
use app\modules\forum\repositories\ThemeRepository;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentsController extends BaseController
{
    /**
     * @var ThemeRepository
     */
    protected $themes;
    /**
     * @var CommentRepository
     */
    protected $comments;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->themes = new ThemeRepository();
        $this->comments = new CommentRepository();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AjaxOnly::className(),
                'only' => ['create', 'update'],
            ],
            [
                'class' => PostOnly::className(),
                'only' => ['create'],
            ],
            [
                'class' => JsonFormatter::className(),
                'only' => ['create'],
            ]
        ];
    }

    /**
     * @param integer $themeId
     * @return mixed
     */
    public function actionIndex($themeId)
    {
        $theme = $this->themes->byId($themeId);
        $this->ensureExists($theme);

        $dataProvider = $this->comments->search(['themeId' => $theme->id], $theme);
        $fixedComments = $this->comments->findAllFixedByThemeId($theme->id, $theme);

        $commentModel = new CommentForm();

        return $this->render('index', compact('theme', 'dataProvider', 'fixedComments', 'commentModel'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);

        $dataProvider = $this->comments->search(['themeId' => $comment->themeId], $comment->theme);
        $pageParam = $dataProvider->pagination->pageParam;
        $page = $this->comments->calcPage($comment, $dataProvider);

        $route = ['index', 'themeId' => $comment->themeId, '#' => 'c' . $comment->id];
        if ($page) {
            $route[ $pageParam ] = $page;
        }

        $this->redirect($route, 301);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionRaw($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);

        return $comment->content;
    }

    /**
     * @param integer $themeId
     * @return mixed
     */
    public function actionCreate($themeId)
    {
        $theme = $this->themes->byId($themeId);
        $this->ensureExists($theme);
        $this->ensureAllowed('createForumComment', ['theme' => $theme]);

        $model = new CommentForm();
        $model->load(Yii::$app->request->post());

        if ($model->validate()) {
            $comment = $this->comments->create(['themeId' => $theme->id, 'content' => $model->content]);

            $dataProvider = $this->comments->search(['themeId' => $theme->id], $theme);

            $page = $this->comments->calcPage($comment, $dataProvider);
            $html = $this->renderPartial('_item', ['model' => $comment]);
            $url = Url::entity($comment);

            return compact('html', 'page', 'url');
        }

        Yii::$app->response->statusCode = 400;
        return ['message' => $model->getFirstError('content')];
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('updateForumComment', ['comment' => $comment]);

        $model = new CommentForm(['content' => $comment->content]);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $this->comments->replicate($comment, Yii::$app->user->id);
                $this->comments->update($comment, ['content' => $model->content]);
                return $this->renderPartial('_item', ['model' => $comment]);
            } else {
                Yii::$app->response->statusCode = 400;
                return $model->getFirstError('content');
            }
        }

        return $this->renderPartial('_form-update', compact('comment', 'model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('deleteForumComment', ['comment' => $comment]);

        if ($comment->isInitial) {
            $redirectUrl = Url::entity($comment->theme->board);
        } else {
            $redirectUrl = Url::entity($comment->theme);
        }

        $this->comments->delete($comment);

        return $this->redirect($redirectUrl);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionHide($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('hideForumComment', ['comment' => $comment]);

        $this->comments->hide($comment);

        return $this->redirect(Url::entity($comment->theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionShow($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('hideForumComment', ['comment' => $comment]);

        $this->comments->show($comment);

        return $this->redirect(Url::entity($comment->theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionPin($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('pinForumComment', ['comment' => $comment]);

        $this->comments->pin($comment);

        return $this->redirect(Url::entity($comment->theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUnpin($id)
    {
        $comment = $this->comments->byId($id);
        $this->ensureExists($comment);
        $this->ensureAllowed('pinForumComment', ['comment' => $comment]);

        $this->comments->unpin($comment);

        return $this->redirect(Url::entity($comment->theme));
    }
}
