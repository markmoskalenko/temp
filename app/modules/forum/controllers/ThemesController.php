<?php
namespace app\modules\forum\controllers;

use app\db\records\Seller;
use app\filters\LoginRequired;
use app\helpers\Url;
use app\modules\forum\models\MoveThemeForm;
use app\modules\forum\models\ThemeForm;
use app\modules\forum\repositories\BoardRepository;
use app\modules\forum\repositories\CommentRepository;
use app\modules\forum\repositories\ThemeRepository;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ThemesController extends BaseController
{
    /**
     * @var BoardRepository
     */
    protected $boards;
    /**
     * @var ThemeRepository
     */
    protected $themes;
    /**
     * @var CommentRepository
     */
    protected $comments;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->boards = new BoardRepository();
        $this->themes = new ThemeRepository();
        $this->comments = new CommentRepository();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => LoginRequired::className(),
                'only' => ['create', 'update', 'delete'],
            ],
        ];
    }

    /**
     * @param integer $boardId
     * @return string
     */
    public function actionIndex($boardId)
    {
        $board = $this->boards->getById($boardId);
        $this->ensureExists($board);

        $dataProvider = $this->themes->search(['boardId' => $board->id], $board->hideEmpty);
        $fixedThemes = $this->themes->findAllFixedByTopicId($board->id);

        return $this->render('index', compact('board', 'dataProvider', 'fixedThemes'));
    }

    /**
     * @param integer $boardId
     * @return mixed
     */
    public function actionCreate($boardId)
    {
        $board = $this->boards->getById($boardId);
        $this->ensureExists($board);
        $this->ensureAllowed('createForumTheme', ['board' => $board]);

        $model = new ThemeForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $theme = $this->themes->create(['boardId' => $board->id, 'title' => $model->title]);
            $this->comments->create(['themeId' => $theme->id, 'content' => $model->comment], $initial = true);
            return $this->redirect(Url::entity($theme));
        }

        return $this->render('create', compact('board', 'model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('updateForumTheme', ['theme' => $theme]);

        $comment = $this->comments->getInitial($theme->id);

        $model = new ThemeForm();
        $model->setScenario(ThemeForm::SCENARIO_UPDATE);
        $model->title = $theme->title;
        if ($comment) {
            $model->comment = $comment->content;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->themes->update($theme, $model->attributes);

            if ($comment) {
                $this->comments->update($comment, ['content' => $model->comment]);
            }

            return $this->redirect(Url::entity($theme));
        }

        return $this->render('update', compact('theme', 'model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('deleteForumTheme', ['theme' => $theme]);

        $boardUrl = Url::entity($theme->board);
        $theme->delete();

        return $this->redirect($boardUrl);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionMove($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('moveForumTheme', ['theme' => $theme]);

        $model = new MoveThemeForm(['boardId' => $theme->boardId]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->themes->move($theme, $model->boardId);
            return $this->redirect(Url::entity($theme));
        }

        $boardMap = $this->boards->getMap();

        return $this->render('move', compact('theme', 'model', 'boardMap'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionClose($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('closeForumTheme', ['theme' => $theme]);

        $this->themes->close($theme);

        return $this->redirect(Url::entity($theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionOpen($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('closeForumTheme', ['theme' => $theme]);

        $this->themes->open($theme);

        return $this->redirect(Url::entity($theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionPin($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('pinForumTheme', ['theme' => $theme]);

        $this->themes->pin($theme);

        return $this->redirect(Url::entity($theme));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUnpin($id)
    {
        $theme = $this->themes->byId($id);
        $this->ensureExists($theme);
        $this->ensureAllowed('pinForumTheme', ['theme' => $theme]);

        $this->themes->unpin($theme);

        return $this->redirect(Url::entity($theme));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionSeller($id)
    {
        /* @var $seller Seller */
        $seller = Seller::findOne($id);
        $this->ensureExists($seller);

        $theme = $this->themes->getBySellerId($seller->id);

        if ( ! $theme) {
            $theme = $this->themes->createForSeller($seller);
        }

        return $this->redirect(Url::entity($theme), 301);
    }
}