<?php
namespace app\modules\forum\controllers;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseController extends \app\controllers\BaseController
{
    /**
     * @var string
     */
    public $layout = '/content';


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->view->params['disableSearchArea'] = true;
    }
}