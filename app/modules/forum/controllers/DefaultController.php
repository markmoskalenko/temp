<?php
namespace app\modules\forum\controllers;

use app\filters\JsonFormatter;
use app\helpers\Url;
use app\models\ImageUploadForm;
use app\modules\forum\repositories\CategoryRepository;
use app\modules\forum\repositories\ImageRepository;
use Yii;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DefaultController extends BaseController
{
    /**
     * @var CategoryRepository
     */
    protected $categories;
    /**
     * @var ImageRepository
     */
    protected $images;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => JsonFormatter::className(),
                'only' => ['upload-image'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->categories = new CategoryRepository();
        $this->images = new ImageRepository();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $categories = $this->categories->all();
        return $this->render('index', compact('categories'));
    }

    /**
     * @return array
     */
    public function actionUploadImage()
    {
        $form = new ImageUploadForm();
        $form->file = UploadedFile::getInstanceByName('file');

        if ($form->validate()) {
            $image = $this->images->create($form->file);
            return ['id' => $image->id, 'url' => Url::uploaded($image)];
        }

        Yii::$app->response->statusCode = 400;
        return ['message' => $form->getFirstError('file')];
    }
}
