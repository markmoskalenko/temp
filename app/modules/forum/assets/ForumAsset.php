<?php
namespace app\modules\forum\assets;

use yii\web\AssetBundle;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ForumAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/frontend-forum.js',
    ];

    public $depends = [
        'app\assets\JqueryFormAsset',
        'app\assets\TimeAgoAsset',
        'yii\web\JqueryAsset',
    ];
}
