<?php
namespace app\modules\forum\models;

use Yii;
use yii\base\Model;

/**
 * @property string $content
 *
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentForm extends Model
{
    /**
    * @var string 
    */
    public $content;


    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['content', 'string'],
            ['content', 'trim', 'enableClientValidation' => false],
            ['content', 'required', 'message' => 'Необходимо укзать текст сообщения.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content' => 'Комментарий',
        ];
    }
}
