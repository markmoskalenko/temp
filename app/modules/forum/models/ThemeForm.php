<?php
namespace app\modules\forum\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $boardId
 * @property string  $title
 * @property string  $comment
 *
 * @author Mark Moskalenko <office@it-yes.com>
 * @author Artem Belov <razor2909@gmail.com>
 */
class ThemeForm extends Model
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_UPDATE = 'update';

    /**
    * @var string 
    */
    public $title;
    /**
    * @var string 
    */
    public $comment;


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => ['title', 'comment'],
            static::SCENARIO_UPDATE => ['title', 'comment'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            ['title', 'string'],
            ['title', 'app\validators\HeaderFilter'],
            ['title', 'string', 'max' => 255],
            ['title', 'required'],

            ['comment', 'string'],
            ['comment', 'trim'],
            ['comment', 'required', 'except' => static::SCENARIO_UPDATE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название темы',
            'comment' => 'Текст сообщения',
        ];
    }
}
