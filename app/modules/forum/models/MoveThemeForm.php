<?php
namespace app\modules\forum\models;

use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MoveThemeForm extends Model
{
    /**
     * @var integer
     */
    public $boardId;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['boardId', 'integer'],
            ['boardId', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'boardId' => 'Куда переместить тему?',
        ];
    }
}