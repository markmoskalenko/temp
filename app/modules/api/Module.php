<?php
namespace app\modules\api;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            [
                'class' => 'yii\rest\UrlRule',
                'controller' => ['api/stores', 'api/boasts'],
            ],
        ]);
    }
}