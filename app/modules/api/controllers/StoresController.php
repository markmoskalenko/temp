<?php
namespace app\modules\api\controllers;

use app\db\records\Seller;
use app\helpers\Url;
use yii\rest\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoresController extends Controller
{
    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'view' => ['GET', 'HEAD'],
        ];
    }

    /**
     * @param integer $id
     * @return array
     */
    public function actionView($id)
    {
        $store = Seller::findOr404(['originalStoreId' => $id]);

        return [
            'id' => $store->originalStoreId,
            'name' => $store->storeName,
            'url' => Url::toSeller($store, [], true),
            'reviewsUrl' => Url::toSellerComplaints($store, [], true),
            'reviewsCount' => $store->getComplaintsCount(),
        ];
    }
}