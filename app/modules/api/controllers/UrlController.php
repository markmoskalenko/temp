<?php
namespace app\modules\api\controllers;

use aliexpress\api\Client;
use aliexpress\api\Exception as ApiException;
use app\filters\JsonFormatter;
use Yii;
use yii\web\Controller;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UrlController extends Controller
{
    /**
     * @inheritdoc
     */
    public $enableCsrfValidation = false;


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFormatter::className(),
                'only' => ['index'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        $url = Yii::$app->request->post('url');

        $api = new Client(Yii::$app->params['ali.apiKey']);
        $params = ['trackingId' => 'extlink', 'urls' => $url];

        try {
            if ( ! $url) {
                throw new ApiException('URL must be set.');
            }

            $result = $api->getLinks($params);

            if (empty($result['promotionUrls'][0]['promotionUrl'])) {
                throw new ApiException('Failed to find a promotion url.');
            }

            $url = $result['promotionUrls'][0]['promotionUrl'];

            $this->logSuccessRequest();
        } catch (ApiException $e) {
            Yii::$app->raven->extraContext($params);
            Yii::$app->raven->captureException($e);

            $this->logFailedRequest();
            $this->logFailedUrl($url);

            return ['status' => false];
        }

        return ['status' => true, 'url' => $url];
    }

    protected function logSuccessRequest()
    {
        $sql = 'INSERT INTO api_url SET date = :date, success = 1 ON DUPLICATE KEY UPDATE success = success + 1';
        $params = [':date' => date('Y-m-d')];
        Yii::$app->db->createCommand($sql, $params)->execute();
    }

    protected function logFailedRequest()
    {
        $sql = 'INSERT INTO api_url SET date = :date, failed = 1 ON DUPLICATE KEY UPDATE failed = failed + 1';
        $params = [':date' => date('Y-m-d')];
        Yii::$app->db->createCommand($sql, $params)->execute();
    }

    protected function logFailedUrl($url)
    {
        Yii::$app->db->createCommand()->insert('api_url_error', ['url' => $url])->execute();
    }
}
