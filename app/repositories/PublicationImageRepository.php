<?php
namespace app\repositories;

use app\db\records\PublicationImage;
use app\models\UploadTrait;
use app\services\ImageUploadService;
use Yii;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PublicationImageRepository extends ActiveRepository
{
    use UploadTrait;

    /**
     * @param integer $count
     * @return PublicationImage[]
     */
    public function last($count = null)
    {
        return PublicationImage::find()->last()->limit($count)->all();
    }

    /**
     * @param UploadedFile $file
     * @return PublicationImage
     */
    public function create(UploadedFile $file)
    {
        $filename = $this->createFileName($file, true);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, Yii::getAlias('@webroot/uploads/'.$filename));

        $image = new PublicationImage();
        $image->path = $filename;
        $this->saveOrFail($image);

        return $image;
    }
}