<?php

namespace app\repositories;

use app\db\records\TrackingNumberUser;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberUserRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\TrackingNumberUser';

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = TrackingNumberUser::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     */
    public function create($data)
    {
        $exists = TrackingNumberUser::find()->andWhere(['userId'=>$data['userId'], 'trackingNumberId'=>$data['trackingNumberId']])->exists();

        if(!$exists){
            /* @var $model TrackingNumberUser */
            $model = \Yii::createObject($this->recordClass);
            $model->setAttributes($data, false);
            $this->saveOrFail($model);
        }
    }
}
