<?php
namespace app\repositories;

use app\db\records\QuestionAnswer;
use app\db\records\QuestionAnswerCategory;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class QuestionAnswerCategoryRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\QuestionAnswerCategory';

    /**
     * @param integer $id
     * @return QuestionAnswer
     */
    public function getById($id)
    {
        return $this->findOne($id);
    }

    public function getMap(){
        return ArrayHelper::map(QuestionAnswerCategory::find()->all(), 'id', 'name');
    }

    /**
     * @param $slug
     * @return null|static
     * @throws NotFoundHttpException
     */
    public function getBySlugOr404($slug)
    {
        $model = QuestionAnswerCategory::findOne(['slug' => $slug]);

        if(!$model){
            throw new NotFoundHttpException();
        }

        return $model;
    }

    public function getOne()
    {
        $model = QuestionAnswerCategory::find()->one();

        if(!$model){
            throw new NotFoundHttpException();
        }

        return $model;
    }
}