<?php
namespace app\repositories;

use app\db\records\News;
use app\search\NewsSearch;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\News';


    /**
     * @param NewsSearch $searchModel
     * @return ActiveDataProvider
     */
    public function search(NewsSearch $searchModel = null)
    {
        $query = News::find()
            ->last()
            ->published()
            ->speaking($searchModel->language);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return News|null
     */
    public function getById($id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * @param string $slug
     * @return News|null
     */
    public function getBySlug($slug)
    {
        return $this->findOne(['slug' => $slug]);
    }
}