<?php
namespace app\repositories;

use Yii;
use yii\base\Configurable;
use yii\base\InvalidValueException;
use yii\db\BaseActiveRecord;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ActiveRepository implements Configurable
{
    /**
     * @var string class of ActiveRecord instance.
     */
    public $recordClass;


    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        if ( ! empty($config)) {
            Yii::configure($this, $config);
        }
    }

    /**
     * @param mixed $condition
     * @return mixed
     */
    public function findAll($condition)
    {
        return call_user_func([$this->recordClass, 'findAll'], $condition);
    }

    /**
     * @param mixed $condition
     * @return mixed
     */
    public function findOne($condition)
    {
        return call_user_func([$this->recordClass, 'findOne'], $condition);
    }

    /**
     * @param mixed $condition
     * @return integer
     */
    public function findCount($condition)
    {
        $query = call_user_func([$this->recordClass, 'find']);
        return (int) $query->where($condition)->count();
    }

    /**
     * @param mixed $record
     */
    public function viewed($record)
    {
        $record->updateCounters(['viewCount' => 1]);
    }

    /**
     * @param BaseActiveRecord $record
     * @param boolean          $runValidation
     * @param array            $attributeNames
     * @throws InvalidValueException
     */
    protected function saveOrFail($record, $runValidation = true, $attributeNames = null)
    {
        if ( ! $record->save($runValidation, $attributeNames)) {
            Yii::$app->raven->extraContext(['record' => $record->toArray(), 'errors' => $record->getErrors()]);
            throw new InvalidValueException('Record failed to be saved into database.');
        }
    }

    /**
     * @param mixed $condition
     */
    protected function deleteOrFail($condition)
    {
        if ( ! call_user_func([$this->recordClass, 'deleteAll'], $condition)) {
            Yii::$app->raven->extraContext(['condition' => $condition]);
            throw new InvalidValueException('No records were deleted from database.');
        }
    }
}