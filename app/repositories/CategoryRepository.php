<?php
namespace app\repositories;

use app\db\records\Category;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoryRepository extends ActiveRepository
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Category::find()->orderLikeTree();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false,
        ]);
    }

    /**
     * @return Category[]
     */
    public function getAll()
    {
        return Category::find()->orderLikeTree()->all();
    }

    /**
     * @param integer $id
     * @return Category|null
     */
    public function getById($id)
    {
        return Category::findOne(['id' => $id]);
    }

    /**
     * @param string $slug
     * @return Category|null
     */
    public function getBySlug($slug)
    {
        return Category::findOne(['slug' => $slug]);
    }

    /**
     * @param Category $category
     * @return Category|null
     */
    public function getParent($category)
    {
        return $category->parents(1)->one();
    }

    /**
     * @param string  $data
     * @param integer $parentId
     * @return Category
     */
    public function create($data, $parentId = null)
    {
        $category = new Category($data);
        $this->moveCategory($category, $parentId);
        return $category;
    }

    /**
     * @param Category $category
     * @param array    $data
     * @param integer  $parentId
     */
    public function update($category, $data, $parentId = null)
    {
        $safeOnly = false;
        $category->setAttributes($data, $safeOnly);

        $category->save();

        if (ArrayHelper::getValue($this->getParent($category), 'id') != $parentId) {
            $this->moveCategory($category, $parentId);
        }
    }

    /**
     * @param Category $category
     */
    public function delete($category)
    {
        $category->deleteWithChildren();
    }

    /**
     * @param Category     $category
     * @param integer|null $parentId
     */
    protected function moveCategory($category, $parentId)
    {
        if ($parentId) {
            $parent = $this->getById($parentId);
            $category->appendTo($parent);
        } else {
            $category->makeRoot();
        }
    }
}