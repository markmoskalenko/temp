<?php

namespace app\repositories;

use app\db\records\Follower;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class FollowerRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\Follower';

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Follower::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param $id
     * @return Follower|bool
     * @throws \yii\base\InvalidConfigException
     */
    public function create($id)
    {
        $exist = Follower::find()
            ->getOwner()
            ->byObjectId($id)
            ->exists();

        if ($exist || \Yii::$app->user->identity->id == $id) {
            return false;
        }

        /* @var $model Follower */
        $model = \Yii::createObject($this->recordClass);
        $model->objectId = $id;
        $this->saveOrFail($model);

        return $model;
    }

    /**
     * @param $id
     * @return bool|false|int
     * @throws \Exception
     */
    public function delete($id)
    {
        $exist = Follower::find()
            ->getOwner()
            ->byObjectId($id)
            ->one();

        if (!$exist) {
            return false;
        }
        return $exist->delete();
    }

    /**
     * @param $userId
     * @param $objectId
     * @return bool
     */
    public function getIsSubscribe($userId, $objectId)
    {
        return Follower::find()
            ->byUserId($userId)
            ->byObjectId($objectId)
            ->exists();
    }

    /**
     * @param integer $id
     * @return Follower
     */
    public function getById($id)
    {
        return $this->findOne($id);
    }
}
