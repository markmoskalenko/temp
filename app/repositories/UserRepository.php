<?php
namespace app\repositories;

use app\db\records\Article;
use app\db\records\User;

/**
 * Class UserRepository
 * @package app\repositories
 */
class UserRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\User';

    /**
     * @param integer $id
     * @return Article|null
     */
    public function getById($id)
    {
        return $this->findOne(['id' => (int)$id]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAll()
    {
        return User::find()->all();
    }
}