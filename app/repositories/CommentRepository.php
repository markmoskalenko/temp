<?php
namespace app\repositories;

use app\db\records\Comment;
use app\models\CommentForm;
use Yii;
use yii\helpers\StringHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentRepository extends ActiveRepository
{
    /**
     * @var string
     */
    public $recordClass = 'app\db\records\Comment';


    /**
     * @param integer $id
     * @return Comment
     */
    public function getById($id)
    {
        return $this->findOne($id);
    }

    /**
     * @param object $entity
     * @return Comment[]
     */
    public function getAllFor($entity)
    {
        return $this->findAll([
            'entityId' => $this->createEntityId($entity),
            'entityName' => $this->createEntityName($entity),
            'isDeleted' => 0,
        ]);
    }

    /**
     * @param object $entity
     * @return integer
     */
    public function count($entity)
    {
        return $this->findCount([
            'entityId' => $this->createEntityId($entity),
            'entityName' => $this->createEntityName($entity),
            'isDeleted' => 0,
        ]);
    }

    /**
     * @param CommentForm $model
     * @param Comment     $comment
     */
    public function load(CommentForm $model, Comment $comment)
    {
        $model->parentId = $comment->parentId;
        $model->content = $comment->content;
    }

    /**
     * Creates comment for target entity.
     *
     * @param CommentForm $model
     * @param object      $targetEntity
     * @return Comment
     */
    public function create($model, $targetEntity)
    {
        $comment = Yii::createObject($this->recordClass);
        $comment->entityId = $this->createEntityId($targetEntity);
        $comment->entityName = $this->createEntityName($targetEntity);
        $comment->parentId = $model->parentId;
        $comment->content = $model->content;
        $comment->save();

        return $comment;
    }

    /**
     * Deletes comment.
     *
     * @param Comment $comment
     */
    public function delete($comment)
    {
        $comment->updateAttributes(['isDeleted' => 1]);
    }

    /**
     * Generates [[entityId]] property for comment.
     *
     * @param object $entity
     * @return mixed
     */
    public function createEntityId($entity)
    {
        return $entity->id;
    }

    /**
     * Generates [[entityName]] property for comment.
     *
     * @param object $entity
     * @return string
     */
    public function createEntityName($entity)
    {
        return mb_strtolower(StringHelper::basename(get_class($entity)));
    }
}