<?php
namespace app\repositories;

use app\db\records\Article;
use app\db\records\ArticleCategory;
use app\search\ArticleSearch;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticleRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\Article';


    /**
     * @param ArticleSearch $searchModel
     * @return ActiveDataProvider
     */
    public function search(ArticleSearch $searchModel = null)
    {
        $query = Article::find()
            ->last()
            ->published()
            ->speaking($searchModel->language);

        $query->andFilterWhere([
            'categoryId' => $searchModel->categoryId,
        ]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return Article|null
     */
    public function getById($id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * @param string $slug
     * @return Article|null
     */
    public function getBySlug($slug)
    {
        return $this->findOne(['slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return Article|null
     */
    public function getCategoryBySlug($slug)
    {
        return ArticleCategory::findOne(['slug' => $slug]);
    }
}