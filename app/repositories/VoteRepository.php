<?php
namespace app\repositories;

use app\db\records\Boast;
use app\db\records\Vote;
use yii\helpers\StringHelper;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class VoteRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\Vote';


    /**
     * Returns total count of votes for entity.
     *
     * @param object $entity
     * @return integer
     */
    public function count($entity)
    {
        return $this->findCount([
            'entityName' => $this->createEntityName($entity),
            'entityId' => $this->createEntityId($entity),
        ]);
    }

    /**
     * @param $userId
     * @param $entity
     * @return int|string
     */
    public function getCountByUserOfDay($userId, $entity)
    {
        return Vote::find()
            ->andWhere('DATE(`timeCreated`) = CURDATE()')
            ->andWhere(['userId' => $userId, 'entityName' => $this->createEntityName($entity)])
            ->count();
    }

    /**
     * Returns true if user has already voted for this entity.
     *
     * @param integer $userId
     * @param object $entity
     * @return boolean
     */
    public function exists($userId, $entity)
    {
        return (bool)$this->findCount([
            'userId' => $userId,
            'entityName' => $this->createEntityName($entity),
            'entityId' => $this->createEntityId($entity),
        ]);
    }

    /**
     * Creates user vote for entity.
     * @param $userId
     * @param $entity
     * @param int $value
     * @throws ForbiddenHttpException
     */
    public function create($userId, $entity, $value = 1)
    {
        if(!\Yii::$app->user->can('voteBoast', ['user'=>\Yii::$app->user->identity])){
            \Yii::$app->response->data = ['message' => 'К сожалению, новые пользователи не могут голосовать более чем за 10 хвастов в сутки. Не забывайте комментировать хвасты. Хорошего дня.'];
            \Yii::$app->response->format = Response::FORMAT_JSON;
            \Yii::$app->response->statusCode = 403;
            \Yii::$app->end();
        }

        $vote = new Vote();
        $vote->userId = $userId;
        $vote->entityName = $this->createEntityName($entity);
        $vote->entityId = $this->createEntityId($entity);
        $vote->value = $value;
        $this->saveOrFail($vote);
    }

    /**
     * Deletes user vote for entity.
     *
     * @param integer $userId
     * @param object $entity
     */
    public function delete($userId, $entity)
    {
        $this->deleteOrFail([
            'userId' => $userId,
            'entityName' => $this->createEntityName($entity),
            'entityId' => $this->createEntityId($entity),
        ]);
    }

    /**
     * Creates/removes user vote for entity.
     *
     * @param integer $userId
     * @param object $entity
     * @return boolean whether the entity is voted
     */
    public function toggle($userId, $entity)
    {
        if ($this->exists($userId, $entity)) {
            $this->delete($userId, $entity);

            return false;
        } else {
            $this->create($userId, $entity);

            return true;
        }
    }

    /**
     * Generates [[entityId]] property.
     *
     * @param object $entity
     * @return mixed
     */
    public function createEntityId($entity)
    {
        return $entity->id;
    }

    /**
     * Generates [[entityName]] property.
     *
     * @param object $entity
     * @return string
     */
    public function createEntityName($entity)
    {
        return mb_strtolower(StringHelper::basename(get_class($entity)));
    }
}