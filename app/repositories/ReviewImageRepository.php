<?php
namespace app\repositories;

use app\db\records\ReviewImage;
use app\models\UploadTrait;
use app\services\ImageUploadService;
use Yii;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewImageRepository extends ActiveRepository
{
    use UploadTrait;

    /**
     * @param integer $count
     * @return ReviewImage[]
     */
    public function last($count = null)
    {
        $userId = Yii::$app->user->id;
        return ReviewImage::find()->andWhere(['userId' => $userId])->last()->limit($count)->all();
    }

    /**
     * @param UploadedFile $file
     * @return ReviewImage
     */
    public function create(UploadedFile $file)
    {
        $filename = $this->createFileName($file, true);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, Yii::getAlias('@webroot/uploads/'.$filename));

        $image = new ReviewImage();
        $image->filename = $filename;
        $this->saveOrFail($image);

        return $image;
    }
}