<?php

namespace app\repositories;

use app\db\records\TrackingNumber;
use yii\data\ActiveDataProvider;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\TrackingNumber';

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = TrackingNumber::find();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     * @return TrackingNumber 
     */
    public function create($data)
    {
        $model = TrackingNumber::find()->andWhere(['trackingNumber'=>$data['trackingNumber']])->one();

        if(!$model){
            /* @var $model TrackingNumber */
            $model = \Yii::createObject($this->recordClass);
            $model->setAttributes($data, false);
            $this->saveOrFail($model);
        }

        if(!\Yii::$app->user->isGuest){
            (new TrackingNumberUserRepository())->create(['userId'=>\Yii::$app->user->identity->id, 'trackingNumberId'=>$model->id]);
        }



    }
}
