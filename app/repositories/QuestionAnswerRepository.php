<?php
namespace app\repositories;

use app\db\records\QuestionAnswer;
use app\db\records\QuestionAnswerPhoto;
use app\models\UploadTrait;
use app\services\ImageUploadService;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class QuestionAnswerRepository extends ActiveRepository
{
    use UploadTrait;

    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\QuestionAnswer';

    /**
     * @param integer $id
     * @return QuestionAnswer
     */
    public function getById($id)
    {
        return $this->findOne($id);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = QuestionAnswer::find()->orderBy(['id' => SORT_DESC]);

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array $data
     * @return QuestionAnswer
     */
    public function create($data)
    {
        /* @var $qa QuestionAnswer */
        $qa = \Yii::createObject($this->recordClass);
        $qa->setAttributes($data, false);
        if (!\Yii::$app->user->isGuest) {
            $qa->userId = \Yii::$app->user->identity->getId();
        }
        $this->saveOrFail($qa);

        return $qa;
    }

    /**
     * @param UploadedFile $file
     * @return QuestionAnswerPhoto
     */
    public function createPhoto(UploadedFile $file)
    {
        $filename = $this->createUniqueFileName($file);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, \Yii::getAlias('@webroot/uploads/' . $filename));

        $photo = new QuestionAnswerPhoto();
        $photo->filename = $filename;
        $photo->saveOrFail();

        return $photo;
    }

    /**
     * @param integer $boastId
     * @param array $photoKeys
     */
    public function syncPhotos($boastId, array $photoKeys)
    {
        if (!count($photoKeys)) {
            return;
        }
        $exists = QuestionAnswerPhoto::findAll(['qaId' => $boastId]);
        $exists = ArrayHelper::getColumn($exists, 'id');

        $keysToDelete = array_filter($exists, function ($photoId) use ($photoKeys) {
            return !in_array($photoId, $photoKeys);
        });

        $keysToAssign = array_filter($photoKeys, function ($photoId) use ($exists) {
            return !in_array($photoId, $exists);
        });

        QuestionAnswerPhoto::deleteAll(['id' => $keysToDelete]);
        QuestionAnswerPhoto::updateAll(['qaId' => $boastId], ['id' => $keysToAssign]);
    }

    /**
     * @param string $slug
     * @return QuestionAnswer|null
     */
    public function getCategoryBySlug($slug)
    {
        return QuestionAnswer::findOne(['slug' => $slug]);
    }
}