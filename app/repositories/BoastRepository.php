<?php
namespace app\repositories;

use app\db\records\Boast;
use app\db\records\BoastPhoto;
use app\models\UploadTrait;
use app\services\ImageUploadService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastRepository extends ActiveRepository
{
    use UploadTrait;

    /**
     * @var string
     */
    public $recordClass = 'app\db\records\Boast';


    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Boast::find()
            ->with(['category', 'mainPhoto'])
            ->last()
            ->visible();

        if ($params) {
            $query->andWhere($params);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort' => false,
        ]);
    }

    /**
     * @param array   $params
     * @param integer $limit
     * @return ActiveDataProvider
     */
    public function searchRandom($params = [], $limit = null)
    {
        $query = Boast::find()
            ->with(['category', 'mainPhoto'])
            ->random()
            ->visible();

        if ($params) {
            $query->andWhere($params);
        }

        if ($limit) {
            $query->limit($limit);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return Boast
     */
    public function getById($id)
    {
        return $this->findOne($id);
    }

    /**
     * @param array $data
     * @return Boast
     */
    public function create($data)
    {
        /* @var $boast Boast */
        $boast = Yii::createObject($this->recordClass);
        $boast->setAttributes($data, false);
        $this->saveOrFail($boast);

        return $boast;
    }

    /**
     * @param Boast $boast
     * @param array  $data
     */
    public function update($boast, $data)
    {
        $boast->setAttributes($data, false);
        $this->saveOrFail($boast);
    }

    /**
     * @param Boast $boast
     */
    public function delete($boast)
    {
        $boast->updateAttributes(['visible' => 0, 'isDeleted' => 1]);
    }

    /**
     * @param UploadedFile $file
     * @return BoastPhoto
     */
    public function createPhoto(UploadedFile $file)
    {
        $filename = $this->createUniqueFileName($file);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, Yii::getAlias('@webroot/uploads/'.$filename));

        $photo = new BoastPhoto();
        $photo->filename = $filename;
        $photo->saveOrFail();

        return $photo;
    }

    /**
     * @param integer $boastId
     * @param array   $photoKeys
     */
    public function syncPhotos($boastId, array $photoKeys)
    {
        $exists = BoastPhoto::findAll(['boastId' => $boastId]);
        $exists = ArrayHelper::getColumn($exists, 'id');

        $keysToDelete = array_filter($exists, function ($photoId) use ($photoKeys) {
            return ! in_array($photoId, $photoKeys);
        });

        $keysToAssign = array_filter($photoKeys, function ($photoId) use ($exists) {
            return ! in_array($photoId, $exists);
        });

        BoastPhoto::deleteAll(['id' => $keysToDelete]);
        BoastPhoto::updateAll(['boastId' => $boastId], ['id' => $keysToAssign, 'boastId' => null]);
    }

    /**
     * Хотфикс для тупорылого бага, когда публикуются дубли хвастов без фоток.
     *
     * @param Boast $boast
     */
    public function ensureHasPhotos($boast)
    {
        $imagesCount = BoastPhoto::find()->where(['boastId' => $boast->id])->count();

        if ( ! $imagesCount) {
            $this->delete($boast);
        }
    }
}