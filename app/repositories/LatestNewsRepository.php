<?php
namespace app\repositories;

use app\db\records\LatestNews;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Class LatestNewsRepository
 * @package app\repositories
 */
class LatestNewsRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\LatestNews';

    /**
     * @param $type
     * @return array|\yii\redis\ActiveRecord[]
     */
    public function getLast($type)
    {
        $items = LatestNews::find()
            ->asArray()
            ->andWhere(['type'=>(int)$type])
            ->all();

        return array_slice(array_reverse($items), 0, 10);
    }

    /**
     * @param array $data
     * @return LatestNews
     */
    public function create($data)
    {
        /* @var $model LatestNews */
        $model = \Yii::createObject($this->recordClass);
        $model->setAttributes($data, false);
        $this->saveOrFail($model);

        return $model;
    }
}