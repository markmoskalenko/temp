<?php
namespace app\repositories;

use app\db\records\Notifications;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class NotificationRepository extends ActiveRepository
{
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\Notifications';

    /**
     * @return int
     */
    public function getCountNew()
    {
        return Notifications::find()
            ->andWhere([
                'isRead' => Notifications::NOTIFICATION_NOT_READ,
                'recipientId' => \Yii::$app->user->identity->id
            ])
            ->count();
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Notifications::find()->andWhere(['recipientId' => \Yii::$app->user->identity->id]);

        if ($params) {
            $query->andWhere($params);
        }

        $model = $query->all();

        $model = array_reverse($model);

        return new ArrayDataProvider([
            'allModels' => $model,
            'pagination' => ['defaultPageSize' => 50],
        ]);
    }

    /**
     * @param $id
     * @param $all
     */
    public function setRead($id, $all = false)
    {
        if ($all) {
            Notifications::updateAll(['isRead' => Notifications::NOTIFICATION_READ],
                ['recipientId' => \Yii::$app->user->identity->id]);
        } else {
            $id = (int)$id;
            Notifications::updateAll(['isRead' => Notifications::NOTIFICATION_READ],
                ['id' => $id, 'recipientId' => \Yii::$app->user->identity->id]
            );
        }
    }


    /**
     * @param array $data
     * @return Notifications
     */
    public function create($data)
    {
        /* @var $model Notifications */
        $model = \Yii::createObject($this->recordClass);
        $model->setAttributes($data, false);

        if ($this->getCount([
            'type' => $model->type,
            'entityId' => $model->entityId,
            'authorId' => \Yii::$app->user->id,
            'isRead' => Notifications::NOTIFICATION_NOT_READ,
        ]) == 0) {
            $this->saveOrFail($model);
        }

        return $model;
    }

    public function getCount($params = [])
    {
        return Notifications::find()
            ->andWhere($params)
            ->count();
    }
}
