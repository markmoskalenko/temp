<?php
namespace app\repositories;

use app\db\records\Coupon;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CouponsRepository
{
    /**
     * @return ActiveDataProvider
     */
    public function searchProfitable()
    {
        $query = Coupon::find()->last()->profitable()->with('category');

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * @param integer $id
     * @return Coupon|null
     */
    public function getById($id)
    {
        return Coupon::findOne($id);
    }
}