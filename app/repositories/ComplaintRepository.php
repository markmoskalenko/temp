<?php
namespace app\repositories;

use app\db\records\Complaint;
use app\db\records\ComplaintPhoto;
use app\models\UploadTrait;
use app\services\ImageUploadService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintRepository extends ActiveRepository
{
    use UploadTrait;
    
    /**
     * @inheritdoc
     */
    public $recordClass = 'app\db\records\Complaint';


    /**
     * @param array   $params
     * @param integer $limit
     * @return ActiveDataProvider
     */
    public function search($params = [], $limit = null)
    {
        $query = Complaint::find();

        if ($params) {
            $query->andWhere($params);
        }

        if ($limit) {
            $query->limit($limit);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return Complaint|null
     */
    public function getById($id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * @param array $data
     * @param array $photos
     * @return Complaint
     */
    public function create($data, $photos)
    {
        /* @var $complaint Complaint */
        $complaint = Yii::createObject($this->recordClass);
        $complaint->setAttributes($data);
        $complaint->originalText = $complaint->text;
        $this->saveOrFail($complaint);

        $this->syncPhotos($complaint->id, $photos);

        return $complaint;
    }

    /**
     * @param Complaint $complaint
     * @param array $data
     * @param array $photos
     */
    public function update($complaint, $data, $photos)
    {
        $complaint->setAttributes($data);
        $this->saveOrFail($complaint);

        $this->syncPhotos($complaint->id, $photos);
    }

    /**
     * @param UploadedFile $file
     * @return ComplaintPhoto
     */
    public function createPhoto(UploadedFile $file)
    {
        $filename = $this->createUniqueFileName($file);

        $uploader = new ImageUploadService();
        $uploader->save($file->tempName, Yii::getAlias('@webroot/uploads/'.$filename));

        $photo = new ComplaintPhoto();
        $photo->path = $filename;
        $photo->saveOrFail();

        return $photo;
    }

    /**
     * @param integer $complaintId
     * @param array   $photoKeys
     */
    public function syncPhotos($complaintId, $photoKeys)
    {
        if ( ! is_array($photoKeys)) {
            $photoKeys = [];
        }

        $exists = ComplaintPhoto::findAll(['complaintId' => $complaintId]);
        $exists = ArrayHelper::getColumn($exists, 'id');

        $keysToDelete = array_filter($exists, function ($photoId) use ($photoKeys) {
            return ! in_array($photoId, $photoKeys);
        });

        $keysToAssign = array_filter($photoKeys, function ($photoId) use ($exists) {
            return ! in_array($photoId, $exists);
        });

        ComplaintPhoto::deleteAll(['id' => $keysToDelete]);
        ComplaintPhoto::updateAll(['complaintId' => $complaintId], ['id' => $keysToAssign, 'complaintId' => null]);
    }
}
