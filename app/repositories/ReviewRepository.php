<?php
namespace app\repositories;

use app\db\records\Review;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewRepository extends ActiveRepository
{
    /**
     * @var string
     */
    public $recordClass = 'app\db\records\Review';


    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Review::find()->last()->andWhere($params);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return Review|null
     */
    public function getById($id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * @param array $data
     * @return Review
     */
    public function create($data)
    {
        /* @var $review Review */
        $review = Yii::createObject($this->recordClass);
        $review->setAttributes($data, false);
        $this->saveOrFail($review);

        return $review;
    }

    /**
     * @param Review $review
     * @param array  $data
     */
    public function update($review, $data)
    {
        $review->setAttributes($data, false);
        $this->saveOrFail($review);
    }

    /**
     * @param Review $review
     */
    public function delete($review)
    {
        $review->delete();
    }
}