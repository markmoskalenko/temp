<?php
namespace app\behaviors;

use app\components\User as UserManager;
use app\db\queries\VoteQuery;
use app\repositories\VoteRepository;
use Yii;
use yii\base\Behavior;

/**
 * @property boolean $isVoted
 * @property integer $votesCount
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class VoteableBehavior extends Behavior
{
    /**
     * @var VoteRepository
     */
    protected $votes;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->votes = new VoteRepository();
    }

    /**
     * @return boolean
     */
    public function getIsVoted()
    {
        $userId = Yii::$app->user->id;
        return $this->votes->exists($userId, $this->owner);
    }

    /**
     * @return integer
     */
    public function getVotesCount()
    {
        return $this->votes->count($this->owner);
    }
}