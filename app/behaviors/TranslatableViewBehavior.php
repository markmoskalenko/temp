<?php
namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\NotSupportedException;
use yii\web\Controller;
use yii\web\View;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class TranslatableViewBehavior extends Behavior
{
    /**
     * @var View
     */
    public $owner;


    /**
     * @return array
     */
    public function events()
    {
        return [
            View::EVENT_BEGIN_PAGE => 'setI18nTitle',
        ];
    }

    /**
     * @param string $message
     * @param array  $params
     * @param string $language
     * @return string
     */
    public function t($message, $params = [], $language = null)
    {
        return Yii::t('app/views', $this->wrapI18nMessage($message), $params, $language);
    }

    /**
     * @param string $message
     * @throws NotSupportedException
     * @return string
     */
    public function wrapI18nMessage($message)
    {
        $controller = $this->owner->context;

        if ($controller instanceof Controller) {
            return $controller->id . '.' . $controller->action->id . '.' . $message;
        } else {
            throw new NotSupportedException('This behavior does not supports any other contexts than yii\web\Controller.');
        }
    }

    /**
     * Set default view title
     */
    public function setI18nTitle()
    {
        if ($this->owner->context instanceof Controller && ! $this->owner->title) {
            $this->owner->title = $this->t('title');
        }
    }
}