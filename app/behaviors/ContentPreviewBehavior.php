<?php
namespace app\behaviors;

use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ContentPreviewBehavior extends Behavior
{
    /**
     * @var string
     */
    public $contentAttribute = 'content';
    /**
     * @var string
     */
    public $contentPreviewAttribute = 'contentPreview';
    /**
     * @var integer
     */
    public $minLength = 100;


    /**
     * @return string
     */
    public function getContentPreview()
    {
        $content = $this->owner->{$this->contentAttribute};
        $contentPreview = $this->owner->{$this->contentPreviewAttribute};

        if ( ! $contentPreview) {
            $delimiter = '<!-- explode -->';
            $paragraphs = explode($delimiter, str_replace('</p>', '</p>'.$delimiter, $content));

            foreach ($paragraphs as $paragraph) {
                if (mb_strpos($paragraph, '<img') !== false) {
                    continue;
                }

                $contentPreview .= $paragraph;

                if (mb_strlen($contentPreview) >= $this->minLength) {
                    break;
                }
            }
        }

        return $contentPreview;
    }

    /**
     * @return array
     */
    public function getContentImages()
    {
        preg_match_all('#<img[^>]+src="([^">]+)"#s', $this->owner->{$this->contentAttribute}, $matches);
        return $matches[1];
    }

    /**
     * @return string
     */
    public function getPreviewImage()
    {
        return ArrayHelper::getValue($this->getContentImages(), 0);
    }
}