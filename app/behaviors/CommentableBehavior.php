<?php
namespace app\behaviors;

use app\db\records\Comment;
use app\repositories\CommentRepository;
use yii\base\Behavior;

/**
 * @property Comment[] $comments
 * @property integer   $commentCount
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class CommentableBehavior extends Behavior
{
    /**
     * @var CommentRepository
     */
    protected $repository;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->repository = new CommentRepository();
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->repository->getAllFor($this->owner);
    }

    /**
     * @return integer
     */
    public function getCommentCount()
    {
        return $this->repository->count($this->owner);
    }
}