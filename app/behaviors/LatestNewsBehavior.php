<?php
namespace app\behaviors;

use app\db\ActiveRecord;
use app\repositories\LatestNewsRepository;
use yii\base\Behavior;
use Yii;

/**
 * Class LatestNewsBehavior
 * @package app\behaviors
 */
class LatestNewsBehavior extends Behavior
{

    /**
     * @var LatestNewsRepository
     */
    protected $_latestNewsRepository;

    /**
     * @var
     */
    public $title;
    /**
     * @var
     */
    public $text;
    /**
     * @var int
     */
    public $type = 1;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->_latestNewsRepository = new LatestNewsRepository();
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
        ];
    }

    /**
     *
     */
    public function afterSave()
    {
        $owner = $this->owner;

        $title = $this->title;

        $type = $this->type;

        if (is_callable($title)) {
            $title = call_user_func($title, $owner);
        }

        $text = $this->text;

        if (is_callable($text)) {
            $text = call_user_func($text, $owner);
        }

        $this->_latestNewsRepository->create(compact('title', 'text', 'type'));
    }
}