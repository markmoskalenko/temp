<?php
namespace app\behaviors;

use yii\base\Behavior;
use yii\db\BaseActiveRecord;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class RelationalSetterBehavior extends Behavior
{
    /**
     * @var BaseActiveRecord
     */
    public $owner;
    /**
     * @var string
     */
    public $primaryKeyAttribute;
    /**
     * @var string
     */
    public $setterAttribute;
    /**
     * @var string
     */
    public $className;

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function __set($key, $value)
    {
        $attribute = $key;
        $className = $this->className;
        $record = $value;

        if (is_scalar($value)) {
            /* @var $record BaseActiveRecord|null */
            $record = call_user_func([$className, 'findOne'], $value);
        }

        if ($record instanceof $className) {
            $this->owner->populateRelation($attribute, $record);
            $this->owner->{$this->primaryKeyAttribute} = $record->primaryKey;
        } else {
            $this->owner->populateRelation($attribute, null);
            $this->owner->{$this->primaryKeyAttribute} = null;
        }
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return ($name === $this->setterAttribute) || parent::canSetProperty($name, $checkVars);
    }
}