<?php
namespace app\behaviors;

use Yii;
use yii\base\Behavior;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerRatingBehavior extends Behavior
{
    /**
     * @param string $category
     * @param string $label
     * @param mixed  $format
     *
     * @return integer|float
     */
    public function getDetailed($category, $label, $format = null)
    {
        $attribute = $this->detailedAttribute($category, $label);
        $value = $this->owner->$attribute;

        return ($format)
            ? Yii::$app->formatter->format($value, $format)
            : $value;
    }

    /**
     * @param string $category
     * @param string $label
     * @param integer|float $value
     */
    public function setDetailed($category, $label, $value)
    {
        $attribute = $this->detailedAttribute($category, $label);
        $this->owner->$attribute = $value;
    }

    /**
     * @param string $category
     * @param string $period
     * @param mixed  $format
     *
     * @return integer|float
     */
    public function getHistory($category, $period, $format = null)
    {
        $attribute = $this->historyAttribute($category, $period);
        $value = $this->owner->$attribute;

        if ($value === null) {
            return 0;
        }

        return ($format)
            ? Yii::$app->formatter->format($value, $format)
            : $value;
    }

    /**
     * @param string $category
     * @param string $period
     * @param integer|float $value
     */
    public function setHistory($category, $period, $value)
    {
        $attribute = $this->historyAttribute($category, $period);
        $this->owner->$attribute = $value;
    }

    /**
     * @param string $category
     * @param string $label
     *
     * @return string
     */
    public function detailedAttribute($category, $label)
    {
        return $label . ucfirst($category);
    }

    /**
     * @param string  $category
     * @param integer $period
     *
     * @return string
     */
    public function historyAttribute($category, $period)
    {
        return $category . $period;
    }
}