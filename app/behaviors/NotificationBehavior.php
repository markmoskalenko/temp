<?php
namespace app\behaviors;

use app\db\ActiveRecord;
use app\db\records\Comment;
use app\db\records\News;
use app\db\records\Follower;
use app\db\records\Notifications;
use app\db\records\User;
use app\db\records\Vote;
use app\repositories\ActiveRepository;
use app\repositories\BoastRepository;
use app\repositories\CommentRepository;
use app\repositories\ComplaintRepository;
use app\repositories\FollowerRepository;
use app\repositories\NewsRepository;
use app\repositories\NotificationRepository;
use app\repositories\QuestionAnswerRepository;
use app\repositories\UserRepository;
use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * Class NotificationBehavior
 * @package app\behaviors
 */
class NotificationBehavior extends Behavior
{
    /**
     * @var NotificationRepository
     */
    protected $notification;
    /**
     * @var UserRepository
     */
    protected $user;
    /**
     * @var QuestionAnswerRepository
     */
    protected $questionAnswer;
    /**
     * @var BoastRepository
     */
    protected $boast;
    /**
     * @var ComplaintRepository
     */
    protected $complaint;
    /**
     * @var CommentRepository
     */
    protected $comment;
    /**
     * @var NewsRepository
     */
    protected $news;
    /**
     * @var FollowerRepository
     */
    protected $follower;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->notification = new NotificationRepository();
        $this->questionAnswer = new QuestionAnswerRepository();
        $this->boast = new BoastRepository();
        $this->complaint = new ComplaintRepository();
        $this->comment = new CommentRepository();
        $this->user = new UserRepository();
        $this->news = new NewsRepository();
        $this->follower = new FollowerRepository();
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'processingModel',
        ];
    }

    /**
     *
     */
    public function processingModel()
    {
        $owner = $this->owner;
        $attributes = [
            'type' => null,
            'objectId' => null,
            'recipientId' => [],
            'entityId' => null,
        ];

        $recipients = [];

        if ($owner instanceof Comment) {
            $recipients = $this->processingModelComment($attributes);
        } elseif ($owner instanceof Vote) {
            $recipients = $this->processingModelVote($attributes);
        } elseif ($owner instanceof News) {
            $recipients = $this->processingModelNews($attributes);
        } elseif ($owner instanceof Follower) {
            $recipients = $this->processingModelFollower($attributes);
        }

        if (is_array($recipients)) {
            foreach ($recipients as $attributes) {
                $this->create($attributes);
            }
        }
    }

    /**
     * @param $attributes
     * @return mixed
     */
    protected function processingModelComment($attributes)
    {
        /**
         * @var $owner Comment
         */

        $owner = $this->owner;
        $type = null;
        $repository = null;

        $attributes['objectId'] = $owner->id;
        $attributes['entityId'] = $owner->entityId;

        $result[0] = $attributes;

        if ($owner->entityName == 'questionanswer') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_QA;
            $repository = $this->questionAnswer;
        } elseif ($owner->entityName == 'boast') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_BOAST;
            $repository = $this->boast;
        } elseif ($owner->entityName == 'complaint') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_COMPLAINT;
            $repository = $this->complaint;
        }

        if ($repository instanceof ActiveRepository) {
            // Уведомление владельца объекта
            $entity = $repository->getById($owner->entityId);

            if ($entity instanceof Comment) {
                $result[0]['recipientId'] = $entity->authorId;
            } else {
                $result[0]['recipientId'] = $entity->userId;
            }

            // Если есть рут, то мы его уведомляем об ответе
            if ($owner->rootId) {
                $result[1] = $result[0];

                if ($owner->entityName == 'questionanswer') {
                    $result[1]['type'] = Notifications::TYPE_NOTIFICATION_QA_COMMENT;
                } elseif ($owner->entityName == 'boast') {
                    $result[1]['type'] = Notifications::TYPE_NOTIFICATION_BOAST_COMMENT;
                } elseif ($owner->entityName == 'complaint') {
                    $result[1]['type'] = Notifications::TYPE_NOTIFICATION_COMPLAINT_COMMENT;
                }

                $comment = $this->comment->getById($owner->rootId);
                $result[1]['recipientId'] = $comment->authorId;
            }
        }

        return $result;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    protected function processingModelVote($attributes)
    {
        /**
         * @var $owner Vote
         */

        $owner = $this->owner;
        $type = null;
        $repository = null;

        $attributes['objectId'] = $owner->id;
        $attributes['entityId'] = $owner->entityId;

        $result[0] = $attributes;

        if ($owner->entityName == 'complaint') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_VOTE_COMPLAINT;
            $repository = $this->complaint;
        } elseif ($owner->entityName == 'comment') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_VOTE_COMMENT;
            $repository = $this->comment;
        } elseif ($owner->entityName == 'boast') {
            $result[0]['type'] = Notifications::TYPE_NOTIFICATION_VOTE_BOAST;
            $repository = $this->boast;
        }

        if ($repository instanceof ActiveRepository) {
            // Уведомление владельца объекта
            $entity = $repository->getById($owner->entityId);

            if ($entity instanceof Comment) {
                $result[0]['recipientId'] = $entity->authorId;
            } else {
                $result[0]['recipientId'] = $entity->userId;
            }
        }

        return $result;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    protected function processingModelNews($attributes)
    {
        /**
         * @var $owner News
         */
        $owner = $this->owner;

        $attributes['objectId'] = $owner->id;
        $attributes['entityId'] = $owner->id;

        $data = $attributes;
        $data['type'] = Notifications::TYPE_NOTIFICATION_NEWS;

        $allUsers = $this->user->getAll();

        $result = [];

        foreach($allUsers as $user){
            $result[] = ArrayHelper::merge($data, ['recipientId'=>$user->id]);
        }

        return $result;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    protected function processingModelFollower($attributes)
    {
        /**
         * @var $owner Follower
         */

        $owner = $this->owner;
        $type = null;
        $repository = null;

        $attributes['objectId'] = $owner->id;
        $attributes['entityId'] = $owner->id;

        $result[0] = $attributes;

        $result[0]['type'] = Notifications::TYPE_NOTIFICATION_NEW_FOLLOWER;
        $repository = $this->follower;


        if ($repository instanceof ActiveRepository) {
            // Уведомление владельца объекта
            $entity = $repository->getById($owner->id);

            $result[0]['recipientId'] = $entity->objectId;
        }

        return $result;
    }

    /**
     * @param $attributes
     * @return bool
     */
    protected function ensureOwnerObject($attributes)
    {
        return $attributes['recipientId'] != Yii::$app->user->identity->id;
    }

    /**
     * @param $attributes
     * @return bool
     */
    protected function ensureFilled($attributes)
    {
        $result = true;
        foreach ($attributes as $v) {
            if (empty($v)) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    /**
     * @param $attributes
     * @return bool
     */
    protected function ensureType($attributes)
    {
        $user = $this->user->getById($attributes['recipientId']);

        return is_array($user->notifications) ? in_array($attributes['type'], $user->notifications) : false;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function create($attributes)
    {
        if (!$this->ensureOwnerObject($attributes)) {
            return false;
        }
        if (!$this->ensureFilled($attributes)) {
            return false;
        }

        if (!$this->ensureType($attributes)) {
            return false;
        }

        $attributes = ArrayHelper::merge($attributes, []);

        $this->notification->create($attributes);
    }
}
