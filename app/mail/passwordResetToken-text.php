<?php
/* @var $user      app\db\records\User */
/* @var $message   yii\swiftmailer\Message */
/* @var $this      yii\web\View */
/* @var $email     string */
/* @var $homeUrl   string */
/* @var $resetUrl  string */

echo Yii::t('app/mails', 'password-reset-token.body-text');
echo PHP_EOL;
echo $resetUrl;