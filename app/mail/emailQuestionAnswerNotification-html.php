<?php
/* @var $user        app\db\records\User */
/* @var $message     yii\swiftmailer\Message */
/* @var $this        yii\web\View */
/* @var $email       string */
/* @var $homeUrl     string */
/* @var $profileUrl  string */
/* @var $reviewsUrl  string */
/* @var $checkoutUrl string */
?>

<div class="emailSummary" style="display: none; font-size: 0; max-height: 0; line-height: 0; padding: 0; mso-hide: all; overflow: hidden; float: none; width: 0; height: 0;">Проверьте информацию, которую вы оставляли о себе. Мы очень были бы признательны, если бы вы уточнили своё имя на странице профиля.</div>
<table id="emailBody" width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0; height: 100%; width: 100%; min-height: 1000px; background-color: #f2f2f2;">
    <tr>
        <td align="center" valign="top" class="emailBodyCell" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 32px; padding-bottom: 32px; padding-left: 16px; padding-right: 16px; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; height: 100%; width: 100%; min-height: 1000px; background-color: #f2f2f2;">
            <table width="544" border="0" cellpadding="0" cellspacing="0" class="eBox" style="margin-top: 0; margin-left: auto; margin-right: auto; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0; width: 544px;">
                <tr>
                    <td class="topCorners" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; width: 544px; height: 16px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <tr>
                                <td align="left" valign="top" class="crn_lftp" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; height: 16px; font-size: 0; width: 16px; text-align: left; vertical-align: top; background-color: #ffffff;"><img src="<?= $message->embed(Yii::getAlias('@app/mail/images/header_left.png')) ?>" alt="" width="8" height="8" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; height: 8px; width: 8px; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /></td>
                                <td class="emptyCell space16" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; line-height: 0; font-size: 0; width: 94%; height: 16px; background-color: #ffffff;">&nbsp;</td>
                                <td align="right" valign="top" class="crn_rgtp" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; height: 16px; font-size: 0; width: 16px; text-align: right; vertical-align: top; background-color: #ffffff;"><img src="<?= $message->embed(Yii::getAlias('@app/mail/images/header_right.png')) ?>" alt="" width="8" height="8" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; height: 8px; width: 8px; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="eHeader" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; width: 512px; background-color: #ffffff; border-bottom: 1px solid #ebebeb;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0;">
                            <tr>
                                <td class="alignCenter" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; text-align: center;">
                                    <a class="logo" href="<?= $homeUrl ?>" style="padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; color: #d5171d;"><img class="imageFix" src="<?= $message->embed(Yii::getAlias('@app/mail/images/logo-old.png')) ?>" width="200" height="50" alt="" style="margin-top: 0; margin-left: auto; margin-right: auto; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border: none; height: auto; width: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block;" /></a>
                                </td>
                                <!-- end .eHeaderLogo-->
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="highlight pdTp32" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 32px; padding-bottom: 0; padding-left: 16px; padding-right: 16px; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; width: 512px; text-align: center; background-color: #fafafa; border-bottom: 1px solid #ebebeb;">
                        <h1 style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 5px; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; font-size: 24px; line-height: 36px; font-weight: bold; color: #242424;">
                            <span style="color: #242424;">Ваш вопрос прокомментирован</span>
                        </h1>
                    </td>
                    <!-- end .highlight-->
                </tr>
                <tr>
                    <td class="eBody pdTp32 pdBt24" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 32px; padding-bottom: 24px; padding-left: 16px; padding-right: 16px; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; width: 512px; color: #242424; background-color: #ffffff;">
                        <p style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 24px; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; text-align: left;">
                            Перейдите на сайт чтобы прочитать ответ на Ваш вопрос. <a href="<?= $homeUrl ?>">Открыть вопрос и прочиать ответ.</a>
                        </p>

                        <p style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 24px; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 22px; text-align: left;">
                            С уважением,<br>
                            администрация ресурса.
                        </p>
                    </td>
                    <!-- end .eBody-->
                </tr>
                <tr>
                    <td class="bottomCorners" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; width: 544px; height: 16px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <tr>
                                <td align="left" valign="bottom" class="crn_lfbt" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; height: 16px; font-size: 0; width: 16px; text-align: left; vertical-align: bottom; background-color: #ffffff;"><img src="<?= $message->embed(Yii::getAlias('@app/mail/images/body_lfbt.png')) ?>" alt="" width="6" height="6" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; height: 8px; width: 8px; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /></td>
                                <td class="emptyCell" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; line-height: 0; font-size: 0; width: 94%; height: 16px; background-color: #ffffff;">&nbsp;</td>
                                <td align="right" valign="bottom" class="crn_rgbt" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; height: 16px; font-size: 0; width: 16px; text-align: right; vertical-align: bottom; background-color: #ffffff;"><img src="<?= $message->embed(Yii::getAlias('@app/mail/images/body_rgbt.png')) ?>" alt="" width="6" height="6" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; height: 8px; width: 8px; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="eFooter" style="margin-top: 0; margin-left: 0; margin-right: 0; margin-bottom: 0; padding-top: 14px; padding-bottom: 0; padding-left: 0; padding-right: 0; border-collapse: collapse; border-spacing: 0; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; text-align: center; font-size: 12px; line-height: 21px; width: 544px; color: #b2b2b2;">
                        © 2015
                        <a href="<?= $homeUrl ?>" class="highFix" style="padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; font-family: Arial, Helvetica, sans-serif; cursor: pointer; color: #b2b2b2;"><span style="text-decoration: none; cursor: pointer; color: #b2b2b2;"><?= Yii::$app->name ?></span></a>.
                        Все права защищены.
                    </td>
                </tr>
            </table>
            <!-- end .eBox -->
        </td>
        <!-- end .emailBodyCell -->
    </tr>
</table>