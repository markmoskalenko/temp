<?php
use yii\helpers\Html;

/* @var $user      app\db\records\User */
/* @var $message   yii\swiftmailer\Message */
/* @var $this      yii\web\View */
/* @var $email     string */
/* @var $homeUrl   string */
/* @var $password  string */

echo Yii::t('app/mails', 'password-delivery.body');
echo PHP_EOL;
echo Yii::t('app/mails', 'password-delivery.field-email'), ' ', Html::encode($email);
echo Yii::t('app/mails', 'password-delivery.field-text'), ' ', Html::encode($password);
