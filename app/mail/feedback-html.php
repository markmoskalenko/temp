<?php
use yii\widgets\DetailView;

/* @var $feedback app\db\records\FeedbackMessage */
/* @var $message  yii\swiftmailer\Message */
/* @var $this     yii\web\View */

echo DetailView::widget([
    'model' => $feedback,
    'attributes' => [
        'id',
        'userId',
        'userIp',
        'userAgent',
        'userName',
        'userEmail',
        'subject',
        'body:ntext',
    ],
]);
