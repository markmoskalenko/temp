<?php
use yii\helpers\Html;

/* @var $user         app\db\records\User */
/* @var $message      yii\swiftmailer\Message */
/* @var $this         yii\web\View */
/* @var $email        string */
/* @var $homeUrl      string */
/* @var $reviewsUrl   string */
/* @var $rejectReason string */
?>
Добрый день, <?= Html::encode($user->name) ?>.

К сожалению, мы не будем показывать один из ваших отзывов на нашем проекте.
Модератор отклонил его и указал следующую причину:

<?= Yii::$app->formatter->asNtext($rejectReason) ?>

Отредактировать отзыв можно на этой странице:
<?= $reviewsUrl ?>

С уважением,
администрация ресурса.
