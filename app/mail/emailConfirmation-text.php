<?php
/* @var $user            app\db\records\User */
/* @var $message         yii\swiftmailer\Message */
/* @var $this            yii\web\View */
/* @var $email           string */
/* @var $homeUrl         string */
/* @var $confirmationUrl string */

echo Yii::t('app/mails', 'email-confirmation.body-text');
echo PHP_EOL;
echo $confirmationUrl;
