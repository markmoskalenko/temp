<?php
use app\db\records\Boast;
use app\db\records\Complaint;
use app\db\records\QuestionAnswer;
use app\helpers\Url;
use yii\helpers\Html;

/* @var $entity  Boast|Complaint|QuestionAnswer */
/* @var $message yii\swiftmailer\Message */
/* @var $this    yii\web\View */

if ($entity instanceof Boast) {
    $subject = 'Добавлен хваст #' . $entity->id;
    $title = $entity->title;
    $content = $entity->text;
} elseif ($entity instanceof Complaint) {
    $subject = 'Добавлена жалоба #' . $entity->id;
    $title = $subject;
    $content = $entity->text;
} elseif ($entity instanceof QuestionAnswer) {
    $subject = 'Добавлен вопрос #' . $entity->id;
    $title = $entity->title;
    $content = $entity->question;
}

$message->setSubject($subject);
$message->setTo('feedback@alitrust.ru');
$message->setFrom(['robot@alitrust.ru' => 'Робот alitrust.ru']);

$url = Url::entity($entity, true);

echo Html::tag('h4', $title);
echo Html::tag('p', Yii::$app->formatter->asNtext($content));
echo Html::a($url);
