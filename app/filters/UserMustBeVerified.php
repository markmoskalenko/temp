<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;
use yii\web\Response;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserMustBeVerified extends ActionFilter
{
    /**
     * @param Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isEmailConfirmed) {
            return true;
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->data = ['message' => 'Вам необходимо подтвердить свою электронную почту.'];
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->statusCode = 403;
        } else {
            Yii::$app->session->setFlash('error', 'Вам необходимо подтвердить свою электронную почту. <a class="alert-link" href="http://alitrust.ru/forum/theme/409">Зачем?</a>');
            Yii::$app->response->redirect(['profile/index']);
        }

        return false;
    }
}