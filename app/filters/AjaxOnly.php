<?php
namespace app\filters;

use Yii;
use yii\base\Action;
use yii\base\ActionFilter;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AjaxOnly extends ActionFilter
{
    /**
     * @var integer
     */
    public $responseCode = 404;


    /**
     * @param Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            return true;
        }

        Yii::$app->response->statusCode = $this->responseCode;

        return false;
    }
}