<?php
namespace app\filters;

use Yii;
use yii\base\ActionFilter;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class LoginRequired extends ActionFilter
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->loginRequired();
            return false;
        }

        return parent::beforeAction($action);
    }
}