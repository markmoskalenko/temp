<?php
namespace app\senders;

use app\db\records\Complaint;
use app\db\records\UserToken;
use app\helpers\Url;
use Yii;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewRejectedSender extends BaseSender
{
    /**
     * @var Complaint
     */
    private $_review;


    /**
     * @param string  $email
     * @param integer $userId
     * @param integer $reviewId
     * @throws ImpossibleDeliveryException
     */
    public function __construct($email, $userId, $reviewId)
    {
        try {
            $this->_review = Complaint::findOrFail($reviewId);
        } catch (InvalidParamException $e) {
            throw new ImpossibleDeliveryException($e->getMessage(), $e->getCode(), $e);
        }

        parent::__construct($email, $userId);
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return Yii::t('app/mails', 'review-rejected.subject');
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return ['html' => 'reviewRejected-html', 'text-reviewRejected-text'];
    }

    /**
     * @return array
     */
    public function getViewParams()
    {
        $token = $this->getUser()->createToken(MONTH, UserToken::TYPE_EMAIL);

        return [
            'reviewsUrl' => Url::auth(['reviews/mine', 'utm_source' => 'reviewRejected', 'utm_medium' => 'button', 'utm_campaign' => 'transaction.emails'], $token, true),
            'rejectReason' => $this->getReview()->getAttribute('rejectReason'),
        ];
    }

    /**
     * @return Complaint
     */
    public function getReview()
    {
        return $this->_review;
    }
}
