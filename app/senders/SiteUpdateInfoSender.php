<?php
namespace app\senders;

use app\db\records\UserToken;
use app\helpers\Url;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SiteUpdateInfoSender extends BaseSender
{
    /**
     * @return string
     */
    public function getSubject()
    {
        return 'Новая информация о проекте';
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return 'Максим Андреевич';
    }

    /**
     * @return string
     */
    public function getSenderEmail()
    {
        return 'maxim@alitrust.ru';
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return [
            'html' => 'siteUpdateInfo-html',
            'text' => 'siteUpdateInfo-text',
        ];
    }

    /**
     * @return array
     */
    public function getViewParams()
    {
        $token = $this->getUser()->createToken(MONTH, UserToken::TYPE_EMAIL);
        $profileUrl = Url::auth(['profile/index'], $token, true);
        $reviewsUrl = Url::auth(['reviews/mine'], $token, true);
        $checkoutUrl = Url::auth('/', $token, true);
        return compact('profileUrl', 'reviewsUrl', 'checkoutUrl');
    }
}