<?php
namespace app\senders;

use app\db\records\UserToken;
use app\helpers\Url;
use Yii;
use yii\base\Exception;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PasswordSender extends BaseSender
{
    /**
     * @return string
     */
    public function getSubject()
    {
        return Yii::t('app/mails', 'password-delivery.subject', ['appName' => Yii::$app->name]);
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return [
            'html' => 'passwordDelivery-html',
            'text' => 'passwordDelivery-text',
        ];
    }

    /**
     * @throws Exception
     * @return array
     */
    public function getViewParams()
    {
        $password = Yii::$app->security->generateRandomString(10);
        $token = $this->getUser()->createToken(MONTH, UserToken::TYPE_EMAIL);

        $user = $this->getUser();
        $user->setPassword($password);
        $user->saveOrFail();

        return [
            'homeUrl' => Url::auth(['site/index', 'utm_source' => 'passwordSender', 'utm_medium' => 'button', 'utm_campaign' => 'transaction.emails'], $token, true),
            'password' => $password,
        ];
    }
}
