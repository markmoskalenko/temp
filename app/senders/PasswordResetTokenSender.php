<?php
namespace app\senders;

use app\db\records\User;
use Yii;
use yii\helpers\Url;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PasswordResetTokenSender extends BaseSender
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $user = $this->getUser();
        $token = $user->passwordResetToken;

        if ( ! $token || User::isPasswordTokenExpired($token)) {
            $user->generatePasswordResetToken();
            $user->saveOrFail();
        }
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return Yii::t('app/mails', 'password-reset-token.subject', ['appName' => Yii::$app->name]);
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return [
            'html' => 'passwordResetToken-html',
            'text' => 'passwordResetToken-text',
        ];
    }

    /**
     * @return array
     */
    public function getViewParams()
    {
        $user = $this->getUser();

        return [
            'resetUrl' => Url::to(['auth/reset-password', 'token' => $user->passwordResetToken, 'utm_source' => 'passwordReset', 'utm_medium' => 'button', 'utm_campaign' => 'transaction.emails'], true),
        ];
    }
}