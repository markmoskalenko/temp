<?php
namespace app\senders;

use app\db\records\User;
use Yii;
use yii\helpers\Url;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class EmailConfirmationSender extends BaseSender
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $user = $this->getUser();
        $token = $user->emailConfirmationToken;

        if ( ! $token || User::isEmailTokenExpired($token)) {
            $user->generateEmailConfirmationToken();
            $user->saveOrFail();
        }
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return Yii::t('app/mails', 'email-confirmation.subject', ['appName' => Yii::$app->name]);
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return [
            'html' => 'emailConfirmation-html',
            'text' => 'emailConfirmation-text',
        ];
    }

    /**
     * @return array
     */
    public function getViewParams()
    {
        $user = $this->getUser();

        return [
            'confirmationUrl' => Url::to(['profile/email-confirmation', 'token' => $user->emailConfirmationToken, 'utm_source' => 'emailConfirmation', 'utm_medium' => 'button', 'utm_campaign' => 'transaction.emails'], true),
        ];
    }
}