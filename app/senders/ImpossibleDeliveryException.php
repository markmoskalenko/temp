<?php
namespace app\senders;

use yii\base\Exception;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ImpossibleDeliveryException extends Exception
{
}