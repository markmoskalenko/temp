<?php
namespace app\senders;

use app\db\records\Mail;
use yii\base\Exception;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SenderFactory
{
    /**
     * @param Mail $record
     * @throws Exception
     * @return BaseSender
     */
    public static function create(Mail $record)
    {
        if ($record->type === Mail::TYPE_EMAIL_CONFIRMATION) {
            return new EmailConfirmationSender($record->email, $record->userId);
        } elseif ($record->type === Mail::TYPE_PASSWORD_RESET_TOKEN_REQUEST) {
            return new PasswordResetTokenSender($record->email, $record->userId);
        } elseif ($record->type === Mail::TYPE_PASSWORD_DELIVERY) {
            return new PasswordSender($record->email, $record->userId);
        } elseif ($record->type === Mail::TYPE_SITE_UPDATE_INFO) {
            return new SiteUpdateInfoSender($record->email, $record->userId);
        } elseif ($record->type === Mail::TYPE_REVIEW_REJECTED) {
            return new ReviewRejectedSender($record->email, $record->userId, $record->reviewId);
        } elseif ($record->type === Mail::TYPE_QUESTION_ANSWER_NOTIFICATION) {
                return new QuestionAnswerSender($record->email, $record->userId, $record->reviewId);
        } else {
            throw new Exception(sprintf('Mail #%d with type %s has no sender', $record->id, $record->type));
        }
    }
}