<?php
namespace app\senders;

use app\db\records\User;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Object;
use yii\helpers\Url;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseSender extends Object
{
    /**
     * @var string
     */
    private $_email;
    /**
     * @var User
     */
    private $_user;

    /**
     * @param string  $email
     * @param integer $userId
     * @throws ImpossibleDeliveryException
     */
    public function __construct($email, $userId)
    {
        try {
            $this->_email = $email;
            $this->_user = User::findOrFail($userId);
        } catch (InvalidParamException $e) {
            throw new ImpossibleDeliveryException($e->getMessage(), $e->getCode(), $e);
        }

        parent::__construct();
    }

    /**
     * @return string
     */
    abstract public function getSubject();

    /**
     * @return array|string
     */
    abstract public function getView();

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @return array|string
     */
    public function getSender()
    {
        return [$this->getSenderEmail() => $this->getSenderName()];
    }

    /**
     * @return string
     */
    public function getSenderEmail()
    {
        return Yii::$app->polyglot->getSupportEmail();
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return Yii::$app->name;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return array
     */
    public function getViewParams()
    {
        return [];
    }

    /**
     * @throws ImpossibleDeliveryException
     * @return boolean
     */
    public function send()
    {
        $params = array_merge([
            'email' => $this->getEmail(),
            'user' => $this->getUser(),
            'homeUrl' => Url::to('/', true),
        ], $this->getViewParams());

        try {
            return Yii::$app->mailer->compose($this->getView(), $params)
                ->setTo($this->getEmail())
                ->setSubject($this->getSubject())
                ->setFrom($this->getSender())
                ->send();
        } catch (\Swift_RfcComplianceException $e) {
            throw new ImpossibleDeliveryException($e->getMessage(), $e->getCode(), $e);
        }
    }
}