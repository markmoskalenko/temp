<?php
namespace app\senders;

use app\db\records\QuestionAnswer;
use app\db\records\UserToken;
use app\helpers\Url;
use app\repositories\QuestionAnswerRepository;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class QuestionAnswerSender extends BaseSender
{
    /**
     * @var
     */
    private $_questionAnswer;

    /**
     * @return string
     */
    public function getSubject()
    {
        return 'Получен ответ на Ваш вопрос';
    }

    /**
     * @return array|string
     */
    public function getView()
    {
        return [
            'html' => 'emailQuestionAnswerNotification-html',
            'text' => 'emailQuestionAnswerNotification-text',
        ];
    }

    /**
     * @param string  $email
     * @param integer $userId
     * @param integer $questionId
     * @throws ImpossibleDeliveryException
     */
    public function __construct($email, $userId, $questionId)
    {
        try {
            $this->_questionAnswer = QuestionAnswer::findOrFail($questionId);
        } catch (InvalidParamException $e) {
            throw new ImpossibleDeliveryException($e->getMessage(), $e->getCode(), $e);
        }

        parent::__construct($email, $userId);
    }

    /**
     * @throws Exception
     * @return array
     */
    public function getViewParams()
    {
        $url = Url::toRoute(['question-answer/view', 'id'=>$this->_questionAnswer->id, 'slug'=>$this->_questionAnswer->slug], true);

        return [
            'homeUrl' => $url
        ];
    }
}
