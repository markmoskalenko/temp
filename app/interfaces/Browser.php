<?php
namespace app\interfaces;

/**
 * Этот интерфейс представляет собой классы, которые скачивают контент со страниц.
 * Основная задача — скачать и вернуть HTML код.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
interface Browser
{
    /**
     * @return string
     */
    public function getHtml();
}