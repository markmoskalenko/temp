<?php
namespace app\interfaces;

/**
 * Interface for a model, that represents uploaded file.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
interface UploadableModel
{
    /**
     * @return string
     */
    public function getFileName();

    /**
     * @return integer
     */
    public function getWidth();

    /**
     * @return integer
     */
    public function getHeight();
}