<?php

namespace app\presenters;

use app\db\records\TrackingNumber;
use yii\helpers\ArrayHelper;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberPresenter extends BasePresenter
{
    /**
     * @var TrackingNumber 
     */
    public $owner;
}
