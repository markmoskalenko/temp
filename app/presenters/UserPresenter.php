<?php
namespace app\presenters;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserPresenter extends BasePresenter
{
    /**
     * @return string
     */
    public function avatarUrl()
    {
        return $this->owner->avatarUrl ?: '/images/avatar.png';
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->owner->name ?: 'Аноним';
    }

    /**
     * @return string
     */
    public function forumCommentCount()
    {
        /* @var $repository \app\modules\forum\repositories\CommentRepository */
        $repository = \Yii::createObject('app\modules\forum\repositories\CommentRepository');
        return $repository->getCountByUser($this->owner->id);
    }
}