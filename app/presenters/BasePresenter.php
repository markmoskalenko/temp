<?php
namespace app\presenters;

use yii\base\Object;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BasePresenter extends Object
{
    /**
     * @var object owner of this presenter.
     */
    public $owner;
}