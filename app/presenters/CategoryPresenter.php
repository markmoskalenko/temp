<?php
namespace app\presenters;

use app\db\records\Category;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoryPresenter extends BasePresenter
{
    /**
     * @var Category
     */
    public $owner;


    /**
     * Получает путь из имён родительских категорий.
     * Например, для категории "Верхняя одежда", такой путь будет "Мужчинам → Одежда для мужчин → Верхняя одежда".
     * Имя текущей категории включено.
     *
     * @return string
     */
    public function namesPath()
    {
        $parentNames = ArrayHelper::getColumn($this->owner->parents()->orderLikeTree()->all(), 'name');
        $parentNames[] = $this->owner->name;
        return implode(' → ', $parentNames);
    }
}