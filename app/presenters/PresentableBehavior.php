<?php
namespace app\presenters;

use Yii;
use yii\base\Behavior;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PresentableBehavior extends Behavior
{
    /**
     * @var string presenter class name.
     */
    public $presenter;

    /**
     * @var BasePresenter cached presenter instance.
     */
    protected $instance;


    /**
     * @return BasePresenter
     */
    public function present()
    {
        if ($this->instance === null) {
            $this->instance = Yii::createObject(['class' => $this->presenter, 'owner' => $this->owner]);
        }

        return $this->instance;
    }
}