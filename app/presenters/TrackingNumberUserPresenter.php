<?php

namespace app\presenters;

use app\db\records\TrackingNumberUser;
use yii\helpers\ArrayHelper;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberUserPresenter extends BasePresenter
{
    /**
     * @var TrackingNumberUser 
     */
    public $owner;
}
