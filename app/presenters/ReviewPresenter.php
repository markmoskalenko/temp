<?php
namespace app\presenters;

use app\helpers\BBCodeParser;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewPresenter extends BasePresenter
{
    /**
     * @return string
     */
    public function content()
    {
        $content = $this->owner->content;
        $content = BBCodeParser::process($content);

        return $content;
    }

    /**
     * @return string
     */
    public function contentPreview()
    {
        $content = $this->owner->content;
        $content = explode('[cut]', $content)[0];
        $content = BBCodeParser::process($content);
        $content = strip_tags($content);
        $content = trim($content);
        $content = nl2br($content);

        return $content;
    }

    /**
     * @return array
     */
    public function contentImages()
    {
        preg_match_all('#\[img\]([\w\/.-]+)\[/img\]#s', $this->owner->content, $matches);
        return $matches[1];
    }

    /**
     * @return string
     */
    public function firstImage()
    {
        return ArrayHelper::getValue($this->contentImages(), 0);
    }

    /**
     * @return boolean
     */
    public function contentStartsWithImage()
    {
        return StringHelper::startsWith($this->owner->content, '[img]');
    }

    /**
     * @return string
     */
    public function timeCreated()
    {
        return Yii::$app->formatter->asDate($this->owner->timeCreated, 'long');
    }

    /**
     * @return integer
     */
    public function commentsCount()
    {
        $repository = Yii::createObject('app\repositories\CommentRepository');
        return $repository->count($this->owner);
    }

    /**
     * @return integer
     */
    public function votesCount()
    {
        $repository = Yii::createObject('app\repositories\VoteRepository');
        return $repository->count($this->owner);
    }
}
