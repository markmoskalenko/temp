<?php
namespace app\presenters;

use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastPresenter extends BasePresenter
{
    /**
     * @return integer
     */
    public function commentsCount()
    {
        $repository = Yii::createObject('app\repositories\CommentRepository');
        return $repository->count($this->owner);
    }

    /**
     * @return integer
     */
    public function votesCount()
    {
        $repository = Yii::createObject('app\repositories\VoteRepository');
        return $repository->count($this->owner);
    }
}
