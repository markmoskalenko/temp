<?php
namespace app\presenters;

use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class QuestionAnswerPresenter extends BasePresenter
{
    /**
     * @return string
     */
    public function userName()
    {
        return ($this->owner->userId)
            ? $this->owner->user->name
            : $this->owner->firstName;
    }

    /**
     * @return string
     */
    public function userAvatarUrl()
    {
        return ($this->owner->userId)
            ? $this->owner->user->present()->avatarUrl()
            : '/images/avatar.png';
    }

    /**
     * @return string
     */
    public function timeCreated()
    {
        return Yii::$app->formatter->asDate($this->owner->timeCreated, 'long');
    }

    /**
     * @return string
     */
    public function commentCount()
    {
        return Yii::$app->i18n->format(
            '{n, plural, =0{нет ответов} one{# ответ} few{# ответа} many{# ответов} other{# ответа}}',
            ['n' => $this->owner->commentCount],
            Yii::$app->language
        );
    }
}