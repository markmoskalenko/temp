<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\LatestNewsBehavior;
use app\behaviors\VoteableBehavior;
use app\db\ActiveRecord;
use app\db\queries\ProductQuery;
use app\db\queries\ComplaintPhotoQuery;
use app\db\queries\ComplaintQuery;
use app\db\queries\SellerQuery;
use app\db\queries\UserQuery;
use app\helpers\Url;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $sellerId
 * @property integer $productId
 * @property string  $link
 * @property string  $text
 * @property string  $originalText
 * @property string  $mood
 * @property string  $moderated
 * @property string  $rejected
 * @property string  $rejectReason
 * @property integer $viewCount
 * @property integer $isFeatured
 * @property string  $timeCreated
 * @property string  $timeUpdated
 *
 * @property Product          $product
 * @property ComplaintPhoto[] $photos
 * @property Seller           $seller
 * @property User             $user
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin CommentableBehavior
 * @mixin VoteableBehavior
 */
class Complaint extends ActiveRecord
{
    const MOOD_NEUTRAL = 'neutral';
    const MOOD_POSITIVE = 'positive';
    const MOOD_NEGATIVE = 'negative';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'complaint';
    }

    /**
     * @return ComplaintQuery
     */
    public static function find()
    {
        return new ComplaintQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated', 'timeUpdated'],
                ],
            ],
            'vote' => [
                'class' => VoteableBehavior::className(),
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
            'latestNews' => [
                'class' => LatestNewsBehavior::className(),
                'title' => function ($owner) {
                    $user = $owner->user;

                    return Html::a($user->name, Url::entity($user));
                },
                'text' => function ($owner) {
                    $seller = $owner->seller;

                    return 'Жалоба на магазин ' . Html::a($seller->storeName, Url::entity($owner));
                }
            ],
        ];
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ['sellerId', 'productId', 'link', 'text'];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'link' => Yii::t('app/models', 'complaint.link'),
            'text' => Yii::t('app/models', 'complaint.text'),
        ];
    }

    /**
     * @return array
     */
    public function moodLabels()
    {
        return [
            static::MOOD_NEUTRAL => 'Нейтральный',
            static::MOOD_POSITIVE => 'Положительный',
            static::MOOD_NEGATIVE => 'Отрицательный',
        ];
    }

    /**
     * @return string
     */
    public function getFormattedText()
    {
        return Yii::$app->formatter->asNtext($this->text);
    }

    /**
     * Marks review as approved
     */
    public function approve()
    {
        $this->moderated = 1;
        $this->rejected = 0;
    }

    /**
     * @param string $reason
     */
    public function reject($reason = null)
    {
        $this->moderated = 1;
        $this->rejected = 1;
        $this->rejectReason = $reason;
    }

    /**
     * @return SellerQuery
     */
    public function getSeller()
    {
        return $this->hasOne(Seller::className(), ['id' => 'sellerId']);
    }

    /**
     * @return ComplaintPhotoQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(ComplaintPhoto::className(), ['complaintId' => 'id'])
            ->inverseOf('complaint');
    }

    /**
     * @return ProductQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}