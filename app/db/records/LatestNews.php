<?php
namespace app\db\records;

use yii\behaviors\TimestampBehavior;
use yii\redis\ActiveRecord;

/**
 * @property integer $id
 * @property string  $title
 * @property string  $text
 * @property string  $type
 * @property string  $timeCreated
 */
class LatestNews extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'id',
            'title',
            'text',
            'timeCreated',
            'type'
        ];
    }
}