<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\ComplaintQuery;
use app\db\queries\SocialProfileQuery;
use app\db\queries\UserQuery;
use app\db\queries\UserTokenQuery;
use app\presenters\PresentableBehavior;
use app\presenters\UserPresenter;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * @property integer $id
 * @property string  $group
 * @property string  $email
 * @property string  $name
 * @property string  $city
 * @property string  $avatarUrl
 * @property string  $authKey
 * @property string  $passwordHash
 * @property string  $passwordResetToken
 * @property integer $emailConfirmation
 * @property string  $emailConfirmationToken
 * @property string  $status
 * @property string  $timeCreated
 *
 * @property SocialProfile[] $socialProfiles
 * @property integer $countComplaint
 * @property integer $countBoast
 * @property integer $countComment
 * @property Complaint[]        $reviews
 * @property UserToken[]     $tokens
 *
 * @method UserPresenter present()
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin TimestampBehavior
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $notifications = [];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @param integer $id
     * @return User|null
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['id' => $id])->active()->one();
    }

    /**
     * @param string $token
     * @param mixed $type
     * @return User|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /* @var $token UserToken|null */
        if ($token = UserToken::findValid($token, $type)) {
            /* @var $user User */
            $user = $token->getUser()->active()->one();

            if ($token->type == UserToken::TYPE_EMAIL && !$user->emailConfirmation) {
                $user->setEmailConfirmed();
                $user->saveOrFail();
            }

            return $user;
        }

        return null;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'presenter' => [
                'class' => PresentableBehavior::className(),
                'presenter' => UserPresenter::className(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/models', 'user.email'),
            'name' => Yii::t('app/models', 'user.name'),
            'city' => Yii::t('app/models', 'user.city'),
            'password' => Yii::t('app/models', 'user.password'),
            'notifications' => 'Уведомления',
        ];
    }

    /**
     * @param string $token
     * @return boolean
     */
    public static function isEmailTokenExpired($token)
    {
        $duration = Yii::$app->params['user.emailConfirmationTokenDuration'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $duration < time();
    }

    /**
     * @param string $token
     * @return boolean
     */
    public static function isPasswordTokenExpired($token)
    {
        $duration = Yii::$app->params['user.passwordResetTokenDuration'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $duration < time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Real password, that was just set
     * @var string
     */
    protected $password;

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (!$this->passwordHash) {
            return false;
        }

        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates new email confirmation token
     */
    public function generateEmailConfirmationToken()
    {
        $this->emailConfirmationToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Marks that user has a confirmed email.
     *
     * @param boolean $confirmed
     */
    public function setEmailConfirmed($confirmed = true)
    {
        $this->emailConfirmation = ($confirmed) ? 1 : 0;
        $this->emailConfirmationToken = null;
    }

    /**
     * @return SocialProfileQuery
     */
    public function getSocialProfiles()
    {
        return $this->hasMany(SocialProfile::className(), ['userId' => 'id'])
            ->inverseOf('user');
    }

    /**
     * @return ComplaintQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Complaint::className(), ['userId' => 'id'])
            ->inverseOf('user');
    }

    /**
     * @return UserTokenQuery
     */
    public function getTokens()
    {
        return $this->hasMany(UserToken::className(), ['userId' => 'id']);
    }

    /**
     * @param integer $lifetime
     * @param string $type
     * @return UserToken
     */
    public function createToken($lifetime, $type = null)
    {
        $token = new UserToken();
        $token->userId = $this->id;
        $token->value = Yii::$app->security->generateRandomString();
        $token->type = $type;
        $token->timeExpired = date('Y-m-d H:i:s', time() + $lifetime);
        $token->saveOrFail();

        return $token;
    }

    /**
     *
     */
    public function afterFind()
    {
        $this->notifications = Json::decode($this->notificationsJson);
    }

    /**
     * @return int|string
     */
    public function getCountComplaint()
    {
        return $this->hasMany(Complaint::className(), ['userId' => 'id'])->andWhere(['rejected' => 0])->count();
    }

    /**
     * @return int|string
     */
    public function getCountBoast()
    {
        return $this->hasMany(Boast::className(), ['userId' => 'id'])->andWhere(['isDeleted' => 0])->count();
    }

    /**
     * @return int|string
     */
    public function getCountComment()
    {
        return $this->hasMany(Comment::className(), ['authorId' => 'id'])->andWhere(['isDeleted' => 0])->count();
    }

    /**
     * @return int|string
     */
    public function getIsSubscribe()
    {
        /* @var $repository \app\repositories\FollowerRepository */
        $repository = Yii::createObject('app\repositories\FollowerRepository');
        return $repository->getIsSubscribe(Yii::$app->user->identity->id, $this->id);
    }
}