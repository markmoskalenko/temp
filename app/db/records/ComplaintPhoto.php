<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\ComplaintPhotoQuery;
use app\db\queries\ComplaintQuery;
use app\interfaces\UploadableModel;
use Yii;

/**
 * @property integer $id
 * @property integer $complaintId
 * @property string  $storage
 * @property string  $path
 * @property string  $rejected
 * @property string  $timeCreated
 *
 * @property Complaint  $complaint
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintPhoto extends ActiveRecord implements UploadableModel
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'complaint_photo';
    }

    /**
     * @return ComplaintPhotoQuery
     */
    public static function find()
    {
        return new ComplaintPhotoQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->path;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->path);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->path);
        $size = getimagesize($fullpath);
        return $size[1];
    }

    /**
     * @return ComplaintQuery
     */
    public function getComplaint()
    {
        return $this->hasOne(Complaint::className(), ['id' => 'complaintId']);
    }
}