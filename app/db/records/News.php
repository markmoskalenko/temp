<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\ContentPreviewBehavior;
use app\behaviors\LatestNewsBehavior;
use app\behaviors\NotificationBehavior;
use app\db\ActiveRecord;
use app\db\queries\ArticleCategoryQuery;
use app\db\queries\PublicationImageQuery;
use app\db\queries\NewsQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property integer $categoryId
 * @property string  $language
 * @property integer $imageId
 * @property string  $title
 * @property string  $slug
 * @property string  $content
 * @property string  $contentPreview
 * @property string  $seoTitle
 * @property string  $seoDescription
 * @property integer $viewCount
 * @property integer $isFeatured
 * @property string  $timeCreated
 * @property string  $timePublished
 *
 * @property NewsCategory     $category
 * @property PublicationImage $mainImage
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin ContentPreviewBehavior
 * @mixin TimestampBehavior
 * @mixin CommentableBehavior
 */
class News extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return NewsQuery
     */
    public static function find()
    {
        return new NewsQuery(__CLASS__);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'preview' => [
                'class' => ContentPreviewBehavior::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'latestNews' => [
                'class' => LatestNewsBehavior::className(),
                'title' => function ($owner) {
                    return 'Администрация';
                },
                'text' => function ($owner) {
                    return Html::a($owner->title, \app\helpers\Url::entity($owner));
                }
            ],
            'notification' => [
                'class' => NotificationBehavior::className()
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'integer'],
            ['categoryId', 'required'],

            ['imageId', 'required', 'except' => static::SCENARIO_DEFAULT],

            ['title', 'string'],
            ['title', 'trim'],
            ['title', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],

            ['contentPreview', 'string'],
            ['contentPreview', 'trim'],
            ['contentPreview', 'default'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],

            ['timePublished', 'string'],
            ['timePublished', 'trim'],
            ['timePublished', 'required'],

            ['isFeatured', 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'language' => 'Язык',
            'imageId' => 'Основное изображение',
            'title' => 'Название',
            'content' => 'Текст статьи',
            'contentPreview' => 'Краткое описание',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
            'timePublished' => 'Время публикации',
            'isFeatured' => 'Закрепить новость',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'categoryId']);
    }

    /**
     * @return PublicationImageQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(PublicationImage::className(), ['id' => 'imageId']);
    }
}