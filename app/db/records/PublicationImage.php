<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\PublicationImageQuery;
use app\interfaces\UploadableModel;
use app\rest\ArrayableTrait;
use app\rest\FormatterBehavior;
use app\rest\ImperaviRedactorFormatter;
use Yii;

/**
 * @property integer $id
 * @property string  $path
 * @property string  $timeCreated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class PublicationImage extends ActiveRecord implements UploadableModel
{
    use ArrayableTrait;

    /**
     * @return array
     */
    public static function tableName()
    {
        return 'publication_image';
    }

    /**
     * @return PublicationImageQuery
     */
    public static function find()
    {
        return new PublicationImageQuery(get_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'formatter' => [
                'class' => FormatterBehavior::className(),
                'formatter' => ImperaviRedactorFormatter::className(),
            ],
        ];
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->path;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[1];
    }
}