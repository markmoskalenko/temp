<?php
namespace app\db\records;

use app\db\queries\CategoryQuery;
use app\presenters\CategoryPresenter;
use app\presenters\PresentableBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string  $name
 * @property string  $synonyms
 * @property string  $slug
 * @property string  $seoTitle
 * @property string  $seoDescription
 *
 * @method CategoryPresenter present()
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin NestedSetsBehavior
 */
class Category extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
            ],
            'present' => [
                'class' => PresentableBehavior::className(),
                'presenter' => CategoryPresenter::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * Получение родительская категории через стандартный механизм рилейшенов.
     * Для новой записи возвращяется [[ActiveQuery]] с заведомо несуществующим ID.
     *
     * @return CategoryQuery
     */
    public function getParent()
    {
        if ($this->isNewRecord) {
            $query = static::find()->where(['id' => 0]);
        } else {
            $query = $this->parents(1);
        }

        $query->multiple = false;
        return $query;
    }
}