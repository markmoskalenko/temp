<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\UserQuery;
use app\db\queries\MailQuery;
use Yii;
use yii\base\ModelEvent;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property integer $type
 * @property string  $language
 * @property string  $email
 * @property integer $userId
 * @property integer $reviewId
 * @property integer $priority
 * @property string  $error
 * @property string  $timeCreated
 * @property string  $timeSendFailed
 * @property string  $timeSendSucceed
 *
 * @property User   $user
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin TimestampBehavior
 */
class Mail extends ActiveRecord
{
    const TYPE_EMAIL_CONFIRMATION = 'emailConfirmation';
    const TYPE_PASSWORD_RESET_TOKEN_REQUEST = 'passwordResetTokenRequest';
    const TYPE_PASSWORD_DELIVERY = 'passwordDelivery';
    const TYPE_SITE_UPDATE_INFO = 'siteUpdateInfo';
    const TYPE_REVIEW_REJECTED = 'reviewRejected';
    const TYPE_QUESTION_ANSWER_NOTIFICATION = 'emailQuestionAnswerNotification';

    /**
     * @return array
     */
    public static function tableName()
    {
        return 'mail';
    }

    /**
     * @return MailQuery
     */
    public static function find()
    {
        return new MailQuery(__CLASS__);
    }

    /**
     * Puts mail to mailer queue.
     *
     * @param string $type
     * @param User   $user
     * @param $review
     *
     * @return boolean
     */
    public static function queue($type, $user, $review = null)
    {
        $mail = new Mail();
        $mail->type = $type;
        $mail->language = Yii::$app->polyglot->normalizeLanguage(Yii::$app->language);
        $mail->email = $user->email;
        $mail->userId = $user->id;
        $mail->reviewId = ($review) ? $review->id : null;
        return $mail->save();
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
            'priority' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'priority',
                ],
                'value' => function (ModelEvent $event) {
                    /* @var Mail $mail */
                    $mail = $event->sender;
                    return ArrayHelper::getValue($mail->priorities(), $mail->type, 0);
                },
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['type', 'required'],

            ['language', 'string'],
            ['language', 'trim'],
            ['language', 'required'],
            ['language', 'in', 'range' => Yii::$app->polyglot->getAllowed()],

            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],

            ['userId', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'language' => 'Language',
            'email' => 'Email',
            'userId' => 'User',
            'timeCreated' => 'Time Created',
            'timeSendFailed' => 'Time Send Failed',
            'timeSendSucceed' => 'Time Send Succeed',
        ];
    }

    /**
     * @return array
     */
    public function priorities()
    {
        return [
            static::TYPE_PASSWORD_RESET_TOKEN_REQUEST => 2,
            static::TYPE_EMAIL_CONFIRMATION => 1,
        ];
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
