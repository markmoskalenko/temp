<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\SocialProfileQuery;
use app\db\queries\UserQuery;

/**
 * @property integer $id
 * @property integer $userId
 * @property string  $socialNetwork
 * @property string  $socialId
 * @property string  $accessToken
 * @property string  $userName
 * @property string  $userEmail
 * @property string  $userAvatarUrl
 * @property string  $userAttributesStorage
 * @property string  $timeCreated
 *
 * @property User $user
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class SocialProfile extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_social';
    }

    /**
     * @return SocialProfileQuery
     */
    public static function find()
    {
        return new SocialProfileQuery(get_called_class());
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        if (is_scalar($user)) {
            $user = User::findOne($user);
        }

        if ($user instanceof User) {
            $this->userId = $user->id;
            $this->populateRelation('user', $user);
        } else {
            $this->userId = null;
            $this->populateRelation('user', null);
        }
    }

    /**
     * @return boolean
     */
    public function hasUser()
    {
        return (bool) $this->userId;
    }

    /**
     * @return boolean
     */
    public function updateUserInfo()
    {
        $user = $this->user;

        $user->name = $this->userName;
        $user->email = $this->userEmail ?: $user->email;
        $user->avatarUrl = $this->userAvatarUrl;

        return $user->save();
    }

    /**
     * @return array
     */
    public function getUserAttributes()
    {
        return json_decode($this->userAttributesStorage);
    }

    /**
     * @param array $data
     */
    public function setUserAttributes($data)
    {
        $this->userAttributesStorage = json_encode($data);
    }
}