<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\ArticleCategoryQuery;
use app\db\queries\ArticleQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Url;

/**
 * @property integer $id
 * @property string  $language
 * @property string  $name
 * @property string  $slug
 * @property string  $content
 * @property string  $seoTitle
 * @property string  $seoDescription
 *
 * @property string  $url
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticleCategory extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'article_category';
    }

    /**
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return new ArticleCategoryQuery(get_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['language', 'string'],
            ['language', 'trim'],
            ['language', 'required'],
            ['language', 'filter', 'filter' => [Yii::$app->polyglot, 'normalizeLanguage']],

            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'language' => 'Язык',
            'content' => 'Контент',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
        ];
    }

    /**
     * @param boolean|string $scheme
     * @return string
     */
    public function getUrl($scheme = false)
    {
        return Url::to(['articles/category', 'slug' => $this->slug], $scheme);
    }

    /**
     * @return ArticleQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['categoryId' => 'id']);
    }
}