<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\VoteableBehavior;
use app\db\queries\CategoryQuery;
use app\db\queries\ReviewQuery;
use app\db\queries\UserQuery;
use app\presenters\PresentableBehavior;
use app\presenters\ReviewPresenter;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $categoryId
 * @property string  $externalUrl
 * @property integer $storeId
 * @property integer $productId
 * @property integer $snapshotId
 * @property string  $title
 * @property string  $slug
 * @property string  $content
 * @property integer $viewCount
 * @property integer $visible
 * @property integer $isFeatured
 * @property integer $isRejected
 * @property string  $rejectReason
 * @property string  $timeCreated
 *
 * @property Category $category
 * @property User     $user
 *
 * @method ReviewPresenter present()
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin CommentableBehavior
 * @mixin VoteableBehavior
 */
class Review extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @return ReviewQuery
     */
    public static function find()
    {
        return new ReviewQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'present' => [
                'class' => PresentableBehavior::className(),
                'presenter' => ReviewPresenter::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => false,
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
            'votes' => [
                'class' => VoteableBehavior::className(),
            ],
        ];
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ['categoryId', 'externalUrl', 'title', 'content', 'slug', 'seoTitle', 'seoDescription'];
    }

    /**
     * @return CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}