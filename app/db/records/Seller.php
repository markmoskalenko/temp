<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\behaviors\SellerRatingBehavior;
use app\db\queries\ProductQuery;
use app\db\queries\ComplaintQuery;
use app\db\queries\SellerQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property integer $id
 * @property integer $originalStoreId
 * @property integer $originalSellerId
 * @property string  $storeName
 * @property string  $sellerName
 * @property string  $slug
 * @property integer $closed
 * @property integer $searchCount
 * @property integer $score
 * @property string  $scoreIcon
 * @property integer $likesCount
 * @property integer $saleItemsCount
 * @property string  $registerDate
 * @property string  $country
 * @property string  $region
 * @property string  $city
 * @property string  $zipCode
 * @property string  $timeCreated
 * @property string  $timeUpdated
 * @property string  $timeReplicationStarted
 * @property string  $timeReplicationSucceed

 * @property Product[] $products
 * @property Complaint[]  $complaints

 * @author Artem Belov <razor2909@gmail.com>
 * @mixin SellerRatingBehavior
 * @mixin TimestampBehavior
 */
class Seller extends ActiveRecord
{
    const TIME_CREATED = 'timeCreated';
    const TIME_UPDATED = 'timeUpdated';
    const TIME_REPLICATION_STARTED = 'timeReplicationStarted';
    const TIME_REPLICATION_SUCCEED = 'timeReplicationSucceed';

    const CATEGORY_ITEM_AS_DESCRIBED = 'itAsDesc';
    const CATEGORY_COMMUNICATION = 'comm';
    const CATEGORY_SHIPPING_SPEED = 'shipSpd';

    const LABEL_VALUE = 'ratVal';
    const LABEL_REVIEW_COUNT = 'revCount';
    const LABEL_COMPARING = 'ratComp';

    const HISTORY_POSITIVE = 'histPos';
    const HISTORY_NEUTRAL = 'histNeu';
    const HISTORY_NEGATIVE = 'histNeg';
    const HISTORY_POSITIVE_RATE = 'histPosRate';

    const PERIOD_MONTH = 1;
    const PERIOD_3_MONTHS = 3;
    const PERIOD_6_MONTHS = 6;
    const PERIOD_12_MONTHS = 12;
    const PERIOD_OVERALL = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seller';
    }

    /**
     * @inheritdoc
     * @return SellerQuery
     */
    public static function find()
    {
        return new SellerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'rating' => [
                'class' => SellerRatingBehavior::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'storeName',
                'ensureUnique' => false,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => static::TIME_CREATED,
                'updatedAtAttribute' => static::TIME_UPDATED,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['searchCount', 'default', 'value' => 0],
            ['likesCount', 'default', 'value' => 0],
            ['saleItemsCount', 'default', 'value' => 0],
        ];
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        $segments = array_filter([
            $this->zipCode,
            $this->country,
            $this->region,
            $this->city,
        ]);

        return implode(', ', $segments);
    }

    /**
     * @return float
     */
    public function getSummaryRating()
    {
        $ratings = [
            $this->getDetailed(static::CATEGORY_ITEM_AS_DESCRIBED, static::LABEL_VALUE),
            $this->getDetailed(static::CATEGORY_COMMUNICATION, static::LABEL_VALUE),
            $this->getDetailed(static::CATEGORY_SHIPPING_SPEED, static::LABEL_VALUE),
        ];

        return array_sum($ratings) / count($ratings);
    }

    /**
     * @param boolean $value
     */
    public function setClosed($value = true)
    {
        $this->closed = ($value) ? 1 : 0;
    }

    /**
     * @return string
     */
    public function getOriginalStoreUrl()
    {
        return 'http://aliexpress.com/store/' . $this->originalStoreId;
    }

    /**
     * @return string
     */
    public function getOriginalStoreSaleUrl()
    {
        return 'http://ru.aliexpress.com/store/sale-items/' . $this->originalStoreId . '.html#node-gallery';
    }

    /**
     * @return ProductQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['sellerId' => 'id'])
            ->inverseOf('seller');
    }

    /**
     * @return ComplaintQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaint::className(), ['sellerId' => 'id'])
            ->inverseOf('seller');
    }

    /**
     * @return integer
     */
    public function getComplaintsCount()
    {
        return $this->getComplaints()->visible()->count();
    }
}