<?php
namespace app\db\records;

use app\helpers\Url;
use app\repositories\ArticleRepository;
use app\repositories\FollowerRepository;
use app\repositories\NewsRepository;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\repositories\BoastRepository;
use app\repositories\CommentRepository;
use app\repositories\ComplaintRepository;
use app\repositories\NotificationRepository;
use app\db\queries\UserQuery;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * Class Notifications
 */
class Notifications extends \yii\redis\ActiveRecord
{
    /**
     * @var NotificationRepository
     */
    protected $notification;
    /**
     * @var BoastRepository
     */
    protected $boast;
    /**
     * @var ComplaintRepository
     */
    protected $complaint;
    /**
     * @var ArticleRepository
     */
    protected $article;
    /**
     * @var CommentRepository
     */
    protected $comment;
    /**
     * @var NewsRepository
     */
    protected $news;
    /**
     * @var FollowerRepository
     */
    protected $follower;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->notification = new NotificationRepository();
        $this->boast = new BoastRepository();
        $this->complaint = new ComplaintRepository();
        $this->comment = new CommentRepository();
        $this->article = new ArticleRepository();
        $this->follower = new FollowerRepository();
        $this->news = new NewsRepository();
    }

    /**
     *
     */
    const NOTIFICATION_READ = 200;
    /**
     *
     */
    const NOTIFICATION_NOT_READ = 201;

    /**
     *
     */
    const TYPE_NOTIFICATION_BOAST = 'NOTICE_BOAST';
    const TYPE_NOTIFICATION_BOAST_COMMENT = 'NOTICE_BOAST_COMMENT';
    const TYPE_NOTIFICATION_COMPLAINT = 'NOTICE_COMPLAINT';
    const TYPE_NOTIFICATION_COMPLAINT_COMMENT = 'NOTICE_COMPLAINT_COMMENT';
    const TYPE_NOTIFICATION_NEWS = 'NOTICE_NEWS';

    const TYPE_NOTIFICATION_VOTE_BOAST = 'VOTE_BOAST';
    const TYPE_NOTIFICATION_VOTE_COMPLAINT = 'VOTE_COMPLAINT';
    const TYPE_NOTIFICATION_VOTE_COMMENT = 'VOTE_COMMENT';

    const TYPE_NOTIFICATION_NEW_FOLLOWER = 'NEW_FOLLOWER';

    /**
     * @var array
     */
    private static $_typeTitle = [
        self::TYPE_NOTIFICATION_BOAST => 'Комментарий хваста',
        self::TYPE_NOTIFICATION_BOAST_COMMENT => 'Ответ на ваш комментарий в "Хвасты"',
        self::TYPE_NOTIFICATION_COMPLAINT => 'Комментарий жалобы',
        self::TYPE_NOTIFICATION_COMPLAINT_COMMENT => 'Ответ на ваш комментарий в "Жалобы"',
        self::TYPE_NOTIFICATION_VOTE_BOAST => 'Оценка "Хвасты"',
        self::TYPE_NOTIFICATION_VOTE_COMPLAINT => 'Оценка "Жалобы"',
        self::TYPE_NOTIFICATION_VOTE_COMMENT => 'Оценка "Комментарий"',
        self::TYPE_NOTIFICATION_NEW_FOLLOWER => 'Новый подписчик',
        self::TYPE_NOTIFICATION_NEWS => 'Новости проекта',
    ];

    private static $_typeComment = [
        self::TYPE_NOTIFICATION_BOAST => '{user} прокомментировал ваш хваст {title}',
        self::TYPE_NOTIFICATION_BOAST_COMMENT => '{user} ответил на ваш комментарий {comment}',
        self::TYPE_NOTIFICATION_COMPLAINT => '{user} добавил новый комментарий к вашей жалобе на магазин {title}',
        self::TYPE_NOTIFICATION_COMPLAINT_COMMENT => '{user} ответил на ваш комментарий {comment}',
        self::TYPE_NOTIFICATION_VOTE_BOAST => '{user} оценил ваш хваст {title}',
        self::TYPE_NOTIFICATION_VOTE_COMPLAINT => '{user} поблагодарил вас за жалобу на магазин {title}',
        self::TYPE_NOTIFICATION_VOTE_COMMENT => '{user} оценил ваш комментарий {comment}',
        self::TYPE_NOTIFICATION_NEWS => 'Новая новость {title}',
        self::TYPE_NOTIFICATION_NEW_FOLLOWER => 'Пользователь {user} подписался на ваши обновления',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
            'root' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'isRead',
                ],
                'value' => function () {
                    return self::NOTIFICATION_NOT_READ;
                },
            ],
            'author' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'authorId',
                ],
            ],
        ];
    }

    /**
     * @param null $code
     * @return array|bool
     */
    public static function getTypes($code = null)
    {
        if ($code) {
            return isset(static::$_typeTitle[$code]) ? static::$_typeTitle[$code] : false;
        } else {
            return static::$_typeTitle;
        }
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'id',
            'objectId',
            'type',
            'title',
            'message',
            'timeCreated',
            'timeRead',
            'isRead',
            'recipientId',
            'entityId',
            'authorId'
        ];
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    /**
     * @return Article|Boast|Complaint|bool|null
     */
    private function getObject(){
        $object = false;

        if($this->type == self::TYPE_NOTIFICATION_NEWS) {
            $object = $this->news->getById($this->entityId);

        } elseif (in_array($this->type, [
            self::TYPE_NOTIFICATION_BOAST,
            self::TYPE_NOTIFICATION_BOAST_COMMENT,
            self::TYPE_NOTIFICATION_VOTE_BOAST
        ])) {

            $object = $this->boast->getById($this->entityId);

        } elseif (in_array($this->type, [
            self::TYPE_NOTIFICATION_COMPLAINT,
            self::TYPE_NOTIFICATION_COMPLAINT_COMMENT,
            self::TYPE_NOTIFICATION_VOTE_COMPLAINT
        ])) {

            $object = $this->complaint->getById($this->entityId);

        } elseif (in_array($this->type, [
            self::TYPE_NOTIFICATION_VOTE_COMMENT
        ])) {
            $comment = $this->comment->getById($this->entityId);

            if ($comment->entityName == 'boast') {
                $object = $this->boast->getById($comment->entityId);
            } elseif ($comment->entityName == 'article') {
                $object = $this->article->getById($comment->entityId);
            } elseif ($comment->entityName == 'complaint') {
                $object = $this->complaint->getById($comment->entityId);
            }
        } elseif (in_array($this->type, [
            self::TYPE_NOTIFICATION_NEW_FOLLOWER,
        ])) {
            $object = $this->follower->getById($this->entityId);
        }

        return $object;
    }

    /**
     * @return Comment|bool
     */
    private function getComment()
    {

        if ($this->type != self::TYPE_NOTIFICATION_VOTE_COMMENT) {
            $comment = $this->comment->getById($this->objectId);
        } else {
            $comment = $this->comment->getById($this->entityId);
        }

        if ($comment && $comment->rootId > 0) {
            $comment = $this->comment->getById($comment->rootId);
        }

        return $comment;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        $object = $this->getObject();
        $author = $this->author;
        $comment = $this->getComment();

        $text = static::$_typeComment[$this->type];
        $title = null;

        if ($object instanceof Complaint) {
            $title = $object->seller->storeName;
        } elseif ($object instanceof Follower) {
            $title = $object->user->present()->name();
        }elseif ($object instanceof News) {
            $title = 'читать';
        } elseif ($object) {
            $title = $object->title;
        }

        $title = StringHelper::truncate($title, 15);
        $title = Html::a($title, $this->getUrl(), ['target'=>'_blank']);
        $text = str_replace('{title}', $title, $text);

        if ($comment) {
            $commentContent = StringHelper::truncate($comment->content, 15);
            $commentLink = Html::a($commentContent, $this->getUrl(), ['target' => '_blank']);
            $text = str_replace('{comment}', $commentLink, $text);
        }

        $text = str_replace('{user}', Html::a($author->name, ['profile/view', 'id' => $author->id], ['target' => '_blank']), $text);

        return $text;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        $object = $this->getObject();

        $params = [];

        if (!in_array($this->type, [
            self::TYPE_NOTIFICATION_VOTE_COMPLAINT,
            self::TYPE_NOTIFICATION_VOTE_BOAST,
            self::TYPE_NOTIFICATION_VOTE_COMMENT,
            self::TYPE_NOTIFICATION_NEWS
        ])
        ) {
            $params = ['#' => 'comment_' . $this->objectId];
        } elseif ($this->type == self::TYPE_NOTIFICATION_VOTE_COMMENT) {
            $params = ['#' => 'comment_' . $this->entityId];
        } elseif ($this->type == self::TYPE_NOTIFICATION_NEW_FOLLOWER) {
            $params = ['#' => 'user_' . $this->entityId];
        }

        if ($object) {
            return Url::entity($object, false, $params);
        }
    }
}
