<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\LatestNewsBehavior;
use app\behaviors\VoteableBehavior;
use app\db\ActiveRecord;
use app\db\queries\BoastPhotoQuery;
use app\db\queries\BoastQuery;
use app\db\queries\CategoryQuery;
use app\db\queries\UserQuery;
use app\helpers\Url;
use app\presenters\BoastPresenter;
use app\presenters\PresentableBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $categoryId
 * @property string  $externalUrl
 * @property integer $storeId
 * @property integer $productId
 * @property integer $snapshotId
 * @property string  $title
 * @property string  $slug
 * @property string  $text
 * @property float   $price
 * @property integer $score
 * @property integer $viewCount
 * @property integer $visible
 * @property integer $isDeleted
 * @property integer $isFeatured
 * @property string  $timeCreated
 * @property string  $timeUpdated
 *
 * @property Category     $category
 * @property BoastPhoto   $mainPhoto
 * @property BoastPhoto[] $photos
 * @property User         $user
 *
 * @method BoastPresenter present()
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin CommentableBehavior
 * @mixin VoteableBehavior
 */
class Boast extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'boast';
    }

    /**
     * @return BoastQuery
     */
    public static function find()
    {
        return new BoastQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'presenter' => [
                'class' => PresentableBehavior::className(),
                'presenter' => BoastPresenter::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated', 'timeUpdated'],
                ],
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
            'votes' => [
                'class' => VoteableBehavior::className(),
            ],
            'latestNews' => [
                'class' => LatestNewsBehavior::className(),
                'title' => function ($owner) {
                    $user = $owner->user;

                    return Html::a($user->name, Url::entity($user));
                },
                'text' => function ($owner) {
                    return Html::a($owner->title, Url::entity($owner));
                }
            ],
        ];
    }

    /**
     * @return CategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return BoastPhotoQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(BoastPhoto::className(), ['boastId' => 'id'])
            ->inverseOf('boast');
    }

    /**
     * @return BoastPhotoQuery
     */
    public function getMainPhoto()
    {
        return $this->hasOne(BoastPhoto::className(), ['boastId' => 'id'])
            ->inverseOf('boast');
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}