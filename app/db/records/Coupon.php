<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\CouponQuery;
use Yii;

/**
 * @property integer $id
 * @property integer $categoryId
 * @property integer $sellerId
 * @property integer $storeId
 * @property string  $storeName
 * @property string  $imageUrl
 * @property float   $cost
 * @property float   $value
 * @property float   $discount
 * @property string  $hash
 * @property integer $availableProducts
 * @property integer $issusedCount
 * @property integer $totalCount
 * @property string  $status
 * @property string  $dateExpired
 * @property string  $timeCreated
 * @property string  $timeUpdated
 *
 * @property CouponCategory $category
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Coupon extends ActiveRecord
{
    const PROFIT_DISCOUNT = 0.7;

    const STATUS_NOT_IN_STORE = 'unknown';
    const STATUS_AVAILABLE = 'available';
    const STATUS_REMOVED = 'removed';


    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        return Yii::$app->get('dbCoupons');
    }

    /**
     * @return CouponQuery
     */
    public static function find()
    {
        return new CouponQuery(get_called_class());
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CouponCategory::className(), ['categoryId' => 'categoryId']);
    }
}