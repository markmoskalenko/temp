<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\NewsQuery;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property string  $content
 * @property string  $seoTitle
 * @property string  $seoDescription
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsCategory extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'content' => 'Контент',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
        ];
    }

    /**
     * @return NewsQuery
     */
    public function getNews()
    {
        return $this->hasMany(Article::className(), ['categoryId' => 'id']);
    }
}