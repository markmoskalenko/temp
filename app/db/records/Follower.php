<?php

namespace app\db\records;

use Yii;
use app\db\ActiveRecord;
use app\db\queries\FollowersQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\behaviors\NotificationBehavior;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $objectId
 * @property string $timeCreated
 *
 * @property User $object
 * @property User $user
 *
 * @author Mark Moskalenko <office@it-yes.com>
 */
class Follower extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_follower';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
            'notification' => [
                'class' => NotificationBehavior::className()
            ],
        ];
    }

    /**
    * @return FollowersQuery
    */
    public static function find()
    {
        return new FollowersQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(User::className(), ['id' => 'objectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
