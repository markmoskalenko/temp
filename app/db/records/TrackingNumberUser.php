<?php

namespace app\db\records;

use Yii;
use app\db\ActiveRecord;
use app\db\queries\TrackingNumberUserQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tracking_number_user".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $trackingNumberId
 * @property string $timeCreated
 *
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumberUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking_number_user';
    }
    
    /**
    * @return TrackingNumberUserQuery
    */
    public static function find()
    {
        return new TrackingNumberUserQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated'],
                ],
            ],
        ];
    }

}
