<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\SellerQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerReplica extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seller_replica';
    }

    /**
     * @return SellerQuery
     */
    public function getOriginal()
    {
        return $this->hasOne(Seller::className(), ['id' => 'sellerId']);
    }

    /**
     * @param Seller $seller
     */
    public function setOriginal($seller)
    {
        $attributes = $seller->getAttributes();
        $attributes['sellerId'] = $seller->id;

        unset($attributes['id']);
        unset($attributes['timeCreated']);

        $this->setAttributes($attributes, false);
    }
}