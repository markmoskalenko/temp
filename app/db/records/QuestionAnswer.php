<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\ContentPreviewBehavior;
use app\db\queries\QuestionAnswerQuery;
use app\db\queries\UserQuery;
use app\presenters\PresentableBehavior;
use app\presenters\QuestionAnswerPresenter;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use app\db\ActiveRecord;

/**
 * This is the model class for table "qa".
 *
 * @property integer $id
 * @property integer $qaCategoryId
 * @property integer $userId
 * @property string $title
 * @property string $email
 * @property string $firstName
 * @property string $question
 * @property integer $isMailNotification
 * @property integer $status
 * @property string $timeCreated
 * @property string $timeUpdated
 *
 * @property QuestionAnswerCategory $qaCategory
 * @property QuestionAnswerPhoto[]  $photos
 * @property User                   $user
 *
 * @method QuestionAnswerPresenter present()
 *
 * @mixin ContentPreviewBehavior
 * @mixin CommentableBehavior
 * @mixin TimestampBehavior
 */
class QuestionAnswer extends ActiveRecord
{
    /**
     * Новый вопрос
     */
    const STATUS_NEW = 10;
    /**
     * Есть ответ
     */
    const STATUS_ANSWERED = 11;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qa';
    }

    /**
     * @return QuestionAnswerQuery
     */
    public static function find()
    {
        return new QuestionAnswerQuery(get_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'preview' => [
                'class' => ContentPreviewBehavior::className(),
                'contentAttribute'=>'question',
                'contentPreviewAttribute'=>'question'
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                    static::EVENT_BEFORE_UPDATE => 'timeUpdated',
                ],
            ],
            'present' => [
                'class' => PresentableBehavior::className(),
                'presenter' => QuestionAnswerPresenter::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQaCategory()
    {
        return $this->hasOne(QuestionAnswerCategory::className(), ['id' => 'qaCategoryId']);
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return QuestionAnswerPhoto
     */
    public function getPhotos()
    {
        return $this->hasMany(QuestionAnswerPhoto::className(), ['qaId' => 'id'])
            ->inverseOf('questionAnswer');
    }
}