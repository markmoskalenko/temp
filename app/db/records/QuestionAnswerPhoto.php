<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\BoastPhotoQuery;
use app\db\queries\BoastQuery;
use app\db\queries\QuestionAnswerPhotoQuery;
use app\interfaces\UploadableModel;
use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $qaId
 * @property string  $filename
 * @property integer $rejected
 * @property string  $timeCreated
 *
 * @property integer $width
 * @property integer $height
 *
 * @property Boast   $boast
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class QuestionAnswerPhoto extends ActiveRecord implements UploadableModel
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'qa_photo';
    }

    /**
     * @return BoastPhotoQuery
     */
    public static function find()
    {
        return new QuestionAnswerPhotoQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[1];
    }

    /**
     * @return BoastQuery
     */
    public function getQuestionAnswer()
    {
        return $this->hasOne(QuestionAnswer::className(), ['id' => 'qaId']);
    }
}