<?php
namespace app\db\records;

use app\db\queries\ReviewImageQuery;
use app\db\queries\ReviewQuery;
use app\interfaces\UploadableModel;
use app\rest\ArrayableTrait;
use app\rest\FormatterBehavior;
use app\rest\ImperaviRedactorFormatter;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $reviewId
 * @property string  $filename
 * @property string  $timeCreated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class ReviewImage extends ActiveRecord implements UploadableModel
{
    use ArrayableTrait;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'review_image';
    }

    /**
     * @return ReviewImageQuery
     */
    public static function find()
    {
        return new ReviewImageQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'formatter' => [
                'class' => FormatterBehavior::className(),
                'formatter' => ImperaviRedactorFormatter::className(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[1];
    }

    /**
     * @return ReviewQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::className(), ['id' => 'reviewId']);
    }
}