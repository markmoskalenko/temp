<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\BoastPhotoQuery;
use app\db\queries\BoastQuery;
use app\interfaces\UploadableModel;
use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * @property integer $id
 * @property integer $userId
 * @property integer $boastId
 * @property string  $filename
 * @property integer $rejected
 * @property string  $timeCreated
 *
 * @property integer $width
 * @property integer $height
 *
 * @property Boast   $boast
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastPhoto extends ActiveRecord implements UploadableModel
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'boast_photo';
    }

    /**
     * @return BoastPhotoQuery
     */
    public static function find()
    {
        return new BoastPhotoQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'user' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'userId',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->filename;
    }

    /**
     * @return integer
     */
    public function getWidth()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[0];
    }

    /**
     * @return integer
     */
    public function getHeight()
    {
        $fullpath = Yii::getAlias('@webroot/uploads/' . $this->filename);
        $size = getimagesize($fullpath);
        return $size[1];
    }

    /**
     * @return BoastQuery
     */
    public function getBoast()
    {
        return $this->hasOne(Boast::className(), ['id' => 'boastId']);
    }
}