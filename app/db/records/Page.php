<?php
namespace app\db\records;

use app\behaviors\ContentPreviewBehavior;
use app\db\ActiveRecord;
use app\db\queries\ArticleCategoryQuery;
use app\db\queries\PublicationImageQuery;
use app\db\queries\NewsQuery;
use app\db\queries\PageQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * @property integer $id
 * @property string  $language
 * @property string  $name
 * @property string  $slug
 * @property string  $content
 * @property string  $seoTitle
 * @property string  $seoDescription
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Page extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @return NewsQuery
     */
    public static function find()
    {
        return new PageQuery(__CLASS__);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['language', 'string'],
            ['language', 'trim'],
            ['language', 'required'],
            ['language', 'filter', 'filter' => [Yii::$app->polyglot, 'normalizeLanguage']],

            ['name', 'string'],
            ['name', 'trim'],
            ['name', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'language' => 'Язык',
            'name' => 'Название',
            'content' => 'Текст статьи',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
        ];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['@page', 'slug' => $this->slug]);
    }
}