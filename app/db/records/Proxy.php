<?php
namespace app\db\records;

use app\db\queries\ProxyQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string  $ip
 * @property string  $port
 * @property integer $requests
 * @property integer $isValid
 * @property string  $timeCreated
 * @property string  $timeUpdated
 * @property string  $timeFailed
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin TimestampBehavior
 */
class Proxy extends ActiveRecord
{
    /**
     * @return ProxyQuery
     */
    public static function find()
    {
        return new ProxyQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated', 'timeUpdated', 'timeFailed'],
                    static::EVENT_BEFORE_UPDATE => ['timeUpdated'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['ip', 'string'],
            ['ip', 'trim'],

            ['port', 'string'],
            ['port', 'trim'],
        ];
    }
}
