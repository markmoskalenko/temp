<?php

namespace app\db\records;

use Yii;
use app\db\ActiveRecord;
use app\db\queries\TrackingNumberQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tracking_number".
 *
 * @property integer $id
 * @property string $trackingNumber
 * @property string $timeCreated
 *
 * @author Mark Moskalenko <office@it-yes.com>
 */
class TrackingNumber extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking_number';
    }
    
    /**
    * @return TrackingNumberQuery
    */
    public static function find()
    {
        return new TrackingNumberQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => ['timeCreated'],
                ],
            ],
        ];
    }

}
