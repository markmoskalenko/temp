<?php
namespace app\db\records;

use app\db\ActiveRecord;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $userId
 * @property string  $userIp
 * @property string  $userAgent
 * @property string  $userName
 * @property string  $userEmail
 * @property string  $subject
 * @property string  $body
 * @property string  $timeCreated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class FeedbackMessage extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'userId' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'userId'],
            ],
            'userIp' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'userIp'],
                'value' => Yii::$app->request->getUserIP(),
            ],
            'userAgent' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'userAgent'],
                'value' => Yii::$app->request->getUserAgent(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [static::EVENT_BEFORE_INSERT => 'timeCreated'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ['userName', 'userEmail', 'subject', 'body'];
    }
}