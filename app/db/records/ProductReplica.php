<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\ProductQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductReplica extends ActiveRecord
{
    const TIME_CREATED = 'timeCreated';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_replica';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => static::TIME_CREATED,
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return ProductQuery
     */
    public function getOriginal()
    {
        return $this->hasOne(Product::className(), ['id' => 'productId']);
    }

    /**
     * @param Product $product
     */
    public function setOriginal($product)
    {
        $attributes = $product->getAttributes();
        $attributes['productId'] = $product->id;

        unset($attributes['id']);
        unset($attributes['timeCreated']);

        $this->setAttributes($attributes, false);
    }
}