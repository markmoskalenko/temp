<?php
namespace app\db\records;

use app\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $snapshotId
 * @property integer $storeId
 * @property string  $storeName
 * @property integer $productId
 * @property string  $productName
 * @property integer $imageUrl
 * @property string  $price
 * @property string  $unit
 * @property string  $timeCreated
 * @property string  $timeUpdated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Snapshot extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'snapshot';
    }
}