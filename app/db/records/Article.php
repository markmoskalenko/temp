<?php
namespace app\db\records;

use app\behaviors\CommentableBehavior;
use app\behaviors\ContentPreviewBehavior;
use app\db\ActiveRecord;
use app\db\queries\ArticleCategoryQuery;
use app\db\queries\ArticleQuery;
use app\db\queries\PublicationImageQuery;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property integer $categoryId
 * @property integer $imageId
 * @property string  $title
 * @property string  $slug
 * @property string  $content
 * @property string  $contentPreview
 * @property string  $seoTitle
 * @property string  $seoDescription
 * @property integer $viewCount
 * @property string  $timeCreated
 * @property string  $timePublished
 *
 * @property ArticleCategory  $category
 * @property PublicationImage $mainImage
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin ContentPreviewBehavior
 * @mixin TimestampBehavior
 * @mixin CommentableBehavior
 */
class Article extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'comments' => [
                'class' => CommentableBehavior::className(),
            ],
            'preview' => [
                'class' => ContentPreviewBehavior::className(),
            ],
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['categoryId', 'integer'],
            ['categoryId', 'required'],

            ['imageId', 'required', 'except' => static::SCENARIO_DEFAULT],

            ['title', 'string'],
            ['title', 'trim'],
            ['title', 'required'],

            ['slug', 'string'],
            ['slug', 'trim'],
            ['slug', 'filter', 'filter' => ['yii\helpers\Inflector', 'slug']],
            ['slug', 'required', 'enableClientValidation' => false],

            ['content', 'string'],
            ['content', 'trim'],
            ['content', 'required'],

            ['contentPreview', 'string'],
            ['contentPreview', 'trim'],
            ['contentPreview', 'default'],

            ['seoTitle', 'string'],
            ['seoTitle', 'trim'],
            ['seoTitle', 'default'],

            ['seoDescription', 'string'],
            ['seoDescription', 'trim'],
            ['seoDescription', 'default'],

            ['timePublished', 'string'],
            ['timePublished', 'trim'],
            ['timePublished', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Категория',
            'imageId' => 'Основное изображение',
            'title' => 'Название',
            'content' => 'Текст статьи',
            'contentPreview' => 'Краткое описание',
            'slug' => 'Псевдоним URL',
            'seoTitle' => 'Тег TITLE',
            'seoDescription' => 'Тег META Description',
            'timePublished' => 'Время публикации',
        ];
    }

    /**
     * @return ArticleCategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'categoryId']);
    }

    /**
     * @return PublicationImageQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(PublicationImage::className(), ['id' => 'imageId']);
    }
}