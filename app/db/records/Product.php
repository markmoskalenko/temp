<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\behaviors\RelationalSetterBehavior;
use app\db\queries\ProductQuery;
use app\db\queries\SellerQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * @property integer $id
 * @property integer $originalProductId
 * @property integer $sellerId
 * @property string  $title
 * @property integer $closed
 * @property integer $ratingValue
 * @property integer $reviewCount
 * @property integer $purchasesCount
 * @property string  $timeCreated
 * @property string  $timeUpdated
 * @property string  $timeReplicationStarted
 * @property string  $timeReplicationSucceed
 *
 * @property Seller $seller
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin TimestampBehavior
 */
class Product extends ActiveRecord
{
    const TIME_CREATED = 'timeCreated';
    const TIME_UPDATED = 'timeUpdated';
    const TIME_REPLICATION_STARTED = 'timeReplicationStarted';
    const TIME_REPLICATION_SUCCEED = 'timeReplicationSucceed';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'sellerSetter' => [
                'class' => RelationalSetterBehavior::className(),
                'setterAttribute' => 'seller',
                'primaryKeyAttribute' => 'sellerId',
                'className' => Seller::className(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => static::TIME_CREATED,
                'updatedAtAttribute' => static::TIME_UPDATED,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param boolean $value
     */
    public function setClosed($value = true)
    {
        $this->closed = ($value) ? 1 : 0;
    }

    /**
     * @return SellerQuery
     */
    public function getSeller()
    {
        return $this->hasOne(Seller::className(), ['id' => 'sellerId']);
    }
}