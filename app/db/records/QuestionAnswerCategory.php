<?php

namespace app\db\records;

use Yii;

/**
 * This is the model class for table "qa_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $seoTitle
 * @property string $seoDescription
 *
 * @property QuestionAnswer[] $qas
 */
class QuestionAnswerCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qa_category';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Адрес',
            'seoTitle' => 'Seo название',
            'seoDescription' => 'Seo опикание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionAnswer()
    {
        return $this->hasMany(QuestionAnswer::className(), ['qaCategoryId' => 'id']);
    }
}