<?php
namespace app\db\records;

use app\db\ActiveRecord;
use app\db\queries\UserQuery;
use app\db\queries\UserTokenQuery;

/**
 * @property integer $id
 * @property integer $userId
 * @property string  $value
 * @property string  $type
 * @property string  $timeCreated
 * @property string  $timeExpired
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserToken extends ActiveRecord
{
    const TYPE_EMAIL = 'email';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'user_token';
    }

    /**
     * @return UserTokenQuery
     */
    public static function find()
    {
        return new UserTokenQuery(get_class());
    }

    /**
     * @param string $token
     * @param string $type
     * @return static|null
     */
    public static function findValid($token, $type = null)
    {
        return static::find()
            ->andWhere(['value' => $token])
            ->andFilterWhere(['type' => $type])
            ->valid()->one();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}