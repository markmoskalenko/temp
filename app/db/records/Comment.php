<?php
namespace app\db\records;

use app\behaviors\LatestNewsBehavior;
use app\behaviors\NotificationBehavior;
use app\behaviors\VoteableBehavior;
use app\db\ActiveRecord;
use app\db\queries\CommentQuery;
use app\db\queries\UserQuery;
use app\helpers\Url;
use app\rest\ArrayableTrait;
use app\rest\CommentFormatter;
use app\rest\FormatterBehavior;
use Yii;
use yii\base\ModelEvent;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * @property integer $id
 * @property integer $parentId
 * @property integer $rootId
 * @property string  $entityName
 * @property string  $entityId
 * @property string  $authorId
 * @property string  $content
 * @property string  $timeCreated
 * @property integer $isDeleted
 *
 * @property User   $author
 *
 * @author Artem Belov <razor2909@gmail.com>
 * @mixin VoteableBehavior
 */
class Comment extends ActiveRecord
{
    use ArrayableTrait;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @return CommentQuery
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'author' => [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'authorId',
                ]
            ],
            'formatter' => [
                'class' => FormatterBehavior::className(),
                'formatter' => CommentFormatter::className(),
            ],
            'root' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'rootId',
                    static::EVENT_BEFORE_UPDATE => 'rootId',
                ],
                'value' => function (ModelEvent $event) {
                    /* @var $comment Comment */
                    $comment = $event->sender;
                    if ($comment->parentId && $parent = static::findOne($comment->parentId)) {
                        return $parent->rootId ?: $parent->id;
                    }

                    return 0;
                },
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'timeCreated',
                ],
            ],
            'votes' => [
                'class' => VoteableBehavior::className(),
            ],
            'notification' => [
                'class' => NotificationBehavior::className()
            ],
            'latestNews' => [
                'class' => LatestNewsBehavior::className(),
                'type' => 2,
                'title' => function ($owner) {
                    $user = $owner->author;

                    return Html::a($user->name, Url::entity($user));
                },
                'text' => function ($owner) {
                    $object = $owner->{$owner->entityName};

                    if ($object instanceof Complaint) {
                        $text = $object->seller->storeName;
                    } else {
                        $text = $object->title;
                    }

                    $params = ['#' => 'comment_' . $owner->id];

                    return Html::a($text, Url::entity($object, false, $params));
                }
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['parentId', 'integer'],
            ['parentId', 'default', 'value' => 0],
            ['rootId', 'integer'],
            ['rootId', 'default', 'value' => 0],
            ['entityName', 'string'],
            ['entityName', 'required'],
            ['entityId', 'integer'],
            ['entityId', 'required'],
            ['entityId', 'unique', 'targetAttribute' => ['authorId', 'entityName', 'entityId']],
        ];
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoast()
    {
        return $this->hasOne(Boast::className(), ['id' => 'entityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'entityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionanswer()
    {
        return $this->hasOne(QuestionAnswer::className(), ['id' => 'entityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaint()
    {
        return $this->hasOne(Complaint::className(), ['id' => 'entityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'entityId']);
    }
}