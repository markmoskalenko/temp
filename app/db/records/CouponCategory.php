<?php
namespace app\db\records;

use app\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $categoryId aliexpress category_id
 * @property string  $name
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class CouponCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        return Yii::$app->get('dbCoupons');
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupons()
    {
        return $this->hasMany(Coupon::className(), ['categoryId' => 'categoryId']);
    }
}