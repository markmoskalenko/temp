<?php
namespace app\db\records;

use app\behaviors\NotificationBehavior;
use app\db\ActiveRecord;
use app\db\queries\VoteQuery;
use app\db\queries\UserQuery;
use Yii;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\db\BaseActiveRecord;
use yii\helpers\StringHelper;

/**
 * @property integer $id
 * @property integer $userId
 * @property string  $entityName
 * @property string  $entityId
 * @property integer $value
 * @property string  $timeCreated
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class Vote extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'vote';
    }

    /**
     * @return VoteQuery
     */
    public static function find()
    {
        return new VoteQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'notification' => [
                'class' => NotificationBehavior::className()
            ]
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['userId', 'integer'],
            ['userId', 'required'],

            ['entityName', 'string'],
            ['entityName', 'required'],

            ['entityId', 'integer'],
            ['entityId', 'required'],
            ['entityId', 'unique', 'targetAttribute' => ['userId', 'entityName', 'entityId']],

            ['value', 'integer'],
            ['value', 'default', 'value' => 1],
        ];
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}