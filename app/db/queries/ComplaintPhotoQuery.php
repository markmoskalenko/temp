<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintPhotoQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function visible()
    {
        return $this->andWhere(['rejected' => 0]);
    }
}