<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class QuestionAnswerPhotoQuery extends ActiveQuery
{
    /**
     * Visible for a simple user.
     *
     * @return static
     */
    public function visible()
    {
        return $this->andWhere([
            'rejected' => 0,
            'deleted' => 0,
        ]);
    }
}