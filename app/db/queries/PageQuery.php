<?php
namespace app\db\queries;

use Yii;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PageQuery extends ActiveQuery
{
    /**
     * @param string $language
     * @return static
     */
    public function speaking($language)
    {
        return $this->andWhere([
            'language' => Yii::$app->polyglot->normalizeLanguage($language),
        ]);
    }
}