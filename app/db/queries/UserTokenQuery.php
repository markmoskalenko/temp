<?php
namespace app\db\queries;

use yii\base\Exception;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserTokenQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function valid()
    {
        return $this->andWhere(['<=', 'timeExpired', new Exception('NOW()')]);
    }

    /**
     * @return static
     */
    public function expired()
    {
        return $this->andWhere(['>', 'timeExpired', new Exception('NOW()')]);
    }
}