<?php
namespace app\db\queries;

use app\db\records\ArticleCategory;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ArticleQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timePublished DESC');
    }

    /**
     * @return static
     */
    public function random()
    {
        return $this->orderBy('RAND()');
    }

    /**
     * @param integer $id
     * @return static
     */
    public function category($id)
    {
        return $this->andWhere(['categoryId' => $id]);
    }

    /**
     * @return static
     */
    public function published()
    {
        return $this->andWhere('timePublished < :now', [
            ':now' => new Expression('NOW()'),
        ]);
    }

    /**
     * @param string $language
     * @return static
     */
    public function speaking($language)
    {
        return $this
            ->leftJoin(ArticleCategory::tableName(), 'article_category.id = article.categoryId')
            ->andWhere([
                'article_category.language' => Yii::$app->polyglot->normalizeLanguage($language),
            ]);
    }
}