<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class PublicationImageQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timeCreated DESC');
    }
}