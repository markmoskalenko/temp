<?php
namespace app\db\queries;

use app\db\records\User;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UserQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function active()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }

    /**
     * @return static
     */
    public function deleted()
    {
        return $this->andWhere(['status' => User::STATUS_DELETED]);
    }
}