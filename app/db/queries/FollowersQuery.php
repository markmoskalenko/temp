<?php

namespace app\db\queries;

use yii\db\ActiveQuery;
use Yii;

/**
 * @author Mark Moskalenko <office@it-yes.com>
 */
class FollowersQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function getOwner(){
        return $this->andWhere(['userId'=>Yii::$app->user->id]);
    }

    /**
     * @param $id
     * @return static
     */
    public function byUserId($id){
        return $this->andWhere(['userId'=>$id]);
    }

    /**
     * @param $id
     * @return static
     */
    public function byObjectId($id){
        return $this->andWhere(['objectId'=>(int)$id]);
    }
}
