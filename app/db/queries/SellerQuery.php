<?php
namespace app\db\queries;

use app\db\records\Seller;
use Carbon\Carbon;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->addOrderBy('timeCreated DESC');
    }

    /**
     * @param boolean $status
     * @return static
     */
    public function closed($status = true)
    {
        return $this->andWhere([
            'closed' => ($status) ? 1 : 0,
        ]);
    }

    /**
     * @return static
     */
    public function replicable()
    {
        $timeout = Yii::$app->params['seller.replicationTimeout'];
        $timeout = Carbon::now()->subSeconds($timeout)->toDateTimeString();

        return $this
            ->andWhere('timeReplicationSucceed IS NULL OR timeReplicationSucceed < :timeout')
            ->addParams([':timeout' => $timeout])
            ->closed(false)
            ->orderBy([
                Seller::TIME_REPLICATION_STARTED => SORT_ASC,
                Seller::TIME_REPLICATION_SUCCEED => SORT_ASC,
                Seller::TIME_CREATED => SORT_ASC,
            ]);
    }

    /**
     * @return static
     */
    public function hasComplaints()
    {
        return $this->joinReviews()->andWhere('complaint.id IS NOT NULL');
    }

    /**
     * @return static
     */
    public function orderByComplaintsCount()
    {
        return $this->joinReviews()
            ->select(['seller.*', new Expression('COUNT(complaint.id) AS complaintsCount')])
            ->addOrderBy('complaintsCount DESC')
            ->groupBy('seller.id');
    }

    /**
     * @var boolean
     */
    private $_complaintsJoined = false;

    /**
     * @return static
     */
    protected function joinReviews()
    {
        if ( ! $this->_complaintsJoined) {
            $this->leftJoin('complaint', 'complaint.sellerId = seller.id AND complaint.rejected = 0');
            $this->_complaintsJoined = true;
        }
        return $this;
    }
}