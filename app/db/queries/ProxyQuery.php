<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProxyQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function prioritized()
    {
        return $this->orderBy('isValid DESC, requests ASC');
    }
}