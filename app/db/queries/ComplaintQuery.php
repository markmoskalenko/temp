<?php
namespace app\db\queries;

use Yii;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ComplaintQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function approved()
    {
        return $this->andWhere([
            'moderated' => 1,
            'rejected' => 0,
        ]);
    }

    /**
     * @return static
     */
    public function rejected()
    {
        return $this->andWhere([
            'moderated' => 1,
            'rejected' => 1,
        ]);
    }

    /**
     * @return static
     */
    public function moderatable()
    {
        return $this->andWhere([
            'moderated' => 0,
        ]);
    }

    /**
     * @return static
     */
    public function visible()
    {
        return $this->andWhere([
            'rejected' => 0,
        ]);
    }

    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timeCreated DESC');
    }

    /**
     * Last reviews for the specified period.
     *
     * @param integer $period in seconds
     * @return static
     */
    public function per($period)
    {
        $time = date('Y-m-d H:i:s', time() - $period);
        return $this->andWhere(['>', 'timeCreated', $time]);
    }

    /**
     * @return static
     */
    public function hasUser()
    {
        return $this->andWhere('userId IS NOT NULL');
    }
}