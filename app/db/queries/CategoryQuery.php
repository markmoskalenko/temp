<?php
namespace app\db\queries;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CategoryQuery extends ActiveQuery
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    /**
     * @return static
     */
    public function orderLikeTree()
    {
        return $this->orderBy('root, lft');
    }

    /**
     * @return static
     */
    public function hasReviews()
    {
        return $this->select('category.*, COUNT(review.id) AS reviews')
            ->leftJoin('review', 'review.categoryId = category.id')
            ->groupBy('category.id')
            ->having('reviews > 0');
    }
}