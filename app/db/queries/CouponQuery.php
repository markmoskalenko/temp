<?php
namespace app\db\queries;

use app\db\records\Coupon;
use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CouponQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timeCreated DESC');
    }

    /**
     * @return static
     */
    public function profitable()
    {
        return $this->andWhere(['>=', 'discount', Coupon::PROFIT_DISCOUNT]);
    }
}