<?php
namespace app\db\queries;

use app\db\records\Product;
use Carbon\Carbon;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class ProductQuery
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductQuery extends ActiveQuery
{
    /**
     * @param boolean $status
     * @return static
     */
    public function closed($status = true)
    {
        return $this->andWhere([
            'closed' => ($status) ? 1 : 0,
        ]);
    }

    /**
     * @return static
     */
    public function replicable()
    {
        $timeout = Yii::$app->params['product.replicationTimeout'];
        $timeout = Carbon::now()->subSeconds($timeout)->toDateTimeString();

        return $this
            ->andWhere('timeReplicationSucceed IS NULL OR timeReplicationSucceed < :timeout')
            ->addParams([':timeout' => $timeout])
            ->closed(false)
            ->orderBy([
                Product::TIME_REPLICATION_STARTED => SORT_ASC,
                Product::TIME_REPLICATION_SUCCEED => SORT_ASC,
                Product::TIME_CREATED => SORT_ASC,
            ]);
    }
}