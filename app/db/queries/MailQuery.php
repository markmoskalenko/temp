<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MailQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function sendable()
    {
        return $this
            ->where('timeSendSucceed IS NULL')
            ->orderBy('timeSendFailed ASC, priority DESC, timeCreated ASC');
    }
}