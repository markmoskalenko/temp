<?php
namespace app\db\queries;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NewsQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timePublished DESC');
    }

    /**
     * @return static
     */
    public function random()
    {
        return $this->orderBy('RAND()');
    }

    /**
     * @return static
     */
    public function featured()
    {
        return $this->andWhere(['isFeatured' => 1]);
    }

    /**
     * @return static
     */
    public function notFeatured()
    {
        return $this->andWhere(['isFeatured' => 0]);
    }

    /**
     * @return static
     */
    public function published()
    {
        return $this->andWhere('timePublished < :now', [
            ':now' => new Expression('NOW()'),
        ]);
    }

    /**
     * @param string $language
     * @return static
     */
    public function speaking($language)
    {
        return $this->andWhere([
            'language' => Yii::$app->polyglot->normalizeLanguage($language),
        ]);
    }
}