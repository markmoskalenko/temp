<?php
namespace app\db\queries;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class BoastQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timeCreated DESC');
    }

    /**
     * @return static
     */
    public function random()
    {
        return $this->orderBy('RAND()');
    }

    /**
     * @return static
     */
    public function visible()
    {
        return $this->andWhere(['visible' => 1, 'isDeleted' => 0]);
    }
}