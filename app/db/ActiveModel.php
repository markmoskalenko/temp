<?php
namespace app\db;

use Yii;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\base\ModelEvent;

/**
 * @property ActiveRecord $record
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class ActiveModel extends Model
{
    /**
     * @event Event an event raised at the beginning of [[save()]]
     */
    const EVENT_BEFORE_SAVE = 'beforeSave';
    /**
     * @event Event an event raised at the end of [[save()]]
     */
    const EVENT_AFTER_SAVE = 'afterSave';

    /**
     * @var string
     */
    protected $recordClass = 'app\db\ActiveRecord';


    /**
     * @return array
     */
    public function fillable()
    {
        return $this->safeAttributes();
    }

    /**
     * Assigns attributes using [[fillable()]].
     *
     * @param Model $target
     * @param Model $source
     */
    protected function fill(Model $target, Model $source)
    {
        $target->setAttributes(
            $source->getAttributes($this->fillable()),
            false
        );
    }

    /**
     * This method is invoked before saving starts.
     *
     * @return boolean whether the saving should be executed. Defaults to true.
     */
    public function beforeSave()
    {
        $event = new ModelEvent;
        $this->trigger(self::EVENT_AFTER_SAVE, $event);

        return $event->isValid;
    }

    /**
     * This method is invoked after saving ends.
     */
    public function afterSave()
    {
        $this->trigger(self::EVENT_AFTER_SAVE);
    }

    /**
     * @param boolean $runValidation
     * @throws InvalidValueException
     * @return boolean
     */
    public function save($runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $this->fill($this->getRecord(), $this);

        if ( ! $this->beforeSave()) {
            return false;
        }

        $this->getRecord()->saveOrFail();

        $this->afterSave();

        return true;
    }

    /**
     * @var ActiveRecord
     */
    protected $_record;

    /**
     * @throws InvalidValueException
     * @return ActiveRecord
     */
    protected function getRecord()
    {
        if ($this->_record === null) {
            $this->_record = Yii::createObject($this->recordClass);
        }

        return $this->_record;
    }

    /**
     * @param ActiveRecord $record
     * @throws InvalidValueException
     */
    protected function setRecord($record)
    {
        $class = $this->recordClass;

        if ( ! ($record instanceof $class)) {
            throw new InvalidValueException("Record must be and instance of {$class}.");
        }

        $this->_record = $record;
    }
}