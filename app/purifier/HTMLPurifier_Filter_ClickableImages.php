<?php
namespace app\purifier;

use DOMDocument;
use DOMElement;
use DOMNode;
use HTMLPurifier_Config;
use HTMLPurifier_Context;
use HTMLPurifier_Filter;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HTMLPurifier_Filter_ClickableImages extends HTMLPurifier_Filter
{
    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return mixed
     */
    public function postFilter($html, $config, $context)
    {
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        libxml_use_internal_errors(true);

        $doc = new DOMDocument('1.0', 'UTF-8');
        $doc->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        /* @var $img DOMElement */
        foreach ($doc->getElementsByTagName('img') as $img) {
            if ($this->isNodeWrappedByLink($img)) {
                continue;
            }

            $link = $doc->createElement('a');
            $link->setAttribute('href', $img->getAttribute('src'));
            $link->setAttribute('target', '_blank');

            $this->wrapNode($img, $link);
        }

        $html = $doc->saveHTML();
        $html = mb_convert_encoding($html, 'UTF-8', 'HTML-ENTITIES');

        return $html;
    }

    /**
     * @param DOMNode $node
     * @return boolean
     */
    protected function isNodeWrappedByLink($node)
    {
        $path = $node->getNodePath();
        return strpos($path, '/a/') !== false;
    }

    /**
     * @param DOMNode $node
     * @param DOMNode $wrapper
     */
    protected function wrapNode($node, $wrapper)
    {
        $node->parentNode->replaceChild($wrapper, $node);
        $wrapper->appendChild($node);
    }
}