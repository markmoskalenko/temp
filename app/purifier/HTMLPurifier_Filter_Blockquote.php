<?php
namespace app\purifier;

use HTMLPurifier_Config;
use HTMLPurifier_Context;
use HTMLPurifier_Filter;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HTMLPurifier_Filter_Blockquote extends HTMLPurifier_Filter
{
    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return mixed
     */
    public function postFilter($html, $config, $context)
    {
        $pattern = '#<blockquote title="([^"]+)">#';
        return preg_replace_callback($pattern, [$this, 'postFilterCallback'], $html);
    }

    /**
     * @param array $matches
     * @return string
     */
    protected function postFilterCallback($matches)
    {
        $userName = $matches[1];
        return '<blockquote><header>' . Html::encode($userName) . '</header>';
    }
}