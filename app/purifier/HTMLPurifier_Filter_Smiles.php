<?php
namespace app\purifier;

use app\helpers\Smile;
use HTMLPurifier_Config;
use HTMLPurifier_Context;
use HTMLPurifier_Filter;
use yii\helpers\Html;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HTMLPurifier_Filter_Smiles extends HTMLPurifier_Filter
{
    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return mixed
     */
    public function postFilter($html, $config, $context)
    {
        $replacements = [];

        foreach (Smile::all() as $code) {
            $replacements[ $code ] = Html::img(Smile::url($code), ['class' => 'forum-smile']);
        }

        return strtr($html, $replacements);
    }
}