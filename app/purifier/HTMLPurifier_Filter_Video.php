<?php
namespace app\purifier;

use HTMLPurifier_Config;
use HTMLPurifier_Context;
use HTMLPurifier_Filter;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HTMLPurifier_Filter_Video extends HTMLPurifier_Filter
{
    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return string
     */
    public function preFilter($html, $config, $context)
    {
        $pattern = '#<video>(http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?)</video>#';
        return preg_replace($pattern, '<video>$2</video>', $html);
    }


    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return mixed
     */
    public function postFilter($html, $config, $context)
    {
        $pattern = '#<video>(.*?)</video>#';
        $replacement = '<iframe width="560" height="315" src="https://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';

        return preg_replace($pattern, $replacement, $html);
    }
}