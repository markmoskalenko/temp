<?php
namespace app\purifier;

use app\widgets\Spoiler;
use HTMLPurifier_Config;
use HTMLPurifier_Context;
use HTMLPurifier_Filter;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HTMLPurifier_Filter_Spoiler extends HTMLPurifier_Filter
{
    /**
     * @param string               $html
     * @param HTMLPurifier_Config  $config
     * @param HTMLPurifier_Context $context
     * @return mixed
     */
    public function postFilter($html, $config, $context)
    {
        $pattern = '#<spoiler(.*?)>(.*?)</spoiler>#msi';
        return preg_replace_callback($pattern, [$this, 'postFilterCallback'], $html);
    }

    /**
     * @param array $matches
     * @return string
     */
    protected function postFilterCallback($matches)
    {
        $attributes = $matches[1];
        $body = $matches[2];
        $title = 'Спойлер';

        if (preg_match('#title="([^"]+)"#', $attributes, $matches)) {
            $title = $matches[1];
        }

        $title = trim($title);
        $body = trim($body);

        return Spoiler::widget(compact('title', 'body'));
    }
}