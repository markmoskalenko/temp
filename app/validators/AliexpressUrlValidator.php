<?php
namespace app\validators;

use app\parser\entities\BaseEntity;
use app\parser\entities\ProductEntity;
use app\parser\entities\SnapshotEntity;
use app\parser\entities\StoreEntity;
use app\parser\UrlParser;
use yii\validators\Validator;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class AliexpressUrlValidator extends Validator
{
    /**
     * @var array
     */
    public $entities = [];


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->entities)) {
            $this->entities = [
                StoreEntity::className(),
                ProductEntity::className(),
                SnapshotEntity::className()
            ];
        }

        if ($this->message === null) {
            $this->message = \Yii::t('yii', 'The format of {attribute} is invalid.');
        }
    }

    /**
     * Validates a value.
     *
     * @param mixed $value the data value to be validated.
     * @return array|null the error message and the parameters to be inserted into the error message.
     * Null should be returned if the data is valid.
     */
    protected function validateValue($value)
    {
        $checker = new UrlParser();
        $entity = $checker->process($value);

        if ( ! $this->isAllowedEntity($entity)) {
            return [$this->message, []];
        }

        return null;
    }

    /**
     * @param BaseEntity $entity
     * @return boolean
     */
    protected function isAllowedEntity($entity)
    {
        return in_array(get_class($entity), $this->entities);
    }
}