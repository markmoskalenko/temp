<?php
namespace app\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Следит за тем, чтобы длинный текст был спрятан под катом.
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class CutValidator extends RegularExpressionValidator
{
    /**
     * Regexp паттерн для поиска ката.
     *
     * @var string
     */
    public $pattern = '#[cut]#';
    /**
     * Допустимое количество символов без ката.
     *
     * @var integer
     */
    public $length = 200;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ( ! $this->whenClient) {
            $this->whenClient = 'function (attribute, value) { return value.length > ' . $this->length . '; }';
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        if (mb_strlen($value) < $this->length) {
            return null;
        }

        return parent::validateValue($value);
    }
}