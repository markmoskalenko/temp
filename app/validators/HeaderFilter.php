<?php
namespace app\validators;

use Yii;
use yii\validators\FilterValidator;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HeaderFilter extends FilterValidator
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->filter = [$this, 'convertHeader'];
        parent::init();
    }

    /**
     * @param string $value
     * @return string
     */
    public function convertHeader($value)
    {
        $value = rtrim($value, '.');
        $value = trim($value);

        $value = $this->ucfirst($value);

        return $value;
    }

    /**
     * Multibyte ucfirst()
     *
     * @param string $value
     * @return string
     */
    public function ucfirst($value)
    {
        $first = mb_substr($value, 0, 1, Yii::$app->charset);
        $rest  = mb_substr($value, 1, null, Yii::$app->charset);
        return mb_convert_case($first, MB_CASE_UPPER, Yii::$app->charset) . $rest;
    }
}