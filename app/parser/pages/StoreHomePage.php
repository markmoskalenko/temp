<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\StoreEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreHomePage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof StoreEntity) {
            return 'http://www.aliexpress.com/store/' . $entity->getId();
        } else {
            throw new InvalidParamException('Entity must be instance of Store');
        }
    }

    /**
     * @inheritdoc
     */
    public function getAttributes()
    {
        return [
            'name' => $this->getName(),
        ];
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->getRegexMatch('#ownerMemberId: \'(\d+)\'#i', 1);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getNodeValue('//*[@id="shop22752045"]//a[@class="shop-name"]');
    }
}