<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\SnapshotEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotPage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof SnapshotEntity) {
            return 'http://www.aliexpress.com/snapshot/' . $entity->getId() . '.html?isOrigTitle=true';
        } else {
            throw new InvalidParamException('Entity must be instance of Snapshot');
        }
    }

    /**
     * Этому SellerID не следует доверять, т.к. он легко может отказаться устаревшим.
     *
     * @return string
     */
    public function getSellerId()
    {
        return $this->getRegexMatch('#"adminSeq":\'(\d+)\'#i', 1);
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->getRegexMatch('/"productId":\'(\d+)\'/', 1);
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->getNodeValue('//*[@id="product-name"]');
    }

    /**
     * @return string
     */
    public function getStoreId()
    {
        return $this->getRegexMatch('/http:\/\/www\.aliexpress\.com\/store\/(\d+)/', 1);
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->getNodeValue('//*[@class="company-name"]/a');
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        $price = $this->getNodeValue('//*[@id="buy-now-form"]//*[contains(@class, "price")]//*[@class="value"]');
        return $this->filterNumber($price);
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->getNodeValue('//*[@id="buy-now-form"]//*[contains(@class, "price")]//*[@class="unit"]');
    }

    /**
     * @return string
     */
    public function getMainImage()
    {
        return $this->getRegexMatch('/MAIN_BIG_PIC=\'(.*?)\';/', 1);
    }
}