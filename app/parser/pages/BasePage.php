<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use DomDocument;
use DOMNode;
use DOMXPath;
use yii\base\Object;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BasePage extends Object
{
    /**
     * @var string
     */
    private $_html;
    /**
     * @var DomDocument
     */
    private $_dom;
    /**
     * @var DOMXpath
     */
    private $_xpath;


    /**
     * @param BaseEntity $entity
     *
     * @return string
     */
    abstract function createUrl(BaseEntity $entity);

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->_html;
    }

    /**
     * @param string $html
     */
    public function setHtml($html)
    {
        $this->_html = $html;
        $this->_dom = null;
        $this->_xpath = null;
    }

    /**
     * Repair html string
     */
    public function tidyHtml()
    {
        $tidy = new \tidy();
        $tidy->parseString($this->getHtml(), ['wrap' => 0], 'utf8');
        $tidy->cleanRepair();

        $html = (string) $tidy;
        $this->setHtml($html);
    }

    /**
     * @return DomDocument
     */
    public function getDOM()
    {
        if ($this->_dom === null) {
            libxml_use_internal_errors(true); // suppress warnings

            $html = $this->getHtml();
            $html = mb_convert_encoding($html, 'utf-8', mb_detect_encoding($html));
            $html = mb_convert_encoding($html, 'html-entities', 'utf-8');

            $this->_dom = new DomDocument('1.0', 'utf-8');

            if ($html) {
                $this->_dom->loadHTML($html);
            }
        }

        return $this->_dom;
    }

    /**
     * @return DOMXPath
     */
    public function getXpath()
    {
        if ($this->_xpath === null) {
            $this->_xpath = new DOMXPath($this->getDOM());
        }

        return $this->_xpath;
    }

    /**
     * @param string  $xpathQuery
     * @param DOMNode $contextNode
     *
     * @return mixed
     */
    public function getNodeValue($xpathQuery, $contextNode = null)
    {
        if ($nodes = $this->getXpath()->query($xpathQuery, $contextNode)) {
            if ($nodes->length) {
                $value = $nodes->item(0)->nodeValue;
                return $this->filterString($value);
            }
        }

        return null;
    }

    /**
     * @param string  $xpathQuery
     * @param string  $attributeName
     * @param DOMNode $contextNode
     *
     * @return mixed
     */
    public function getNodeAttribute($xpathQuery, $attributeName, $contextNode = null)
    {
        if ($nodes = $this->getXpath()->query($xpathQuery, $contextNode)) {
            if ($nodes->length) {
                foreach ($nodes->item(0)->attributes as $attr) {
                    if ($attr->name === $attributeName) {
                        return $this->filterString($attr->value);
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param string  $pattern
     * @param integer $index
     *
     * @return mixed
     */
    public function getRegexMatch($pattern, $index = 0)
    {
        if (preg_match($pattern, $this->getHtml(), $matches)) {
            return $this->filterString($matches[$index]);
        }
        return null;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function filterString($value)
    {
        return trim(strip_tags($value));
    }

    /**
     * @param string $value
     *
     * @return integer|float
     */
    public function filterNumber($value)
    {
        $value = $this->filterString($value);
        return preg_replace('/[^0-9.]/', '', $value);
    }
}