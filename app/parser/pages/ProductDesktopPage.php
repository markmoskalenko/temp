<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\ProductEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductDesktopPage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof ProductEntity) {
            return 'http://www.aliexpress.com/item//' . $entity->getId() . '.html';
        } else {
            throw new InvalidParamException('Entity must be instance of Product');
        }
    }

    /**
     * @inheritdoc
     */
    public function getAttributes()
    {
        return [
            'title' => $this->getTitle(),
            'ratingValue' => $this->getRatingValue(),
            'reviewCount' => $this->getReviewCount(),
            'purchasesCount' => $this->getPurchasesCount(),
        ];
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->getRegexMatch('/window\.runParams\.adminSeq="(\d+)"/', 1);
    }

    /**
     * @return string
     */
    public function getStoreId()
    {
        return $this->getNodeAttribute('//*[@id="hid_storeId"]', 'value');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getNodeValue('//*[@class="product-name"]');
    }

    /**
     * @return float
     */
    public function getRatingValue()
    {
        return (float) $this->getNodeValue('//*[@id="product-star"]//span[@itemprop="ratingValue"]');
    }

    /**
     * @return integer
     */
    public function getReviewCount()
    {
        return (int) $this->getNodeValue('//*[@id="product-star"]//span[@itemprop="reviewCount"]');
    }

    /**
     * @return integer
     */
    public function getPurchasesCount()
    {
        return (int) $this->getNodeValue('//span[@class="orders-count"]/b');
    }
}