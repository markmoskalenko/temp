<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\ProductEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductMobilePage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof ProductEntity) {
            return 'http://m.aliexpress.com/item/' . $entity->getId() . '.html?site=glo';
        } else {
            throw new InvalidParamException('Entity must be instance of Product');
        }
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->getNodeAttribute('//*[@id="buy-now-form"]/input[@name="sellerAdminSeq"]', 'value');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getNodeValue('//*[@class="product-name"]');
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        $price = $this->getNodeValue('//*[@id="sku_price"]');
        return $this->filterNumber($price);
    }

    /**
     * @return float
     */
    public function getRatingValue()
    {
        return (float) $this->getNodeValue('//*[@id="MTK"]//span[@itemprop="ratingValue"]');
    }

    /**
     * @return integer
     */
    public function getReviewCount()
    {
        return (int) $this->getNodeValue('//*[@id="MTK"]//span[@itemprop="ratingCount"]');
    }

    /**
     * @return integer
     */
    public function getPurchasesCount()
    {
        return (int) $this->getNodeValue('//*[@id="MTK"]//span[@class="pr-order-count"]');
    }

    /**
     * @return array
     */
    public function getImages()
    {
        $images = [];

        /* @var $nodes \DOMElement[] */
        $nodes = $this->getXpath()->query('//*[@id="product-images"]//*[@class="item"]/img');
        foreach ($nodes as $node) {
            if ($src = $node->getAttribute('image-src')) {
                $images[] = $this->filterString($src);
            } elseif ($src = $node->getAttribute('src')) {
                $images[] = $this->filterString($src);
            }
        }

        return $images;
    }
}