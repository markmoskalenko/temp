<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\StoreEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreSalePage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof StoreEntity) {
            return 'http://ru.aliexpress.com/store/sale-items/' . $entity->getId() . '.html';
        } else {
            throw new InvalidParamException('Entity must be instance of Store');
        }
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->getRegexMatch('/ownerMemberId: \'(\d+)\'/', 1);
    }

    /**
     * @return integer
     */
    public function getSaleItemsCount()
    {
        $value = $this->getNodeValue('//*[@id="result-info"]/strong');
        return $this->filterNumber($value);
    }
}