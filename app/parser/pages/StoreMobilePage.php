<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\SellerEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreMobilePage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof SellerEntity) {
            return 'http://m.aliexpress.com/store/storeHome.htm?sellerAdminSeq=' . $entity->getId();
        } else {
            throw new InvalidParamException('Entity must be instance of Seller');
        }
    }

    /**
     * @return integer
     */
    public function getSaleItemsCount()
    {
        $value = $this->getNodeValue('(//*[@class="ms-store-title-num"])[4]');
        return $this->filterNumber($value);
    }
}
