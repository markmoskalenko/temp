<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\SellerEntity;
use DateTime;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class FeedbackPage extends BasePage
{
    const DETAILED_CATEGORY_ITEM_AS_DESCRIBED = 0;
    const DETAILED_CATEGORY_COMMUNICATION = 1;
    const DETAILED_CATEGORY_SHIPPING_SPEED = 2;

    const DETAILED_LABEL_RATING_VALUE = './/span[@class="dsr-text"]/em';
    const DETAILED_LABEL_REVIEW_COUNT = './/span[@class="dsr-text"]/span';
    const DETAILED_LABEL_RATING_COMPARING = './/*[@class="compare-info"]/em';

    const HISTORY_CATEGORY_POSITIVE = 1;
    const HISTORY_CATEGORY_NEUTRAL = 2;
    const HISTORY_CATEGORY_NEGATIVE = 3;
    const HISTORY_CATEGORY_POSITIVE_RATE = 4;

    const HISTORY_PERIOD_MONTH = 0;
    const HISTORY_PERIOD_3_MONTHS = 1;
    const HISTORY_PERIOD_6_MONTHS = 2;
    const HISTORY_PERIOD_12_MONTHS = 3;
    const HISTORY_PERIOD_OVERALL = 4;

    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof SellerEntity) {
            return 'http://feedback.aliexpress.com/display/evaluationDetail.htm?ownerMemberId='.$entity->getId().'&memberType=seller';
        } else {
            throw new InvalidParamException('Entity must be instance of Seller');
        }
    }

    /**
     * @return integer
     */
    public function getSellerId()
    {
        $sellerId = $this->getNodeAttribute('//*[@id="d-dispatcher-form"]//input[@name="ownerMemberId"]', 'value');
        return $this->filterNumber($sellerId);
    }

    /**
     * @return integer
     */
    public function getStoreId()
    {
        $storeId = $this->getRegexMatch('/http:\/\/www\.aliexpress\.com\/store\/(\d+)/', 1);
        return $this->filterNumber($storeId);
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->getNodeValue('//*[@id="feedback-summary"]/div[2]/table/tbody/tr[1]/td/a');
    }

    /**
     * @return integer
     */
    public function getScore()
    {
        $score = $this->getNodeValue('//*[@id="feedback-summary"]//table[contains(@class,"summary-tb")]/tbody/tr[3]/td/span[1]');
        return $this->filterNumber($score);
    }

    /**
     * @return string
     */
    public function getScoreIcon()
    {
        return $this->getRegexMatch('/feedback\/icon\/([a-z0-9-]+\.gif)/', 1);
    }

    /**
     * @return DateTime
     */
    public function getRegisterDate()
    {
        $date = $this->getNodeValue('//*[@id="feedback-summary"]//table[contains(@class,"summary-tb")]/tbody/tr[4]/td');
        return new DateTime($date);
    }

    /**
     * @param mixed $category
     * @param mixed $label
     *
     * @return float|integer
     */
    public function getDetailed($category, $label)
    {
        $rows = $this->getXpath()->query('//*[@id="feedback-dsr"]//table/tbody/tr');
        $row = $rows->item($category);

        $value = $this->getNodeValue($label, $row);
        $value = (float) $this->filterNumber($value);

        if ($label === static::DETAILED_LABEL_RATING_COMPARING) {
            if ($this->getNodeAttribute($label, 'class', $row) === 'compare-low') {
                return -$value;
            }
        }

        return $value;
    }

    /**
     * @param mixed $category
     * @param mixed $period
     *
     * @return float
     */
    public function getHistory($category, $period)
    {
        $rows = $this->getXpath()->query('//*[@id="feedback-history"]//table/tbody/tr');
        $row = $rows->item($category);
        $columns = $this->getXpath()->query('./td', $row);
        $column = $columns->item($period);
        return $this->filterNumber($column->nodeValue);
    }
}