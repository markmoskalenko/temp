<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\StoreEntity;
use DOMElement;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreContactsPage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof StoreEntity) {
            return 'http://ru.aliexpress.com/store/contactinfo/' . $entity->getId() . '.html';
        } else {
            throw new InvalidParamException('Entity must be instance of Store');
        }
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->getRegexMatch('/ownerMemberId: \'(\d+)\'/', 1);
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//span[@class="contactName"]');
    }

    /**
     * @return string
     */
    public function getStoreId()
    {
        return $this->getRegexMatch('/storeId: \'(\d+)\'/', 1);
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->getNodeValue('//*[@id="hd"]//*[@class="shop-name"]');
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//table//th[contains(.,"Страна")]/following-sibling::td');
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//table//th[contains(.,"Провинция")]/following-sibling::td');
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//table//th[contains(.,"Город")]/following-sibling::td');
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//table//th[contains(.,"Адрес")]/following-sibling::td');
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->getNodeValue('//*[@id="node-contacts"]//table//th[contains(.,"Почтовый индекс")]/following-sibling::td');
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];

        /* @var DOMElement[] $rows */
        $rows = $this->getXpath()->query('//*[@id="node-contacts"]//tr[@class="divide-line"]/following-sibling::tr');

        foreach ($rows as $row) {
            $label = $this->getNodeValue('./th', $row);
            $label = rtrim($label, ':');

            $value = $this->getNodeValue('./td', $row);

            if ($label && $value) {
                $data[ $label ] = $value;
            }
        }

        return $data;
    }
}