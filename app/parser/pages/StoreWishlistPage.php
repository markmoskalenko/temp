<?php
namespace app\parser\pages;

use app\parser\entities\BaseEntity;
use app\parser\entities\SellerEntity;
use yii\base\InvalidParamException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreWishlistPage extends BasePage
{
    /**
     * @inheritdoc
     */
    public function createUrl(BaseEntity $entity)
    {
        if ($entity instanceof SellerEntity) {
            return 'http://us.ae.aliexpress.com/wishlist/wishlist_shop_count.htm?itemtype=store&itemid=' . $entity->getId();
        } else {
            throw new InvalidParamException('Entity must be instance of Seller');
        }
    }

    /**
     * @return integer
     */
    public function getLikesCount()
    {
        $object = $this->getRegexMatch('/^window\.collectNum={(.*?)}$/', 1);
        $object = '{' . $object . '}';

        if ($data = json_decode($object, true)) {
            if (isset($data['num'])) {
                return $this->filterNumber($data['num']);
            }
        }

        return null;
    }
}