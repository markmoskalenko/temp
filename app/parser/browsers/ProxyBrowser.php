<?php
namespace app\parser\browsers;

use app\db\records\Proxy;
use Yii;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProxyBrowser extends CurlBrowser
{
    /**
     * @inheritdoc
     */
    public function getHtml()
    {
        $proxy = $this->getValidProxy();

        if ($proxy) {
            $this->markProxyUsed($proxy);
            $this->setProxy($proxy->ip, $proxy->port);
            $this->setProxyAuth(Yii::$app->params['proxies.login'] . ':' . Yii::$app->params['proxies.password']);
        }

        $this->log('Get URL: %s', $this->getURL());

        $content = parent::getHtml();

        if ( ! $content) {
            $this->log('No content');
        }

        if ($error = curl_error($this->ch)) {
            $this->log('Curl error: %s', $error);
        }

        if ($proxy) {
            if (empty($content) || $this->isProxyFailed()) {
                $this->markProxyInvalid($proxy);
                $this->log('Proxy %s invalid', $proxy->ip);
                return false;
            }
        }

        return $content;
    }

    /**
     * @return Proxy|null
     */
    protected function getValidProxy()
    {
        return Proxy::find()->prioritized()->one();
    }

    /**
     * @param Proxy $proxy
     */
    protected function markProxyUsed($proxy)
    {
        $proxy->updateCounters(['requests' => 1]);
    }

    /**
     * @param Proxy $proxy
     */
    protected function markProxyInvalid($proxy)
    {
        $proxy->updateAttributes(['isValid' => 0]);
        $proxy->touch('timeFailed');
    }

    /**
     * @return boolean
     */
    protected function isProxyFailed()
    {
        return $this->isTimeout() || $this->isForbidden();
    }

    /**
     * @return boolean
     */
    protected function isTimeout()
    {
        if (curl_errno($this->ch) === CURLE_OPERATION_TIMEOUTED) {
            $this->log('Timeout.');
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    protected function isForbidden()
    {
        $effectiveUrl = curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL);
        $effectiveHost = parse_url($effectiveUrl, PHP_URL_HOST);

        if (strpos($effectiveHost, 'sec') !== false) {
            $this->log('Security page.');
            return true;
        }

        if (strpos($effectiveHost, 'login') !== false) {
            $this->log('Login page.');
            return true;
        }

        return false;
    }

    /**
     * Formats log.
     */
    protected function log()
    {
        $message = call_user_func_array('sprintf', func_get_args());

        $hash = md5($this->getURL());
        $hash = substr($hash, 10);

        Yii::info($hash . '. ' . $message, __CLASS__);
    }
}