<?php
namespace app\parser\browsers;

use app\interfaces\Browser;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class CurlBrowser implements Browser
{
    /**
     * @var resource
     */
    protected $ch;

    /**
     * @var string
     */
    protected $url;


    /**
     * @param string $url
     */
    public function __construct($url = null)
    {
        $this->ch = curl_init();

        $this->setURL($url);

        $this->setOpt(CURLOPT_HEADER, false);
        $this->setOpt(CURLOPT_RETURNTRANSFER, true);
        $this->setOpt(CURLOPT_FOLLOWLOCATION, true);
    }

    /**
     * @param integer $option
     * @return mixed
     */
    public function getInfo($option = 0)
    {
        return curl_getinfo($this->ch, $option);
    }

    /**
     * @param integer $option
     * @param mixed   $value
     */
    public function setOpt($option, $value)
    {
        curl_setopt($this->ch, $option, $value);
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setURL($url)
    {
        $this->url = $url;
        $this->setOpt(CURLOPT_URL, $url);
    }

    /**
     * @param string $ip
     * @param string $port
     */
    public function setProxy($ip, $port)
    {
        $this->setOpt(CURLOPT_PROXY, $ip);
        $this->setOpt(CURLOPT_PROXYPORT, $port);
    }

    /**
     * @param string $userpwd
     * @param mixed  $type
     */
    public function setProxyAuth($userpwd, $type = CURLAUTH_BASIC)
    {
        $this->setOpt(CURLOPT_PROXYUSERPWD, $userpwd);
        $this->setOpt(CURLOPT_PROXYAUTH, $type);
    }

    /**
     * @param string $agent
     */
    public function setUserAgent($agent)
    {
        $this->setOpt(CURLOPT_USERAGENT, $agent);
    }

    /**
     * @param string $url
     */
    public function setReferrer($url)
    {
        $this->setOpt(CURLOPT_REFERER, $url);
    }

    /**
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->getInfo(CURLINFO_HTTP_CODE);
    }

    /**
     * @inheritdoc
     */
    public function getHtml()
    {
        return curl_exec($this->ch);
    }

    /**
     * Close a cURL session
     */
    public function close()
    {
        curl_close($this->ch);
    }
}