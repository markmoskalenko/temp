<?php
namespace app\parser\browsers;

use app\interfaces\Browser;

/**
 * Получает данные через file_get_contents().
 *
 * @author Artem Belov <razor2909@gmail.com>
 */
class SimpleBrowser implements Browser
{
    private $filename;


    /**
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @inheritdoc
     */
    public function getHtml()
    {
        return file_get_contents($this->filename);
    }
}