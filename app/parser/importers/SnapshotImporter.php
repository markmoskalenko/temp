<?php
namespace app\parser\importers;

use app\db\records\Snapshot as SnapshotRecord;
use app\db\records\Snapshot;
use app\parser\entities\SnapshotEntity;
use app\parser\pages\SnapshotPage;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotImporter extends Model
{
    public $storeId;
    public $storeName;
    public $productId;
    public $productName;
    public $imageUrl;
    public $price;
    public $unit;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['storeId', 'integer'],
            ['storeId', 'default', 'value' => null],

            ['storeName', 'string'],
            ['storeName', 'trim'],
            ['storeName', 'default', 'value' => null],

            ['productId', 'integer'],
            ['productId', 'default', 'value' => null],

            ['productName', 'string'],
            ['productName', 'trim'],
            ['productName', 'default', 'value' => null],

            ['imageUrl', 'string'],
            ['imageUrl', 'default', 'value' => null],

            ['price', 'number'],
            ['price', 'default', 'value' => null],

            ['unit', 'string'],
            ['unit', 'trim'],
            ['unit', 'default', 'value' => null],
        ];
    }

    /**
     * @param SnapshotRecord $snapshot
     * @return SnapshotEntity
     */
    public function createSnapshotEntity(SnapshotRecord $snapshot)
    {
        return new SnapshotEntity($snapshot->id);
    }

    /**
     * @param SnapshotPage $page
     */
    public function importSnapshotPage(SnapshotPage $page)
    {
        $this->storeId = $page->getStoreId();
        $this->storeName = $page->getStoreName();
        $this->productId = $page->getProductId();
        $this->productName = $page->getProductName();
        $this->imageUrl = $page->getMainImage();
        $this->price = $page->getPrice();
        $this->unit = $page->getUnit();
    }

    /**
     * @param Snapshot $snapshot
     * @param boolean  $runValidation
     * @return boolean
     */
    public function save(Snapshot $snapshot, $runValidation = true)
    {
        if ($runValidation && ! $this->validate()) {
            return false;
        }

        $snapshot->setAttributes($this->getAttributes(), false);
        $snapshot->saveOrFail();

        return true;
    }
}
