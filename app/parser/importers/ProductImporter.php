<?php
namespace app\parser\importers;

use app\db\records\Product;
use app\parser\entities\ProductEntity;
use app\parser\pages\ProductMobilePage;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductImporter extends Model
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var float
     */
    public $price;
    /**
     * @var string
     */
    public $imageUrl;
    /**
     * @var float
     */
    public $ratingValue;
    /**
     * @var integer
     */
    public $reviewCount;
    /**
     * @var integer
     */
    public $purchasesCount;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string'],
            ['title', 'trim'],
            ['title', 'default', 'value' => null],

            ['price', 'number'],
            ['price', 'default', 'value' => null],

            ['imageUrl', 'string'],
            ['imageUrl', 'trim'],
            ['imageUrl', 'default', 'value' => null],

            ['ratingValue', 'number'],
            ['ratingValue', 'default', 'value' => null],

            ['reviewCount', 'number'],
            ['reviewCount', 'default', 'value' => null],

            ['purchasesCount', 'number'],
            ['purchasesCount', 'default', 'value' => null],
        ];
    }

    /**
     * @param Product $product
     * @return ProductEntity
     */
    public static function createProductEntity(Product $product)
    {
        return new ProductEntity($product->originalProductId);
    }

    /**
     * @param ProductMobilePage $page
     */
    public function loadProductMobilePage(ProductMobilePage $page)
    {
        $this->title = $page->getTitle();
        $this->price = $page->getPrice();
        $this->ratingValue = $page->getRatingValue();
        $this->reviewCount = $page->getReviewCount();
        $this->purchasesCount = $page->getPurchasesCount();

        if ($images = $page->getImages()) {
            $this->imageUrl = $images[0];
        }
    }

    /**
     * @param Product $record
     * @return boolean
     */
    public function save(Product $record)
    {
        if ($this->validate()) {
            $record->setAttributes($this->getAttributes(), false);
            return $record->save();
        }
        return false;
    }
}