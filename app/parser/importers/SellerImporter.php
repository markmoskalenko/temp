<?php
namespace app\parser\importers;

use app\db\records\Seller;
use app\parser\entities\SellerEntity;
use app\parser\entities\StoreEntity;
use app\parser\pages\FeedbackPage;
use app\parser\pages\StoreContactsPage;
use app\parser\pages\StoreMobilePage;
use app\parser\pages\StoreSalePage;
use app\parser\pages\StoreWishlistPage;
use Yii;
use yii\base\Model;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerImporter extends Model
{
    public $storeName;
    public $originalSellerId;
    public $sellerName;
    public $score;
    public $scoreIcon;
    public $likesCount;
    public $saleItemsCount;
    public $registerDate;
    public $country;
    public $region;
    public $city;
    public $address;
    public $zipCode;

    /**
     * @var RatingImporter
     */
    private $ratingModel;

    /**
     * @param Seller $record
     *
     * @return SellerEntity
     */
    public static function createSellerEntity(Seller $record)
    {
        return new SellerEntity($record->originalSellerId);
    }

    /**
     * @param Seller $record
     *
     * @return StoreEntity
     */
    public static function createStoreEntity(Seller $record)
    {
        return new StoreEntity($record->originalStoreId);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->ratingModel = new RatingImporter();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['originalSellerId', 'integer'],
            ['score', 'integer'],
            ['likesCount', 'integer'],
            ['saleItemsCount', 'integer'],

            [['storeName', 'sellerName', 'scoreIcon', 'country', 'region', 'city', 'address', 'zipCode'], 'string'],
            [['storeName', 'sellerName', 'scoreIcon', 'country', 'region', 'city', 'address', 'zipCode'], 'trim'],
            [['storeName', 'sellerName', 'scoreIcon', 'country', 'region', 'city', 'address', 'zipCode'], 'default'],

            ['registerDate', 'string'],
        ];
    }

    /**
     * @param StoreContactsPage $page
     */
    public function loadContactsPage(StoreContactsPage $page)
    {
        $this->originalSellerId = $page->getSellerId();
        $this->sellerName = $page->getSellerName();
        $this->country = $page->getCountry();
        $this->region = $page->getRegion();
        $this->city = $page->getCity();
        $this->address = $page->getAddress();
        $this->zipCode = $page->getZipCode();
    }

    /**
     * @param StoreSalePage $page
     */
    public function loadSalePage(StoreSalePage $page)
    {
        $this->saleItemsCount = $page->getSaleItemsCount();
    }

    /**
     * @param StoreMobilePage $page
     */
    public function loadStoreMobilePage(StoreMobilePage $page)
    {
        $this->saleItemsCount = $page->getSaleItemsCount();
    }

    /**
     * @param FeedbackPage $page
     */
    public function loadFeedbackPage(FeedbackPage $page)
    {
        $this->storeName = $page->getStoreName();
        $this->score = $page->getScore();
        $this->scoreIcon = $page->getScoreIcon();

        if ($registerDate = $page->getRegisterDate()) {
            $this->registerDate = $registerDate->format('Y-m-d');
        }

        $this->ratingModel->loadFeedbackPage($page);
    }

    /**
     * @param StoreWishlistPage $page
     */
    public function loadWishlistPage(StoreWishlistPage $page)
    {
        $this->likesCount = $page->getLikesCount();
    }

    /**
     * @param Seller $record
     *
     * @return boolean
     */
    public function save(Seller $record)
    {
        if ($this->validate() && $this->ratingModel->validate()) {
            $record->setAttributes($this->getAttributes(), false);
            $record->setAttributes($this->ratingModel->getAttributes(), false);
            return $record->save();
        }
        return false;
    }
}