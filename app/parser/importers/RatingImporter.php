<?php
namespace app\parser\importers;

use app\behaviors\SellerRatingBehavior;
use app\db\records\Seller;
use app\parser\pages\FeedbackPage;
use yii\base\DynamicModel;

/**
 * @mixin SellerRatingBehavior
 * @author Artem Belov <razor2909@gmail.com>
 */
class RatingImporter extends DynamicModel
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SellerRatingBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        foreach (array_keys($this->detailedRatingCategories()) as $category) {
            foreach (array_keys($this->detailedRatingLabels()) as $label) {
                $attribute = $this->detailedAttribute($category, $label);
                $this->defineAttribute($attribute);
            }
        }

        foreach (array_keys($this->ratingHistoryCategories()) as $category) {
            foreach (array_keys($this->ratingHistoryPeriods()) as $period) {
                $attribute = $this->historyAttribute($category, $period);
                $this->defineAttribute($attribute);
            }
        }
    }

    /**
     * @param FeedbackPage $page
     */
    public function loadFeedbackPage(FeedbackPage $page)
    {
        foreach ($this->detailedRatingCategories() as $recordCategory => $pageCategory) {
            foreach ($this->detailedRatingLabels() as $recordLabel => $pageLabel) {
                $this->setDetailed($recordCategory, $recordLabel, $page->getDetailed($pageCategory, $pageLabel));
            }
        }

        foreach ($this->ratingHistoryCategories() as $recordCategory => $pageCategory) {
            foreach ($this->ratingHistoryPeriods() as $recordPeriod => $pagePeriod) {
                $this->setHistory($recordCategory, $recordPeriod, $page->getHistory($pageCategory, $pagePeriod));
            }
        }
    }

    /**
     * @return array
     */
    public function detailedRatingCategories()
    {
        return [
            Seller::CATEGORY_ITEM_AS_DESCRIBED => FeedbackPage::DETAILED_CATEGORY_ITEM_AS_DESCRIBED,
            Seller::CATEGORY_COMMUNICATION     => FeedbackPage::DETAILED_CATEGORY_COMMUNICATION,
            Seller::CATEGORY_SHIPPING_SPEED    => FeedbackPage::DETAILED_CATEGORY_SHIPPING_SPEED,
        ];
    }

    /**
     * @return array
     */
    public function detailedRatingLabels()
    {
        return [
            Seller::LABEL_VALUE        => FeedbackPage::DETAILED_LABEL_RATING_VALUE,
            Seller::LABEL_REVIEW_COUNT => FeedbackPage::DETAILED_LABEL_REVIEW_COUNT,
            Seller::LABEL_COMPARING    => FeedbackPage::DETAILED_LABEL_RATING_COMPARING,
        ];
    }

    /**
     * @return array
     */
    public function ratingHistoryCategories()
    {
        return [
            Seller::HISTORY_POSITIVE      => FeedbackPage::HISTORY_CATEGORY_POSITIVE,
            Seller::HISTORY_NEUTRAL       => FeedbackPage::HISTORY_CATEGORY_NEUTRAL,
            Seller::HISTORY_NEGATIVE      => FeedbackPage::HISTORY_CATEGORY_NEGATIVE,
            Seller::HISTORY_POSITIVE_RATE => FeedbackPage::HISTORY_CATEGORY_POSITIVE_RATE,
        ];
    }

    /**
     * @return array
     */
    public function ratingHistoryPeriods()
    {
        return [
            Seller::PERIOD_MONTH     => FeedbackPage::HISTORY_PERIOD_MONTH,
            Seller::PERIOD_3_MONTHS  => FeedbackPage::HISTORY_PERIOD_3_MONTHS,
            Seller::PERIOD_6_MONTHS  => FeedbackPage::HISTORY_PERIOD_6_MONTHS,
            Seller::PERIOD_12_MONTHS => FeedbackPage::HISTORY_PERIOD_12_MONTHS,
            Seller::PERIOD_OVERALL   => FeedbackPage::HISTORY_PERIOD_OVERALL,
        ];
    }
}