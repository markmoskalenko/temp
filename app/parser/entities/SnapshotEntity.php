<?php
namespace app\parser\entities;

use app\parser\pages\SnapshotPage;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotEntity extends BaseEntity
{
    /**
     * @return SnapshotPage
     */
    public function downloadSnapshotPage()
    {
        return $this->downloadPage(SnapshotPage::className());
    }
}