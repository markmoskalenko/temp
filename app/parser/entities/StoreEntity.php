<?php
namespace app\parser\entities;

use app\parser\pages\StoreContactsPage;
use app\parser\pages\StoreHomePage;
use app\parser\pages\StoreSalePage;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreEntity extends BaseEntity
{
    /**
     * @return StoreHomePage
     */
    public function downloadHomePage()
    {
        return $this->downloadPage(StoreHomePage::className());
    }

    /**
     * @return StoreSalePage
     */
    public function downloadSalePage()
    {
        return $this->downloadPage(StoreSalePage::className());
    }

    /**
     * @return StoreContactsPage
     */
    public function downloadContactsPage()
    {
        return $this->downloadPage(StoreContactsPage::className());
    }
}