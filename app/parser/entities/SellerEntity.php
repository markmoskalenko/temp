<?php
namespace app\parser\entities;

use app\parser\pages\FeedbackPage;
use app\parser\pages\StoreMobilePage;
use app\parser\pages\StoreWishlistPage;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SellerEntity extends BaseEntity
{
    /**
     * @return FeedbackPage
     */
    public function downloadFeedbackPage()
    {
        return $this->downloadPage(FeedbackPage::className());
    }

    /**
     * @return StoreMobilePage
     */
    public function downloadStoreMobilePage()
    {
        return $this->downloadPage(StoreMobilePage::className());
    }

    /**
     * @return StoreWishlistPage
     */
    public function downloadWishlistPage()
    {
        return $this->downloadPage(StoreWishlistPage::className());
    }
}