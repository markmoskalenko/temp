<?php
namespace app\parser\entities;

use app\parser\pages\ProductDesktopPage;
use app\parser\pages\ProductMobilePage;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductEntity extends BaseEntity
{
    /**
     * @return ProductDesktopPage
     */
    public function downloadDesktopPage()
    {
        return $this->downloadPage(ProductDesktopPage::className());
    }

    /**
     * @return ProductMobilePage
     */
    public function downloadMobilePage()
    {
        return $this->downloadPage(ProductMobilePage::className());
    }
}