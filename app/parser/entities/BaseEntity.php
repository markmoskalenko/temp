<?php
namespace app\parser\entities;

use app\parser\browsers\ProxyBrowser;
use app\parser\HttpException;
use app\parser\NotFoundHttpException;
use app\parser\pages\BasePage;
use Yii;
use yii\base\Object;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseEntity extends Object
{
    /**
     * @var string
     */
    private $_id;


    /**
     * @param string $id
     */
    public function __construct($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $className
     *
     * @return BasePage
     */
    protected function downloadPage($className)
    {
        /* @var BasePage $page */
        $page = new $className;
        $page->setHtml($this->downloadHtml($page->createUrl($this)));
        return $page;
    }

    /**
     * @param string $url
     *
     * @throws HttpException
     *
     * @return string
     */
    protected function downloadHtml($url)
    {
        $browser = new ProxyBrowser($url);
        $browser->setUserAgent('Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 8.0; Win64; x64; Trident/5.0; .NET4.0E; en)');

        // error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure
        $browser->setOpt(CURLOPT_SSL_CIPHER_LIST, 'SSLv3');

        $attempts = 0;

        do {
            $attempts++;

            $html = $browser->getHtml();
            $statusCode = $browser->getStatusCode();

            if ( ! $html) {
                continue;
            }

            if (in_array($statusCode, [200, 404])) {
                break;
            }
        } while ($attempts < 5);

        $browser->close();

        if ($statusCode === 200) {
            return $html;
        } elseif ($statusCode === 404) {
            throw new NotFoundHttpException($url);
        } else {
            throw new HttpException($statusCode, $url);
        }
    }
}