<?php
namespace app\parser;

use app\parser\entities\BaseEntity;
use app\parser\entities\ProductEntity;
use app\parser\entities\SnapshotEntity;
use app\parser\entities\StoreEntity;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class UrlParser
{
    /**
     * @param string $url
     * @return BaseEntity|null
     */
    public function process($url)
    {
        $segments = parse_url($url);

        if ( ! isset($segments['host'], $segments['path'])) {
            return null;
        }

        if ( ! $this->isHostValid($segments['host'])) {
            return null;
        }

        if ($entity = $this->isProductPath($segments['path'], $segments['host'])) {
            return $entity;
        } elseif ($entity = $this->isSnapshotPath($segments['path'])) {
            return $entity;
        } elseif ($entity = $this->isStorePath($segments['path'])) {
            return $entity;
        } else {
            return null;
        }
    }

    /**
     * @param string $host
     * @return boolean
     */
    public function isHostValid($host)
    {
        return (bool) preg_match('/^([a-z]+\.|)aliexpress\.com$/', $host, $matches);
    }

    /**
     * @param string $path
     * @param string $host
     * @return string|null product identifier
     */
    public function isProductPath($path, $host)
    {
        if ($host === 'group.aliexpress.com' && preg_match('/^\/(\d+)-(\d+)-detail\.html$/', $path, $matches)) {
            return new ProductEntity($matches[2]);
        } elseif (preg_match('/^\/item\/(\d+)\.html$/', $path, $matches)) {
            return new ProductEntity($matches[1]);
        } elseif (preg_match('/^\/item\/([\w-]*)\/(\d+)\.html$/', $path, $matches)) {
            return new ProductEntity($matches[2]);
        } elseif (preg_match('/^\/store\/product\/([\w-]+)\/(\d+)_(\d+)\.html$/', $path, $matches)) {
            return new ProductEntity($matches[3]);
        } else {
            return null;
        }
    }

    /**
     * @param string $path
     * @return string|null snapshot identifier
     */
    public function isSnapshotPath($path)
    {
        if (preg_match('/^\/snapshot\/(\d+)\.html$/', $path, $matches)) {
            return new SnapshotEntity($matches[1]);
        } else {
            return null;
        }
    }

    /**
     * @param string $path
     * @return string|null store identifier
     */
    public function isStorePath($path)
    {
        if (preg_match('/^\/store\/(\d+)$/', $path, $matches)) {
            return new StoreEntity($matches[1]);
        } elseif (preg_match('/^\/store\/([\w-]+)\/(\d+)\.html$/', $path, $matches)) {
            return new StoreEntity($matches[2]);
        } else {
            return null;
        }
    }
}