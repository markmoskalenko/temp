<?php
namespace app\parser\handlers;

use app\parser\entities\SellerEntity;
use app\parser\entities\StoreEntity;
use yii\base\ErrorException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class StoreHandler extends BaseHandler
{
    /**
     * @param StoreEntity $storeEntity
     *
     * @throws ErrorException
     */
    public function handleStoreEntity(StoreEntity $storeEntity)
    {
        if (null === ($sellerRecord = $this->findSellerRecordByStore($storeEntity))) {
            $sellerRecord = $this->createSellerRecord($storeEntity);

            $contactsPage = $storeEntity->downloadContactsPage();
            $sellerEntity = new SellerEntity($contactsPage->getSellerId());

            $this->importSellerRecord(
                $sellerRecord,
                $contactsPage,
                $sellerEntity->downloadStoreMobilePage(),
                $sellerEntity->downloadFeedbackPage(),
                $sellerEntity->downloadWishlistPage()
            );
        }

        $this->sellerRecord = $sellerRecord;
    }

}