<?php
namespace app\parser\handlers;

use app\db\records\Product as ProductRecord;
use app\db\records\Seller as SellerRecord;
use app\db\records\Snapshot as SnapshotRecord;
use app\parser\entities\BaseEntity;
use app\parser\entities\ProductEntity;
use app\parser\entities\SellerEntity;
use app\parser\entities\SnapshotEntity;
use app\parser\entities\StoreEntity;
use app\parser\importers\ProductImporter;
use app\parser\importers\SellerImporter;
use app\parser\importers\SnapshotImporter;
use app\parser\pages\FeedbackPage;
use app\parser\pages\ProductMobilePage;
use app\parser\pages\SnapshotPage;
use app\parser\pages\StoreContactsPage;
use app\parser\pages\StoreMobilePage;
use app\parser\pages\StoreSalePage;
use app\parser\pages\StoreWishlistPage;
use yii\base\ErrorException;
use yii\base\NotSupportedException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
abstract class BaseHandler
{
    /**
     * @var ProductRecord
     */
    protected $productRecord;
    /**
     * @var SellerRecord
     */
    protected $sellerRecord;
    /**
     * @var SnapshotRecord
     */
    protected $snapshotRecord;


    /**
     * @param BaseEntity $entity
     * @throws NotSupportedException
     * @return BaseHandler
     */
    public static function process(BaseEntity $entity)
    {
        if ($entity instanceof SnapshotEntity) {
            $handler = new SnapshotHandler();
            $handler->handleSnapshotEntity($entity);
        } elseif ($entity instanceof ProductEntity) {
            $handler = new ProductHandler();
            $handler->handleProductEntity($entity);
        } elseif ($entity instanceof StoreEntity) {
            $handler = new StoreHandler();
            $handler->handleStoreEntity($entity);
        } else {
            throw new NotSupportedException(sprintf('%s entity class is not supported', get_class($entity)));
        }

        return $handler;
    }

    /**
     * @return SellerRecord|null
     */
    public function getSellerRecord()
    {
        return $this->sellerRecord;
    }

    /**
     * @return ProductRecord|null
     */
    public function getProductRecord()
    {
        return $this->productRecord;
    }

    /**
     * @return SnapshotRecord|null
     */
    public function getSnapshotRecord()
    {
        return $this->snapshotRecord;
    }

    /**
     * @param SellerEntity $entity
     * @return SellerRecord|null
     */
    public function findSellerRecordBySeller(SellerEntity $entity)
    {
        if ($originalSellerId = $entity->getId()) {
            return SellerRecord::findOne(compact('originalSellerId'));
        }
        return null;
    }

    /**
     * @param StoreEntity $entity
     * @return SellerRecord|null
     */
    public function findSellerRecordByStore(StoreEntity $entity)
    {
        return SellerRecord::findOne([
            'originalStoreId' => $entity->getId(),
        ]);
    }

    /**
     * @param StoreEntity $storeEntity
     * @throws ErrorException
     * @return SellerRecord
     */
    public function createSellerRecord(StoreEntity $storeEntity)
    {
        $sellerRecord = new SellerRecord();
        $sellerRecord->originalStoreId = $storeEntity->getId();
        $sellerRecord->save();

        return $sellerRecord;
    }

    /**
     * @param SellerRecord      $sellerRecord
     * @param StoreContactsPage $contactsPage
     * @param StoreMobilePage   $storeMobilePage
     * @param FeedbackPage      $feedbackPage
     * @param StoreWishlistPage $wishlistPage
     */
    public function importSellerRecord(
        SellerRecord $sellerRecord,
        StoreContactsPage $contactsPage,
        StoreMobilePage $storeMobilePage,
        FeedbackPage $feedbackPage,
        StoreWishlistPage $wishlistPage
    ) {
        $importer = new SellerImporter();
        $importer->loadContactsPage($contactsPage);
        $importer->loadStoreMobilePage($storeMobilePage);
        $importer->loadFeedbackPage($feedbackPage);
        $importer->loadWishlistPage($wishlistPage);
        $importer->save($sellerRecord);
    }

    /**
     * @param ProductEntity $entity
     *
     * @return ProductRecord|null
     */
    public function findProductRecord(ProductEntity $entity)
    {
        return ProductRecord::find()
            ->with('seller')
            ->where(['originalProductId' => $entity->getId()])
            ->one();
    }

    /**
     * @param ProductEntity $productEntity
     * @param SellerRecord  $sellerRecord
     *
     * @throws ErrorException
     *
     * @return ProductRecord
     */
    public function createProductRecord(ProductEntity $productEntity, SellerRecord $sellerRecord)
    {
        $productRecord = new ProductRecord();
        $productRecord->originalProductId = $productEntity->getId();
        $productRecord->seller = $sellerRecord;
        $productRecord->save();

        return $productRecord;
    }

    /**
     * @param ProductRecord     $productRecord
     * @param ProductMobilePage $mobilePage
     */
    public function importProductRecord(ProductRecord $productRecord, ProductMobilePage $mobilePage)
    {
        $importer = new ProductImporter();
        $importer->loadProductMobilePage($mobilePage);
        $importer->save($productRecord);
    }

    /**
     * @param SnapshotEntity $entity
     * @return SnapshotRecord|null
     */
    public function findSnapshotRecord(SnapshotEntity $entity)
    {
        return SnapshotRecord::findOne([
            'snapshotId' => $entity->getId(),
        ]);
    }

    /**
     * @param SnapshotEntity $entity
     * @return SnapshotRecord
     */
    public function createSnapshotRecord(SnapshotEntity $entity)
    {
        $record = new SnapshotRecord();
        $record->snapshotId = $entity->getId();
        $record->save();

        return $record;
    }

    /**
     * @param SnapshotRecord $record
     * @param SnapshotPage   $snapshotPage
     */
    public function importSnapshotRecord(SnapshotRecord $record, SnapshotPage $snapshotPage)
    {
        $importer = new SnapshotImporter();
        $importer->importSnapshotPage($snapshotPage);
        $importer->save($record);
    }
}