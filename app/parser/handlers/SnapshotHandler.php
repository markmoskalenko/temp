<?php
namespace app\parser\handlers;

use app\parser\entities\ProductEntity;
use app\parser\entities\SellerEntity;
use app\parser\entities\SnapshotEntity;
use app\parser\entities\StoreEntity;
use yii\base\ErrorException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class SnapshotHandler extends BaseHandler
{
    /**
     * @param SnapshotEntity $snapshotEntity
     * @throws ErrorException
     */
    public function handleSnapshotEntity(SnapshotEntity $snapshotEntity)
    {
        $snapshotPage = $snapshotEntity->downloadSnapshotPage();

        if (null === ($snapshotRecord = $this->findSnapshotRecord($snapshotEntity))) {
            $snapshotRecord = $this->createSnapshotRecord($snapshotEntity);
            $this->importSnapshotRecord($snapshotRecord, $snapshotPage);
        }

        $productEntity = new ProductEntity($snapshotPage->getProductId());
        $storeEntity = new StoreEntity($snapshotPage->getStoreId());

        if ($productRecord = $this->findProductRecord($productEntity)) {
            $sellerRecord = $productRecord->seller;
        } else {
            if (null === ($sellerRecord = $this->findSellerRecordByStore($storeEntity))) {
                $sellerRecord = $this->createSellerRecord($storeEntity);
                $contactsPage = $storeEntity->downloadContactsPage();
                $sellerEntity = new SellerEntity($contactsPage->getSellerId());

                $this->importSellerRecord(
                    $sellerRecord,
                    $contactsPage,
                    $sellerEntity->downloadStoreMobilePage(),
                    $sellerEntity->downloadFeedbackPage(),
                    $sellerEntity->downloadWishlistPage()
                );
            }

            // $productRecord = $this->createProductRecord($productEntity, $sellerRecord);
            // $productMobilePage = $productEntity->downloadMobilePage();
            // $this->importProductRecord($productRecord, $productMobilePage);
        }

        $this->sellerRecord  = $sellerRecord;
        $this->productRecord = $productRecord;
        $this->snapshotRecord = $snapshotRecord;
    }

    /**
     * Просто метод обработки снапшота. Не включает в себя парсинг продавцов и товаров.
     *
     * Используется при добавлении хватов или отзывов о товаре, когда магазин или товар уже мог быть удалён.
     *
     * @param SnapshotEntity $snapshotEntity
     */
    public function parseSnapshotEntity(SnapshotEntity $snapshotEntity)
    {
        $snapshotPage = $snapshotEntity->downloadSnapshotPage();

        if (null === ($snapshotRecord = $this->findSnapshotRecord($snapshotEntity))) {
            $snapshotRecord = $this->createSnapshotRecord($snapshotEntity);
            $this->importSnapshotRecord($snapshotRecord, $snapshotPage);
        }

        $this->snapshotRecord = $snapshotRecord;
    }
}