<?php
namespace app\parser\handlers;

use app\parser\entities\ProductEntity;
use app\parser\entities\SellerEntity;
use app\parser\entities\StoreEntity;
use yii\base\ErrorException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class ProductHandler extends BaseHandler
{
    /**
     * @param ProductEntity $productEntity
     *
     * @throws ErrorException
     */
    public function handleProductEntity(ProductEntity $productEntity)
    {
        if ($productRecord = $this->findProductRecord($productEntity)) {
            $sellerRecord = $productRecord->seller;
        } else {
            $productMobilePage = $productEntity->downloadMobilePage();
            $sellerEntity = new SellerEntity($productMobilePage->getSellerId());

            if (null == ($sellerRecord = $this->findSellerRecordBySeller($sellerEntity))) {
                $feedbackPage = $sellerEntity->downloadFeedbackPage();
                $storeEntity = new StoreEntity($feedbackPage->getStoreId());

                if (null === ($sellerRecord = $this->findSellerRecordByStore($storeEntity))) {
                    $sellerRecord = $this->createSellerRecord($storeEntity);

                    $this->importSellerRecord(
                        $sellerRecord,
                        $storeEntity->downloadContactsPage(),
                        $sellerEntity->downloadStoreMobilePage(),
                        $feedbackPage,
                        $sellerEntity->downloadWishlistPage()
                    );
                }
            }

            $productRecord = $this->createProductRecord($productEntity, $sellerRecord);
            $this->importProductRecord($productRecord, $productMobilePage);
        }

        $this->sellerRecord  = $sellerRecord;
        $this->productRecord = $productRecord;
    }
}