<?php
namespace app\parser;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class NotFoundHttpException extends HttpException
{
    /**
     * Constructor.
     *
     * @param string  $url
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($url, \Exception $previous = null)
    {
        parent::__construct(404, $url, $previous);
    }
}