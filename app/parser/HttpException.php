<?php
namespace app\parser;

use yii\base\Exception;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class HttpException extends Exception
{
    /**
     * @var integer HTTP status code, such as 403, 404, 500, etc.
     */
    public $statusCode;
    /**
     * @var string requested URL
     */
    public $url;


    /**
     * Constructor.
     *
     * @param integer    $statusCode HTTP status code, such as 404, 500, etc.
     * @param string     $url        URL that returned non-standart status code.
     * @param \Exception $previous   The previous exception used for the exception chaining.
     */
    public function __construct($statusCode, $url, \Exception $previous = null)
    {
        $this->statusCode = $statusCode;
        $this->url = $url;

        parent::__construct(
            sprintf('Url %s returned %d status code.', $this->url, $this->statusCode),
            0,
            $previous
        );
    }
}