<?php
error_reporting(-1);

defined('HOST') or define('HOST', "pozitivmag.ru");
defined('SALT') or define('SALT', "c8c1c0f4c59f60edfbd");
defined('API_KEY') or define('API_KEY', "c8c1c0f4c59f60edfbd0c3dcec514046b0f1d4fab8c2981822812b8e8b2f72022ad3c92fbb7f2bdc");

/**
 * Class AliHelper
 */
class AliHelper
{
    /**
     * @param $host
     * @param $data
     * @param $salt
     * @return string
     */
    public static function generateCheckSum($host, $data, $salt)
    {
        return crypt($host . $data, $salt);
    }

    /**
     * @param $name
     * @param null $defaultValue
     * @return null
     */
    public static function getQuery($name, $defaultValue = null)
    {
        return isset($_GET[$name]) ? $_GET[$name] : $defaultValue;
    }
}

/**
 * Class AliRequest
 */
class AliRequest
{
    /**
     *
     */
    const API_URL = 'http://alitrust.ru/widget-api/';
    /**
     * @var
     */
    protected $_host;
    /**
     * @var
     */
    protected $_salt;
    /**
     * @var
     */
    protected $_apiKey;

    /**
     * @param $host
     * @param $salt
     * @param $apiKey
     */
    public function __construct($host, $salt, $apiKey)
    {
        $this->_host = $host;
        $this->_salt = $salt;
        $this->_apiKey = $apiKey;
    }

    /**
     * @param array $data
     * @return string
     */
    protected function _getData(array $data)
    {
        $data['apiKey'] = $this->_apiKey;
        $data['checkSum'] = AliHelper::generateCheckSum($this->_host, $data['checkSumValue'], $this->_salt);

        return http_build_query($data);
    }

    /**
     * @param $action
     * @param array $data
     * @return array
     */
    public function call($action, array $data)
    {
        $data = $this->_getData($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::API_URL . $action);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_REFERER, $this->_host);

        $data = (array)json_decode(curl_exec($curl));
        $info = curl_getinfo($curl);
        $code = $info['http_code'];

        return compact('data', 'code');
    }
}

/**
 * Class AliResponse
 */
class AliResponse
{
    /**
     * @var array
     */
    protected $data = array();

    /**
     * @param $status
     * @return $this
     */
    public function setHttpStatus($status)
    {
        header('Content-Type: application/json', true, $status);

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->data = json_encode($data);

        return $this;
    }

    /**
     * @param $data
     * @return int
     */
    protected function parseHttpStatus($data)
    {
        return isset($data['status']) ? $data['status'] : 200;
    }

    /**
     *
     */
    public function send()
    {
        echo $this->data;
        exit(0);
    }
}

class AliClient
{
    /**
     * @var AliRequest
     */
    protected $request;

    /**
     * @var AliResponse
     */
    protected $response;

    /**
     * @param $host
     * @param $salt
     * @param $apiKey
     */
    public function __construct($host, $salt, $apiKey)
    {
        $this->request = new AliRequest($host, $salt, $apiKey);
        $this->response = new AliResponse();
        $this->router();
    }

    /**
     *
     */
    protected function router()
    {
        $action = AliHelper::getQuery('action', 'Seller');
        $link = AliHelper::getQuery('link', "");

        call_user_func(array($this, 'get' . $action), $link);
    }

    /**
     * @param $link
     * @return array
     */
    public function getSeller($link)
    {
        $data = array('SellerSearchForm' => compact('link'), 'checkSumValue'=>$link);
        $result = $this->request->call('search', $data);

        $this
            ->response
            ->setData($result['data'])
            ->setHttpStatus($result['code'])
            ->send();
    }

    /**
     * @param $name
     * @param array $params
     * @return array
     */
    public function __call($name, array $params)
    {
        return array('code' => 404, 'data' => ['message' => "Метод '{$name}' не найден"]);
    }
}

/**
 *
 */
new AliClient(HOST, SALT, API_KEY);