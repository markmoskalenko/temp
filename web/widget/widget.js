;
(function (jQuery) {
    jQuery('#alitrust-widget-submit').click(function () {
        jQuery('#alitrust-widget-response-error').addClass('none');
        jQuery('#alitrust-widget-response').addClass('none');
        jQuery('#alitrust-widget-loading').removeClass('none');
        jQuery.ajax("/api.php?link=" + $('#alitrust-widget-input').val())
            .done(function (response) {
                jQuery('#alitrust-widget-loading').addClass('none');
                jQuery('#alitrust-widget-response').removeClass('none').html(response.html);
            })
            .fail(function (response) {
                jQuery('#alitrust-widget-loading').addClass('none');
                jQuery('#alitrust-widget-response-error').removeClass('none').html(response.responseJSON.link[0]);
            });
    })
})(jQuery);