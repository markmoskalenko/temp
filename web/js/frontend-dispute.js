
(function () {
    'use strict';

    autosize($('textarea'));

    $('.b-dispute-table__easel').each(function () {
        var $column = $(this),
            $canvas = $column.find('.b-dispute-table__canvas'),
            $uploader = $column.find('.b-dispute-table__file-uploader');

        $uploader.each(function () {
            var $container = $(this),
                $input = $container.find('input[type="file"]');

            $input.on('change', function () {
                var file = this.files[0];

                var reader = new FileReader();

                reader.onload = function () {
                    $input.trigger('file-loaded', reader);
                };

                reader.readAsDataURL(file);
            });

            $input.on('file-loaded', function (event, reader) {
                var img = document.createElement('img');

                img.onload = function () {
                    $uploader.trigger('image-loaded', img);
                };

                img.src = reader.result;
            });
        });

        $uploader.on('image-loaded', function () {
            $uploader.hide();
        });

        $uploader.on('image-loaded', function (event, img) {
            var canvas = $canvas.get(0),
                ctx = canvas.getContext('2d');

            $canvas.show();
            canvas.height = canvas.width / img.width * img.height;

            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);

            $canvas.trigger('image-loaded');
        });

        $canvas.on('image-loaded', function () {
            var canvas = this,
                context = this.getContext('2d');

            context.strokeStyle = "#FF0000";
            context.lineWidth = 4;

            var imageData = copy(canvas);

            $canvas.drawable()
                .on('draw', function (event, coords) {
                    paste(canvas, imageData);
                    drawOval(context, coords.x1, coords.y1, coords.x2, coords.y2);
                })
                .on('mousedown', function () {
                    paste(canvas, imageData); // clean by simple click
                });
        });
    });

    $.fn.drawable = function () {
        var $this = $(this);

        var drawing = false,
            startX = null,
            startY = null;

        $this.on('mousedown', function (event) {
            drawing = true;
            startX = event.offsetX;
            startY = event.offsetY;
        });

        $this.on('mousemove', function (event) {
            if (drawing) {
                $this.trigger('draw', { x1: startX, y1: startY, x2: event.offsetX, y2: event.offsetY });
            }
        });

        $this.on('mouseup', function () {
            drawing = false;
        });

        $this.on('mouseleave', function () {
            drawing = false;
        });

        return $this;
    };

    $('#create').on('click', function () {
        var $modal = $('#dispute-modal'),
            $content = $('.table'),
            $link = $('#save');

        html2canvas($content.get(0), {
            onrendered: function(canvas) {
                var url = canvas.toDataURL();

                $modal.find('img').attr('src', url);
                $link.attr('href', url).attr('download', 'aliexpress_' + Date.now() + '.png');

                $modal.modal();
            }
        });
    });

    function copy (canvas) {
        var ctx = canvas.getContext('2d');
        return ctx.getImageData(0, 0, canvas.width, canvas.height);
    }

    function paste (canvas, data) {
        var ctx = canvas.getContext('2d');
        ctx.putImageData(data, 0, 0);
    }

    function drawOval (ctx, x1, y1, x2, y2) {
        ctx.beginPath();
        ctx.moveTo(x1, y1 + (y2 - y1)/2);
        ctx.bezierCurveTo(x1, y1, x2, y1, x2, y1 + (y2 - y1)/2);
        ctx.bezierCurveTo(x2, y2, x1, y2, x1, y1 + (y2 - y1)/2);
        ctx.closePath();
        ctx.stroke();
    }
})();