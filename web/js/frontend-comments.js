
(function ($, App) {
    'use strict';

    var Comment = Backbone.Model.extend({
        vote: function () {
            if ( ! this.get('canVote') || App.user.isGuest()) {
                return;
            }

            var model = this;

            var voting = $.post('/comments/' + this.get('id') + '/vote');

            voting.done(function (response) {
                if (response.status == true) {
                    model.set({ votesCount: response.votesCount, canVote: false });
                } else {
                    alert(response.message);
                }
            });

            voting.fail(function (xhr) {
                alert(xhr.responseJSON.message);
            });
        }
    });

    var CommentCollection = Backbone.Collection.extend({
        model: Comment
    });

    var CommentFormView = Marionette.ItemView.extend({
        template: '#comment-form-template',

        ui: {
            input: '.b-comment-form__content-input',
            submit: '.b-comment-form__submit'
        },

        events: {
            'focus @ui.input': 'checkIsGuest'
        },

        triggers: {
            'change @ui.input': 'content:edit',
            'keyup @ui.input': 'content:edit',
            'submit': 'form:submit',
            'click [data-action="cancel"]': 'form:cancel'
        },

        modelEvents: {
            "change:content": "showSubmitState"
        },

        initialize: function () {
            this.model = new Backbone.Model({
                parentId: 0,
                content: ''
            });
        },

        onContentEdit: function () {
            this.model.set('content', this.ui.input.val());
        },

        onBeforeSave: function () {
            this.disable();
        },

        onSave: function () {
            this.clear();
            this.enable();
        },

        onRender: function () {
            App.removeViewWrapper(this);
            this.showSubmitState();
        },

        checkIsGuest: function () {
            if (app.user.isGuest()) {
                this.disable();
                app.commands.execute('showLoginModal');
            }
        },

        clear: function () {
            this.ui.input.val('');
            this.model.clear();
        },

        disable: function () {
            this.ui.input.prop('readonly', true);
            this.ui.submit.prop('disabled', true);
        },

        enable: function () {
            this.ui.input.prop('readonly', false);
            this.showSubmitState();
        },

        showSubmitState: function () {
            if (this.model.get('content')) {
                this.ui.submit.prop('disabled', false);
            } else {
                this.ui.submit.prop('disabled', true);
            }
        },

        save: function (collection) {
            var view = this;

            view.triggerMethod('before:save');

            collection.create(this.model.toJSON(), {
                wait: true,
                success: function () {
                    view.triggerMethod('save');
                },
                error: function (model, xhr) {
                    alert(xhr.responseJSON.message);
                }
            });
        }
    });

    var CommentView = Marionette.ItemView.extend({
        template: '#comment-item-template',

        triggers: {
            'click [data-action="delete"]': 'comment:remove',
            'click [data-action="reply"]': 'comment:reply',
            'click [data-action="vote"]': 'comment:vote'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        onCommentRemove: function () {
            if (confirm('Вы уверены?')) {
                this.model.destroy();
            }
        },

        onCommentVote: function () {
            this.model.vote();
        }
    });

    var NestedCommentView = CommentView.extend({
        className: 'b-comment-list__item'
    });

    var NestedListView = Marionette.CollectionView.extend({
        className: 'b-comment-list b-comment-list_level_second',
        childView: NestedCommentView,

        rootId: null,

        _initialEvents: function () {
            this.listenTo(this.collection, 'add', this.render);
            this.listenTo(this.collection, 'remove', this.render);
            this.listenTo(this.collection, 'reset', this.render);
        },

        filter: function (child, index, collection) {
            return child.get('rootId') > 0 && child.get('rootId') === this.rootId;
        }
    });

    var RootCommentView = CommentView.extend({
        onRender: function () {
            App.removeViewWrapper(this);
        }
    });

    var RootItemView = Marionette.LayoutView.extend({
        'template': '#comment-root-item-template',

        regions: {
            content: '[data-region="content"]',
            list: '[data-region="list"]',
            form: '[data-region="form"]'
        },

        onRender: function () {
            App.removeViewWrapper(this);
        },

        renderMainComment: function () {
            var commentView = new RootCommentView({ model: this.model });
            this.content.show(commentView);
        },

        renderNestedComments: function (collection) {
            var nestedListView = new NestedListView();
            nestedListView.rootId = this.model.get('id');
            nestedListView.collection = collection;
            this.list.show(nestedListView);
        },

        renderForm: function (parentId) {
            var formView = new CommentFormView();
            formView.model.set('parentId', parentId);
            this.form.show(formView);
            formView.ui.input.trigger('focus');
        },

        hideForm: function () {
            this.form.reset();
        }
    });

    var RootListView = Marionette.CollectionView.extend({
        className: 'b-comment-list',

        childView: RootItemView,
        childEvents: {
            'show': function (rootItemView) {
                rootItemView.renderMainComment();
                rootItemView.renderNestedComments(this.collection);
            },
            'childview:comment:reply': function (rootItemView, repliedItemView) {
                this.children.call('hideForm');
                rootItemView.renderForm(repliedItemView.model.id);
            },
            'childview:form:cancel': function (rootItemView) {
                rootItemView.hideForm();
            }
        },

        viewComparator: function (comment) {
            return -1 * comment.get('priority'); // desc sort
        },

        // root collection view should show only root comments in collection
        filter: function (child, index, collection) {
            return child.get('rootId') == 0;
        }
    });

    var WidgetLayoutView = Marionette.LayoutView.extend({
        template: '#comment-widget-template',

        regions: {
            content: '[data-region="content"]',
            form: '[data-region="form"]'
        },

        ui: {
            header: '.b-comment-widget__content-header'
        },

        onRender: function () {
            this.hideHeader();
        },

        renderHeader: function (commentsCount) {
            commentsCount ? this.showHeader() : this.hideHeader();
        },

        showHeader: function () {
            this.ui.header.show();
        },

        hideHeader: function () {
            this.ui.header.hide();
        }
    });

    $.fn.comments = function (options) {
        var comments  = new CommentCollection();
        comments.url = options.url;

        var layoutView = new WidgetLayoutView({ el: this });
        layoutView.render();

        var rootListView = new RootListView();
        rootListView.collection = comments;
        layoutView.content.show(rootListView);

        var commentCreateForm = new CommentFormView({ model: new Comment() });
        layoutView.form.show(commentCreateForm);

        comments.on('add', commentsCountChanged);
        comments.on('remove', commentsCountChanged);

        function commentsCountChanged(model, collection) {
            layoutView.renderHeader(collection.length);
        }

        commentCreateForm.on('form:submit', function () {
            commentCreateForm.save(comments);
        });

        rootListView.on('childview:childview:form:submit', function (rootItemView, formView) {
            formView.save(comments);
        });

        comments.once('sync', function () {
            if (location.hash) {
                var hash = location.hash;
                location.hash = '';
                location.hash = hash;
            }
        });

        comments.fetch();
    };

})(jQuery, AliTrust);