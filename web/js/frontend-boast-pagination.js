(function ($) {
    'use strict';

    var ias = $.ias({
        container: ".b-boast-list-view",
        item: ".b-boast-list__item",
        pagination: ".b-boast-list-pagination",
        next: ".next a",
        delay: 1200,
        negativeMargin: 500
    });

    ias.on('render', function(items) {
        $(items).css({ opacity: 0 });
    });

    ias.on('rendered', function(items) {
        var $boasts = $(items).find('.b-boast');

        $boasts.each(function () {
            var $boast = $(this);
            angular.element(document.body).injector().invoke(function($compile) {
                var scope = angular.element($boast).scope();
                $compile($boast)(scope);
            });
        });

        $boasts.imagesLoaded(function () {
            $('.b-boast-list').masonry('appended', items);
        });
    });

    ias.extension(new IASSpinnerExtension({ html: '<div class="b-boast-spinner ias-spinner"></div>' }));
    ias.extension(new IASPagingExtension());
    ias.extension(new IASNoneLeftExtension({ html: '<div class="b-boast-noneleft ias-noneleft">Больше записей нет.</div>' }));

    $.ias().on('pageChange', function(pageNum, scrollOffset, url) {
        window.history.pushState(pageNum, '', url);
    });
})(jQuery);