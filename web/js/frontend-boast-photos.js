
(function ($, app) {
    'use strict';

    var PhotoView = Marionette.ItemView.extend({
        template: '#boast-photo-template',

        triggers: {
            'click .js-photo-delete': 'remove:photo'
        },

        onRender: function () {
            app.removeViewWrapper(this);
        }
    });

    var PhotoListView = Marionette.CollectionView.extend({
        childView: PhotoView,
        childEvents: {
            'remove:photo': function (view) {
                this.collection.remove(view.model);
            }
        },

        initialize: function () {

            var collection = this.collection;

            this.$el.children('input[type="hidden"]').each(function () {
                var $input = $(this),
                    id = $input.val();

                if (id) {
                    collection.add({ id: id, url: '/boasts/photo?id=' + id });
                    $input.remove();
                }
            })
        }
    });

    var BoastFormView = Marionette.View.extend({
        el: '#boast-form',

        ui: {
            dnd: '.js-dnd',
            extraUploader: '.js-extra-uploader'
        },

        initialize: function () {
            this.bindUIElements();

            this.uploadedPhotos = new Backbone.Collection();

            this.listenTo(this.uploadedPhotos, 'add', this.render);
            this.listenTo(this.uploadedPhotos, 'remove', this.render);
        },

        hasUploadedPhotos: function () {
            return Boolean(this.uploadedPhotos.length);
        },

        render: function () {
            if (this.hasUploadedPhotos()) {
                this.ui.dnd.hide();
                this.ui.extraUploader.show();
            } else {
                this.ui.dnd.show();
                this.ui.extraUploader.hide();
            }
        }
    });

    app.on('start', function () {

        var boastFormView = new BoastFormView({
            el: '#boast-form'
        });

        var uploadedPhotosView = new PhotoListView({
            el: '.js-uploaded-photos',
            collection: boastFormView.uploadedPhotos
        });

        $('.field-boastform-photos').fileapi({
            url: '/boasts/upload-photo',
            autoUpload: true,
            accept: 'image/*',
            data: app.getCsrfData(),
            duplicate: true,
            multiple: true,
            maxSize: 10 * FileAPI.MB,
            imageTransform: {
                maxWidth: 1000,
                maxHeight: 1000
            },
            elements: {
                dnd: {
                    el: '.js-dnd',
                    hover: 'b-upload-dnd_state_hover'
                }
            },
            onFileComplete: function (evt, ui) {
                if (ui.status == 200) {
                    boastFormView.uploadedPhotos.add(ui.result);
                }
            }
        });

        uploadedPhotosView.render();
        boastFormView.render();
    });
})(jQuery, AliTrust);