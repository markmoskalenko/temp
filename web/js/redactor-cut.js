(function(plugins) {
    'use strict';

    plugins.cut = function()
    {
        return {
            init: function () {
                var button = this.button.add('horizontalrule', 'Разделитель');
                this.button.addCallback(button, this.cut.pressed);
            },

            pressed: function () {
                if (this.cut.exists()) {
                    this.cut.remove();
                }
                this.cut.insert();
            },

            exists: function () {
                return (this.$editor.find('hr').length > 0);
            },

            insert: function() {
                this.line.insert();
            },

            remove: function () {
                this.selection.save();
                this.$editor.find('hr').remove();
                this.code.sync();
                this.selection.restore();
            }
        };
    };
})(RedactorPlugins);