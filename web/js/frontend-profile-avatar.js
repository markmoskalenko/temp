(function ($, app) {
    'use strict';

    var $control = $('.field-profileform-avatarurl'),
        $image = $control.find('img');

    var defaultAvatarUrl = $image.attr('src'),
        loadingPlaceholder = '/images/loading.gif';

    $control.fileapi({
        url: '/profile/upload-avatar',
        autoUpload: true,
        accept: 'image/*',
        multiple: false,
        data: app.getCsrfData(),
        onUpload: function () {
            $image.attr('src', loadingPlaceholder)
        },
        onFileComplete: function (evt, uiEvt) {
            if (uiEvt.error) {
                $image.attr('src', defaultAvatarUrl);
            } else {
                $image.attr('src', uiEvt.result.url);
            }
        }
    });
})(jQuery, AliTrust);