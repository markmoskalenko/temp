(function (app) {
    'use strict';

    $('.timeago').timeago();

    app.factory('socket', function (socketFactory) {
        return socketFactory({
            ioSocket: io.connect('http://alitrust.ru:6001')
        });
    });

    app.factory('webNotification', function ($q) {
        var service = {};

        service.config = function (options) {
            notify.config(options);
        };

        service.isPermissionRequested = function () {
            return notify.permissionLevel() !== notify.PERMISSION_DEFAULT;
        };

        service.isPermissionGranted = function () {
            return notify.permissionLevel() === notify.PERMISSION_GRANTED;
        };

        service.isPermissionDenied = function () {
            return notify.permissionLevel() === notify.PERMISSION_DENIED;
        };

        service.requestPermission = function () {
            return $q(function (resolve, reject) {
                if (service.isPermissionGranted()) {
                    resolve();
                }

                notify.requestPermission(function () {
                    if (service.isPermissionGranted()) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            })
        };

        service.show = function (title, body, tag) {
            service.requestPermission().then(function () {
                var options = {
                    body: body,
                    icon: '/favicon.ico',
                    tag: tag
                };

                notify.createNotification(title, options);
            })
        };

        return service;
    });

    app.factory('audioNotification', function () {
        return {
            init: function () {
                if (this.initiated) {
                    return;
                }

                if (this.supported()) {
                    this.audio = new Audio('/sounds/sounds-942-what-friends-are-for.mp3');
                }

                this.initiated = true;
            },

            supported: function () {
                var a = document.createElement('audio');
                return !!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''));
            },

            play: function () {
                if ( ! this.supported()) {
                    return;
                }

                if ( ! this.initiated) {
                    this.init();
                }

                this.audio.play();
            }
        };
    });

    app.config(function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('coupons');
    });

    app.factory('couponSettings', function (localStorageService) {
        var service= {};

        if (localStorageService.get('enableNotifications') === null) {
            localStorageService.set('enableNotifications', true);
        }

        if (localStorageService.get('enableAudio') === null) {
            localStorageService.set('enableAudio', true);
        }

        service.isNotificationsEnabled = function () {
            return localStorageService.get('enableNotifications');
        };

        service.isAudioEnabled = function () {
            return localStorageService.get('enableAudio');
        };

        service.bind = function (object, property) {
            localStorageService.bind(object, property);
        };

        return service;
    });

    app.controller('CouponsSettingsController', function ($scope, couponSettings, audioNotification, webNotification) {
        couponSettings.bind($scope, 'enableNotifications');
        couponSettings.bind($scope, 'enableAudio');

        audioNotification.init();

        webNotification.config({ autoClose: 5000 });

        $scope.checkNotifications = function () {
            webNotification.show('Всё работает отлично', 'Вы будете получать такие уведомления, когда будут появляться новые купоны');
        };

        $scope.checkAudio = function () {
            audioNotification.play();
        };
    });

    app.controller('CouponsController', function ($scope, $http, $element, socket, couponSettings, audioNotification, webNotification) {
        var $container = $element;

        socket.on('coupons:App\\Events\\CouponWasCreated', function (data) {
            createCoupon(data.coupon);
        });

        socket.on('coupons:App\\Events\\CouponWasUpdated', function (data) {
            updateCoupon(data.coupon);
        });

        function createCoupon(coupon) {
            loadCoupon(coupon).success(function (html) {
                if (isCouponExists(coupon)) {
                    return;
                }

                var $coupon = $(html);
                $container.find('.coupon-list').prepend($coupon);
                setClientScripts($coupon);
                checkPageItemsCount();

                if (couponSettings.isNotificationsEnabled()) {
                    var title = coupon.storeName;
                    var body = 'Новый купон на $' + coupon.value;
                    var tag = 'coupon_' + coupon.id;

                    webNotification.show(title, body, tag);
                }

                if (couponSettings.isAudioEnabled()) {
                    audioNotification.play();
                }
            });
        }

        function updateCoupon(coupon) {
            var $coupon = findElementById(coupon.id);

            if ( ! $coupon.length) {
                return;
            }

            loadCoupon(coupon).success(function (html) {
                $coupon.html(html);
                setClientScripts($coupon);
            });
        }

        function loadCoupon(coupon) {
            return $http.get('/coupons/view?id=' + coupon.id);
        }

        function isCouponExists(coupon) {
            return findElementById(coupon.id).length;
        }

        function findElementById(couponId) {
            return $container.find('.coupon[data-id="' + couponId + '"]').parent();
        }

        function setClientScripts($coupon) {
            $coupon.find('.timeago').timeago();
        }

        function checkPageItemsCount() {
            $container.find('.coupon-list__item:gt(14)').remove();
        }
    });
})(window.angularApp);