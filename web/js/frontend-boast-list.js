(function ($) {
    'use strict';

    var $images = $('.b-boast__image');

    calculateImageSizes();
    $(window).on('resize', calculateImageSizes);

    function calculateImageSizes() {
        $images.each(function () {
            var $image = $(this);
            setRelativeImageSize($image);
        })
    }

    function setRelativeImageSize($image) {
        $image.removeAttr('style');

        var width = $image.width(),
            height = Math.round(width / $image.attr('width') * $image.attr('height'));

        $image.css({ width: width, height: height });
    }

    $('.b-boast-list').masonry();
})(jQuery);