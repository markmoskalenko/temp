(function ($) {
    'use strict';

    $.fn.sellerPage = function (options) {
        var $this = $(this);

        if ( ! options.url) {
            throw Error('You should specify update URL.')
        }

        $('.seller-info-updated').html( $('#seller-info-loading-template').html() );

        $.post(options.url, function (res) {
            if (res.updated) {
                $('#content').removeAttr('id');
                $.pjax.reload($this);
            }
        });

        $(document).on('pjax:complete', function() {
            $('[data-toggle="popover"]').popoverHideable();
        });
    };
})(jQuery);