
(function ($) {
    'use strict';

    var $list = $('.forum-comment-list');

    function getTextarea () {
        return $('.wysiwyg textarea').get(0);
    }

    function getCreateForm () {
        return $('#comment-form-create');
    }

    function getUpdateForm () {
        return $('#comment-form-update');
    }

    function getCommentBlock(elem) {
        return $(elem).parents('.forum-comment-list__item');
    }

    function getCommentId(elem) {
        var $comment = getCommentBlock(elem);
        return $comment.data('key');
    }

    function getUserName (elem) {
        var $comment = getCommentBlock(elem),
            userName = $comment.find('.forum-profile__username').text();
        return $.trim(userName);
    }

    function getCurrentPage () {
        var page = $('.forum-pagination .active a').eq(0).text();
        return parseInt(page) || 1;
    }

    function clearEmptyMessage () {
        $('.forum-comment-list__empty').remove();
    }

    function registerImageUploaderEvents ($elem) {
        $elem.on('add.imageUploader select.imageUploader', function (event, image) {
            var textarea = getTextarea();
            redactor.insertTag(textarea, '[img]' + image.url + '[/img]\n', '');
        });
    }

    function registerCommentScripts ($elem) {
        $elem.find('[data-toggle="collapse"]').collapse({ toggle: false });
    }

    function registerFormScripts ($elem) {
        var $textarea = $elem.find('textarea');
        autosize($textarea);
        registerImageUploaderEvents($elem);
    }

    registerImageUploaderEvents(getCreateForm());

    window.referUser = function (elem) {
        var userName = getUserName(elem);
        redactor.insertTag(getTextarea(), '[b]' + userName + '[/b], ', '');
    };

    window.quoteComment = function (elem) {
        var userName = getUserName(elem),
            commentId = getCommentId(elem);

        $.get('/forum/comments/raw?id=' + commentId, function (data) {
            var textarea = getTextarea();
            redactor.insertTag(textarea, '[quote="' + userName + '"]' + data + '[/quote]\n', '');
            autosize.update(textarea);
        });
    };

    window.updateComment = function (elem) {
        var commentId = getCommentId(elem);

        cancelUpdate();

        $.get('/forum/comments/update?id=' + commentId, function (html) {
            var $comment = getCommentBlock(elem);
            $comment.hide();

            var $form = $comment.after(html).next();

            angular.element(document.body).injector().invoke(function($compile) {
                var scope = angular.element($form).scope();
                $compile($form)(scope);
                scope.$digest();
                registerFormScripts($form);
            });
        });
    };

    window.saveCreatedComment = function () {
        var $form = getCreateForm(),
            $button = $form.find('button');

        $form.ajaxSubmit({
            beforeSend: function () {
                $button.prop('disabled', true);
            },
            complete: function () {
                $button.prop('disabled', false);
            },
            success: function (data) {
                if (data.page == getCurrentPage()) {
                    clearEmptyMessage();
                    $list.append(data.html);
                    $form.trigger('reset');
                    var $comment = $list.find(':last-child');
                    registerCommentScripts($comment);
                } else {
                    location.href = data.url;
                }
            },
            error: function (jqXHR) {
                alert(jqXHR.responseText);
            }
        });
    };

    window.saveUpdatedComment = function () {
        var $form = getUpdateForm(),
            $button = $form.find('button');

        $form.ajaxSubmit({
            beforeSend: function () {
                $button.prop('disabled', true);
            },
            complete: function () {
                $button.prop('disabled', false);
            },
            success: function (data) {
                var $old = $form.prev();
                var $new = $old.after(data).next();
                $old.remove();
                $form.remove();
                registerCommentScripts($new);
            },
            error: function (jqXHR) {
                alert(jqXHR.responseText);
            }
        });
    };

    window.cancelUpdate = function () {
        var $form = getUpdateForm(),
            $comment = $form.prev();

        $form.remove();
        $comment.show();
    };
})(jQuery);