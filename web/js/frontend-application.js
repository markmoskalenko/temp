
(function ($) {
    'use strict';

    var UserModel = Backbone.Model.extend({
        isGuest: function () {
            return (this.get('id') === null);
        }
    });

    window.angularApp = angular.module('AngularApp', [
        'ngSanitize',
        'btford.socket-io',
        'LocalStorageModule'
    ]);

    window.angularApp.config(['$locationProvider', function($locationProvider){

        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    }]);

    var Application = Marionette.Application.extend({
        initialize: function () {
            this.user = new UserModel();
        },

        getCsrfData: function () {
            var data = {};
            data[ yii.getCsrfParam() ] = yii.getCsrfToken();
            return data;
        },

        removeViewWrapper: function (view) {
            view.$el = view.$el.children();
            view.$el.unwrap();
            view.setElement(view.$el);
        }
    });

    window.app = window.AliTrust = new Application();

    app.commands.setHandler('showLoginModal', function () {
        $('#login-modal').modal();
    });

    app.vent.on('reviewVotedByGuest', function () {
        ga('send', 'event', 'reviews.vote', 'click', 'guest');
    });

    app.vent.on('reviewVotedByUser', function () {
        ga('send', 'event', 'reviews.vote', 'click', 'user');
    });

    app.vent.on('extensionModalShowed', function (data) {
        ga('send', 'event', 'browser-extension', 'modalview', data.browser);
    });

    app.vent.on('extensionModalInstalled', function (data) {
        ga('send', 'event', 'browser-extension', 'install', data.browser);
    });

    app.vent.on('extensionModalRefused', function () {
        ga('send', 'event', 'browser-extension', 'refuse');
    });
})(jQuery);