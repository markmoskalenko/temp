
(function ($) {
    'use strict';

    var $modal = $('#chrome-extension-modal'),
        cookieName = 'chrome-extension';

    if ($.cookie(cookieName)) {
        return;
    }

    $.cookie(cookieName, 1, { expires: 3 });

    setTimeout(function () {
        if (isExtensionInstalled()) {
            return;
        }

        $modal.modal();
    }, 3000);

    function isExtensionInstalled() {
        return $('meta[name="browser-extension"]').length > 0;
    }

    $modal.on('shown.bs.modal', function () {
        app.vent.trigger('extensionModalShowed', { browser: 'chrome' });
    });

    $modal.on('click', '.js-install', function () {
        app.vent.trigger('extensionModalInstalled', { browser: 'chrome' });
    });

    $modal.on('click', '.js-refuse', function () {
        app.vent.trigger('extensionModalRefused');
    });
})(jQuery);