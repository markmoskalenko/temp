
(function ($) {
    'use strict';

    $.fn.categorize = function (settings) {

        var defaultSettings = {
            options: [],
            valueField: 'id',
            labelField: 'namesPath',
            searchField: ['namesPath', 'synonyms'],
            sortField: 'namesPath'
        };

        this.selectize($.extend(defaultSettings, settings));
    }
})(jQuery);