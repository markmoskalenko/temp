
redactor = {
    insertTagWithText: function (link, tagName) {
        var startTag = '[' + tagName + ']';
        var endTag = '[/' + tagName + ']';
        redactor.insertTag(link, startTag, endTag);
        return false;
    },

    insertImage: function (link) {
        var src = prompt('Введите src картинки', 'http://');
        if(src) {
            redactor.insertTag(link, '[img]' + src + '[/img]', '');
        }
        return false;
    },

    insertSmile: function (link, code) {
        redactor.insertTag(link, code + ' ', '');
        return false;
    },

    insertLink: function (link) {
        var href = prompt('Введите URL ссылки', 'http://');

        if ( ! href) {
            return false;
        }

        var wysiwyg = $(link).parents('.wysiwyg'),
            textarea = $('textarea', wysiwyg).get(0);

        textarea.focus();
        var cursorPos = redactor.getCursor(textarea);
        var selection = textarea.value.substring(cursorPos.start, cursorPos.end);

        if (selection) {
            redactor.insertTag(link, '[url="' + href + '"]', '[/url]');
        } else {
            redactor.insertTag(link, '[url="' + href + '"]Текст ссылки[/url]', '');
        }

        return false;
    },

    insertSpoiler: function (link) {
        redactor.insertTag(link, '[spoiler="Заголовок спойлера"]\n', '\n[/spoiler]');
        return false;
    },

    insertUser: function (link) {
        var login = prompt('Введите никнейм пользователя', '');
        if(login) {
            redactor.insertTag(link, '@' + login + '', '');
        }
        return false;
    },

    insertCut: function (link) {
        redactor.insertTag(link, '[cut]', '');
        return false;
    },

    insertTag: function (link, startTag, endTag, repObj) {
        var wysiwyg = $(link).parents('.wysiwyg'),
            textarea = $('textarea', wysiwyg).get(0);

        textarea.focus();

        var scrtop = textarea.scrollTop;
        var cursorPos = redactor.getCursor(textarea);
        var txt_pre = textarea.value.substring(0, cursorPos.start);
        var txt_sel = textarea.value.substring(cursorPos.start, cursorPos.end);
        var txt_aft = textarea.value.substring(cursorPos.end);

        if (repObj) {
            txt_sel = txt_sel.replace(/\r/g, '');
            txt_sel = txt_sel != '' ? txt_sel : ' ';
            txt_sel = txt_sel.replace(new RegExp(repObj.findStr, 'gm'), repObj.repStr);
        }

        var nuCursorPos;
        if (cursorPos.start == cursorPos.end) {
            nuCursorPos = cursorPos.start + startTag.length;
        } else {
            nuCursorPos=String(txt_pre + startTag + txt_sel + endTag).length;
        }

        textarea.value = txt_pre + startTag + txt_sel + endTag + txt_aft;

        redactor.setCursor(textarea, nuCursorPos, nuCursorPos);

        if (scrtop) {
            textarea.scrollTop = scrtop
        }

        $(textarea).trigger('change');

        // Dispatch a 'autosize:update' event to trigger a resize:
        var evt = document.createEvent('Event');
        evt.initEvent('autosize:update', true, false);
        textarea.dispatchEvent(evt);

        return false;
    },

    insertTagFromDropBox: function (link) {
        redactor.insertTagWithText(link, link.value);
        link.selectedIndex = 0;
    },

    insertList: function (link) {

        var startTag = '[' + link.value + ']\n';
        var endTag = '\n[/' + link.value + ']';

        var repObj = {
            findStr: '^(.+)',
            repStr: '[*] $1'
        };

        redactor.insertTag(link, startTag, endTag, repObj);

        link.selectedIndex = 0;
    },

    getCursor: function (input) {
        var result = {start: 0, end: 0};
        if (input.setSelectionRange) {
            result.start= input.selectionStart;
            result.end = input.selectionEnd;
        } else if (!document.selection) {
            return false;
        } else if (document.selection && document.selection.createRange) {
            var range = document.selection.createRange();
            var stored_range = range.duplicate();
            stored_range.moveToElementText(input);
            stored_range.setEndPoint('EndToEnd', range);
            result.start = stored_range.text.length - range.text.length;
            result.end = result.start + range.text.length;
        }
        return result;
    },

    setCursor: function (textarea, start, end) {
        if (textarea.createTextRange) {
            var range = textarea.createTextRange();
            range.move("character", start);
            range.select();
        } else if (textarea.selectionStart) {
            textarea.setSelectionRange(start, end);
        }
    }
};
