(function ($) {
    'use strict';

    window.angularApp
        .controller('LatestNewsListCtrl', [
            '$scope',
            '$compile',
            '$document',
            '$interval',
            '$timeout',
            '$http', function ($scope,
                               $compile,
                               $document,
                               $interval,
                               $timeout,
                               $http) {

                $scope.data = [];
                $scope.ready = false;
                $scope.activeType = 1;

                /**
                 *
                 */
                $scope.onLoadData = function () {
                    $scope.ready = false;
                    $http.get('/latest-news?type=' + $scope.activeType).success(function (response) {
                        $scope.data = response;

                        $timeout(function () {
                            $scope.ready = true;
                        }, 400);
                    });
                };

                $scope.onLoadData();

                /**
                 *
                 * @param type
                 */
                $scope.onSetActiveType = function (type) {
                    if ($scope.activeType == type) return;

                    $scope.activeType = type;
                    $scope.onLoadData();
                };
            }]);
})(jQuery);