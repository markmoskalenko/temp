
(function ($, app) {
    'use strict';

    // Loadable forms

    $.fn.loadable = function () {
        return this.each(function () {
            var $form = $(this),
                $disable = $form.find('[data-toggle="disable"]'),
                $idlers = $form.find('.js-loadable-idler'),
                $hunkies = $form.find('.js-loadable-hunky');

            $form.on('loadable:start', function () {
                $disable.attr('disabled', 'disabled');
                $idlers.addClass('hidden');
                $hunkies.removeClass('hidden');
            });

            $form.on('loadable:stop', function () {
                $disable.removeAttr('disabled');
                $idlers.removeClass('hidden');
                $hunkies.addClass('hidden');
            });
        });
    };

    $('.js-loadable-form').loadable();

    $('#form-seller-search').each(function () {
        var $form = $(this),
            $input = $form.find('input'),
            $submit = $form.find('[data-icon="default"]'),
            $loading = $form.find('[data-icon="loading"]');

        $form.on('beforeValidate beforeSubmit', function () {
            $input.prop('readonly', true);
            $submit.addClass('hidden');
            $loading.removeClass('hidden');
        });

        $form.on('afterValidate', function () {
            $input.prop('readonly', false);
            $loading.addClass('hidden');
            $submit.removeClass('hidden');
        });
    });

    // Hideable popover

    $.fn.popoverHideable = function () {
        this.each(function () {
            var $this = $(this);

            $this.popover();

            $this.on('shown.bs.popover', function () {
                $(document).on('click.hide-popover', function (event) {
                    if ( ! $(event.target).parents('.popover').length) {
                        $this.popover('hide');
                        $(document).off('.hide-popover');
                    }
                });
            });
        });
    };

    // Tooltips and popovers

    $('[data-toggle="tooltip"]').tooltip();

    $('[data-toggle="popover"]').popoverHideable();

    // Raty

    $('.b-raty').on('click', function () {
        $(this).prev().trigger('change');
    });

    // Login required forms

    $('.js-login-required').on('click', function (event) {
        if (app.user.isGuest()) {
            event.preventDefault();
            app.commands.execute('showLoginModal');
        }
    });

    // Autosize textareas

    $('.autosize').each(function () {
        autosize(this);
    });

})(jQuery, AliTrust);

// GA Events ==================================================================

(function ($) {
    'use strict';

    $(document).on('click', '.js-analytic-event', function() {
        var data = $(this).data();

        if ( ! data['analyticCategory']) {
            return;
        }

        var config = {
            'hitType': 'event',
            'eventCategory': data['analyticCategory']
        };

        if (data['analyticAction']) {
            config.eventAction = data['analyticAction'];
        } else {
            config.eventAction = 'click';
        }

        if (data['analyticLabel']) {
            config.eventLabel = data['analyticLabel'];
        }

        if (data['analyticValue']) {
            config.eventValue = data['analyticValue'];
        }

        ga('send', config);
    });
})(jQuery);