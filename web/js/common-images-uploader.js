
(function ($, app) {
    'use strict';

    window.angularApp.directive('imagesUploader', function () {

        return {
            restrict: 'AE',
            scope: {},
            template: function (elem, attrs) {
                var selector = attrs.template || '#images-uploader-template';
                return $(selector).html();
            },
            link: function (scope, elem, attrs) {
                scope.images = attrs['images'] ? JSON.parse(attrs['images']) : [];
                scope.dropHover = false;
                scope.uploading = false;

                scope.add = function (image) {
                    scope.images.push(image);
                    scope.$digest();
                    elem.trigger('add.imageUploader', image);
                };

                scope.remove = function (image) {
                    var index = scope.images.indexOf(image);
                    scope.images.splice(index, 1);
                    scope.$digest();
                    elem.trigger('remove.imageUploader', image);
                };

                scope.select = function (image) {
                    elem.trigger('select.imageUploader', image);
                };

                elem.parents('form').on('reset', function () {
                    scope.images = [];
                    scope.$digest();
                });

                elem.fileapi({
                    url: attrs['uploadUrl'],
                    data: app.getCsrfData(),
                    autoUpload: true,
                    accept: 'image/*',
                    duplicate: true,
                    multiple: true,
                    maxSize: 10 * FileAPI.MB,
                    imageTransform: {
                        maxWidth: 1000,
                        maxHeight: 1000
                    },
                    elements: {
                        dnd: {
                            el: '.js-dnd'
                        }
                    },
                    onDropHover: function (evt, uiEvt) {
                        scope.dropHover = uiEvt.state;
                        scope.$digest();
                    },
                    onUpload: function () {
                        scope.uploading = true;
                        scope.$digest();
                    },
                    onComplete: function () {
                        scope.uploading = false;
                        scope.$digest();
                    },
                    onFileComplete: function (evt, ui) {
                        if (ui.status == 200) {
                            scope.add(ui.result);
                        } else if (ui.error) {
                            alert(ui.result.message);
                        }
                    }
                });
            }
        };
    });
})(jQuery, AliTrust);