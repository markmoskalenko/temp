
(function () {
    'use strict';

    AliTrust.module('Complaints', function (Complaints, App, Backbone, Marionette, $, _) {

        var fancyboxOptions = {
            padding: false,
            helpers	: {
                thumbs	: {
                    width	: 75,
                    height	: 75
                }
            }
        };

        Complaints.Model = Backbone.Model.extend({

            initialize: function () {
                this.photos = new Backbone.Collection();
                this.set('authorId', null);
                this.set('voteable', null);
                this.set('voted', false);
            },

            isAuthor: function () {
                if (App.user.isGuest()) {
                    return false;
                }
                return App.user.get('id') === this.get('authorId');
            },

            isVoteable: function () {
                return this.get('voteable');
            },

            isVoted: function () {
                return this.get('voted');
            },

            allowVoting: function () {
                this.set('voteable', true);
            },

            denyVoting: function () {
                this.set('voteable', false);
            },

            loadPhotos: function (photos) {
                if (_.isArray(photos) && photos.length) {
                    this.photos.add(photos);
                }
            },

            setVotesCount: function (count) {
                count = parseInt(count);
                this.set('votesCount', isNaN(count) ? 0 : count);
            },

            vote: function () {
                if (this.isAuthor() || this.isVoted() || App.user.isGuest()) {
                    return false;
                }

                $.post(this.get('voteUrl'));

                this.set('votesCount', this.get('votesCount') + 1);
                this.set('voted', true);
                this.denyVoting();

                App.vent.trigger('complaintVotedByUser');

                return true;
            }
        });

        Complaints.ItemView = Marionette.ItemView.extend({

            ui: {
                body: '.b-complaint__body',
                text: '.b-complaint__text',
                innerText: '.b-complaint__text-inner',
                redactor: '.b-complaint__redactor',
                redactorInput: '.b-complaint__redactor-input',
                voteBtn: '.b-complaint__vote',
                votesCounter: '[data-bind="votesCount"]',
                gallery: '.js-gallery',
                counter: '.js-photos-counter',
                expand: '.js-expand',
                collapse: '.js-collapse'
            },

            events: {
                'click @ui.gallery': 'openGallery',
                'click @ui.expand': 'expandText',
                'click @ui.collapse': 'collapseText',
                'click @ui.voteBtn': 'vote',
                'click [data-action="edit"]': 'openRedactor',
                'click [data-action="edit-cancel"]': 'closeRedactor'
            },

            initialize: function () {
                this.bindUIElements();

                this.model = new Complaints.Model();
                this.model.set('id', this.$el.data('key'));
                this.model.set('authorId', this.ui.voteBtn.data('author-id'));
                this.model.set('voted', this.ui.voteBtn.data('voted'));
                this.model.set('voteUrl', this.ui.voteBtn.data('url'));
                this.model.loadPhotos(this.ui.gallery.data('photos'));
                this.model.setVotesCount(this.ui.votesCounter.text());

                this.listenTo(this.model.photos, 'add', this.renderPhotosCounter);
                this.listenTo(this.model.photos, 'remove', this.renderPhotosCounter);
                this.listenTo(this.model, 'change:votesCount', this.renderVoteButton);
                this.listenTo(this.model, 'change:voteable', this.renderVoteButton);

                if (this.ui.innerText.height() > this.ui.text.height()) {
                    this.collapseText();
                }

                if (this.model.isAuthor() || this.model.isVoted()) {
                    this.model.denyVoting();
                } else {
                    this.model.allowVoting();
                }
            },

            renderPhotosCounter: function () {
                var count = this.model.photos.length;
                this.ui.counter.text(count)
                    .parents('.complaint__meta').toggleClass('hidden', Boolean(count));
            },

            renderVoteButton: function () {
                if (this.model.isVoteable()) {
                    this.ui.voteBtn.addClass('border-link border-link-dashed').tooltip();
                } else {
                    this.ui.voteBtn.removeClass('border-link border-link-dashed').tooltip('destroy');
                }

                this.ui.votesCounter.text(this.model.get('votesCount'));
            },

            expandText: function () {
                this.ui.text.removeClass('b-complaint__text_state_collapsed');
                this.ui.expand.addClass('hidden');
                this.ui.collapse.removeClass('hidden');
            },

            collapseText: function () {
                this.ui.text.addClass('b-complaint__text_state_collapsed');
                this.ui.expand.removeClass('hidden');
                this.ui.collapse.addClass('hidden');
            },

            openRedactor: function () {
                this.ui.redactorInput.data('original', this.ui.redactorInput.val());
                this.ui.body.addClass('hidden');
                this.ui.redactor.removeClass('hidden');
                this.ui.redactorInput.trigger('focus');
            },

            closeRedactor: function () {
                this.ui.body.removeClass('hidden');
                this.ui.redactor.addClass('hidden');
                this.ui.redactorInput.val(this.ui.redactorInput.data('original'));
            },

            openGallery: function () {
                var urls = this.model.photos.pluck('url');
                $.fancybox(urls, fancyboxOptions);
            },

            vote: function () {
                if (App.user.isGuest()) {
                    App.vent.trigger('complaintVotedByGuest');
                    App.commands.execute('showLoginModal');
                } else {
                    this.model.vote();
                }
            }
        });

        Complaints.views = [];

        Complaints.on('start', function () {
            $('.b-complaint').each(function () {
                var view = new Complaints.ItemView({ el: this })
                Complaints.views[ view.model.id ] = view;
            });
        });
    });
})();