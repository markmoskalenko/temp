(function ($) {
    'use strict';

    window.angularApp
        .controller('NotificatorCtrl', [
            '$scope',
            '$compile',
            '$document',
            '$interval',
            '$http', function ($scope,
                               $compile,
                               $document,
                               $interval,
                               $http) {

                /**
                 * Количество новых уведомлений
                 *
                 * @type {number}
                 */
                $scope.newNotifications = 0;
                /**
                 * Елемент тайтла
                 */
                var titleElement = $document.find('title') || angular.element('<title>').appendTo($document.find('head'));
                /**
                 * Первичный заголовок
                 */
                var titleHtmlOld = titleElement.html();
                /**
                 * Хранит интервал мигания заголовка
                 */
                var stopNotifyTitle;

                /**
                 * @param {int} n
                 * @param {Array} forms
                 * @returns {*}
                 */
                function pluralize(n, forms) {
                    return forms[n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2];
                }

                /**
                 * Запуск мигания заголовка
                 */
                $scope.onNotifyTitleStart = function () {
                    var trigger = 0;
                    stopNotifyTitle = $interval(function () {
                        if (trigger == 0) {
                            titleElement.html('У вас ' + $scope.newNotifications + ' ' + pluralize($scope.newNotifications, ['уведомление', 'уведомления', 'уведомлений']));
                            trigger = 1;
                        } else {
                            titleElement.html(titleHtmlOld);
                            trigger = 0;
                        }
                        $compile(titleElement)($scope);
                    }, 2000);
                };

                /**
                 * Остановка мигания заголовка
                 */
                $scope.onNotifyTitleStop = function () {
                    if (angular.isDefined(stopNotifyTitle)) {
                        $interval.cancel(stopNotifyTitle);
                        stopNotifyTitle = undefined;
                        titleElement.html(titleHtmlOld);
                        $compile(titleElement)($scope);
                    }
                };

                /**
                 * Следим за изменением переменной. Запускаем логику в зависимости от кол-ва уведомлений.
                 */
                $scope.$watch('newNotifications', function (newValue, oldValue) {
                    if (newValue != oldValue && newValue > 0) {
                        $scope.onNotifyTitleStop();
                        $scope.onNotifyTitleStart();
                    }
                    if (newValue == 0) {
                        $scope.onNotifyTitleStop();
                    }
                });

                /**
                 * Запрос на сервер для поучения кол-ва уведомлений
                 */
                $scope.onUpdateNewNotifications = function () {
                    $http.get('/notifications/new-notifications').success(function (response) {
                        $scope.newNotifications = parseInt(response);
                    });
                };

                /**
                 * Запрос на сервер для поучения кол-ва уведомлений
                 */
                $scope.onUpdateNewNotifications();

                /**
                 * Раз в 5 сек получаем кол-во новых уведомлений
                 */
                $interval(function () {
                    $scope.onUpdateNewNotifications();
                }, 10000);
            }]);

    window.angularApp
        .controller('NotificationListCtrl', [
            '$scope',
            '$window',
            '$http', function ($scope,
                               $window,
                               $http) {

                /**
                 * Прочтение уведомления
                 * @param isRead
                 * @param id
                 */
                $scope.onRead = function (isRead, id) {
                    if (isRead) return;
                    $http.get('/notifications/notification-read?id='+id);
                };

                $scope.onReadAll = function () {
                    $http.get('/notifications/notification-read?all=1').success(function(){
                        $window.location.reload();
                    });
                };
            }]);
})(jQuery);