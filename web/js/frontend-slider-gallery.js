(function ($) {
    'use strict';

    $.fn.sliderGallery = function () {

        var fancyOptions = {
            padding: false,
            helpers	: {
                thumbs	: {
                    width	: 75,
                    height	: 75
                }
            }
        };

        return this.each(function () {
            var $items = $(this),
                fancyImages = [];

            $items.find('img').each(function () {
                fancyImages.push({ href: $(this).attr('src') });
            });

            $items.lightSlider({
                loop: true,
                addClass: 'b-slider-gallery',
                autoWidth: true,
                enableDrag: false,
                onSliderLoad: function() {
                    $items.removeClass('cs-hidden');
                }
            });

            $items.on('click', 'img', function (event) {
                var $target = $(event.target),
                    href = $target.attr('src');

                for (var index = 0; index < fancyImages.length; index++) {
                    if (fancyImages[index]['href'] == href) {
                        var tail = fancyImages.splice(index);
                        fancyImages = tail.concat(fancyImages);
                        break;
                    }
                }

                $.fancybox.open(fancyImages, fancyOptions);
            });
        });
    };
})(jQuery);