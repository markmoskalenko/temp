(function ($, app) {
    'use strict';

    jQuery.fn.snapshotField = function (options) {

        var defaults = {
            container: '.form-group',
            error: '.help-block-error',
            loader: '.form-control-loader',
            snapshot: '#snapshot-widget',
            price: null
        };

        options = $.extend(defaults, options);

        var $field = this,
            $container = $field.parents(options.container),
            $loader = $container.find(options.loader),
            $error = $container.find(options.error),
            $snapshot = $(options.snapshot),
            $price = $(options.price);

        $field.on('change', function () {
            removeError();
            clearWidget();

            var url = $field.val();
            url && checkUrl(url);
        });

        var matches = location.search.match(/snapshotId=(\d+)/);
        if (matches && matches.length == 2 && $field.val() == '') {
            $field.val('http://www.aliexpress.com/snapshot/' + matches[1] + '.html').trigger('change');
        }

        function checkUrl(url)
        {
            $.ajax({
                url: '/aliexpress/check-url',
                type: 'post',
                data: $.extend(app.getCsrfData(), { url: url }),
                beforeSend: function () {
                    showLoader();
                },
                success: function (reponse) {
                    if (reponse.status) {
                        loadWidget(reponse['snapshot']['snapshotId'], function () {
                            setPrice(reponse['snapshot']['price']);
                            hideLoader();
                        });
                    } else {
                        setError('Такая ссылка не поддерживается.');
                        clearPrice();
                        hideLoader();
                    }
                },
                error: function () {
                    hideLoader();
                }
            })
        }

        function setError(message) {
            $container.addClass('has-error');
            $error.text(message);
        }

        function removeError()
        {
            $container.removeClass('has-error');
            $error.text('');
        }

        function loadWidget(snapshotId, complete)
        {
            var url = '/aliexpress/snapshot?id=' + snapshotId;
            $snapshot.load(url, complete);
        }

        function clearWidget()
        {
            $snapshot.html('');
        }

        function showLoader()
        {
            $field.prop('readonly', true);
            $container.addClass('has-feedback has-loader');
            $loader.removeClass('hidden');
        }

        function hideLoader()
        {
            $field.prop('readonly', false);
            $container.removeClass('has-feedback has-loader');
            $loader.addClass('hidden');
        }

        function setPrice(price)
        {
            $price.val(price).trigger('change');
        }

        function clearPrice()
        {
            $price.val('');
        }
    };

})(jQuery, AliTrust);