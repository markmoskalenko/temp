(function($, app) {
    'use strict';

    window.angularApp.controller('LikeableController', function ($scope, $http, $window) {
        $scope.like = function () {
            if (app.user.isGuest()) {
                app.commands.execute('showLoginModal');
                return;
            }

            var oldVotes = $scope.votesCount,
                oldState = $scope.isVoted;

            $scope.votesCount += ($scope.isVoted) ? -1 : 1;
            $scope.isVoted = ! $scope.isVoted;

            var request = {
                method: 'POST',
                url: $scope.voteUrl,
                data: app.getCsrfData(),
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            };

            var onSuccess = function (response) {
                $scope.isVoted = response.data.isVoted;
                $scope.votesCount = response.data.votesCount;
            };

            var onError = function (reponse) {
                $scope.votesCount = oldVotes;
                $scope.isVoted = oldState;
                $window.alert(reponse.data.message);
            };

            $http(request).then(onSuccess, onError);
        }
    });


    $.fn.likable = function () {

        var $control = this,
            $action = $control.find('.btn-action'),
            $counter = $control.find('.btn-counter');

        var options = {
            votesCount: parseInt($control.data('votescount')),
            isVoted: Boolean($control.data('isvoted')),
            url: $control.data('url')
        };

        $action.on('click', function () {
            if (app.user.isGuest()) {
                app.commands.execute('showLoginModal');
                return;
            }

            var oldVotes = options.votesCount,
                oldState = options.isVoted;

            options.votesCount += (options.isVoted) ? -1 : 1;
            options.isVoted = ! options.isVoted;
            updateButton();

            $.ajax({
                type: 'POST',
                url: options.url,
                success: function (data) {
                    options.votesCount = data.votesCount;
                    updateButton();
                },
                error: function (xhr) {
                    options.votesCount = oldVotes;
                    options.isVoted = oldState;
                    updateButton();

                    alert(xhr.responseJSON.message);
                }
            });
        });

        function updateButton() {
            if (options.isVoted) {
                $action.removeClass('btn-default');
                $action.addClass('btn-primary');
            } else {
                $action.removeClass('btn-primary');
                $action.addClass('btn-default');
            }

            $counter.text(options.votesCount);
        }
    };
})(jQuery, AliTrust);