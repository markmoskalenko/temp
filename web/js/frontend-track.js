;
(function (app, $) {
    'use strict';

    app
        .controller('TrackCtrl', [
            '$scope',
            '$compile',
            '$document',
            '$timeout',
            '$location',
            '$interval',
            '$http',
            'localStorageService', function ($scope,
                                             $compile,
                                             $document,
                                             $timeout,
                                             $location,
                                             $interval,
                                             $http,
                                             localStorageService) {

                $scope.trackingNumber = Object.keys($location.search())[0] || null;

                $scope.courier = null;

                $scope.couriers = [];

                $scope.info = null;

                $scope.disabled = false;

                $scope.buttonTitle = 'Отследить';

                $scope.trackingStorage = localStorageService.get('tracking') || [];

                $scope.error = null;

                $scope.loader = false;

                /**
                 * Шаг 1
                 * Определяем службу
                 *
                 * @param trackingNumber
                 */
                $scope.onDetectCourier = function (trackingNumber) {

                    if(!trackingNumber) {
                        $scope.error = 'Введите трек код.'
                        return;
                    }else{
                        $scope.error = '';
                    }

                    $location.search(trackingNumber);

                    $scope.notice = null;

                    $scope.trackingNumber = trackingNumber;

                    $scope.disabled = true;

                    $scope.couriers = [];

                    $scope.info = {};

                    $scope.error = null;
                    $scope.notice = null;

                    $scope.loader = true;

                    var url = '/track/ajax-detect-courier?trackingNumber=' + trackingNumber;

                    var onSuccess = function (response) {

                        $scope.couriers = response.data.couriers;

                        if (response.data.total == 1) {
                            $scope.courier = $scope.couriers[0];
                            $scope.onAddTrackingNumber($scope.courier);
                            $scope.disabled = false;
                        } else if (response.data.total > 1) {
                            $scope.loader = false;
                            $scope.buttonTitle = 'Продолжить';
                            $timeout(function () {
                                $("#select-couriers").categorize();
                            }, 0);
                        } else {
                            $scope.buttonTitle = 'Продолжить';
                            $scope.onGetCouriers();
                        }

                    };

                    var onFailed = function (response) {
                        $scope.disabled = false;
                        $scope.loader = false;
                    };

                    $http.get(url).then(onSuccess, onFailed);
                };

                if($scope.trackingNumber){
                    $scope.onDetectCourier($scope.trackingNumber);
                }


                /**
                 * Шаг 2
                 * Показываем все службы
                 *
                 */
                $scope.onGetCouriers = function () {
                    var url = '/track/ajax-get-couriers';
                    $scope.error = null;

                    var onSuccess = function (response) {
                        $scope.loader = false;
                        $scope.couriers = response.data;
                        $timeout(function () {
                            $("#select-couriers").categorize();
                        }, 0);
                    };

                    var onFailed = function (response) {
                        $scope.loader = false;
                    };

                    $http.get(url).then(onSuccess, onFailed);
                };


                /**
                 * Шаг 3
                 * Добавить код в список отслеживаний
                 * @param courier
                 */
                $scope.onAddTrackingNumber = function (courier) {

                    var trackingNumber = courier.trackingNumber ? courier.trackingNumber : $scope.trackingNumber;
                    $scope.error = null;

                    var url = '/track/ajax-add-tracking-number?trackingNumber=' + trackingNumber + '&courierSlug=' + courier.slug;

                    $scope.onAddTrackingNumberStorage(trackingNumber);

                    var onSuccess = function (response) {
                        $scope.onGetTrackingInfo(courier);
                    };

                    var onFailed = function (response) {
                        $scope.loader = false;
                        $scope.error = 'По вашей посылке пока ещё нет никакой информации, зайдите позже.'
                    };

                    $http.get(url).then(onSuccess, onFailed);
                };

                /**
                 *
                 * @param trackingNumber
                 */
                $scope.onAddTrackingNumberStorage = function(trackingNumber){

                    $scope.trackingStorage = localStorageService.get('tracking') || [];

                    var exist = false;

                    if($scope.trackingStorage.length){
                        angular.forEach($scope.trackingStorage, function(v,k){
                            if(v.trackingNumber == trackingNumber){
                                exist = true;
                            }
                        });
                    }

                    if(!exist){
                        $scope.trackingStorage.push({'time':new Date().getTime(), trackingNumber: trackingNumber});
                        localStorageService.set('tracking', $scope.trackingStorage);
                    }
                };


                /**
                 *
                 * @param trackingNumber
                 */
                $scope.onDeleteTrackingNumberStorage = function(trackingNumber){
                    $scope.trackingStorage = localStorageService.get('tracking') || [];
                    angular.forEach($scope.trackingStorage, function(v,k){
                        if(v.trackingNumber == trackingNumber){
                            $scope.trackingStorage.splice(k, 1);
                            localStorageService.set('tracking', $scope.trackingStorage);
                        }
                    });
                };

                /**
                 * Шаг 2
                 * Показываем все службы
                 *
                 */
                $scope.onGetTrackingInfo = function (courier,count) {

                    var count = count || 1;

                    var url = '/track/ajax-get-tracking-info?trackingNumber=' + courier.trackingNumber + '&courierSlug=' + courier.slug;

                    $scope.error = null;
                    var onSuccess = function (response) {
                        if(response.data == 0){
                            $scope.notice = 'Трек номер поставлен в очередь на отслеживание.<br>Пожалуйста ожидайте или зайдите на эту страницу позже.';
                            $timeout(function(){
                                ++count;
                                $scope.onGetTrackingInfo(courier, count);
                            }, (10000*count));
                        }else{
                            $scope.loader = false;
                            $scope.notice = null;
                            $scope.info = response.data;
                        }
                    };

                    var onFailed = function (response) {
                        $scope.error = 'По вашей посылке пока ещё нет никакой информации, зайдите позже.'
                    };

                    $http.get(url).then(onSuccess, onFailed);
                };

                /**
                 *
                 * @param courier
                 */
                $scope.onSelectCourier = function (courier) {
                    $scope.courier = courier;
                    $scope.disabled = false;
                };

                /**
                 *
                 */
                $scope.onReset = function () {
                    $scope.courier = null;
                    $scope.couriers = [];
                    $scope.disabled = false;
                    $scope.notice = null;
                };

                $scope.onSubmit = function () {
                    if(!$scope.trackingNumber) {
                        $scope.error = 'Введите трек код.'
                        return;
                    }else{
                        $scope.error = '';
                    }
                    $scope.loader = true;
                    if ($scope.courier && $scope.trackingNumber) {
                        $scope.onAddTrackingNumber($scope.courier);
                    } else {
                        $scope.onDetectCourier($scope.trackingNumber);
                    }
                };
            }]);

})(window.angularApp, jQuery);
